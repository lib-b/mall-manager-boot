package eqxiu.mall.manager.aop;

import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import eqxiu.mall.manager.service.LogService;
import eqxiu.mall.manager.utils.ManagerUserContext;
import eqxiu.mall.product.model.Log;

@Aspect
@Configuration
public class LoggerAdvice {

	@Autowired
	private LogService logService;

	@Pointcut("(execution(* eqxiu.mall.manager.service.impl.*ServiceImpl.insert*(..)))")
	public void logInsertAspect() {
	}

	@Pointcut("(execution(* eqxiu.mall.manager.service.impl.*ServiceImpl.delete*(..)))")
	public void logDeleteAspect() {
	}

	@Pointcut("(execution(* eqxiu.mall.manager.service.impl.*ServiceImpl.update*(..)))")
	public void logUpdateAspect() {
	}

	@AfterReturning(value = "logInsertAspect()")
	public void beforeInsert(JoinPoint jPoint) throws Exception {
		Log log = new Log();
		log.setUser(ManagerUserContext.getUser().getId());
		log.setTime(getNowDate());
		log.setMessage(jPoint.getTarget().getClass().getName() + ":"
				+ adminOptionContent(jPoint.getArgs(), jPoint.getSignature().getName()));
		logService.addLog(log);
	}

	@Before(value = "logDeleteAspect()")
	public void beforeDelete(JoinPoint jPoint) throws Exception {
		Log log = new Log();
		log.setUser(ManagerUserContext.getUser().getId());
		log.setTime(getNowDate());
		log.setMessage(jPoint.getTarget().getClass().getName() + ":" + jPoint.getSignature().getName()
				+ "[(类型:Integer,id:" + jPoint.getArgs()[0] + ")]");
		logService.addLog(log);
	}

	@Before(value = "logUpdateAspect()")
	public void beforeUpdate(JoinPoint jPoint) throws Exception {
		Log log = new Log();
		log.setUser(ManagerUserContext.getUser().getId());
		log.setTime(getNowDate());
		log.setMessage(jPoint.getTarget().getClass().getName() + ":"
				+ adminOptionContent(jPoint.getArgs(), jPoint.getSignature().getName()));
		logService.addLog(log);
	}

	/**
	 * 使用Java反射来获取被拦截方法(insert、update)的参数值， 将参数值拼接为操作内容
	 */
	public String adminOptionContent(Object[] args, String mName) throws Exception {
		if (args == null) {
			return null;
		}
		StringBuffer rs = new StringBuffer();
		rs.append(mName);
		String className = null;
		// 遍历参数对象
		for (Object info : args) {
			// 获取对象类型
			className = info.getClass().getName();
			className = className.substring(className.lastIndexOf(".") + 1);
			rs.append("[类型:" + className + ",值:");
			// 获取对象的所有方法
			Method[] methods = info.getClass().getDeclaredMethods();
			// 遍历方法，判断get方法
			for (Method method : methods) {
				String methodName = method.getName();
				// 只获取id信息
				if (methodName.indexOf("getId") == -1) {// 不是get方法
					continue;// 不处理
				}
				Object rsValue = null;
				try {
					// 调用get方法，获取返回值
					rsValue = method.invoke(info);
					if (rsValue == null) {// 没有返回值
						continue;
					}
				} catch (Exception e) {
					continue;
				}
				// 将值加入内容中
				rs.append("(" + methodName.replace("get", "") + " : " + rsValue + ")");
			}
			rs.append("]");
		}
		return rs.toString();
	}

	public static Date getNowDate() throws ParseException {
		Date currentTime = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateString = formatter.format(currentTime);
		Date currentTime_2 = formatter.parse(dateString);
		return currentTime_2;
	}
}
