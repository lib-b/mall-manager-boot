package eqxiu.mall.manager.config;

import org.springframework.context.annotation.ImportResource;
import org.springframework.stereotype.Component;

@Component
@ImportResource(locations={"classpath:dubbo-consumer.xml"})
public class DubboConfig {
}
