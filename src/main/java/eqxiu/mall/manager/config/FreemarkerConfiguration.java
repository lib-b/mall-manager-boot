package eqxiu.mall.manager.config;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.freemarker.FreeMarkerAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

/**
 * 
 * @description 把properties配置文件里的传递到freemarker模板中
 * @author ranger
 * @date Oct 28, 2016
 */
@Configuration
public class FreemarkerConfiguration extends FreeMarkerAutoConfiguration.FreeMarkerWebConfiguration {

//	@Value("${cdn.image.host}")
//	private String imageHost;
//	@Value("${userinfo.url}")
//	private String userInfoUrl;
//	@Value("${logout.url}")
//	private String logoutUrl;
//	@Value("${authRegister.url}")
//	private String authRegisterUrl;
//	@Value("${authLogin.url}")
//	private String authLoginUrl;
//	@Value("${store.url}")
//	private String storeUrl;
//	@Value("${audioCallback.url}")
//	private String audioCallbackUrl;
//	@Value("${scene.previewurl}")
//	private String sceneView;
//	@Value("${service.url}")
//	private String serviceUrl;

	@Override
	public FreeMarkerConfigurer freeMarkerConfigurer() {
		FreeMarkerConfigurer configurer = super.freeMarkerConfigurer();
		Map<String, Object> sharedVariables = new HashMap<>();
//		sharedVariables.put("imageHost", imageHost);
//		sharedVariables.put("userInfoUrl", userInfoUrl);
//		sharedVariables.put("logoutUrl", logoutUrl);
//		sharedVariables.put("authRegisterUrl", authRegisterUrl);
//		sharedVariables.put("authLoginUrl", authLoginUrl);
//		sharedVariables.put("storeUrl", storeUrl);
//		sharedVariables.put("audioCallbackUrl", audioCallbackUrl);
//		sharedVariables.put("sceneView", sceneView);
//		sharedVariables.put("serviceUrl", serviceUrl);
		configurer.setFreemarkerVariables(sharedVariables);
		return configurer;
	}
}
