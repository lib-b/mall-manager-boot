package eqxiu.mall.manager.config;

import java.util.HashSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.redis.connection.RedisSentinelConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import redis.clients.jedis.JedisPoolConfig;

@Configuration
public class RedisConfig {
	@Autowired
	private Environment environment;

	@Bean
	public JedisPoolConfig jedisPoolConfig() {
		JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
		jedisPoolConfig.setMaxTotal(Integer.parseInt(environment.getProperty("redis.pool.maxTotal")));
		jedisPoolConfig.setMaxIdle(Integer.parseInt(environment.getProperty("redis.pool.maxIdle")));
		jedisPoolConfig.setMinIdle(Integer.parseInt(environment.getProperty("redis.pool.minIdle")));
		jedisPoolConfig.setMaxWaitMillis(Integer.parseInt(environment.getProperty("redis.pool.maxWaitMillis")));
		jedisPoolConfig.setTestOnBorrow(Boolean.parseBoolean(environment.getProperty("redis.pool.testOnBorrow")));
		jedisPoolConfig.setTestWhileIdle(Boolean.parseBoolean(environment.getProperty("redis.pool.testWhileIdle")));
		jedisPoolConfig.setMinEvictableIdleTimeMillis(
				Integer.parseInt(environment.getProperty("redis.pool.minEvictableIdleTimeMillis")));
		jedisPoolConfig.setTimeBetweenEvictionRunsMillis(
				Integer.parseInt(environment.getProperty("redis.pool.timeBetweenEvictionRunsMillis")));
		jedisPoolConfig
				.setBlockWhenExhausted(Boolean.parseBoolean(environment.getProperty("redis.pool.blockWhenExhausted")));
		return jedisPoolConfig;
	}

	@Bean
	public StringRedisTemplate redisTemplate() {
		String[] sentinelarray;
		// 开发测试环境只有一个哨兵，正式环境有多个用分号隔开
		// 先判断一下有没有逗号，如果有就用逗号分隔
		String sentinels = environment.getProperty("redis.sys.sentinels");
		String master = environment.getProperty("redis.sys.masterName");
		String password = environment.getProperty("redis.sys.password");
		if (sentinels.contains(",")) {
			sentinelarray = sentinels.split(",");
		} else {
			sentinelarray = sentinels.split(";");
		}
		HashSet<String> set = new HashSet<String>();
		for (int i = 0; i < sentinelarray.length; i++) {
			set.add(sentinelarray[i]);
		}
		// 配置连接参数
		RedisSentinelConfiguration redisSentinelConfiguration = new RedisSentinelConfiguration(master, set);
		JedisConnectionFactory conn = new JedisConnectionFactory(redisSentinelConfiguration);
		conn.setPoolConfig(new JedisPoolConfig());
		conn.setPassword(password);
		conn.afterPropertiesSet();
		// 创建Template
		StringRedisTemplate redisTemplate = new StringRedisTemplate();
		redisTemplate.setConnectionFactory(conn);
		redisTemplate.afterPropertiesSet();
		return redisTemplate;
	}

}
