package eqxiu.mall.manager.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Maps;
import eqxiu.mall.commons.web.ResultData;
import eqxiu.mall.manager.service.ArtistService;
import eqxiu.mall.product.model.Artist;

@Controller
@RequestMapping("/m/mall/artist")
public class ArtistController {
	@Autowired
	private ArtistService artistService;

	@PostMapping("/{page}")
	public String index(@PathVariable String page) {
		return "artist/" + page;
	}

	@PostMapping("/artist/list")
	@ResponseBody
	public Map<String, Object> getArtistsList(HttpServletRequest request, HttpServletResponse response) {
		PageHelper.startPage(Integer.parseInt(request.getParameter("pageNumber")),
				Integer.parseInt(request.getParameter("pageSize")));
		List<Artist> artists = new ArrayList<>();
		if (!request.getParameter("id").trim().equals("")) {
			Artist artist = artistService.findByIDAndCount(request);
			if (artist != null) {
				artists.add(artist);
			}
		} else {
			artists = artistService.findByParam(request);
		}
		Map<String, Object> map = Maps.newHashMap();
		PageInfo<Artist> pageInfo = new PageInfo<>(artists);
		map.put("total", pageInfo.getTotal());
		map.put("rows", pageInfo.getList());
		return map;
	}

	@PostMapping("/artist/update")
	@ResponseBody
	public ResultData updateArtist(Artist param) {
		artistService.update(param);
		return ResultData.result(true);
	}

	@PostMapping("/artist/delete")
	@ResponseBody
	public ResultData deleteArtist(HttpServletRequest request, HttpServletResponse response) {
		for (String artistId : request.getParameter("ids").split(";")) {
			artistService.deleteById(Integer.parseInt(artistId));
		}		
		return ResultData.result(true);
	}
}
