package eqxiu.mall.manager.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Maps;
import eqxiu.mall.commons.web.ResultData;
import eqxiu.mall.manager.dto.AttributeDTO;
import eqxiu.mall.manager.service.AttributeGroupKeyService;
import eqxiu.mall.manager.service.AttributeGroupService;
import eqxiu.mall.manager.service.AttributeKeyService;
import eqxiu.mall.manager.service.ProductAttributeService;
import eqxiu.mall.product.model.AttributeGroup;
import eqxiu.mall.product.model.AttributeGroupKey;
import eqxiu.mall.product.model.AttributeKey;
import eqxiu.mall.product.model.ProductAttribute;

@Controller
@RequestMapping("/m/mall/attribute")
public class AttributeController {
	@Autowired
	private AttributeKeyService attributeKeyService;
	@Autowired
	private AttributeGroupKeyService attributeGroupKeyService;
	@Autowired
	private AttributeGroupService attributeGroupService;
	@Autowired
	private ProductAttributeService productAttributeService;

	@RequestMapping("/{page}")
	public String index(@PathVariable String page) {
		return "attribute/" + page;
	}

	@PostMapping("/attribute/list")
	@ResponseBody
	public Map<String, Object> findAttrList(HttpServletRequest request, HttpServletResponse response) {
		List<AttributeDTO> attributeDTOs = new ArrayList<>();
		PageHelper.startPage(Integer.parseInt(request.getParameter("pageNumber")),
				Integer.parseInt(request.getParameter("pageSize")));
		List<AttributeKey> allAttrs = attributeKeyService.selectByParam(request);
		try {
			for (AttributeKey attributeGroup : allAttrs) {
				AttributeDTO attributeDTO = new AttributeDTO();
				BeanUtils.copyProperties(attributeGroup, attributeDTO);
				attributeDTO.setGroups(attributeKeyService.getGroupNamesByAttributeId(attributeDTO.getId()));
				attributeDTOs.add(attributeDTO);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		Map<String, Object> map = Maps.newHashMap();
		PageInfo<AttributeKey> pageInfo = new PageInfo<>(allAttrs);
		map.put("total", pageInfo.getTotal());
		map.put("rows", attributeDTOs);
		return map;
	}

	@PostMapping("/attribute/delete")
	@ResponseBody
	public ResultData deleteAttrByID(HttpServletRequest request, HttpServletResponse response) {
		for (String attrId : request.getParameter("ids").split(";")) {
			// 先删除属性和属性组关联表中的数据
			for (AttributeGroupKey attributeGroupKey : attributeGroupKeyService
					.selectByAttrId(Integer.parseInt(attrId))) {
				attributeGroupKey.setAttrGroupId(attributeGroupKey.getAttrGroupId());
				attributeGroupKey.setAttrKeyId(Integer.parseInt(attrId));
				attributeGroupKeyService.deleteById(attributeGroupKey);
			}
			// 删除属性商品关联表记录
			for (ProductAttribute productAttribute : productAttributeService.findByAttrId(Integer.parseInt(attrId))) {
				productAttributeService.deleteByID(productAttribute);
			}
			attributeKeyService.deleteByID(Integer.parseInt(attrId));
		}
		return ResultData.result(true);
	}

	@PostMapping(value = "/attribute/save")
	@ResponseBody
	public ResultData saveAttribute(AttributeKey params, HttpServletRequest request, HttpServletResponse response) {
		String[] groupIds = request.getParameterValues("groupIds");
		if (attributeKeyService.selectByName(params.getName()) != null) {
			Map<String, Object> map = new HashMap<>();
			map.put("error", "属性名称已经存在!");
			return ResultData.result(false).setMap(map);
		}
		attributeKeyService.insert(params);
		for (String groupId : groupIds) {
			// 在选中的每个属性组中增加该属性
			AttributeGroupKey attributeGroupKey = new AttributeGroupKey();
			attributeGroupKey.setAttrGroupId(Integer.parseInt(groupId));
			attributeGroupKey.setAttrKeyId(params.getId());
			attributeGroupKeyService.insert(attributeGroupKey);
		}
		return ResultData.result(true);
	}

	@PostMapping(value = "/attribute/update")
	@ResponseBody
	public ResultData updateAttribute(AttributeKey params, HttpServletRequest request, HttpServletResponse response) {
		// 先更新属性信息
		attributeKeyService.update(params);
		String[] groupIds = request.getParameterValues("groupIdsUpdate");
		// 删除原有的分组信息
		for (AttributeGroupKey AttributeGroupKey : attributeGroupKeyService.selectByAttrId(params.getId())) {
			attributeGroupKeyService.deleteById(AttributeGroupKey);
		}
		// 插入新的记录
		for (String groupId : groupIds) {
			AttributeGroupKey attributeGroupKey = new AttributeGroupKey();
			attributeGroupKey.setAttrGroupId(Integer.parseInt(groupId));
			attributeGroupKey.setAttrKeyId(params.getId());
			attributeGroupKeyService.insert(attributeGroupKey);
		}
		return ResultData.result(true);
	}

	// 按照条件过滤属性组
	@PostMapping("/attributegroup/list")
	@ResponseBody
	public Map<String, Object> findAttrGroupList(HttpServletRequest request, HttpServletResponse response) {
		PageHelper.startPage(Integer.parseInt(request.getParameter("pageNumber")),
				Integer.parseInt(request.getParameter("pageSize")));
		List<AttributeGroup> allAttrs = new ArrayList<AttributeGroup>();
		AttributeGroup attributeGroup;
		if (Integer.parseInt(request.getParameter("id").trim()) != 0) {
			attributeGroup = attributeGroupService.selectByID(Integer.parseInt(request.getParameter("id").trim()));
			if (attributeGroup != null) {
				allAttrs.add(attributeGroup);
			}
		} else {
			allAttrs = attributeGroupService.selectAll();
		}
		Map<String, Object> map = Maps.newHashMap();
		PageInfo<AttributeGroup> pageInfo = new PageInfo<>(allAttrs);
		map.put("total", pageInfo.getTotal());
		map.put("rows", pageInfo.getList());
		return map;
	}

	// 查询所有属性组
	@PostMapping("/attributegroup/listAll")
	@ResponseBody
	public List<AttributeGroup> findAttrGroupListAll(HttpServletRequest request, HttpServletResponse response) {
		List<AttributeGroup> allAttrs = new ArrayList<AttributeGroup>();
		allAttrs = attributeGroupService.selectAll();
		return allAttrs;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping("/attributegroup/attrbutelist")
	@ResponseBody
	public JSONArray findProductAttrList(@RequestParam(required = true) int groupId) {
		JSONArray jsonArray = new JSONArray();
		Map<String, Object> map = attributeGroupKeyService.selectByGroupId(groupId);
		for (Map.Entry<String, Object> entry : map.entrySet()) {
			JSONObject jsonObject = new JSONObject();
			String title = attributeKeyService.selectByName(entry.getKey()).getTitle();
			jsonObject.put("name", entry.getKey());
			jsonObject.put("value", title);
			jsonArray.add(jsonObject);
		}
		return jsonArray;
	}

	@RequestMapping("/attributegroup/delete")
	@ResponseBody
	public ResultData deleteAttrGroupByID(@RequestParam(required = true) int id) {
		attributeGroupKeyService.findByGroupId(id);
		for (AttributeGroupKey attributeGroupKey : attributeGroupKeyService.findByGroupId(id)) {
			attributeGroupKeyService.deleteById(attributeGroupKey);
		}
		attributeGroupService.deleteByID(id);
		return ResultData.result(true);
	}

	@PostMapping(value = "/attributegroup/save")
	@ResponseBody
	public ResultData saveAttributeGroup(AttributeGroup params) {
		if (params.getId() == null || attributeKeyService.selectById(params.getId()) == null) {
			attributeGroupService.insert(params);
		} else {
			attributeGroupService.update(params);
		}
		return ResultData.result(true);
	}
}
