package eqxiu.mall.manager.controller;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Maps;
import eqxiu.mall.commons.web.ResultData;
import eqxiu.mall.manager.dto.BannerConfigDTO;
import eqxiu.mall.manager.service.BannerAccountRelService;
import eqxiu.mall.manager.service.BannerConfigService;
import eqxiu.mall.manager.service.BannerService;
import eqxiu.mall.product.model.Banner;
import eqxiu.mall.product.model.BannerAccountRel;
import eqxiu.mall.product.model.BannerConfig;

@Controller
@RequestMapping("/m/mall/banner")
public class BannerController {
	@Autowired
	private BannerService bannerService;
	@Autowired
	private BannerConfigService bannerConfigService;
	@Autowired
	private BannerAccountRelService bannerAccountRelService;
	@Autowired
	private StringRedisTemplate redisTemplate;

	@PostMapping("/{page}")
	String Index(@PathVariable String page) {
		return "banner/" + page;
	}

	@PostMapping("/bannerList")
	@ResponseBody
	public Map<String, Object> getBannersList(HttpServletRequest request, HttpServletResponse response) {
		PageHelper.startPage(Integer.parseInt(request.getParameter("pageNumber")),
				Integer.parseInt(request.getParameter("pageSize")));
		Map<String, Object> map = Maps.newHashMap();
		PageInfo<Banner> pageInfo = new PageInfo<>(bannerService.findBannersByParam(request));
		map.put("total", pageInfo.getTotal());
		map.put("rows", pageInfo.getList());
		return map;
	}

	@PostMapping("/allBannerList")
	@ResponseBody
	public List<Map<String, Object>> getAllBannersList(HttpServletRequest request, HttpServletResponse response) {
		List<Map<String, Object>> bannerList = new ArrayList<>();
		for (Banner banner : bannerService.findAllBanner()) {
			Map<String, Object> map = Maps.newHashMap();
			map.put("id", banner.getId());
			map.put("name", banner.getName());
			bannerList.add(map);
		}
		return bannerList;
	}

	@PostMapping("/bannerConfigList")
	@ResponseBody
	public Map<String, Object> getBannerConfigsList(HttpServletRequest request, HttpServletResponse response)
			throws IllegalAccessException, InvocationTargetException {
		PageHelper.startPage(Integer.parseInt(request.getParameter("pageNumber")),
				Integer.parseInt(request.getParameter("pageSize")));
		Map<String, Object> map = Maps.newHashMap();
		List<BannerConfigDTO> bannerDTOs = new ArrayList<>();
		try {
			for (BannerConfig bannerConfig : bannerConfigService.findBannerConfigsByParam(request)) {
				BannerConfigDTO bannerConfigDTO = new BannerConfigDTO();
				BeanUtils.copyProperties(bannerConfigDTO, bannerConfig);
				// 设置适用的账号类型
				bannerConfigDTO.setAccounts(bannerConfigService.findAccountsByBannerConfigId(bannerConfig.getId()));
				// 设置位置名称，也就是所属的banner的名称
				if (bannerConfig.getBannerId() != null
						&& bannerService.findBannerById(bannerConfig.getBannerId()) != null) {
					bannerConfigDTO.setBannerName(bannerService.findBannerById(bannerConfig.getBannerId()).getName());
				}
				bannerDTOs.add(bannerConfigDTO);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		PageInfo<BannerConfig> pageInfo = new PageInfo<>(bannerConfigService.findBannerConfigsByParam(request));
		map.put("total", pageInfo.getTotal());
		map.put("rows", bannerDTOs);
		return map;
	}

	@PostMapping("/saveOrUpdateBanner")
	@ResponseBody
	public ResultData saveBanner(Banner param, HttpServletRequest request, HttpServletResponse response) {
		try {
			if (param.getId() != null) {
				bannerService.updateBanner(param);
				for (int i = 1; i <= 5; i++) {
					redisTemplate.delete("mall:banner:" + param.getId() + ":" + i);
				}
			} else {
				bannerService.insertBanner(param);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ResultData.result(false);
		}
		return ResultData.result(true);
	}

	@PostMapping("/saveBannerConfig")
	@ResponseBody
	public ResultData saveBannerConfig(BannerConfig param, HttpServletRequest request, HttpServletResponse response) {
		try {
			bannerConfigService.insertBannerConfig(param);
			if (request.getParameterValues("accounts") != null) {
				for (String accountGroupId : request.getParameterValues("accounts")) {
					BannerAccountRel bannerAccountRel = new BannerAccountRel();
					bannerAccountRel.setAccountGroupId(Integer.parseInt(accountGroupId));
					bannerAccountRel.setBannerConfigId(param.getId());
					bannerAccountRelService.insertBannerAccount(bannerAccountRel);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ResultData.result(false);
		}
		return ResultData.result(true);
	}

	@PostMapping("/updateBannerConfig")
	@ResponseBody
	public ResultData updateBannerConfig(BannerConfig param, HttpServletRequest request, HttpServletResponse response) {
		try {
			if (request.getParameter("imageUrlUpdate") != null && !request.getParameter("imageUrlUpdate").equals("")) {
				param.setImageUrl(request.getParameter("imageUrlUpdate"));
			}
			bannerConfigService.updateBannerConfig(param);
			for (BannerAccountRel bannerAccountRel : bannerAccountRelService.findByBannerConfigId(param.getId())) {
				bannerAccountRelService.deleteBannderAccountById(bannerAccountRel);
			}
			if (request.getParameterValues("bannerConfigAccounts") != null) {
				for (String accountGroupId : request.getParameterValues("bannerConfigAccounts")) {
					BannerAccountRel bannerAccountRel = new BannerAccountRel();
					bannerAccountRel.setAccountGroupId(Integer.parseInt(accountGroupId));
					bannerAccountRel.setBannerConfigId(param.getId());
					bannerAccountRelService.insertBannerAccount(bannerAccountRel);
				}
			}
			for (int i = 1; i <= 5; i++) {
				redisTemplate.delete("mall:banner:" + param.getBannerId() + ":" + i);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ResultData.result(false);
		}
		return ResultData.result(true);
	}

	@PostMapping("/deleteBanner")
	@ResponseBody
	public ResultData deleteBanner(HttpServletRequest request, HttpServletResponse response) {
		for (String bannerId : request.getParameter("ids").split(";")) {
			bannerService.deleteBannderById(Integer.parseInt(bannerId));
			// 删除下属的所有banner配置
			bannerConfigService.deleteBannerConfigByBannerId(Integer.parseInt(bannerId));
			for (int i = 1; i <= 5; i++) {
				// 1到5代表账号类型
				redisTemplate.delete("mall:banner:" + Integer.parseInt(bannerId) + ":" + i);
			}
		}
		return ResultData.result(true);
	}

	@PostMapping("/deleteBannerConfig")
	@ResponseBody
	public ResultData deleteBannerConfig(HttpServletRequest request, HttpServletResponse response) {
		try {
			for (String bannerCongifId : request.getParameter("ids").split(";")) {
				bannerConfigService.deleteBannderConfigById(Integer.parseInt(bannerCongifId));
				// 删除banner config 关系记录
				bannerAccountRelService.deleteByBannerConfigId(Integer.parseInt(bannerCongifId));
				for (int i = 1; i <= 5; i++) {
					redisTemplate.delete("mall:banner:" + Integer.parseInt(bannerCongifId) + ":" + i);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ResultData.result(false);
		}
		return ResultData.result(true);
	}
}
