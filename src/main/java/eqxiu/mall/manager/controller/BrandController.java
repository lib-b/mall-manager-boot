package eqxiu.mall.manager.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Maps;

import cn.knet.modules.base.util.UserUtils;
import eqxiu.mall.commons.web.ResultData;
import eqxiu.mall.manager.service.BrandService;
import eqxiu.mall.product.model.Brand;

@Controller
@RequestMapping("m/mall/brand")
public class BrandController {
	@Autowired
	private BrandService brandService;

	@PostMapping("/{page}")
	public String index(@PathVariable String page) {
		return "brand/" + page;
	}

	@PostMapping("/brand/list")
	@ResponseBody
	public Map<String, Object> findBrandList(HttpServletRequest request, HttpServletResponse response) {
		PageHelper.startPage(Integer.parseInt(request.getParameter("pageNumber")),
				Integer.parseInt(request.getParameter("pageSize")));
		List<Brand> allBrands = new ArrayList<>();
		if (!request.getParameter("name").trim().equals("")) {
			Brand brand = brandService.findByName(request.getParameter("name").trim());
			if (brand != null) {
				allBrands.add(brand);
			}
		} else {
			allBrands = brandService.findAll();
		}
		PageInfo<Brand> pageInfo = new PageInfo<>(allBrands);
		Map<String, Object> map = Maps.newHashMap();
		map.put("total", pageInfo.getTotal());
		map.put("rows", pageInfo.getList());
		return map;
	}

	@PostMapping("/brand/listAll")
	@ResponseBody
	public List<Map<String, Object>> findBrandListAll(HttpServletRequest request, HttpServletResponse response) {
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
		for (Brand brand : brandService.findAll()) {
			Map<String, Object> map = new HashMap<>();
			map.put("id", brand.getId());
			map.put("name", brand.getName());
			result.add(map);
		}
		return result;
	}

	@PostMapping("/brand/delete")
	@ResponseBody
	public ResultData deleteBrand(HttpServletRequest request, HttpServletResponse response) {
		for (String brandId : request.getParameter("ids").split(";")) {
			brandService.deleteById(Integer.parseInt(brandId));
		}
		return ResultData.result(true);
	}

	@PostMapping("/brand/save")
	@ResponseBody
	public ResultData saveOrUpadateBrand(Brand param) {
		try {
			// 新建
			if (param.getId() == null) {
				param.setCreateTime(new Date());
				param.setCreateUser(UserUtils.getUser().getName());
				brandService.insert(param);
			} else {
				brandService.update(param);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ResultData.result(false);
		}
		return ResultData.result(true);
	}

	@PostMapping("/brand/brandName")
	@ResponseBody
	public Map<String, Object> findBrandNameById(HttpServletRequest request, HttpServletResponse response) {
		String brandName = brandService.findByID(Integer.parseInt(request.getParameter("brandId"))).getName();
		Map<String, Object> map = new HashMap<>();
		map.put("brandName", brandName);
		return map;
	}
}
