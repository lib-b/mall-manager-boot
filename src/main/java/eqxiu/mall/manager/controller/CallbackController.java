package eqxiu.mall.manager.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Maps;

import cn.knet.modules.base.util.UserUtils;
import eqxiu.mall.commons.web.ResultData;
import eqxiu.mall.manager.service.AsyncOrderCallbackService;
import eqxiu.mall.product.model.AsyncOrderCallback;;

@Controller
@RequestMapping("m/mall/callback")
public class CallbackController {
	@Autowired
	private AsyncOrderCallbackService asyncOrderCallbackService;

	@PostMapping("/{page}")
	public String index(@PathVariable String page) {
		return "callback/" + page;
	}

	@PostMapping("/list")
	@ResponseBody
	public Map<String, Object> findCallbackList(HttpServletRequest request, HttpServletResponse response) {
		PageHelper.startPage(Integer.parseInt(request.getParameter("pageNumber")),
				Integer.parseInt(request.getParameter("pageSize")));
		List<AsyncOrderCallback> allCallbacks = new ArrayList<>();
		if (!request.getParameter("id").trim().equals("")) {
			AsyncOrderCallback callback = asyncOrderCallbackService
					.findById(Integer.parseInt(request.getParameter("id").trim()));
			if (callback != null) {
				allCallbacks.add(callback);
			}
		} else {
			allCallbacks = asyncOrderCallbackService.findByParams(request);
		}
		PageInfo<AsyncOrderCallback> pageInfo = new PageInfo<>(allCallbacks);
		Map<String, Object> map = Maps.newHashMap();
		map.put("total", pageInfo.getTotal());
		map.put("rows", pageInfo.getList());
		return map;
	}

	@PostMapping("/update")
	@ResponseBody
	public ResultData UpadateBrand(AsyncOrderCallback asyncOrderCallback) {
		try {
			asyncOrderCallback.setUpdateTime(new Date());
			asyncOrderCallback.setUpdateUser(UserUtils.getUser().getLoginName());
			asyncOrderCallbackService.update(asyncOrderCallback);
		} catch (Exception e) {
			e.printStackTrace();
			return ResultData.result(false);
		}
		return ResultData.result(true);
	}
}
