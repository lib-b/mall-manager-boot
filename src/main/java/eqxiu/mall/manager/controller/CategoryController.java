package eqxiu.mall.manager.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Maps;
import cn.knet.common.mapper.JsonMapper;
import eqxiu.mall.commons.page.PageDTO;
import eqxiu.mall.commons.web.ResultData;
import eqxiu.mall.manager.dto.CategoryDTO;
import eqxiu.mall.manager.service.CategoryProductService;
import eqxiu.mall.manager.service.CategoryService;
import eqxiu.mall.manager.service.ProductService;
import eqxiu.mall.product.model.Category;
import eqxiu.mall.product.model.CategoryProduct;

@Controller
@RequestMapping("/m/mall/category")
public class CategoryController {
	@Autowired
	private CategoryService categoryService;
	@Autowired
	private CategoryProductService categoryProductService;
	@Autowired
	private ProductService productService;

	@RequestMapping("/{page}")
	public String index(@PathVariable String page, Model model) {
		// 首页展示顶层节点
		model.addAttribute("funcs", categoryService.getCategoryByParentId(0));
		return "category/" + page;
	}

	@RequestMapping("/category/categoryDetail")
	public String editCategory(String categoryId, Model model) {
		Category category = new Category();
		String json = "{}";
		if (categoryId != null) {
			category = categoryService.getCategoryByID(Integer.parseInt(categoryId));
		}
		json = JsonMapper.getInstance().toJson(category);
		model.addAttribute("formData", json);
		return "category/categoryDetail";
	}

	@PostMapping("/category/list")
	@ResponseBody
	public Map<String, Object> findCategoryList(HttpServletRequest request, HttpServletResponse resopnse) {
		Map<String, Object> map = Maps.newHashMap();
		try {
			PageHelper.startPage(Integer.parseInt(request.getParameter("pageNumber")),
					Integer.parseInt(request.getParameter("pageSize")));
			List<Category> allCategorys = new ArrayList<Category>();
			if (!request.getParameter("id").trim().equals("")) {
				Category category = categoryService
						.getCategoryByID(Integer.parseInt(request.getParameter("id").trim()));
				if (category != null) {
					allCategorys.add(category);
				}
			} else {
				allCategorys = categoryService.getCategoryByParam(request);
			}
			PageInfo<Category> pageInfo = new PageInfo<>(allCategorys);

			map.put("total", pageInfo.getTotal());
			map.put("rows", pageInfo.getList());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}

	@PostMapping("/category/children")
	@ResponseBody
	public List<CategoryDTO> findChidrenByCategoryId(@RequestParam(required = true) Integer categoryId) {
		return categoryService.getCategoryByParentId(categoryId);
	}

	@PostMapping("/category/productlist")
	@ResponseBody
	public Map<String, Object> findProductList(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> map = Maps.newHashMap();
		if (!request.getParameter("id").trim().equals("")) {
			PageDTO<Map<String, Object>> pageInfo = productService.findProductsByCategoryId(
					Integer.parseInt(request.getParameter("id").trim()),
					Integer.parseInt(request.getParameter("pageNumber")),
					Integer.parseInt(request.getParameter("pageSize")));
			map.put("total", pageInfo.getCount());
			map.put("rows", pageInfo.getList());
		}
		return map;
	}

	@PostMapping("/category/delete")
	// @RequiresPermissions("mall:category:delete")
	@ResponseBody
	public ResultData deleteCategoryByID(@RequestParam(required = true) int id) {
		try {
			// 删除分类下挂的所有商品关联记录
			List<CategoryProduct> categoryProducts = categoryProductService.findByCategoryId(id);
			for (CategoryProduct categoryProduct : categoryProducts) {
				categoryProductService.delete(categoryProduct);
			}
			// 删除所有子分类
			List<Category> children = categoryService.iterateChild(id);
			List<Integer> subcat = categoryService.iterateChildIds(children);
			for (Integer categoryId : subcat) {
				categoryService.deleteByID(categoryId);
			}
			// 删除自己
			categoryService.deleteByID(id);
		} catch (Exception e) {
			return ResultData.result(false);
		}
		return ResultData.result(true);
	}

	@PostMapping("/category/batchDeleteProduct")
	@ResponseBody
	public ResultData batchDeleteProductByCategoryID(HttpServletRequest request,
			@RequestParam(required = true) int categoryId) {
		try {
			String[] productIds = request.getParameter("ids").split(";");
			for (String productId : productIds) {
				CategoryProduct categoryProduct = new CategoryProduct();
				categoryProduct.setCategoryId(categoryId);
				categoryProduct.setProductId(Integer.parseInt(productId));
				categoryProductService.delete(categoryProduct);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ResultData.result(false);
		}
		return ResultData.result(true);
	}

	@PostMapping("/category/addProduct")
	@ResponseBody
	public ResultData addProductByCategoryID(@RequestParam(required = true) String productIds,
			@RequestParam(required = true) int id) {
		try {
			if (!productIds.equals("")) {
				String[] pIds = productIds.split(";");
				for (String productId : pIds) {
					if (productService.getProduct(Integer.parseInt(productId)) != null) {
						CategoryProduct categoryProduct = new CategoryProduct();
						categoryProduct.setCategoryId(id);
						categoryProduct.setProductId(Integer.parseInt(productId));
						if (categoryProductService.findByProductAndCategoryId(Integer.parseInt(productId),
								id) == null) {
							categoryProductService.insert(categoryProduct);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ResultData.result(false);
		}
		return ResultData.result(true);
	}

	@PostMapping("/category/update")
	@ResponseBody
	public ResultData updateCategoryByID(Category params) {
		try {
			Integer parentId = params.getParentId();
			if (parentId != 0) {
				Category parent = categoryService.getCategoryByID(parentId);
				params.setPath(parent.getPath() + "/" + params.getId());
			} else {
				params.setPath(params.getId().toString());
			}
			categoryService.update(params);
		} catch (Exception e) {
			e.printStackTrace();
			return ResultData.result(false);
		}
		return ResultData.result(true);
	}

	@PostMapping(value = "/category/save")
	@ResponseBody
	public ResultData saveCategory(Category params) {
		try {
			// type暂时从前台去掉了，后台手动初始化一下
			params.setType(0);
			// 先插入记录，自动生成id后再处理path
			categoryService.insert(params);
			if (params.getParentId() == null || categoryService.getCategoryByID(params.getParentId()) == null) {
				params.setParentId(0);
				params.setPath(String.valueOf(params.getId()));
			} else {
				String pPath = categoryService.getCategoryByID(params.getParentId()).getPath();
				params.setPath(pPath + "/" + String.valueOf(params.getId()));
			}
			categoryService.update(params);
		} catch (Exception e) {
			e.printStackTrace();
			return ResultData.result(false);
		}
		return ResultData.result(true);
	}

	@PostMapping("/category/checkPath")
	@ResponseBody
	public ResultData checkPath(HttpServletRequest request, HttpServletResponse response) {
		int pId = 0;
		Category category = null;
		for (String categoryName : request.getParameter("path").split("/")) {
			category = categoryService.getCategoryByNameAndParentId(categoryName, pId);
			if (category == null) {
				return ResultData.result(false);
			} else {
				pId = category.getId();
			}
		}
		return ResultData.result(true);
	}
}
