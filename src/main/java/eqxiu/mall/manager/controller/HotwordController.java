package eqxiu.mall.manager.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Maps;
import eqxiu.mall.commons.web.ResultData;
import eqxiu.mall.manager.service.HotWordService;
import eqxiu.mall.product.model.HotWord;

@Controller
@RequestMapping("/m/mall/hotword")
public class HotwordController {
	@Autowired
	private HotWordService hotWordService;

	@PostMapping("/{page}")
	public String index(@PathVariable String page) {
		return "hotword/" + page;
	}

	@PostMapping("/list")
	@ResponseBody
	public JSONArray findHotwordList(HttpServletRequest request, HttpServletResponse response)
			throws NumberFormatException, Exception {
		return hotWordService.getKeywordsByCategory(request.getParameter("category").trim(), 0,
				Integer.parseInt(request.getParameter("end").trim()));

	}

	@PostMapping("/hotwordList")
	@ResponseBody
	public Map<String, Object> getHotwordList(HttpServletRequest request, HttpServletResponse response) {
		PageHelper.startPage(Integer.parseInt(request.getParameter("pageNumber")),
				Integer.parseInt(request.getParameter("pageSize")));
		List<HotWord> allHotwords = new ArrayList<>();
		if (!request.getParameter("id").trim().equals("")) {
			HotWord hotWord = hotWordService.getHotwordById(Integer.parseInt(request.getParameter("id").trim()));
			if (hotWord != null) {
				allHotwords.add(hotWord);
			}
		} else {
			allHotwords = hotWordService.getHotwordByParam(request);
		}
		PageInfo<HotWord> pageInfo = new PageInfo<>(allHotwords);
		Map<String, Object> map = Maps.newHashMap();
		map.put("total", pageInfo.getTotal());
		map.put("rows", pageInfo.getList());
		return map;
	}

	@PostMapping("/hotwordDelete")
	@ResponseBody
	public ResultData deleteHotWord(HttpServletRequest request, HttpServletResponse response) {
		for (String hotwordId : request.getParameter("ids").split(";")) {
			hotWordService.deleteHotwordById(Integer.parseInt(hotwordId));
		}
		return ResultData.result(true);
	}

	@PostMapping("/hotwordSaveOrUpdate")
	@ResponseBody
	public ResultData saveOrUpadateHotWord(HotWord param) {
		try {
			if (param.getSort() == null) {
				param.setSort(0);
			}
			if (param.getId() == null) {
				param.setCreateTime(new Date());
				hotWordService.insertHotword(param);
			} else {
				HotWord hotWord = hotWordService.getHotwordById(param.getId());
				if (hotWord != null) {
					hotWordService.updateHotword(param);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ResultData.result(false);
		}
		return ResultData.result(true);
	}
}
