package eqxiu.mall.manager.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Maps;

import eqxiu.mall.commons.web.ResultData;
import eqxiu.mall.manager.service.LogService;
import eqxiu.mall.product.model.Log;

@Controller
@RequestMapping("/m/mall/log")
public class LogController {

	@Autowired
	private LogService logService;

	@PostMapping("/{page}")
	public String index(@PathVariable String page) {
		return "log/" + page;
	}

	@PostMapping("/list")
	@ResponseBody
	public Map<String, Object> getLogLists(HttpServletRequest request, HttpServletResponse response) {
		PageHelper.startPage(Integer.parseInt(request.getParameter("pageNumber")),
				Integer.parseInt(request.getParameter("pageSize")));
		Map<String, Object> map = Maps.newHashMap();
		PageInfo<Log> pageInfo = new PageInfo<>(logService.selectLogsByParams(request));
		map.put("total", pageInfo.getTotal());
		map.put("rows", pageInfo.getList());
		return map;
	}

	@PostMapping("/delete")
	@ResponseBody
	public ResultData deleteLogs(HttpServletRequest request, HttpServletResponse response) {

		try {
			logService.deleteLogsByParams(request);
		} catch (Exception e) {
			return ResultData.result(false);
		}
		return ResultData.result(true);
	}
}
