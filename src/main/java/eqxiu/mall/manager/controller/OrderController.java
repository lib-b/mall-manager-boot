package eqxiu.mall.manager.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.eqxiu.user.model.User;
import com.eqxiu.user.service.UserService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Maps;
import eqxiu.mall.commons.web.ResultData;
import eqxiu.mall.manager.service.ArtistService;
import eqxiu.mall.manager.service.OrderService;
import eqxiu.mall.manager.service.ProductService;
import eqxiu.mall.order.model.Order;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

//只有查询，删除和修改
@Controller
@RequestMapping("/m/mall/order")
public class OrderController {
	@Autowired
	private OrderService orderService;
	@Autowired
	private ProductService productService;
	@Autowired
	private ArtistService artistService;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private UserService userService;

	@PostMapping("/{page}")
	public String index(@PathVariable String page) {
		return "order/" + page;
	}

	@PostMapping("/list")
	@ResponseBody
	public Map<String, Object> findOrderList(HttpServletRequest request, HttpServletResponse response) {
		PageHelper.startPage(Integer.parseInt(request.getParameter("pageNumber")),
				Integer.parseInt(request.getParameter("pageSize")));
		List<Order> orders = new ArrayList<>();
		// 按条件过滤
		if (!request.getParameter("id").trim().equals("")) {
			Order mallOrder = orderService.findOrder(Integer.parseInt(request.getParameter("id").trim()));
			if (mallOrder != null) {
				orders.add(mallOrder);
			}
		} else {
			try {
				orders = orderService.findByParams(request);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		PageInfo<Order> pageInfo = new PageInfo<>(orders);
		Map<String, Object> map = Maps.newHashMap();
		map.put("total", pageInfo.getTotal());
		map.put("rows", pageInfo.getList());
		return map;
	}

	@PostMapping("/numbers")
	@ResponseBody
	public List<Map<String, Object>> getOrderNumbers(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> result = new HashMap<>();
		List<Map<String, Object>> list = new ArrayList<>();
		// 按条件过滤
		if (!request.getParameter("id").trim().equals("")) {
			result = orderService.getStatisticalParameterById(Integer.parseInt(request.getParameter("id").trim()));
		} else {
			result = orderService.getStatisticalParameterByParams(request);
		}
		list.add(result);
		return list;
	}

	@PostMapping("/delete")
	@ResponseBody
	public ResultData deleteOrder(@RequestParam(required = true) int id) {
		orderService.deleteByID(id);
		return ResultData.result(true);
	}

	@PostMapping("/userId")
	@ResponseBody
	public ResultData getUserId(@RequestParam(required = true) String loginName) {
		Map<String, Object> map = new HashMap<>();
		User user = userService.getUserBaseByLoginName(loginName);
		if (user == null) {
			return ResultData.result(false);
		}
		map.put("userId", user.getId());
		return ResultData.result(true).setMap(map);
	}

	@PostMapping("/update")
	@ResponseBody
	public ResultData updateOrder(Order pMallOrder) {
		orderService.updateByID(pMallOrder);
		return ResultData.result(true);
	}

	@PostMapping("/export")
	@ResponseBody
	public ResultData exportOrderToExcel(HttpServletRequest request, HttpServletResponse response) {
		List<Order> orders = null;
		try {
			if (!request.getParameter("startTime").equals("") && !request.getParameter("endTime").equals("")) {
				orders = orderService.findByTime(request.getParameter("startTime").trim(),
						request.getParameter("endTime").trim());
				// 分析过滤出来的订单，生成统计信息保存到excel中
				exportDataToExcel(orders);
			} else {
				return ResultData.result(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ResultData.result(false);
		}
		return ResultData.result(true);
	}

	public void exportDataToExcel(List<Order> orders) throws IOException, RowsExceededException, WriteException {
		int numbers = orders.size();
		int totalFee = 0;
		int users = 0;
		Map<String, Integer> map = new HashMap<>();
		// 存放商品id和购买次数
		Map<Integer, Integer> productMap = new HashMap<>();
		for (Order order : orders) {
			// 统计订单总金额
			totalFee += order.getTotalFee();
			// 统计订单用户信息
			if (map.containsKey(order.getCreateUser())) {
				Integer fee = map.get(order.getCreateUser()).intValue();
				map.put(order.getCreateUser(), fee + order.getTotalFee());
			} else {
				map.put(order.getCreateUser(), order.getTotalFee());
			}
			// 统计订单商品信息
			if (productMap.containsKey(order.getProductId())) {
				Integer frequency = productMap.get(order.getProductId());
				productMap.put(order.getProductId(), frequency + 1);
			} else {
				productMap.put(order.getProductId(), 1);
			}
		}
		users = map.size();
		WritableWorkbook workbook = Workbook
				.createWorkbook(new File(System.getProperty("java.io.tmpdir") + "/orderStatistics.xls"));

		// 第一页总体统计
		WritableSheet sheet = workbook.createSheet("总体统计", 0);
		String headerArr[] = { "总订单数", "订单金额", "用户数" };
		// 第一页第一行设置表头
		for (int i = 0; i < headerArr.length; i++) {
			sheet.addCell(new Label(i, 0, headerArr[i]));
		}
		// 第一页第二行设置数据
		sheet.addCell(new Label(0, 1, String.valueOf(numbers)));
		sheet.addCell(new Label(1, 1, String.valueOf(totalFee)));
		sheet.addCell(new Label(2, 1, String.valueOf(users)));

		// 第二页按用户统计
		WritableSheet sheet1 = workbook.createSheet("按用户统计", 1);
		String headerArr1[] = { "用户ID", "用户名称", "登录名称", "注册时间", "账号类型", "当前秀点余额", "消耗数据(订单总金额)" };
		// 第二页第一行设置表头
		for (int i = 0; i < headerArr1.length; i++) {
			sheet1.addCell(new Label(i, 0, headerArr1[i]));
		}
		int row1 = 1;
		// 第二页遍历用户，填充数据
		for (Map.Entry<String, Integer> entry : map.entrySet()) {
			String username = entry.getKey();

			List<Map<String, Object>> userEqs = jdbcTemplate
					.queryForList("select * from EQS_USER where USER_ID=" + "'" + username + "'");
			List<Map<String, Object>> userBase = jdbcTemplate
					.queryForList("select * from BASE_USER where ID=" + "'" + username + "'");
			sheet1.addCell(new Label(0, row1, username));
			if (userEqs.size() > 0) {
				sheet1.addCell(new Label(2, row1, userEqs.get(0).get("LOGIN_NAME").toString()));
				sheet1.addCell(new Label(5, row1, userEqs.get(0).get("XD").toString()));
			}
			if (userBase.size() > 0) {
				sheet1.addCell(new Label(1, row1, userBase.get(0).get("NAME").toString()));
				sheet1.addCell(new Label(3, row1, userBase.get(0).get("REG_TIME").toString()));
				sheet1.addCell(new Label(4, row1, userBase.get(0).get("TYPE").toString()));
			}
			sheet1.addCell(new Label(6, row1, entry.getValue().toString()));
			row1++;
		}
		// 第三页按商品统计
		WritableSheet sheet2 = workbook.createSheet("按商品统计", 2);
		String headerArr2[] = { "订单商品名", "购买次数", "作者", "是否秀客" };
		// 第三页第一行设置表头
		for (int i = 0; i < headerArr2.length; i++) {
			sheet2.addCell(new Label(i, 0, headerArr2[i]));
		}
		// 第三页根据用户id查找用户
		int row2 = 1;
		for (Map.Entry<Integer, Integer> entry : productMap.entrySet()) {
			Integer productId = entry.getKey();
			if (productService.getProduct(productId) != null) {
				Integer artistId = productService.getProduct(productId).getArtistId();
				sheet2.addCell(new Label(0, row2, productService.getProduct(productId).getTitle()));
				sheet2.addCell(new Label(1, row2, entry.getValue().toString()));
				if (artistId != null && artistService.findByID(artistId) != null) {
					sheet2.addCell(new Label(2, row2, artistService.findByID(artistId).getArtistName()));
					if (artistId != null && artistService.findByID(artistId).getType() != null
							&& artistService.findByID(artistId).getType() == 2) {
						sheet2.addCell(new Label(3, row2, "是"));
					} else {
						sheet2.addCell(new Label(3, row2, "否"));
					}
				}
				row2++;
			}
		}
		workbook.write();
		workbook.close();
	}

	@GetMapping("/downLoadExcel")
	public ResponseEntity<byte[]> downLoadExcelData(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		String filePath = System.getProperty("java.io.tmpdir") + "/orderStatistics.xls";
		File f = new File(filePath);
		HttpHeaders headers = new HttpHeaders();
		String fileName = new String("orderStatistics.xls".getBytes("UTF-8"), "iso-8859-1");
		headers.setContentDispositionFormData("attachment", fileName);
		headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
		return new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(f), headers, HttpStatus.CREATED);
	}

	@RequestMapping("/statisticByProductType")
	@ResponseBody
	public ResultData statisticByProductType(HttpServletRequest request, HttpServletResponse response) {
		List<Map<String, Object>> list = new ArrayList<>();
		try {
			list = orderService.countByProductType(request.getParameter("startTime"), request.getParameter("endTime"),
					request.getParameter("sort"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ResultData.result(true).setList(list);
	}

	@RequestMapping("/statisticsByArtist")
	@ResponseBody
	public ResultData statisticsByArtist(HttpServletRequest request, HttpServletResponse response) {
		List<Map<String, Object>> list = new ArrayList<>();
		try {
			list = orderService.countByArtist(request.getParameter("startTime"), request.getParameter("endTime"),
					request.getParameter("sort"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ResultData.result(true).setList(list);
	}
}
