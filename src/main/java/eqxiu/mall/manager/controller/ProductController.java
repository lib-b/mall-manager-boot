package eqxiu.mall.manager.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import cn.knet.common.utils.Identities;
import cn.knet.modules.base.util.UserUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Maps;
import eqxiu.mall.commons.constants.BatchSetType;
import eqxiu.mall.commons.web.ResultData;
import eqxiu.mall.manager.dto.CategoryDTO;
import eqxiu.mall.manager.service.AttributeGroupKeyService;
import eqxiu.mall.manager.service.AttributeKeyService;
import eqxiu.mall.manager.service.CategoryProductService;
import eqxiu.mall.manager.service.CategoryService;
import eqxiu.mall.manager.service.IndexQueueService;
import eqxiu.mall.manager.service.ProductAttributeService;
import eqxiu.mall.manager.service.ProductService;
import eqxiu.mall.manager.service.ProductTagService;
import eqxiu.mall.manager.service.SpecialProductService;
import eqxiu.mall.manager.service.SpecialService;
import eqxiu.mall.manager.service.TagService;
import eqxiu.mall.manager.service.IndexQueueService.IndexOption;
import eqxiu.mall.partner.qiniu.QiniuStorService;
import eqxiu.mall.product.model.AttributeGroupKey;
import eqxiu.mall.product.model.Category;
import eqxiu.mall.product.model.CategoryProduct;
import eqxiu.mall.product.model.Product;
import eqxiu.mall.product.model.ProductAttribute;
import eqxiu.mall.product.model.ProductTag;
import eqxiu.mall.product.model.Special;
import eqxiu.mall.product.model.SpecialProduct;
import eqxiu.mall.product.model.Tag;

@Controller
@RequestMapping("/m/mall/product")
public class ProductController {

	@Autowired
	private ProductService productService;
	@Autowired
	private ProductTagService productTagService;
	@Autowired
	private CategoryProductService categoryProductService;
	@Autowired
	private TagService tagService;
	@Autowired
	private CategoryService categoryService;
	@Autowired
	private ProductAttributeService productAttributeService;
	@Autowired
	private AttributeGroupKeyService AttributeGroupKeyService;
	@Autowired
	private AttributeKeyService attributeKeyService;
	@Autowired
	private StringRedisTemplate redisTemplate;
	@Autowired
	private QiniuStorService qiniuStorService;
	@Autowired
	private IndexQueueService indexQueueService;
	@Autowired
	private SpecialService specialService;
	@Autowired
	private SpecialProductService specialProductService;

	@PostMapping("/{page}")
	public String index(@PathVariable String page) {
		return "product/" + page;
	}

	@PostMapping("/list")
	@ResponseBody
	public Map<String, Object> findProductList(@RequestParam(required = true) String orderByClause,
			HttpServletRequest request, HttpServletResponse response) {
		PageHelper.startPage(Integer.parseInt(request.getParameter("pageNumber")),
				Integer.parseInt(request.getParameter("pageSize")));
		List<Product> allProducts = new ArrayList<Product>();
		if (!request.getParameter("id").trim().equals("")) {
			Product curproduct = productService.getProduct(Integer.parseInt(request.getParameter("id").trim()));
			if (curproduct != null) {
				allProducts.add(curproduct);
			}
		} else if (!request.getParameter("code").trim().equals("")) {
			Product curproduct = productService.getProductByCode(request.getParameter("code").trim());
			if (curproduct != null) {
				allProducts.add(curproduct);
			}
		} else if (request.getParameter("category1").equals("-1")) {
			allProducts = productService.selectNonCategory();
		} else {
			try {
				allProducts = productService.selectByParams(request);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		setCategoryPath(allProducts);
		PageInfo<Product> pageInfo = new PageInfo<>(allProducts);
		Map<String, Object> map = Maps.newHashMap();
		map.put("total", pageInfo.getTotal());
		map.put("rows", pageInfo.getList());
		return map;
	}

	public void setCategoryPath(List<Product> products) {
		for (Product product : products) {
			product.setCategoryPath(categoryService.getCategoryPathByProductId(product.getId()));
		}
	}

	@SuppressWarnings("unchecked")
	@PostMapping("/attrbutelist")
	@ResponseBody
	public JSONArray findProductAttrList(@RequestParam(required = true) int productId) {
		JSONArray jsonArray = new JSONArray();
		// 商品属性组中的属性有可能修改，需要调整一下商品属性表记录
		Product product = productService.getProduct(productId);
		List<AttributeGroupKey> attributeGroupKeys = AttributeGroupKeyService.findByGroupId(product.getAttrGroupId());
		List<Integer> attIds = new ArrayList<>();
		// 增加新的产品属性记录，值为空
		for (AttributeGroupKey attributeGroupKey : attributeGroupKeys) {
			ProductAttribute productAttribute = new ProductAttribute();
			productAttribute.setAttrKeyId(attributeGroupKey.getAttrKeyId());
			productAttribute.setProductId(productId);
			if (productAttributeService.selectByKey(productAttribute) == null) {
				productAttributeService.insert(productAttribute);
			}
			attIds.add(attributeGroupKey.getAttrKeyId());
		}
		// 删除已经删除的属性记录
		List<ProductAttribute> productAttributes = productAttributeService.findByProductId(productId);
		for (ProductAttribute productAttribute : productAttributes) {
			if (!attIds.contains(productAttribute.getAttrKeyId())) {
				productAttributeService.deleteByID(productAttribute);
			}
		}
		// 最后查询属性信息
		List<Map<String, Object>> maps = productAttributeService.selectByProductId(productId);
		for (Map<String, Object> map : maps) {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("name", map.get("attrName").toString());
			jsonObject.put("value", map.get("attrValue").toString());
			jsonObject.put("title", map.get("attrTitle"));
			jsonObject.put("type", map.get("attrType").toString());
			jsonArray.add(jsonObject);
		}
		return jsonArray;
	}

	@PostMapping("/categoryList")
	@ResponseBody
	public List<List<Map<String, List<String>>>> findProductCategoryList(@RequestParam(required = true) int productId) {

		List<List<Map<String, List<String>>>> result = new ArrayList<>();
		for (CategoryProduct categoryProduct : categoryProductService.findByProductId(productId)) {
			List<Map<String, List<String>>> list = new ArrayList<>();
			Category category = categoryService.getCategoryByID(categoryProduct.getCategoryId());
			for (String categoryId : category.getPath().split("/")) {
				list.add(getAllCategoryById(Integer.parseInt(categoryId)));
			}
			result.add(list);
		}
		return result;
	}

	// 根据给定的分类id查出跟分类所有同等级的分类，按照(ID:name)的形式存入到list中。
	// 返回的map，key是传入的分类的id，value是上面的list
	public Map<String, List<String>> getAllCategoryById(int categoryId) {
		Map<String, List<String>> map = new HashMap<>();
		List<String> cateIdNames = new ArrayList<>();
		Category curCategory = categoryService.getCategoryByID(categoryId);
		List<CategoryDTO> categories = categoryService.getCategoryByParentId(curCategory.getParentId());
		for (CategoryDTO categoryDTO : categories) {
			cateIdNames.add(categoryDTO.getId().toString() + ":" + categoryDTO.getName());
		}
		map.put(curCategory.getId().toString(), cateIdNames);
		return map;
	}

	@PostMapping("/updateCategory")
	@ResponseBody
	public ResultData updateProductCategory(HttpServletRequest request, HttpServletResponse response) {
		try {
			String[] newCategoryIds = null;
			String[] oldCategoryIds = null;
			if (!request.getParameter("newCategoryIds").equals("")) {
				newCategoryIds = request.getParameter("newCategoryIds").split(",");
			}
			if (!request.getParameter("oldCategoryIds").equals("")) {
				oldCategoryIds = request.getParameter("oldCategoryIds").split(",");
			}
			// 获取新的分类id
			List<Integer> categoryIds = new ArrayList<>();
			if (newCategoryIds != null) {
				for (String newCategoryId : newCategoryIds) {
					categoryIds.add(Integer.parseInt(request.getParameter("newcategory3" + newCategoryId)));
				}
			}
			if (oldCategoryIds != null) {
				for (String oldCategoryId : oldCategoryIds) {
					categoryIds.add(Integer.parseInt(request.getParameter("oldCategory3" + oldCategoryId)));
				}
			}
			// 校验通过后再删除掉之前的产品分类关系
			Integer productId = Integer.parseInt(request.getParameter("productId"));
			List<CategoryProduct> categoryProducts = categoryProductService.findByProductId(productId);
			for (CategoryProduct categoryProduct : categoryProducts) {
				categoryProductService.delete(categoryProduct);
			}
			// 插入新的产品分类关系
			for (Integer categoryId : categoryIds) {
				CategoryProduct categoryProduct = new CategoryProduct();
				categoryProduct.setCategoryId(categoryId);
				categoryProduct.setProductId(productId);
				if (categoryProductService.findByProductAndCategoryId(productId, categoryId) == null) {
					categoryProductService.insert(categoryProduct);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ResultData.result(false);
		}
		return ResultData.result(true);
	}

	@SuppressWarnings("unchecked")
	@PostMapping("/tagList")
	@ResponseBody
	public JSONArray findProductTagList(@RequestParam(required = true) int productId) {
		JSONArray jsonArray = new JSONArray();
		for (ProductTag productTag : productTagService.findByProductId(productId)) {
			Tag tag = tagService.selectByPrimaryKey(productTag.getTagId());
			JSONObject tagjson = new JSONObject();
			if (tag != null && !tag.getName().equals("")) {
				tagjson.put("value", tag.getName());
				jsonArray.add(tagjson);
			}
		}
		return jsonArray;
	}

	@PostMapping("/updateTag")
	@ResponseBody
	public ResultData updateProductTag(HttpServletRequest request, HttpServletResponse response) {
		try {
			List<Tag> newTags = new ArrayList<>();
			// 先进行名称校验，如果名称不存在直接返回错误
			String[] productTags = request.getParameterValues("tag");
			if (productTags != null) {
				for (String tagName : productTags) {
					if (!tagName.equals("")) {
						if (tagService.selectByName(tagName) == null) {
							Map<String, Object> map = new HashMap<>();
							map.put("tagName", tagName);
							return ResultData.result(false).setMap(map);
						} else {
							newTags.add(tagService.selectByName(tagName));
						}
					}
				}
				// 校验通过后再删除掉之前的产品分类关系
				Integer productId = Integer.parseInt(request.getParameter("productId"));
				List<ProductTag> tags = productTagService.findByProductId(productId);
				for (ProductTag productTag : tags) {
					productTagService.deleteByID(productTag);
				}
				// 插入新的产品分类关系
				for (Tag tag : newTags) {
					ProductTag productTag = new ProductTag();
					productTag.setTagId(tag.getId());
					productTag.setProductId(productId);
					if (productTagService.selectByProductAndTagId(productId, tag.getId()) == null) {
						productTagService.insert(productTag);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ResultData.result(false);
		}
		return ResultData.result(true);
	}

	@PostMapping("/updateAttributeValue")
	@ResponseBody
	public ResultData updateAttrValue(HttpServletRequest request, HttpServletResponse response) {
		// 获取当前产品的属性名称和值
		Integer productId = Integer.parseInt(request.getParameter("productId"));
		List<Map<String, Object>> namevalues = productAttributeService.selectByProductId(productId);
		try {
			for (Map<String, Object> map : namevalues) {
				ProductAttribute record = new ProductAttribute();
				record.setAttrKeyId(attributeKeyService.selectByName(map.get("attrName").toString()).getId());
				record.setProductId(productId);
				record.setAttrValue(request.getParameter(map.get("attrName").toString()));
				productAttributeService.update(record);
			}
			// 清理redis缓存
			redisTemplate.delete("mall:prod:map:" + productId);
			redisTemplate.delete("mall:prod:" + productId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ResultData.result(true);
	}

	@PostMapping("/delete")
	@ResponseBody
	public ResultData deleteProductByID(HttpServletRequest request) {
		try {
			for (String productId : request.getParameter("ids").split(";")) {
				// 删除商品
				productService.deleteProduct(Integer.parseInt(productId));
				// 删除商品标签
				productTagService.deleteByProductId(Integer.parseInt(productId));
				// 删除商品分类
				categoryProductService.deleteByProductId(Integer.parseInt(productId));
				// 删除商品属性（指定属性组下的）
				productAttributeService.deleteByProductId(Integer.parseInt(productId));
				// 清理redis缓存
				redisTemplate.delete("mall:prod:map:" + Integer.parseInt(productId));
				redisTemplate.delete("mall:prod:" + Integer.parseInt(productId));
				// 发送消息到rabbit，重建索引
				indexQueueService.indexProduct(Integer.parseInt(productId), IndexOption.DELETE);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ResultData.result(false);
		}
		return ResultData.result(true);
	}

	@PostMapping("/save")
	@ResponseBody
	public ResultData saveProduct(Product params, @RequestParam(required = false) String productTag,
			@RequestParam(required = false) String productCategory) {
		if (!params.getCode().equals("") && productService.getProductByCode(params.getCode()) != null) {
			Map<String, Object> map = new HashMap<>();
			map.put("codeError", "code已经存在,请重新输入！");
			return ResultData.result(false).setMap(map);
		}
		// 创建时间默认使用当前系统时间
		params.setCreateTime(new Date());
		// 自动生成code
		params.setCode(Identities.randomBase62(8));
		// 商品创建者
		params.setCreateUser(UserUtils.getUser().getName());
		// type默认为零
		if (params.getType() == null) {
			params.setType((byte) 0);
		}
		productService.insertProduct(params);
		// 根据属性组设置属性信息
		setProductAttr(params);

		// 字体更改sourceId
		if (params.getAttrGroupId() == 4) {
			params.setSourceId(params.getId());
			productService.updateProduct(params);
		}

		// 发送消息到rabbit，重建索引
		indexQueueService.indexProduct(params.getId(), IndexOption.ADD);
		return ResultData.result(true);
	}

	@PostMapping("/uploadToken")
	@ResponseBody
	public Map<String, Object> getQiniuToken(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> result = new HashMap<>();
		Product product = null;
		if (request.getParameter("productId") != null) {
			int productId = Integer.parseInt(request.getParameter("productId"));
			product = productService.getProduct(productId);
		}
		result.put("uptoken", qiniuStorService.getUptoken(null));
		if (product != null) {
			result.put("brandId", product.getBrandId());
		}
		return result;
	}

	@PostMapping("/batchAdd")
	@ResponseBody
	public ResultData batchAdd(HttpServletRequest request, HttpServletResponse response) {
		List<String> list = new ArrayList<>();
		Map<String, Object> map = new HashMap<>();
		try {
			Integer type = Integer.parseInt(request.getParameter("type"));
			if (type == BatchSetType.TAG.getCode()) {
				list = batchAddTags(request.getParameter("value").split(";"), request.getParameter("ids").split(";"));
			} else if (type == BatchSetType.SPECIAL.getCode()) {
				boolean result = batchAddSpecial(request.getParameter("value"), request.getParameter("ids").split(";"));
				return ResultData.result(result).setList(list).setMap(map);
			} else if (type == BatchSetType.CATEGORY.getCode()) {
				map = batchAddCategory(request.getParameter("value").split("/"),
						request.getParameter("ids").split(";"));
				if (!map.isEmpty()) {
					return ResultData.result(false).setList(list).setMap(map);
				}
			}
		} catch (Exception e) {
			return ResultData.result(false).setList(list).setMap(map);
		}
		return ResultData.result(true).setList(list).setMap(map);
	}

	@PostMapping("/batchDelete")
	@ResponseBody
	public ResultData batchDelete(HttpServletRequest request, HttpServletResponse response) {
		try {
			Integer type = Integer.parseInt(request.getParameter("type"));
			if (type == BatchSetType.TAG.getCode()) {
				batchDeleteTags(request.getParameter("value").split(";"), request.getParameter("ids").split(";"));
			} else if (type == BatchSetType.SPECIAL.getCode()) {
				batchDeleteSpecial(request.getParameter("value"), request.getParameter("ids").split(";"));
			} else if (type == BatchSetType.CATEGORY.getCode()) {
				batchDeleteCategory(request.getParameter("value").split("/"), request.getParameter("ids").split(";"));
			}
		} catch (Exception e) {
			return ResultData.result(false);
		}
		return ResultData.result(true);
	}

	public List<String> batchAddTags(String[] tags, String[] productIds) {
		List<String> unExistTags = new ArrayList<>();
		ProductTag productTag = new ProductTag();
		for (String tagName : tags) {
			Tag tag = tagService.selectByName(tagName);
			if (tag == null) {
				unExistTags.add(tagName);
			} else {
				for (String productId : productIds) {
					if (productTagService.selectByProductAndTagId(tag.getId(), Integer.parseInt(productId)) == null) {
						productTag.setProductId(Integer.parseInt(productId));
						productTag.setTagId(tag.getId());
						productTagService.insert(productTag);
					}
				}
			}
		}
		return unExistTags;
	}

	public void batchDeleteTags(String[] tags, String[] productIds) {
		for (String tagName : tags) {
			Tag tag = tagService.selectByName(tagName);
			if (tag != null) {
				for (String productId : productIds) {
					productTagService.deleteByProductAndTagId(tag.getId(), Integer.parseInt(productId));
				}
			}
		}
	}

	public boolean batchAddSpecial(String code, String[] productIds) {
		Special special = specialService.findByCode(code);
		if (special != null) {
			SpecialProduct specialProduct = new SpecialProduct();
			for (String productId : productIds) {
				specialProduct.setProductId(Integer.parseInt(productId));
				specialProduct.setSpecialId(special.getId());
				if (specialProductService.selectByKey(Integer.parseInt(productId), special.getId()) == null) {
					specialProductService.insert(specialProduct);
				}
			}
			// 清理redis缓存
			redisTemplate.delete("mall:prod:map:special:" + special.getId());
			return true;
		} else {
			return false;
		}
	}

	public void batchDeleteSpecial(String code, String[] productIds) {
		Special special = specialService.findByCode(code);
		if (special != null) {
			SpecialProduct specialProduct = new SpecialProduct();
			for (String productId : productIds) {
				specialProduct.setProductId(Integer.parseInt(productId));
				specialProduct.setSpecialId(special.getId());
				specialProductService.deleteByID(specialProduct);
			}
			// 清理redis缓存
			redisTemplate.delete("mall:prod:map:special:" + special.getId());
		}
	}

	public Map<String, Object> batchAddCategory(String[] productCategorys, String[] productIds) {
		int pId = 0;
		Category category = null;
		Map<String, Object> map = new HashMap<>();
		for (String categoryName : productCategorys) {
			category = categoryService.getCategoryByNameAndParentId(categoryName, pId);
			if (category == null) {
				map.put("categoryName", categoryName);
				if (pId == 0) {
					map.put("parentCategoryName", "顶级");
				} else {
					String value = categoryService.getCategoryByID(pId).getName();
					map.put("parentCategoryName", value);
				}

			} else {
				pId = category.getId();
			}
		}
		for (String productId : productIds) {
			if (categoryProductService.findByProductAndCategoryId(Integer.parseInt(productId), pId) == null) {
				CategoryProduct categoryProduct = new CategoryProduct();
				categoryProduct.setCategoryId(pId);
				categoryProduct.setProductId(Integer.parseInt(productId));
				categoryProductService.insert(categoryProduct);
			}
			indexQueueService.indexProduct(Integer.parseInt(productId), IndexOption.ADD);
		}
		return map;
	}

	public void batchDeleteCategory(String[] productCategorys, String[] productIds) {
		int pId = 0;
		Category category = null;
		for (String categoryName : productCategorys) {
			category = categoryService.getCategoryByNameAndParentId(categoryName, pId);
			if (category != null) {
				pId = category.getId();
			} else {
				return;
			}
		}
		for (String productId : productIds) {
			CategoryProduct categoryProduct = new CategoryProduct();
			categoryProduct.setCategoryId(pId);
			categoryProduct.setProductId(Integer.parseInt(productId));
			categoryProductService.delete(categoryProduct);
			indexQueueService.indexProduct(Integer.parseInt(productId), IndexOption.ADD);
		}
	}

	@PostMapping("/update")
	@ResponseBody
	public ResultData updateProduct(Product params) {
		// 更新产品信息
		int id = params.getId();
		int groupId = params.getAttrGroupId();
		// 获取当前产品
		Product product = productService.getProduct(id);
		if (!product.getCode().equals(params.getCode()) && productService.getProductByCode(params.getCode()) != null) {
			Map<String, Object> map = new HashMap<>();
			map.put("codeError", "code已经存在,请重新输入！");
			return ResultData.result(false).setMap(map);
		}
		if (product.getAttrGroupId() != groupId) {
			// 如果属性组变了，先把产品属性关系表清理一下
			for (ProductAttribute productAttribute : productAttributeService.findByProductId(product.getId())) {
				productAttributeService.deleteByID(productAttribute);
			}
			setProductAttr(params);
		}
		productService.updateProduct(params);
		redisTemplate.delete("mall:prod:map:" + params.getId());
		redisTemplate.delete("mall:prod:" + params.getId());
		// 发送消息到rabbit，重建索引
		indexQueueService.indexProduct(id, IndexOption.UPDATE);
		return ResultData.result(true);
	}

	@PostMapping("/uploadMusic")
	@ResponseBody
	public ResultData uploadMusic(HttpServletRequest request, HttpServletResponse response) {
		try {
			Product product = new Product();
			// 设置特有属性
			product.setAttrGroupId(3);
			if (request.getParameter("price").equals("")) {
				product.setPrice(0);
			} else {
				product.setPrice(Integer.parseInt(request.getParameter("price")));
			}
			// 设置通用属性
			setCommonAttr(product, request);
			// 设置专辑封面和音乐路径
			setMusicAttrValue(request.getParameter("cover"), request.getParameter("titles").split(":")[1],
					product.getId());

		} catch (Exception e) {
			e.printStackTrace();
			return ResultData.result(false);
		}
		return ResultData.result(true);
	}

	public void setMusicAttrValue(String tmbPath, String path, Integer productId) {
		ProductAttribute productAttribute = new ProductAttribute();
		// 设置图片tmbpath属性值
		productAttribute.setAttrKeyId(6);
		productAttribute.setProductId(productId);
		productAttribute.setAttrValue(tmbPath);
		productAttributeService.update(productAttribute);
		// 设置图片
		productAttribute.setAttrKeyId(32);
		productAttribute.setAttrValue(path);
		productAttributeService.update(productAttribute);
	}

	@PostMapping("/uploadFont")
	@ResponseBody
	public ResultData uploadFont(HttpServletRequest request, HttpServletResponse response) {
		try {
			// 新建产品对象
			Product product = new Product();
			product.setAttrGroupId(4);
			// 设置特有的属性
			if (request.getParameter("personPrice").equals("")) {
				product.setPrice(0);
			} else {
				product.setPrice(Integer.parseInt(request.getParameter("personPrice")));
			}
			// 设置通用的属性
			setCommonAttr(product, request);
			// 设置属性值
			setFontAttrValue(request, product);
		} catch (Exception e) {
			e.printStackTrace();
			return ResultData.result(false);
		}
		return ResultData.result(true);
	}

	public void setFontAttrValue(HttpServletRequest request, Product product) {
		// 设置字体企业价格
		ProductAttribute productAttribute = new ProductAttribute();
		productAttribute.setAttrKeyId(61);
		productAttribute.setProductId(product.getId());
		if (!request.getParameter("companyPrice").equals("")) {
			productAttribute.setAttrValue(request.getParameter("companyPrice"));
		} else {
			productAttribute.setAttrValue("100");
		}
		productAttributeService.update(productAttribute);
		// 设置字体预览文字
		productAttribute.setAttrKeyId(50);
		if (!request.getParameter("text").equals("")) {
			productAttribute.setAttrValue(request.getParameter("text"));
		} else {
			productAttribute.setAttrValue("易企秀简单免费好用");
		}
		productAttributeService.update(productAttribute);
		String path = request.getParameter("titles").split(":")[1];
		String type = path.split("[.]")[1];
		// 设置woff_path
		if (type.equals("woff")) {
			productAttribute.setAttrKeyId(43);
			productAttribute.setAttrValue(path);
			productAttributeService.update(productAttribute);
		} else if (type.equals("ttf")) {
			productAttribute.setAttrKeyId(44);
			productAttribute.setAttrValue(path);
			productAttributeService.update(productAttribute);
		}
	}

	@PostMapping("/uploadPicture")
	@ResponseBody
	public ResultData uploadPicture(HttpServletRequest request, HttpServletResponse response) {
		try {
			// 新建产品对象
			Product product = new Product();
			// 设置图片特有的属性
			product.setAttrGroupId(1);
			if (request.getParameter("price").equals("")) {
				product.setPrice(0);
			} else {
				product.setPrice(Integer.parseInt(request.getParameter("price")));
			}
			// 设置通用的属性
			setCommonAttr(product, request);
			// 设置属性值
			setPictureAttrValue(request, product);
		} catch (Exception e) {
			e.printStackTrace();
			return ResultData.result(false);
		}
		return ResultData.result(true);
	}

	@PostMapping("/countByBrand")
	@ResponseBody
	public ResultData countByBrand(HttpServletRequest request, HttpServletResponse response) {
		List<Map<String, Object>> list = new ArrayList<>();
		try {
			list = productService.countProductsByBrand(request.getParameter("createStartTime"),
					request.getParameter("createEndTime"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ResultData.result(true).setList(list);
	}

	@PostMapping("/countByGroupId")
	@ResponseBody
	public ResultData countByGroupId(HttpServletRequest request, HttpServletResponse response) {
		List<Map<String, Object>> list = new ArrayList<>();
		try {
			list = productService.countProductsByGroupId(request.getParameter("createStartTime"),
					request.getParameter("createEndTime"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ResultData.result(true).setList(list);
	}

	public void setPictureAttrValue(HttpServletRequest request, Product product) {
		String path = request.getParameter("titles").split(":")[1];
		ProductAttribute productAttribute = new ProductAttribute();
		// 设置图片tmbpath属性值
		productAttribute.setAttrKeyId(6);
		productAttribute.setProductId(product.getId());
		productAttribute.setAttrValue(path);
		productAttributeService.update(productAttribute);
		// 设置图片
		productAttribute.setAttrKeyId(32);
		productAttribute.setAttrValue(path);
		productAttributeService.update(productAttribute);
	}

	public void setCommonAttr(Product product, HttpServletRequest request) {
		product.setTitle(request.getParameter("titles").split(":")[0]);
		product.setBrandId(Integer.parseInt(request.getParameter("brandId")));
		product.setPlatform(Byte.parseByte(request.getParameter("platForm")));
		product.setArtistId(Integer.parseInt(request.getParameter("artistId")));
		product.setStatus(Byte.parseByte(request.getParameter("status")));
		product.setSort(Integer.parseInt(request.getParameter("sort")));
		// 创建时间默认使用当前系统时间
		product.setCreateTime(new Date());
		// 自动生成code
		product.setCode(Identities.randomBase62(8));
		// 商品创建者
		product.setCreateUser(UserUtils.getUser().getName());
		// type默认为零
		if (product.getType() == null) {
			product.setType((byte) 0);
		}
		productService.insertProduct(product);
		// 初始化字体属性
		setProductAttr(product);
		// 设置标签
		setTags(product, request.getParameter("tags"));
		// 设置分类
		setCategory(product, request.getParameter("category"));
	}

	public void setTags(Product product, String productTag) {
		String[] tags = null;
		// 解析分类和标签名称
		if (!productTag.equals("")) {
			tags = productTag.split(";");
		}
		List<Integer> tagids = new ArrayList<>();
		// 把分类和标签名称转换成id
		if (tags != null) {
			for (String tagname : tags) {
				Tag tag = tagService.selectByName(tagname);
				if (tag != null) {
					tagids.add(tag.getId());
				}
			}
		}
		ProductTag curProductTag = new ProductTag();
		// 新增产品标签信息
		for (Integer tagid : tagids) {
			curProductTag.setProductId(product.getId());
			curProductTag.setTagId(tagid);
			if (productTagService.selectByProductAndTagId(tagid, product.getId()) == null) {
				productTagService.insert(curProductTag);
			}
		}
	}

	public void setCategory(Product product, String fullPath) {
		String[] productCategorys = fullPath.split("/");
		String productIds[] = new String[1];
		productIds[0] = product.getId().toString();
		batchAddCategory(productCategorys, productIds);
	}

	public void setProductAttr(Product product) {
		List<AttributeGroupKey> attrgroups = AttributeGroupKeyService.findByGroupId(product.getAttrGroupId());
		for (AttributeGroupKey attributeGroupKey : attrgroups) {
			ProductAttribute productAttribute = new ProductAttribute();
			productAttribute.setProductId(product.getId());
			productAttribute.setAttrKeyId(attributeGroupKey.getAttrKeyId());
			productAttributeService.insert(productAttribute);
		}
	}
}
