package eqxiu.mall.manager.controller;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Maps;
import eqxiu.mall.commons.page.PageDTO;
import eqxiu.mall.commons.web.ResultData;
import eqxiu.mall.manager.service.ProductService;
import eqxiu.mall.manager.service.SpecialProductService;
import eqxiu.mall.manager.service.SpecialService;
import eqxiu.mall.product.model.Special;
import eqxiu.mall.product.model.SpecialProduct;

@Controller
@RequestMapping("/m/mall/special")
public class SpecialController {
	@Autowired
	private SpecialService specialService;
	@Autowired
	private SpecialProductService specialProductService;
	@Autowired
	private StringRedisTemplate redisTemplate;
	@Autowired
	private ProductService productService;

	@PostMapping("/{page}")
	String Index(@PathVariable String page) {
		return "special/" + page;
	}

	@PostMapping("/list")
	@ResponseBody
	public Map<String, Object> getSpecialsList(HttpServletRequest request, HttpServletResponse response) {
		PageHelper.startPage(Integer.parseInt(request.getParameter("pageNumber")),
				Integer.parseInt(request.getParameter("pageSize")));
		Map<String, Object> map = Maps.newHashMap();
		PageInfo<Special> pageInfo = new PageInfo<>(specialService.findSpecialsByParam(request));
		map.put("total", pageInfo.getTotal());
		map.put("rows", pageInfo.getList());
		return map;
	}

	@PostMapping("/productlist")
	@ResponseBody
	public Map<String, Object> findProductList(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> map = Maps.newHashMap();
		if (request.getParameter("specialPid") != null && !request.getParameter("specialPid").trim().equals("")) {
			PageDTO<Map<String, Object>> pageInfo = specialProductService.findBySpecialId(
					Integer.parseInt(request.getParameter("specialPid").trim()),
					Integer.parseInt(request.getParameter("pageNumber")),
					Integer.parseInt(request.getParameter("pageSize")));
			map.put("total", pageInfo.getCount());
			map.put("rows", pageInfo.getList());
		}
		return map;
	}

	@PostMapping("/addProduct")
	@ResponseBody
	public ResultData addProductByCategoryID(@RequestParam(required = true) String productIds,
			@RequestParam(required = true) int specialPid) {
		try {
			if (!productIds.equals("")) {
				String[] pIds = productIds.split(";");
				for (String productId : pIds) {
					if (productService.getProduct(Integer.parseInt(productId)) != null) {
						SpecialProduct specialProduct = new SpecialProduct();
						specialProduct.setSpecialId(specialPid);
						specialProduct.setProductId(Integer.parseInt(productId));
						if (specialProductService.selectByKey(Integer.parseInt(productId), specialPid) == null) {
							specialProductService.insert(specialProduct);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ResultData.result(false);
		}
		return ResultData.result(true);
	}

	@PostMapping("/deleteProduct")
	@ResponseBody
	public ResultData deleteProduct(HttpServletRequest request, HttpServletResponse response) {
		SpecialProduct specialProduct = new SpecialProduct();
		specialProduct.setSpecialId(Integer.parseInt(request.getParameter("specialId")));
		for (String productId : request.getParameter("ids").split(";")) {
			specialProduct.setProductId(Integer.parseInt(productId));
			specialProductService.deleteByID(specialProduct);
		}
		redisTemplate.delete("mall:prod:map:special:" + Integer.parseInt(request.getParameter("specialId")));
		return ResultData.result(true);
	}

	@PostMapping("/saveOrUpdateSpecial")
	@ResponseBody
	public ResultData saveSpecial(Special param, HttpServletRequest request, HttpServletResponse response) {
		try {
			if (param.getId() != null) {
				specialService.update(param);
			} else {
				specialService.insert(param);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ResultData.result(false);
		}
		return ResultData.result(true);
	}

	@PostMapping("/delete")
	@ResponseBody
	public ResultData deleteSpecial(HttpServletRequest request, HttpServletResponse response) {
		for (String specialId : request.getParameter("ids").split(";")) {
			// 删除专题
			specialService.deleteById(Integer.parseInt(specialId));
			// 删除专题下配置的商品
			specialProductService.deleteBySpecialID(Integer.parseInt(specialId));
		}
		return ResultData.result(true);
	}
}
