package eqxiu.mall.manager.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Maps;
import eqxiu.mall.commons.page.PageDTO;
import eqxiu.mall.commons.web.ResultData;
import eqxiu.mall.manager.service.ProductService;
import eqxiu.mall.manager.service.ProductTagService;
import eqxiu.mall.manager.service.TagService;
import eqxiu.mall.product.model.ProductTag;
import eqxiu.mall.product.model.Tag;

@Controller
@RequestMapping("/m/mall/tag")
public class TagController {

	@Autowired
	private TagService tagService;
	@Autowired
	private ProductTagService producttagService;
	@Autowired
	private ProductService productService;

	@PostMapping("/{page}")
	public String index(@PathVariable String page) {
		return "tag/" + page;
	}

	@PostMapping("/tag/list")
	@ResponseBody
	public Map<String, Object> findTagList(HttpServletRequest request, HttpServletResponse response) {
		List<Tag> Alltags = new ArrayList<Tag>();
		PageHelper.startPage(Integer.parseInt(request.getParameter("pageNumber")),
				Integer.parseInt(request.getParameter("pageSize")));
		if (request.getParameter("name").trim().equals("")) {
			Alltags = tagService.selectAllTags();
		} else {
			Tag tag = tagService.selectByName(request.getParameter("name").trim());
			if (tag != null) {
				Alltags.add(tag);
			}
		}
		PageInfo<Tag> pageInfo = new PageInfo<>(Alltags);
		Map<String, Object> map = Maps.newHashMap();
		map.put("total", pageInfo.getTotal());
		map.put("rows", pageInfo.getList());
		return map;
	}

	@PostMapping("/tag/productlist")
	@ResponseBody
	public Map<String, Object> findProductList(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> map = Maps.newHashMap();
		try {
			if (!request.getParameter("id").equals("")) {
				PageDTO<Map<String, Object>> allProducts = productService.findProductsByTagId(
						Integer.parseInt(request.getParameter("id")),
						Integer.parseInt(request.getParameter("pageNumber")),
						Integer.parseInt(request.getParameter("pageSize")));
				map.put("total", allProducts.getCount());
				map.put("rows", allProducts.getList());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}

	@PostMapping("/tag/deleteProduct")
	@ResponseBody
	public ResultData deleteProductByTagID(HttpServletRequest request, HttpServletResponse response) {
		try {
			Integer tagId = Integer.parseInt(request.getParameter("tagId"));
			ProductTag productTag = new ProductTag();
			productTag.setTagId(tagId);
			for (String productId : request.getParameter("ids").split(";")) {
				productTag.setProductId(Integer.parseInt(productId));
				producttagService.deleteByID(productTag);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ResultData.result(false);
		}
		return ResultData.result(true);
	}

	@PostMapping("/tag/delete")
	@ResponseBody
	public ResultData deleteTagByID(HttpServletRequest request, HttpServletResponse response) {
		try {
			for (String tagId : request.getParameter("ids").split(";")) {
				tagService.deleteByPrimaryKey(Integer.parseInt(tagId));
				producttagService.deleteByTagId(Integer.parseInt(tagId));
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ResultData.result(false);
		}
		return ResultData.result(true);
	}

	@PostMapping(value = "/tag/save")
	@ResponseBody
	public ResultData saveTag(Tag params) {
		try {
			if (tagService.selectByName(params.getName()) != null) {
				Tag tag = tagService.selectByName(params.getName());
				tag.setBizType(params.getBizType());
				tagService.updateByPrimaryKey(tag);

			} else {
				tagService.insert(params);
			}
		} catch (Exception e) {
			ResultData.result(false);
		}
		return ResultData.result(true);
	}
}
