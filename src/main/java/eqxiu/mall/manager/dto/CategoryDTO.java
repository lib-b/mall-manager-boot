package eqxiu.mall.manager.dto;

public class CategoryDTO {
	private Integer id;

	private String name;

	private String description;

	private Integer parentId;
	
	private boolean haschild;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public boolean isHaschild() {
		return haschild;
	}

	public void setHaschild(boolean haschild) {
		this.haschild = haschild;
	}

}
