package eqxiu.mall.manager.dto;

public class UserDTO {
    private String id;

    private String name;

    private String loginName;

    private String phone;

    public UserDTO() {
    }

    public UserDTO(String id, String loginName, String name) {
        super();
        this.id = id;
        this.loginName = loginName;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "id: [ " + id + "],loginName : [" + loginName + "],name: [" + name + "],phone : [" + phone + "]";
    }

}
