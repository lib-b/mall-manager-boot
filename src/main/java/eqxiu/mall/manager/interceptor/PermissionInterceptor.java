package eqxiu.mall.manager.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import eqxiu.mall.manager.utils.CookieUtils;
import eqxiu.mall.manager.utils.ManagerUserContext;
import eqxiu.mall.manager.utils.ManagerUserService;
import eqxiu.mall.manager.dto.UserDTO;

@Component
public class PermissionInterceptor implements HandlerInterceptor {
	private static final String cookieName = "JSESSIONID";

	@Value("${sessionIdCookie.domain}")
	private String sessionIdCookieDomain;

	@Autowired
	private ManagerUserService managerUserService;
	protected Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		if ("options".equalsIgnoreCase(request.getMethod())) {
			return true;
		}
		String sessionId = CookieUtils.getCookie(request, cookieName, sessionIdCookieDomain);
		if (StringUtils.isEmpty(sessionId)) {
			return false;
		}
		logger.info("session id {} login", sessionId);

		try {
			UserDTO user = managerUserService.getUserInfo(sessionId);
			if (user == null) {
				response.getWriter().print("user not login!");
				return false;
			} else {
				ManagerUserContext.setUser(user);
			}
		} catch (Exception e) {
			logger.info(e.getMessage());
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {

	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {

	}

}
