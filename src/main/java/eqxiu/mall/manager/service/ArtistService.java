package eqxiu.mall.manager.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import eqxiu.mall.product.model.Artist;

public interface ArtistService {

	/**
	 * @description //根据id删除作者
	 * @author libin
	 * @date 2016年9月20日
	 * @modified
	 * @param id
	 * @return int
	 */
	int deleteById(int id);

	/**
	 * @description //插入给定的作者
	 * @author libin
	 * @date 2016年9月20日
	 * @modified
	 * @param record
	 * @return int
	 */
	int insert(Artist record);

	/**
	 * @description //根据id查找作者
	 * @author libin
	 * @date 2016年9月20日
	 * @modified
	 * @param id
	 * @return Artist
	 */

	Artist findByID(int id);

	/**
	 * @description //更新给定作者，给方法会覆盖所有属性，该对象必须有id属性值
	 * @author libin
	 * @date 2016年9月20日
	 * @modified
	 * @param record
	 * @return int
	 */
	int update(Artist record);

	/**
	 * @description //根据配置的参数来过滤作者
	 * @author libin
	 * @date 2016年9月20日
	 * @modified
	 * @return List<Artist>
	 */

	List<Artist> findByParam(HttpServletRequest request);

	Artist findByIDAndCount(HttpServletRequest request);
}
