package eqxiu.mall.manager.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import eqxiu.mall.product.model.AsyncOrderCallback;

public interface AsyncOrderCallbackService {

	AsyncOrderCallback findById(Integer id);

	List<AsyncOrderCallback> findByParams(HttpServletRequest request);

	int deleteById(Integer id);

	int update(AsyncOrderCallback record);

}
