package eqxiu.mall.manager.service;

import java.util.List;
import java.util.Map;

import eqxiu.mall.product.model.AttributeGroupKey;

public interface AttributeGroupKeyService {

	/**
	 * @description 根据id删除属性组关键字
	 * @author libin
	 * @date 2016年9月19日
	 * @modified
	 * @param record
	 * @return int
	 */
	int deleteById(AttributeGroupKey record);

	/**
	 * @description 插入属性组关键字
	 * @author libin
	 * @date 2016年9月19日
	 * @modified by xxx 修改说明
	 * @param record
	 * @return int
	 */
	int insert(AttributeGroupKey record);

	/**
	 * @description 根据属性组id查找属性组关键字
	 * @author libin
	 * @date 2016年9月19日
	 * @modified
	 * @param Id
	 * @return Map<String, Object> 属性名称和id对
	 */
	Map<String, Object> selectByGroupId(Integer Id);

	/**
	 * @description 根据属性id查找属性组关键字（唯一）
	 * @author libin
	 * @date 2016年9月19日
	 * @modified
	 * @param Id
	 * @return AttributeGroupKey
	 */
	List<AttributeGroupKey> selectByAttrId(Integer Id);

	/**
	 * @description 根据id查找属性组关键字
	 * @author libin
	 * @date 2016年9月19日
	 * @modified
	 * @param Id
	 * @return List<AttributeGroupKey>
	 */
	List<AttributeGroupKey> findByGroupId(Integer Id);

}
