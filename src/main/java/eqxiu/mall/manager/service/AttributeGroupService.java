package eqxiu.mall.manager.service;

import java.util.List;

import eqxiu.mall.product.model.AttributeGroup;

public interface AttributeGroupService {
	/**
	 * @description 根据id删除属性组
	 * @author libin
	 * @date 2016年9月19日
	 * @modified
	 * @param Id
	 * @return int
	 */
	int deleteByID(Integer id);

	/**
	 * @description 插入给定的属性组
	 * @author libin
	 * @date 2016年9月19日
	 * @modified
	 * @param record
	 * @return int
	 */
	int insert(AttributeGroup record);

	/**
	 * @description 根据id查找属性组
	 * @author libin
	 * @date 2016年9月19日
	 * @modified
	 * @param Id
	 * @return AttributeGroup
	 */
	AttributeGroup selectByID(Integer id);

	/**
	 * @description 更新属性组所有属性
	 * @author libin
	 * @date 2016年9月19日
	 * @modified
	 * @param record
	 * @return int
	 */
	int update(AttributeGroup record);

	/**
	 * @description 根据name查找属性组
	 * @author libin
	 * @date 2016年9月19日
	 * @modified
	 * @param name
	 * @return AttributeGroup
	 */
	AttributeGroup selectByName(String name);

	/**
	 * @description 查找全部（使用时必须手动分页）
	 * @author libin
	 * @date 2016年9月19日
	 * @modified
	 * @return List<AttributeGroup>
	 */
	List<AttributeGroup> selectAll();
}
