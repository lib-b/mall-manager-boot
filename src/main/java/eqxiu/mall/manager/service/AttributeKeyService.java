package eqxiu.mall.manager.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import eqxiu.mall.manager.dto.AttributeDTO;
import eqxiu.mall.product.model.AttributeKey;

public interface AttributeKeyService {

	/**
	 * @description 根据id删除属性关键字
	 * @author libin
	 * @date 2016年9月19日
	 * @modified
	 * @param id
	 * @return int
	 */
	int deleteByID(Integer id);

	/**
	 * @description 插入属性关键字
	 * @author libin
	 * @date 2016年9月19日
	 * @modified by xxx 修改说明
	 * @param record
	 * @return int
	 */
	int insert(AttributeKey record);

	/**
	 * @description 根据id查找属性关键字
	 * @author libin
	 * @date 2016年9月19日
	 * @modified
	 * @param id
	 * @return AttributeKey
	 */
	AttributeKey selectById(Integer id);

	/**
	 * @description 根据name查找属性关键字
	 * @author libin
	 * @date 2016年9月19日
	 * @modified
	 * @param name
	 * @return AttributeKey
	 */
	AttributeKey selectByName(String name);

	/**
	 * @description 根据类型查找所有属性关键字（调用的地方需要手动分页）
	 * @author libin
	 * @date 2016年9月19日
	 * @modified
	 * @return List<AttributeKey>
	 */
	List<AttributeKey> selectByType(Byte type);

	/**
	 * @description 更新属性关键字
	 * @author libin
	 * @date 2016年9月19日
	 * @modified
	 * @param record
	 * @return int
	 */
	int update(AttributeKey record);

	/**
	 * @description 根据参数查找属性
	 * @author libin
	 * @date 2016年9月19日
	 * @modified
	 * @return List<AttributeKey>
	 */
	List<AttributeKey> selectByParam(HttpServletRequest request);

	/**
	 * @description 根据查找某个属性所在的属性组信息，多个用分号隔开
	 * @author libin
	 * @date 2016年9月19日
	 * @modified
	 * @return String
	 */
	String getGroupNamesByAttributeId(Integer id);
}
