package eqxiu.mall.manager.service;

import java.util.List;

import eqxiu.mall.product.model.BannerAccountRel;

public interface BannerAccountRelService {
	/**
	 * @description //根据id删除bannerAccount
	 * @author libin
	 * @date 2016年11月29日
	 * @modified
	 * @param id
	 * @return int
	 */
	int deleteBannderAccountById(BannerAccountRel record);

	/**
	 * @description //新增bannerAccount
	 * @author libin
	 * @date 2016年11月29日
	 * @modified
	 * @param record
	 * @return int
	 */
	int insertBannerAccount(BannerAccountRel record);

	/**
	 * @description //根据bannerId查找记录
	 * @author libin
	 * @date 2016年11月29日
	 * @modified
	 * @param bannerId
	 * @return List<BannerAccountRel>
	 */
	List<BannerAccountRel> findByBannerConfigId(Integer bannerConfigId);

	/**
	 * @description //根据bannerId删除记录
	 * @author libin
	 * @date 2017年1月11日
	 * @modified
	 * @param bannerId
	 * @return int
	 */
	int deleteByBannerConfigId(Integer bannerConfigId);
}
