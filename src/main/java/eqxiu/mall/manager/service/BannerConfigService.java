package eqxiu.mall.manager.service;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import eqxiu.mall.product.model.BannerConfig;

public interface BannerConfigService {
	/**
	 * @description //根据id删除banner
	 * @author libin
	 * @date 2016年11月29日
	 * @modified
	 * @param id
	 * @return int
	 */
	int deleteBannderConfigById(int id);

	/**
	 * @description //新增banner
	 * @author libin
	 * @date 2016年11月29日
	 * @modified
	 * @param record
	 * @return int
	 */
	int insertBannerConfig(BannerConfig record);

	/**
	 * @description //更新banner
	 * @author libin
	 * @date 2016年11月29日
	 * @modified
	 * @param record
	 * @return int
	 */
	int updateBannerConfig(BannerConfig record);

	/**
	 * @description //根据id查找banner
	 * @author libin
	 * @date 2016年11月29日
	 * @modified
	 * @param id
	 * @return Banner
	 */

	BannerConfig findBannerConfigById(int id);

	/**
	 * @description //根据banner id查找banner
	 * @author libin
	 * @date 2016年11月29日
	 * @modified
	 * @param bannerId
	 * @return List<BannerConfig>
	 */

	List<BannerConfig> findBannerConfigByBannerId(int bannerId);

	/**
	 * @description //根据banner id删除所有banner配置
	 * @author libin
	 * @date 2016年11月29日
	 * @modified
	 * @param bannerId
	 * @return int
	 */

	int deleteBannerConfigByBannerId(int bannerId);

	/**
	 * @description //根据参数查找banner
	 * @author libin
	 * @date 2016年11月29日
	 * @modified
	 * @param request
	 * @return List<BannerConfig>
	 */
	List<BannerConfig> findBannerConfigsByParam(HttpServletRequest request);

	/**
	 * @description //根据bannerConfigId查找bannerConfig使用的账号分号隔开
	 * @author libin
	 * @date 2016年11月30日
	 * @modified
	 * @param id
	 * @return String
	 */
	String findAccountsByBannerConfigId(Integer id);
}
