package eqxiu.mall.manager.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import eqxiu.mall.product.model.Banner;

public interface BannerService {
	/**
	 * @description //根据id删除banner
	 * @author libin
	 * @date 2016年11月29日
	 * @modified
	 * @param id
	 * @return int
	 */
	int deleteBannderById(int id);

	/**
	 * @description //新增banner
	 * @author libin
	 * @date 2016年11月29日
	 * @modified
	 * @param record
	 * @return int
	 */
	int insertBanner(Banner record);

	/**
	 * @description //更新banner
	 * @author libin
	 * @date 2016年11月29日
	 * @modified
	 * @param record
	 * @return int
	 */
	int updateBanner(Banner record);

	/**
	 * @description //根据id查找banner
	 * @author libin
	 * @date 2016年11月29日
	 * @modified
	 * @param id
	 * @return Banner
	 */

	Banner findBannerById(int id);

	/**
	 * @description //根据名称查找banner
	 * @author libin
	 * @date 2016年12月9日
	 * @modified
	 * @param id
	 * @return Banner
	 */

	Banner findBannerByName(String name);

	/**
	 * @description //查找所有banner
	 * @author libin
	 * @date 2016年11月29日
	 * @modified
	 * @param request
	 * @return List<Banner>
	 */
	public List<Banner> findAllBanner();

	/**
	 * @description //根据参数查找banner
	 * @author libin
	 * @date 2016年11月29日
	 * @modified
	 * @param request
	 * @return List<Banner>
	 */
	List<Banner> findBannersByParam(HttpServletRequest request);
}
