package eqxiu.mall.manager.service;

import java.util.List;

import eqxiu.mall.product.model.Brand;

/**
 * @author libin/2016.9.19
 *
 */
public interface BrandService {

	/**
	 * @description 根据id删除商标
	 * @author libin
	 * @date 2016年9月19日
	 * @modified
	 * @param id
	 * @return int
	 */
	int deleteById(int id);

	/**
	 * @description 插入给定的商标
	 * @author libin
	 * @date 2016年9月19日
	 * @modified
	 * @param record
	 * @return int
	 */
	int insert(Brand record);

	/**
	 * @description 根据id查找商标
	 * @author libin
	 * @date 2016年9月19日
	 * @modified
	 * @param id
	 * @return Brand
	 */
	Brand findByID(int id);

	/**
	 * @description 更新给定商标
	 * @author libin
	 * @date 2016年9月19日
	 * @modified
	 * @param record
	 * @return int
	 */
	int update(Brand record);

	/**
	 * @description 根据名称查找商标
	 * @author libin
	 * @date 2016年9月19日
	 * @modified
	 * @param name
	 * @return Brand
	 */
	Brand findByName(String name);

	/**
	 * @description 查找所有商标（调用的地方需要手动分页）
	 * @author libin
	 * @date 2016年9月19日
	 * @modified
	 * @return List<Brand>
	 */
	List<Brand> findAll();
}
