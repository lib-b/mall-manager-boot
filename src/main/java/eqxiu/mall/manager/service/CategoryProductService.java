package eqxiu.mall.manager.service;

import java.util.List;
import eqxiu.mall.product.model.CategoryProduct;
import eqxiu.mall.product.model.Product;

public interface CategoryProductService {
	/**
	 * @description //插入给定的记录
	 * @author libin
	 * @date 2016年9月19日
	 * @modified
	 * @param record
	 * @return int
	 */
	int insert(CategoryProduct record);

	/**
	 * @description //删除指定记录
	 * @author libin
	 * @date 2016年9月19日
	 * @modified
	 * @param record
	 * @return int
	 */
	int delete(CategoryProduct record);

	/**
	 * 
	 * @description 通过分类id精确查找
	 * @author libin
	 * @date 10 8, 2016
	 * @modified
	 * @param category
	 * @return List<CategoryProduct>
	 */
	List<CategoryProduct> findByCategoryId(Integer categoryId);

	/**
	 * 
	 * @description 通过产品id查找
	 * @author libin
	 * @date Oct 8, 2016
	 * @modified
	 * @param category
	 * @return List<CategoryProduct>
	 */
	List<CategoryProduct> findByProductId(Integer productId);

	/**
	 * 
	 * @description 通过产品id删除记录
	 * @author libin
	 * @date Oct 8, 2016
	 * @modified
	 * @param productId
	 * @return int
	 */
	int deleteByProductId(Integer productId);

	/**
	 * 
	 * @description 通过产品和分类id查找
	 * @author libin
	 * @date Oct 8, 2016
	 * @modified
	 * @param productId,categoryId
	 * @return CategoryProduct
	 */
	CategoryProduct findByProductAndCategoryId(Integer productId, Integer categoryId);

	/**
	 * @description 获取给定分类id下所有记录数量,不包括子分类的记录数
	 * @author libin
	 * @date 2016年9月19日
	 * @modified
	 * @param categoryId
	 * @return int
	 */
	int getCountByCategory(Integer categoryId);

}
