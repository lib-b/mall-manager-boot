package eqxiu.mall.manager.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import eqxiu.mall.manager.dto.CategoryDTO;
import eqxiu.mall.product.model.Category;

public interface CategoryService {

	/**
	 * 
	 * @description //新增分类
	 * @author libin
	 * @date Sep 14, 2016
	 * @modified
	 * @param record
	 * @return int
	 */
	int insert(Category record);

	/**
	 * 
	 * @description 删除指定id的记录
	 * @author libin
	 * @date Sep 14, 2016
	 * @modified
	 * @param record
	 * @return int
	 */
	int deleteByID(int id);

	/**
	 * 
	 * @description 根据id获取分类记录(不包含子)
	 * @author libin
	 * @date Sep 14, 2016
	 * @modified
	 * @param id
	 * @return Category
	 */
	public Category getCategoryByID(int id);

	/**
	 * 
	 * @description 根据名称获取分类记录(不包含子)
	 * @author libin
	 * @date Sep 14, 2016
	 * @modified
	 * @param categoryName
	 * @return Category
	 */
	public List<Category> getCategoryByName(String categoryName);

	/**
	 * 
	 * @description 根据父id获取分类记录(不包含子)
	 * @author libin
	 * @date Sep 14, 2016
	 * @modified
	 * @param categoryName
	 * @return List<CategoryDTO>
	 */
	public List<CategoryDTO> getCategoryByParentId(Integer pId);

	/**
	 * 
	 * @description 根据父id获取分类ID(不包含子)
	 * @author libin
	 * @date Sep 14, 2016
	 * @modified
	 * @param categoryName
	 * @return List<CategoryDTO>
	 */
	public List<Integer> getCategoryIdByParentId(Integer pId);

	/**
	 * 
	 * @description 根据父idg和名称获取分类记录(唯一)
	 * @author libin
	 * @date Sep 14, 2016
	 * @modified
	 * @param categoryName
	 * @return Category
	 */
	public Category getCategoryByNameAndParentId(String categoryName, Integer pId);

	/**
	 * 
	 * @description //更新分类
	 * @author libin
	 * @date Sep 14, 2016
	 * @modified
	 * @param record
	 * @return int
	 */
	int update(Category record);

	/**
	 * 
	 * @description 根据参数获取所有的分类
	 * @author libin
	 * @date Sep 14, 2016
	 * @param request
	 * @return List<Category>
	 */
	public List<Category> getCategoryByParam(HttpServletRequest request);

	/**
	 * 
	 * @description 根据父ID初始化自己和所有子的children。
	 * @author libin
	 * @date Sep 14, 2016
	 * @param request
	 * @return List<Category>
	 */
	public List<Category> iterateChild(int parentId);

	/**
	 * 
	 * @description 获取传入的分类对象的所有id，包含所有子的，传入的分类要确保children已经初始化过
	 * @author ranger
	 * @date Oct 21, 2016
	 * @modified by xxx 修改说明
	 * @param parentId
	 * @return
	 */
	public List<Integer> iterateChildIds(List<Category> cats);

	/**
	 * 
	 * @description 根据分类id判断是否子节点
	 * @author libin
	 * @date Oct 21, 2016
	 * @modified by xxx 修改说明
	 * @param categoryId
	 * @return
	 */
	public boolean hasChild(Integer categoryId);

	/**
	 * 
	 * @description 根据商品id获取所有的分类全路径，用分号隔开
	 * @author libin
	 * @date 2017.2.17
	 * @modified
	 * @param productId
	 * @return
	 */
	public String getCategoryPathByProductId(Integer productId);
}
