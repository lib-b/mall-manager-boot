package eqxiu.mall.manager.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import eqxiu.mall.product.model.HotWord;

/**
 * 搜索热门词汇
 * 
 * @author libin
 * 
 */
public interface HotWordService {
	/**
	 * 
	 * @description 先查找关键词，如果存在将对访问量+1，如果不存在,创建并返回热词的状态
	 * @author libin
	 * @date Sep 14, 2016
	 * @modified
	 * @param keyword,category
	 * @return boolean
	 */
	boolean createOrUpdateHotWord(String keyword, String category) throws Exception;

	/**
	 * 
	 * @description 根据分类查找所有热词
	 * @author libin
	 * @date Sep 14, 2016
	 * @modified
	 * @param start,category,end
	 * @return JSONArray
	 */
	JSONArray getKeywordsByCategory(String category, int start, int end) throws Exception;

	/**
	 * 
	 * @description 根据分类查找所有热词数量
	 * @author libin
	 * @date Sep 14, 2016
	 * @modified
	 * @param category
	 * @return long
	 */
	long getCountByCategory(String category) throws Exception;

	/**
	 * 
	 * @description 根据id查找热词
	 * @author libin
	 * @date Sep 14, 2016
	 * @modified
	 * @param category
	 * @return long
	 */
	HotWord getHotwordById(Integer hotWordId);

	/**
	 * 
	 * @description 根据相关参数查找热词
	 * @author libin
	 * @date Sep 14, 2016
	 * @modified
	 * @param category
	 * @return long
	 */
	List<HotWord> getHotwordByParam(HttpServletRequest request);

	/**
	 * 
	 * @description 根据ID删除热词
	 * @author libin
	 * @date Sep 14, 2016
	 * @modified
	 * @param hotwordId
	 * @return int
	 */
	int deleteHotwordById(Integer hotWordId);

	/**
	 * 
	 * @description 新增热词
	 * @author libin
	 * @date Sep 14, 2016
	 * @modified
	 * @param hotWord
	 * @return long
	 */
	int insertHotword(HotWord hotWord);

	/**
	 * 
	 * @description 更新热词
	 * @author libin
	 * @date Sep 14, 2016
	 * @modified
	 * @param hotWord
	 * @return long
	 */
	int updateHotword(HotWord hotWord);
}
