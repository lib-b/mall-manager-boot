package eqxiu.mall.manager.service;

/**
 * 
 * @description 提供搜索商品功能
 * @author liujijun
 * @date 11.15, 2016
 */
public interface IndexQueueService {

	/**
	 * 
	 * @description 索引商品
	 * @author liujijun
	 * @date 11.15, 2016
	 * @modified by xxx 修改说明
	 * @param id
	 *            option
	 */
	void indexProduct(Integer id, IndexOption option);

	public enum IndexOption {

		ADD(0), UPDATE(1), DELETE(2);

		private int option;

		IndexOption(int option) {
			this.option = option;
		}

		public int getOption() {
			return option;
		}
	}

}
