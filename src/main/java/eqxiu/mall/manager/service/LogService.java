package eqxiu.mall.manager.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import eqxiu.mall.product.model.Log;

public interface LogService {
	/**
	 * 
	 * @description 增加日志
	 * @author libin
	 * @date 2016.11.30
	 * @modified
	 * @param log
	 * @return int
	 */
	public int addLog(Log log);

	/**
	 * 
	 * @description 按照条件过滤日志
	 * @author libin
	 * @date 2016.12.6
	 * @modified
	 * @param request
	 * @return List<Log>
	 */
	public List<Log> selectLogsByParams(HttpServletRequest request);

	/**
	 * 
	 * @description 按照条件删除日志
	 * @author libin
	 * @date 2016.12.7
	 * @modified
	 * @param request
	 * @return int
	 */
	public int deleteLogsByParams(HttpServletRequest request);
}
