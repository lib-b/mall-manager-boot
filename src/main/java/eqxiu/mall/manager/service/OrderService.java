package eqxiu.mall.manager.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import eqxiu.mall.order.model.Order;
import eqxiu.mall.order.model.OrderExample;

/**
 * 
 * @description 订单相关类
 * @author ranger
 * @date Sep 20, 2016
 */
public interface OrderService {
	/**
	 * 
	 * @description //根据id查找订单
	 * @author libin
	 * @date Sep 20, 2016
	 * @modified
	 * @param id
	 * @return Order
	 */
	public Order findOrder(Integer id);

	/**
	 * 
	 * @description 保存订单
	 * @author libin
	 * @date Sep 22, 2016
	 * @modified
	 * @param order
	 * @return int
	 */
	public int insertOrder(Order order);

	/**
	 * 
	 * @description 根据订单id删除订单
	 * @author libin
	 * @date Sep 22, 2016
	 * @modified
	 * @param id
	 * @return int
	 */
	int deleteByID(Integer id);

	/**
	 * 
	 * @description 更新指定订单（覆盖所有属性）
	 * @author libin
	 * @date Sep 22, 2016
	 * @modified
	 * @param record
	 * @return int
	 */
	int updateByID(Order record);

	/**
	 * 
	 * @description //根据参数查找订单
	 * @author libin
	 * @date Sep 20, 2016
	 * @modified
	 * @param id
	 * @return Order
	 */
	public List<Order> findByParams(HttpServletRequest request);

	/**
	 * 
	 * @description //根据参数查找订单统计三个参数（订单总数，总金额，总用户数）
	 * @author libin
	 * @date Sep 20, 2016
	 * @modified
	 * @param id
	 * @return Map<String, Object>
	 */
	public Map<String, Object> getStatisticalParameterByParams(HttpServletRequest request);

	/**
	 * 
	 * @description //根据订单id查找订单统计三个参数（订单总数，总金额，总用户数）
	 * @author libin
	 * @date Sep 20, 2016
	 * @modified
	 * @param id
	 * @return Map<String, Object>
	 */
	public Map<String, Object> getStatisticalParameterById(Integer orderId);

	// 下面的是导出excel临时功能
	/**
	 * 
	 * @description //根据时间区间查找订单
	 * @author libin
	 * @date Sep 20, 2016
	 * @modified
	 * @param id
	 * @return Order
	 */
	public List<Order> findByTime(String startTime, String endTime);

	/**
	 * 
	 * @description 根据商品类型统计订单数量
	 * @author libin
	 * @date 2017.2.27
	 * @modified
	 * @param startTime,endTime
	 * @return List<Map<String, Object>>
	 */
	public List<Map<String, Object>> countByProductType(String startTime, String endTime, String orderByClause);

	/**
	 * 
	 * @description 根据作者统计订单数量
	 * @author libin
	 * @date 2017.2.27
	 * @modified
	 * @param startTime,endTime
	 * @return List<Map<String, Object>>
	 */
	public List<Map<String, Object>> countByArtist(String startTime, String endTime, String orderByClause);

}
