package eqxiu.mall.manager.service;

import java.util.List;
import java.util.Map;

import eqxiu.mall.product.model.ProductAttribute;

public interface ProductAttributeService {
	/**
	 * @description //根据id删除指定记录
	 * @author libin
	 * @date 2016年9月19日
	 * @modified
	 * @param record
	 * @return int
	 */
	int deleteByID(ProductAttribute record);

	/**
	 * @description //插入给定的记录
	 * @author libin
	 * @date 2016年9月19日
	 * @modified
	 * @param record
	 * @return int
	 */
	int insert(ProductAttribute record);

	/**
	 * @description //更新给定的记录
	 * @author libin
	 * @date 2016年9月19日
	 * @modified
	 * @param record
	 * @return int
	 */
	int update(ProductAttribute record);

	/**
	 * @description //根据产品id查找所有记录
	 * @author libin
	 * @date 2016年9月19日
	 * @modified
	 * @param productid
	 * @return List<ProductAttribute>
	 */
	List<ProductAttribute> findByProductId(Integer productid);

	/**
	 * 
	 * @description 通过产品id删除记录
	 * @author libin
	 * @date Oct 8, 2016
	 * @modified
	 * @param productId
	 * @return int
	 */
	int deleteByProductId(Integer productId);

	/**
	 * @description //根据产品id查找所有记录
	 * @author libin
	 * @date 2016年9月19日
	 * @modified
	 * @param record
	 * @return ProductAttribute
	 */
	ProductAttribute selectByKey(ProductAttribute record);

	/**
	 * 
	 * @description //根据产品id查找所有属性名称和对应值
	 * @author libin
	 * @date Sep 19, 2016
	 * @modified
	 * @param productId
	 * @return List<Map<String, Object>>
	 */
	List<Map<String, Object>> selectByProductId(Integer productId);

	/**
	 * @description //根据属性id查找所有记录
	 * @author libin
	 * @date 2016年9月19日
	 * @modified
	 * @param productid
	 * @return List<ProductAttribute>
	 */
	List<ProductAttribute> findByAttrId(Integer attrId);
}
