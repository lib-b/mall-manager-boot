package eqxiu.mall.manager.service;

import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import eqxiu.mall.commons.page.PageDTO;
import eqxiu.mall.product.model.Product;

public interface ProductService {

	/**
	 * @description //根据商品id获取商品对象
	 * @author libin
	 * @date 2016年9月20日
	 * @modified
	 * @param productId
	 * @return Product
	 */
	Product getProduct(int productId);

	/**
	 * @description //根据属性组id获取商品对象
	 * @author libin
	 * @date 2016年9月20日
	 * @modified
	 * @param groupId
	 * @return List<Product>
	 */
	List<Product> getProductByGroupId(int groupId);

	/**
	 * @description //根据商品id删除指定商品对象
	 * @author libin
	 * @date 2016年9月20日
	 * @modified
	 * @param productId
	 * @return int
	 */
	int deleteProduct(int productId);

	/**
	 * @description //保存指定的商品
	 * @author libin
	 * @date 2016年9月20日
	 * @modified
	 * @param product
	 * @return int
	 */
	int insertProduct(Product product);

	/**
	 * @description //更新指定的商品
	 * @author libin
	 * @date 2016年9月20日
	 * @modified
	 * @param product
	 * @return int
	 */
	int updateProduct(Product product);

	/**
	 * @description //根据商品对象中的值来选择满足条件的商品记录
	 * @author libin
	 * @date 2016年9月20日
	 * @modified
	 * @param product
	 * @return List<ProductDTO>
	 */

	List<Product> selectByParams(HttpServletRequest request);

	/**
	 * @description //根据code选择商品
	 * @author libin
	 * @date 2016年9月20日
	 * @modified
	 * @param code
	 * @return Product
	 */
	Product getProductByCode(String code);

	/**
	 * @description //根据title模糊选择商品
	 * @author libin
	 * @date 2016年9月20日
	 * @modified
	 * @param code
	 * @return Product
	 */
	List<Product> getProductByTitle(String title);

	/**
	 * @description //根据商品id查找商品的属性
	 * @author libin
	 * @date 2016年9月20日
	 * @modified
	 * @param code
	 * @return Product
	 */
	List<Map<String, Object>> getAttrsByGroupId(Integer groupId);

	/**
	 * 
	 * @description 通过分类id精确查找所包含的所有商品
	 * @author libin
	 * @date 10 8, 2016
	 * @modified
	 * @param category
	 * @return PageDTO<Map<String, Object>>
	 */
	PageDTO<Map<String, Object>> findProductsByCategoryId(Integer categoryId, int start, int pageSize);

	/**
	 * 
	 * @description 通过分类id精确查找所包含的所有商品
	 * @author libin
	 * @date 10 8, 2016
	 * @modified
	 * @param tagId
	 * @return List<Map<String, Object>>
	 */
	PageDTO<Map<String, Object>> findProductsByTagId(Integer tagId, int start, int pageSize);
	/**
	 * 
	 * @description 根据商标统计商品
	 * @author libin
	 * @date 2017.2.23
	 * @modified
	 * @param startTime,endTime
	 * @return List<Map<String, Object>>
	 */
	List<Map<String, Object>> countProductsByBrand(String startTime,String endTime);
	/**
	 * 
	 * @description 根据商品类型统计商品
	 * @author libin
	 * @date 2017.2.23
	 * @modified
	 * @param startTime,endTime
	 * @return List<Map<String, Object>>
	 */
	List<Map<String, Object>> countProductsByGroupId(String startTime,String endTime);
	
	/**
	 * 
	 * @description 查询未设置分类的产品
	 * @author libin
	 * @date 2017.3.7
	 * @modified
	 */
	List<Product> selectNonCategory();
}
