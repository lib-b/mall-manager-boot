package eqxiu.mall.manager.service;

import java.util.List;

import eqxiu.mall.commons.page.PageDTO;
import eqxiu.mall.product.dao.TagDao;
import eqxiu.mall.product.model.ProductTag;

public interface ProductTagService {
	/**
	 * @description //删除记录
	 * @author libin
	 * @date 2016年9月19日
	 * @modified
	 * @param id
	 * @return int
	 */
	int deleteByID(ProductTag record);

	/**
	 * @description //插入给定产品标签记录
	 * @author libin
	 * @date 2016年9月19日
	 * @modified
	 * @param record
	 * @return int
	 */
	int insert(ProductTag record);

	/**
	 * @description //根据商品id查找Tag列表
	 * @author libin
	 * @date 2016年9月20日
	 * @modified
	 * @param productId
	 * @return List<ProductTag>
	 */
	List<ProductTag> findByProductId(int productId);

	/**
	 * 
	 * @description 通过产品id删除记录
	 * @author libin
	 * @date Oct 8, 2016
	 * @modified
	 * @param productId
	 * @return int
	 */
	int deleteByProductId(Integer productId);

	/**
	 * 
	 * @description 通过标签id查找（后台使用）
	 * @author libin
	 * @date Oct 8, 2016
	 * @modified
	 * @param tagid
	 * @return List<ProductTag>
	 */
	List<ProductTag> findByTagId(int tagid);

	/**
	 * 
	 * @description 通过标签id查找（后台使用）
	 * @author libin
	 * @date Oct 8, 2016
	 * @modified
	 * @param productid
	 * @return List<ProductTag>
	 */
	List<ProductTag> selectByProductId(int productid);

	/**
	 * 
	 * @description 通过商品和标签id查找（后台使用）
	 * @author libin
	 * @date Oct 8, 2016
	 * @modified
	 * @param tagid,productid
	 * @return ProductTag
	 */
	ProductTag selectByProductAndTagId(int tagid, int productid);

	/**
	 * 
	 * @description 通过商品和标签id删除（后台使用）
	 * @author libin
	 * @date Oct 8, 2016
	 * @modified by xxx 修改说明
	 * @param tagid,productid
	 * @return int
	 */
	int deleteByProductAndTagId(int tagid, int productid);

	/**
	 * 
	 * @description 查找指定tag下所有商品
	 * @author libin
	 * @date 11 8, 2016
	 * @modified
	 * @param tagid
	 * @return int
	 */
	int getCountByTagId(Integer tagid);

	/**
	 * 
	 * @description 通过标签id删除（后台使用）
	 * @author libin
	 * @date 2017.1.9
	 * @modified
	 * @param tagid
	 * @return int
	 */
	int deleteByTagId(int tagid);

}
