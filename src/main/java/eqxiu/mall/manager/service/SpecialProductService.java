package eqxiu.mall.manager.service;

import java.util.Map;
import eqxiu.mall.commons.page.PageDTO;
import eqxiu.mall.product.model.SpecialProduct;

public interface SpecialProductService {
	/**
	 * @description //删除记录
	 * @author libin
	 * @date 2016年9月19日
	 * @modified
	 * @param id
	 * @return int
	 */
	int deleteByID(SpecialProduct record);

	/**
	 * @description //根据specialId删除记录
	 * @author libin
	 * @date 2016年9月19日
	 * @modified
	 * @param specialId
	 * @return int
	 */
	int deleteBySpecialID(Integer specialId);

	/**
	 * @description //插入给定产品标签记录
	 * @author libin
	 * @date 2016年9月19日
	 * @modified
	 * @param record
	 * @return int
	 */
	int insert(SpecialProduct record);

	/**
	 * @description //查询给定主题产品记录
	 * @author libin
	 * @date 2016年9月19日
	 * @modified
	 * @param Integer
	 *            productId, Integer specialId
	 * @return int
	 */
	SpecialProduct selectByKey(Integer productId, Integer specialId);

	/**
	 * 
	 * @description 通过 专题id查找（后台使用）
	 * @author libin
	 * @date Oct 8, 2016
	 * @modified
	 * @param specialId, pageNumber, pageSize
	 * @return PageDTO<SpecialProduct>
	 */
	PageDTO<Map<String, Object>> findBySpecialId(int specialId, int pageNumber, int pageSize);

}
