package eqxiu.mall.manager.service;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import eqxiu.mall.product.model.Special;

public interface SpecialService {
	/**
	 * @description //根据id删除专题
	 * @author libin
	 * @date 2016年12月28日
	 * @modified
	 * @param id
	 * @return int
	 */
	int deleteById(int id);

	/**
	 * @description //新增专题
	 * @author libin
	 * @date 2016年12月28日
	 * @modified
	 * @param record
	 * @return int
	 */
	int insert(Special record);

	/**
	 * @description //更新专题
	 * @author libin
	 * @date 2016年11月29日
	 * @modified
	 * @param record
	 * @return int
	 */
	int update(Special record);

	/**
	 * @description //根据id查找专题
	 * @author libin
	 * @date 2016年12月28日
	 * @modified
	 * @param id
	 * @return Special
	 */

	Special findById(int id);

	/**
	 * @description //根据标识查找
	 * @author libin
	 * @date 2016年12月28日
	 * @modified
	 * @param id
	 * @return Special
	 */

	Special findByCode(String code);

	/**
	 * @description //查找所有专题
	 * @author libin
	 * @date 2016年12月28日
	 * @modified
	 * @param request
	 * @return List<Special>
	 */
	public List<Special> findAllSpecials();

	/**
	 * @description //根据参数查找banner
	 * @author libin
	 * @date 2016年11月29日
	 * @modified
	 * @param request
	 * @return List<Special>
	 */
	List<Special> findSpecialsByParam(HttpServletRequest request);
}
