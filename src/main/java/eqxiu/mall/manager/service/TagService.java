package eqxiu.mall.manager.service;

import java.util.List;

import eqxiu.mall.product.model.Tag;

public interface TagService {
	/**
	 * @description //根据ID删除记录
	 * @author libin
	 * @date 2016年9月20日
	 * @modified
	 * @param id
	 * @return int
	 */
	int deleteByPrimaryKey(Integer id);

	/**
	 * @description //插入Tag记录
	 * @author libin
	 * @date 2016年9月20日
	 * @modified
	 * @param record
	 * @return int
	 */
	int insert(Tag record);

	/**
	 * @description //根据主键查找TAG记录
	 * @author libin
	 * @date 2016年9月20日
	 * @modified
	 * @param id
	 * @return Tag
	 */
	Tag selectByPrimaryKey(Integer id);

	/**
	 * @description //根据名称查找TAG记录
	 * @author libin
	 * @date 2016年9月20日
	 * @modified
	 * @param id
	 * @return Tag
	 */
	Tag selectByName(String name);

	/**
	 * @description //查询所有tags
	 * @author libin
	 * @date 2016年9月20日
	 * @modified by xxx 修改说明
	 * @param id
	 * @return
	 */
	List<Tag> selectAllTags();

	/**
	 * @description //更新TAG记录
	 * @author libin
	 * @date 2016年9月20日
	 * @modified
	 * @param record
	 * @return int
	 */
	int updateByPrimaryKey(Tag record);

}
