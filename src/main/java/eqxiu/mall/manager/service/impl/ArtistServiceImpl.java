package eqxiu.mall.manager.service.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import eqxiu.mall.manager.service.ArtistService;
import eqxiu.mall.product.dao.ArtistDao;
import eqxiu.mall.product.model.Artist;
import eqxiu.mall.product.model.ArtistExample;
import eqxiu.mall.product.model.ArtistExample.Criteria;

@Service
@Transactional
public class ArtistServiceImpl implements ArtistService {
	@Autowired
	private ArtistDao artistDao;

	@Override
	public int deleteById(int id) {
		return artistDao.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(Artist record) {
		return artistDao.insert(record);
	}

	@Override
	public Artist findByID(int id) {
		return artistDao.selectByPrimaryKey(id);
	}
	@Override
	public int update(Artist record) {
		return artistDao.updateByPrimaryKeySelective(record);
	}

	@Override
	public List<Artist> findByParam(HttpServletRequest request) {
		ArtistExample artistExample = new ArtistExample();
		Criteria criteria = artistExample.createCriteria();
		if (!request.getParameter("artistName").trim().equals("")) {
			criteria.andArtistNameEqualTo(request.getParameter("artistName").trim());
		}
		if (!request.getParameter("phone").trim().equals("")) {
			criteria.andPhoneEqualTo(request.getParameter("phone").trim());
		}
		if (Byte.parseByte(request.getParameter("artistType")) != 0) {
			criteria.andTypeEqualTo(Byte.parseByte(request.getParameter("artistType")));
		}
		if (Byte.parseByte(request.getParameter("artistStatus")) != -1) {
			criteria.andStatusEqualTo(Byte.parseByte(request.getParameter("artistStatus")));
		}
		String startTime = request.getParameter("createStartTime");
		String endTime = request.getParameter("createEndTime");

		String sortName = request.getParameter("sortName");
		String sortOrder = request.getParameter("sortOrder");

		return artistDao.selectExamCountByExample(artistExample,startTime,endTime,sortName,sortOrder);
	}

	@Override
	public Artist findByIDAndCount(HttpServletRequest request) {
		Integer id = Integer.parseInt(request.getParameter("id").trim());
		String startTime = request.getParameter("createStartTime");
		String endTime = request.getParameter("createEndTime");
		ArtistExample artistExample = new ArtistExample();
		artistExample.createCriteria().andIdEqualTo(id);
		String sortName = request.getParameter("sortName");
		String sortOrder = request.getParameter("sortOrder");
		List<Artist> list = artistDao.selectExamCountByExample(artistExample,startTime,endTime,sortName,sortOrder);
		if(list != null && list.size() >0){
			return list.get(0);
		}else{
			return null;
		}
	}

}
