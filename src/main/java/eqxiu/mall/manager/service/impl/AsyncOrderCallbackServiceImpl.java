package eqxiu.mall.manager.service.impl;

import eqxiu.mall.product.dao.AsyncOrderCallbackDao;
import eqxiu.mall.product.model.AsyncOrderCallback;
import eqxiu.mall.product.model.AsyncOrderCallbackExample;
import eqxiu.mall.product.model.AsyncOrderCallbackExample.Criteria;
import eqxiu.mall.manager.service.AsyncOrderCallbackService;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AsyncOrderCallbackServiceImpl implements AsyncOrderCallbackService {

	@Autowired
	private AsyncOrderCallbackDao asyncOrderCallbackDao;

	@Override
	public AsyncOrderCallback findById(Integer id) {
		return asyncOrderCallbackDao.selectByPrimaryKey(id);
	}

	@Override
	public List<AsyncOrderCallback> findByParams(HttpServletRequest request) {
		AsyncOrderCallbackExample asyncOrderCallbackExample = new AsyncOrderCallbackExample();
		Criteria criteria = asyncOrderCallbackExample.createCriteria();
		if (!request.getParameter("status").equals("") && Byte.parseByte(request.getParameter("status")) != -1) {
			criteria.andStatusEqualTo(Byte.parseByte(request.getParameter("status")));
		}
		if (!request.getParameter("type").equals("") && Byte.parseByte(request.getParameter("type")) != 0) {
			criteria.andTypeEqualTo(Byte.parseByte(request.getParameter("type")));
		}
		if (!request.getParameter("orderId").trim().equals("")) {
			criteria.andOrderIdEqualTo(Integer.parseInt(request.getParameter("orderId").trim()));
		}
		if (!request.getParameter("productId").trim().equals("")) {
			criteria.andProductIdEqualTo(Integer.parseInt(request.getParameter("productId").trim()));
		}
		if (!request.getParameter("createUser").trim().equals("")) {
			criteria.andCreateUserEqualTo(request.getParameter("createUser").trim());
		}
		if (!request.getParameter("errorMsg").equals("全部")) {
			criteria.andErrorMsgEqualTo(request.getParameter("errorMsg").trim());
		}
		if (Integer.parseInt(request.getParameter("tee")) != 0) {
			if (Integer.parseInt(request.getParameter("tee")) == 1) {
				criteria.andOrderIdIsNotNull();
			} else {
				criteria.andOrderIdIsNull();
			}
		}
		if (!request.getParameter("createStartTime").equals("") && !request.getParameter("createEndTime").equals("")) {
			criteria.andCreateTimeBetween(request.getParameter("createStartTime"),
					request.getParameter("createEndTime"));
		} else if (!request.getParameter("createStartTime").equals("")) {
			criteria.andCreateTimeGreaterThanOrEqualTo(request.getParameter("createStartTime"));
		} else if (!request.getParameter("createEndTime").equals("")) {
			criteria.andCreateTimeLessThanOrEqualTo(request.getParameter("createEndTime"));
		}
		return asyncOrderCallbackDao.selectByExample(asyncOrderCallbackExample);
	}

	@Override
	public int deleteById(Integer id) {
		return asyncOrderCallbackDao.deleteByPrimaryKey(id);
	}

	@Override
	public int update(AsyncOrderCallback record) {
		return asyncOrderCallbackDao.updateByPrimaryKeySelective(record);
	}
}
