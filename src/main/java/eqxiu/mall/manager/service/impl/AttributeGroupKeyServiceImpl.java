package eqxiu.mall.manager.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import eqxiu.mall.manager.service.AttributeGroupKeyService;
import eqxiu.mall.product.dao.AttributeGroupKeyDao;
import eqxiu.mall.product.model.AttributeGroupKey;

@Service
@Transactional
public class AttributeGroupKeyServiceImpl implements AttributeGroupKeyService {

	@Autowired
	private AttributeGroupKeyDao attributeGroupKeyDao;

	@Override
	public int deleteById(AttributeGroupKey record) {
		return attributeGroupKeyDao.deleteByPrimaryKey(record);
	}

	@Override
	public int insert(AttributeGroupKey record) {
		return attributeGroupKeyDao.insert(record);
	}

	@Override
	public Map<String, Object> selectByGroupId(Integer Id) {
		List<Map<String, Object>> kvMap = attributeGroupKeyDao.selectByGroupId(Id);
		Map<String, Object> nameValuePairs = new HashMap<String, Object>();
		for (Map<String, Object> kv : kvMap) {
			String name = (String) kv.get("name");
			if (!name.startsWith("real") && !name.startsWith("yuan")) {
				Object id = kv.get("attr_key_id");
				nameValuePairs.put(name, id);
			}
		}
		return nameValuePairs;
	}

	@Override
	public List<AttributeGroupKey> selectByAttrId(Integer Id) {
		return attributeGroupKeyDao.selectByAttributeId(Id);
	}

	@Override
	public List<AttributeGroupKey> findByGroupId(Integer Id) {
		return attributeGroupKeyDao.findByGroupId(Id);
	}

}
