package eqxiu.mall.manager.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import eqxiu.mall.manager.service.AttributeGroupService;
import eqxiu.mall.product.dao.AttributeGroupDao;
import eqxiu.mall.product.model.AttributeGroup;

@Service
@Transactional
public class AttributeGroupServiceImpl implements AttributeGroupService {
	@Autowired
	private AttributeGroupDao attributeGroupDao;

	@Override
	public int deleteByID(Integer id) {
		return attributeGroupDao.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(AttributeGroup record) {
		return attributeGroupDao.insert(record);
	}

	@Override
	public AttributeGroup selectByID(Integer id) {
		return attributeGroupDao.selectByPrimaryKey(id);
	}

	@Override
	public int update(AttributeGroup record) {
		return attributeGroupDao.updateByPrimaryKeySelective(record);
	}

	@Override
	public AttributeGroup selectByName(String name) {
		return attributeGroupDao.selectByName(name);
	}

	@Override
	public List<AttributeGroup> selectAll() {
		return attributeGroupDao.selectAll();
	}

}
