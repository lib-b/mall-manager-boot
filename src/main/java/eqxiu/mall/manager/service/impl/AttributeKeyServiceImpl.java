package eqxiu.mall.manager.service.impl;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import aj.org.objectweb.asm.Attribute;
import eqxiu.mall.manager.dto.AttributeDTO;
import eqxiu.mall.manager.service.AttributeKeyService;
import eqxiu.mall.product.dao.AttributeGroupDao;
import eqxiu.mall.product.dao.AttributeKeyDao;
import eqxiu.mall.product.model.AttributeGroup;
import eqxiu.mall.product.model.AttributeKey;
import eqxiu.mall.product.model.AttributeKeyExample;
import eqxiu.mall.product.model.AttributeKeyGroupExample.Criteria;
import eqxiu.mall.product.model.AttributeKeyGroupExample;

@Service
@Transactional
public class AttributeKeyServiceImpl implements AttributeKeyService {

	@Autowired
	private AttributeKeyDao attributeKeyDao;

	@Override
	public int deleteByID(Integer id) {
		return attributeKeyDao.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(AttributeKey record) {
		return attributeKeyDao.insert(record);
	}

	@Override
	public AttributeKey selectById(Integer id) {
		return attributeKeyDao.selectByPrimaryKey(id);
	}

	@Override
	public int update(AttributeKey record) {
		return attributeKeyDao.updateByPrimaryKeySelective(record);
	}

	@Override
	public AttributeKey selectByName(String name) {
		return attributeKeyDao.selectByName(name);
	}

	@Override
	public List<AttributeKey> selectByType(Byte type) {
		return attributeKeyDao.selectByType(type);
	}

	@Override
	public List<AttributeKey> selectByParam(HttpServletRequest request) {
		AttributeKeyGroupExample attributeKeyGroupExample = new AttributeKeyGroupExample();
		Criteria criteria = attributeKeyGroupExample.createCriteria();
		if (!request.getParameter("name").trim().equals("")) {
			criteria.andAttributeNameLike("%" + request.getParameter("name").trim() + "%");
		}
		if (!request.getParameter("type").trim().equals("")
				&& Byte.parseByte(request.getParameter("type").trim()) != 0) {
			criteria.andAttributeTypeEqualTo(Byte.parseByte(request.getParameter("type").trim()));
		}
		if (!request.getParameter("attrGroupId").trim().equals("")
				&& Integer.parseInt(request.getParameter("attrGroupId").trim()) != 0) {
			criteria.andGroupIdEqualTo(Integer.parseInt(request.getParameter("attrGroupId").trim()));
		}
		return attributeKeyDao.selectByExampleJoinGroup(attributeKeyGroupExample);
	}

	@Override
	public String getGroupNamesByAttributeId(Integer id) {
		String groupNames = "";
		AttributeKeyGroupExample attributeKeyGroupExample = new AttributeKeyGroupExample();
		Criteria criteria = attributeKeyGroupExample.createCriteria();
		criteria.andAttributeIdEqualTo(id);
		List<AttributeGroup> attributeGroups = attributeKeyDao.selectGroupsByAttributeId(id);
		for (AttributeGroup attributeKey : attributeGroups) {
			if (groupNames.equals("")) {
				groupNames = attributeKey.getName();
			} else {
				groupNames = groupNames + ";" + attributeKey.getName();
			}
		}
		return groupNames;
	}
}
