package eqxiu.mall.manager.service.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import eqxiu.mall.manager.service.BannerAccountRelService;
import eqxiu.mall.product.dao.BannerAccountRelDao;
import eqxiu.mall.product.model.BannerAccountRel;
import eqxiu.mall.product.model.BannerAccountRelExample;

@Service
@Transactional
public class BannerAccountServiceImpl implements BannerAccountRelService {
	@Autowired
	private BannerAccountRelDao bannerAccountRelDao;

	@Override
	public int deleteBannderAccountById(BannerAccountRel record) {
		return bannerAccountRelDao.deleteByPrimaryKey(record);
	}

	@Override
	public int insertBannerAccount(BannerAccountRel record) {
		return bannerAccountRelDao.insert(record);
	}

	@Override
	public List<BannerAccountRel> findByBannerConfigId(Integer bannerConfigId) {
		BannerAccountRelExample bannerAccountRelExample = new BannerAccountRelExample();
		bannerAccountRelExample.createCriteria().andBannerConfigIdEqualTo(bannerConfigId);
		return bannerAccountRelDao.selectByExample(bannerAccountRelExample);
	}

	@Override
	public int deleteByBannerConfigId(Integer bannerConfigId) {
		BannerAccountRelExample bannerAccountRelExample = new BannerAccountRelExample();
		bannerAccountRelExample.createCriteria().andBannerConfigIdEqualTo(bannerConfigId);
		return bannerAccountRelDao.deleteByExample(bannerAccountRelExample);
	}
}
