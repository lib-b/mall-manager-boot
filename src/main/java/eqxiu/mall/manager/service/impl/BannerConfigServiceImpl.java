package eqxiu.mall.manager.service.impl;

import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import eqxiu.mall.manager.service.BannerConfigService;
import eqxiu.mall.product.dao.BannerAccountRelDao;
import eqxiu.mall.product.dao.BannerConfigDao;
import eqxiu.mall.product.model.BannerAccountRelExample;
import eqxiu.mall.product.model.BannerAccountRelJoinExample;
import eqxiu.mall.product.model.BannerConfig;
import eqxiu.mall.product.model.BannerConfigExample;
import eqxiu.mall.product.model.BannerAccountRelJoinExample.Criteria;

@Service
@Transactional
public class BannerConfigServiceImpl implements BannerConfigService {
	@Autowired
	private BannerConfigDao bannerConfigDao;
	@Autowired
	private BannerAccountRelDao bannerAccountRelDao;

	@Override
	public int deleteBannderConfigById(int id) {
		return bannerConfigDao.deleteByPrimaryKey(id);
	}

	@Override
	public int insertBannerConfig(BannerConfig record) {
		return bannerConfigDao.insert(record);
	}

	@Override
	public int updateBannerConfig(BannerConfig record) {
		return bannerConfigDao.updateByPrimaryKeySelective(record);
	}

	@Override
	public BannerConfig findBannerConfigById(int id) {
		return bannerConfigDao.selectByPrimaryKey(id);
	}

	@Override
	public List<BannerConfig> findBannerConfigsByParam(HttpServletRequest request) {
		BannerAccountRelJoinExample bannerAccountRelJoinExample = new BannerAccountRelJoinExample();
		Criteria criteria = bannerAccountRelJoinExample.createCriteria();
		if (!request.getParameter("bannerId").trim().equals("")
				&& Integer.parseInt(request.getParameter("bannerId")) != 0) {
			criteria.andBannerIdEqualTo(Integer.parseInt(request.getParameter("bannerId")));
		}
		if (!request.getParameter("bannerConfigName").trim().equals("")) {
			criteria.andBannerConfigNameEqualTo(request.getParameter("bannerConfigName").trim());
		}
		if (!request.getParameter("accounts").equals("") && Integer.parseInt(request.getParameter("accounts")) != 0) {
			criteria.andAccountGroupIdEqualTo(Integer.parseInt(request.getParameter("accounts")));
		}
		if (!request.getParameter("status").equals("") && Byte.parseByte(request.getParameter("status")) != 0) {
			criteria.andBannerConfigStatusEqualTo(Byte.parseByte(request.getParameter("status")));
		}
		if (!request.getParameter("startTime").equals("") && !request.getParameter("endTime").equals("")) {
			criteria.andBannerConfigStartTimeGreaterThanOrEqualTo(request.getParameter("startTime"))
					.andBannerConfigEndTimeLessThanOrEqualTo(request.getParameter("endTime"));
		} else if (!request.getParameter("endTime").equals("")) {
			criteria.andBannerConfigEndTimeLessThanOrEqualTo(request.getParameter("endTime"));
		} else if (!request.getParameter("startTime").equals("")) {
			criteria.andBannerConfigStartTimeGreaterThanOrEqualTo(request.getParameter("startTime"));
		}
		return bannerConfigDao.selectByExampleJoin(bannerAccountRelJoinExample);
	}

	@Override
	public String findAccountsByBannerConfigId(Integer id) {
		String result = "";
		List<Map<String, Object>> maps = bannerAccountRelDao.selectAccountsByBannerConfigId(id);
		for (Map<String, Object> map : maps) {
			result = result + map.get("id") + ";";
		}
		return result;
	}

	@Override
	public List<BannerConfig> findBannerConfigByBannerId(int bannerId) {
		BannerConfigExample bannerConfigExample = new BannerConfigExample();
		bannerConfigExample.createCriteria().andBannerIdEqualTo(bannerId);
		return bannerConfigDao.selectByExample(bannerConfigExample);
	}

	@Override
	public int deleteBannerConfigByBannerId(int bannerId) {
		// 根据bannerId获取所有的bannerConfig
		List<BannerConfig> bannerConfigs = findBannerConfigByBannerId(bannerId);
		// 删除bannerConfig对应account表中的记录
		for (BannerConfig bannerConfig : bannerConfigs) {
			BannerAccountRelExample bannerAccountRelExample = new BannerAccountRelExample();
			bannerAccountRelExample.createCriteria().andBannerConfigIdEqualTo(bannerConfig.getId());
			bannerAccountRelDao.deleteByExample(bannerAccountRelExample);
		}
		// 删除所有bannerConfig
		BannerConfigExample bannerConfigExample = new BannerConfigExample();
		bannerConfigExample.createCriteria().andBannerIdEqualTo(bannerId);
		return bannerConfigDao.deleteByExample(bannerConfigExample);
	}

}
