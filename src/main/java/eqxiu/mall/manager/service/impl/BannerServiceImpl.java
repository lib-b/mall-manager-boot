package eqxiu.mall.manager.service.impl;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import eqxiu.mall.manager.service.BannerService;
import eqxiu.mall.product.dao.BannerAccountRelDao;
import eqxiu.mall.product.dao.BannerDao;
import eqxiu.mall.product.model.Banner;
import eqxiu.mall.product.model.BannerExample;
import eqxiu.mall.product.model.BannerExample.Criteria;

@Service
@Transactional
public class BannerServiceImpl implements BannerService {

	@Autowired
	private BannerDao bannerDao;
	@Autowired
	private BannerAccountRelDao bannerAccountRelDao;

	@Override
	public int deleteBannderById(int id) {
		return bannerDao.deleteByPrimaryKey(id);
	}

	@Override
	public int insertBanner(Banner record) {
		return bannerDao.insert(record);
	}

	@Override
	public int updateBanner(Banner record) {
		return bannerDao.updateByPrimaryKeySelective(record);
	}

	@Override
	public Banner findBannerById(int id) {
		return bannerDao.selectByPrimaryKey(id);
	}

	@Override
	public List<Banner> findAllBanner() {
		BannerExample bannerExample = new BannerExample();
		return bannerDao.selectByExample(bannerExample);
	}

	@Override
	public List<Banner> findBannersByParam(HttpServletRequest request) {
		BannerExample bannerExample = new BannerExample();
		Criteria criteria = bannerExample.createCriteria();
		if (!request.getParameter("name").trim().equals("")) {
			criteria.andNameEqualTo(request.getParameter("name").trim());
		}
		if (!request.getParameter("code").trim().equals("")) {
			criteria.andCodeEqualTo(request.getParameter("code").trim());
		}
		if (!request.getParameter("status").equals("") && Byte.parseByte(request.getParameter("status")) != 0) {
			criteria.andStatusEqualTo(Byte.parseByte(request.getParameter("status")));
		}
		return bannerDao.selectByExample(bannerExample);
	}

	@Override
	public Banner findBannerByName(String name) {
		return bannerDao.selectByName(name);
	}
}
