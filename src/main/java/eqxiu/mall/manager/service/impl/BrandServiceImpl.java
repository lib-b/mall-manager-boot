package eqxiu.mall.manager.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import eqxiu.mall.manager.service.BrandService;
import eqxiu.mall.product.dao.BrandDao;
import eqxiu.mall.product.model.Brand;

@Service
@Transactional
public class BrandServiceImpl implements BrandService {

	@Autowired
	private BrandDao brandDao;

	@Override
	public int deleteById(int id) {
		return brandDao.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(Brand record) {
		return brandDao.insert(record);
	}

	@Override
	public Brand findByID(int id) {
		return brandDao.selectByPrimaryKey(id);
	}

	@Override
	public int update(Brand record) {
		return brandDao.updateByPrimaryKeySelective(record);
	}

	@Override
	public Brand findByName(String name) {
		return brandDao.selectByName(name);
	}

	@Override
	public List<Brand> findAll() {
		return brandDao.selectAll();
	}

}
