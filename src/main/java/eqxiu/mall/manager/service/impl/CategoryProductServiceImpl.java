package eqxiu.mall.manager.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import eqxiu.mall.product.dao.CategoryProductDao;
import eqxiu.mall.product.model.CategoryProduct;
import eqxiu.mall.manager.service.*;

@Service
@Transactional
public class CategoryProductServiceImpl implements CategoryProductService {

	@Autowired
	private CategoryProductDao categoryProductDao;

	@Override
	public int insert(CategoryProduct record) {
		return categoryProductDao.insert(record);
	}

	@Override
	public List<CategoryProduct> findByCategoryId(Integer categoryId) {
		return categoryProductDao.findByCategoryId(categoryId);
	}

	@Override
	public List<CategoryProduct> findByProductId(Integer productId) {
		return categoryProductDao.findByProductId(productId);
	}

	@Override
	public int delete(CategoryProduct record) {
		return categoryProductDao.delete(record);
	}

	@Override
	public CategoryProduct findByProductAndCategoryId(Integer productId, Integer categoryId) {
		return categoryProductDao.findByCategoryAndProductId(categoryId, productId);
	}

	@Override
	public int getCountByCategory(Integer categoryId) {
		return categoryProductDao.countByCategoryId(categoryId);
	}

	@Override
	public int deleteByProductId(Integer productId) {
		return categoryProductDao.deleteByProductId(productId);
	}

}
