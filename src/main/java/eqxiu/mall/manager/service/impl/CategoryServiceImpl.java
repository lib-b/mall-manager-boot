package eqxiu.mall.manager.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import eqxiu.mall.product.dao.CategoryDao;
import eqxiu.mall.product.dao.CategoryProductDao;
import eqxiu.mall.product.model.Category;
import eqxiu.mall.product.model.CategoryExample;
import eqxiu.mall.product.model.CategoryProduct;
import eqxiu.mall.product.model.CategoryExample.Criteria;
import eqxiu.mall.manager.dto.CategoryDTO;
import eqxiu.mall.manager.service.*;

@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	private CategoryDao cateDao;
	@Autowired
	private CategoryProductDao categoryProductDao;

	@Override
	public int insert(Category record) {
		return cateDao.insert(record);
	}

	@Override
	public Category getCategoryByID(int id) {
		return cateDao.selectByPrimaryKey(id);
	}

	@Override
	public int deleteByID(int id) {
		return cateDao.deleteByPrimaryKey(id);
	}

	@Override
	public int update(Category record) {
		return cateDao.updateByPrimaryKeySelective(record);
	}

	@Override
	public List<Category> getCategoryByParam(HttpServletRequest request) {
		CategoryExample categoryExample = new CategoryExample();
		categoryExample.setOrderByClause("sort " + request.getParameter("orderByClause").trim());
		Criteria criteria = categoryExample.createCriteria();
		if (!request.getParameter("name").trim().equals("")) {
			criteria.andNameLike("%" + request.getParameter("name") + "%");
		}
		if (!request.getParameter("parentId").trim().equals("")) {
			criteria.andParentIdEqualTo(Integer.parseInt(request.getParameter("parentId").trim()));
		}
		if (!request.getParameter("status").trim().equals("") && Byte.parseByte(request.getParameter("status")) != 0) {
			criteria.andStatusEqualTo(Byte.parseByte(request.getParameter("status")));
		}
		return cateDao.selectByExample(categoryExample);
	}

	@Override
	public List<Category> getCategoryByName(String categoryName) {
		CategoryExample categoryExample = new CategoryExample();
		categoryExample.createCriteria().andNameEqualTo(categoryName);
		return cateDao.selectByExample(categoryExample);
	}

	public List<Category> iterateChild(int parentId) {
		CategoryExample example = new CategoryExample();
		// 如果是0，代表顶级节点
		example.createCriteria().andParentIdEqualTo(parentId).andStatusEqualTo((byte) 1);
		example.setOrderByClause("sort");
		List<Category> cats = cateDao.selectByExample(example);
		for (Category cat : cats) {
			List<Category> childs = iterateChild(cat.getId());
			if (childs != null) {
				cat.getChilds().addAll(childs);
			}
		}
		return cats;
	}

	public List<Integer> iterateChildIds(List<Category> cats) {
		List<Integer> catIds = new ArrayList<Integer>();
		for (Category cat : cats) {
			catIds.add(cat.getId());
			List<Integer> childs = iterateChildIds(cat.getChilds());
			if (childs != null) {
				catIds.addAll(childs);
			}
		}
		return catIds;
	}

	@Override
	public List<CategoryDTO> getCategoryByParentId(Integer pId) {
		CategoryExample categoryExample = new CategoryExample();
		categoryExample.createCriteria().andParentIdEqualTo(pId);
		List<CategoryDTO> categorieDTOs = new ArrayList<>();
		for (Category category : cateDao.selectByExample(categoryExample)) {
			CategoryDTO categoryDTO = new CategoryDTO();
			BeanUtils.copyProperties(category, categoryDTO);
			if (hasChild(category.getId())) {
				categoryDTO.setHaschild(true);
			}
			categorieDTOs.add(categoryDTO);
		}
		return categorieDTOs;
	}

	@Override
	public boolean hasChild(Integer categoryId) {
		boolean result = false;
		CategoryExample categoryExample = new CategoryExample();
		categoryExample.createCriteria().andParentIdEqualTo(categoryId);
		if (cateDao.countByExample(categoryExample) > 0) {
			result = true;
		}
		return result;
	}

	@Override
	public Category getCategoryByNameAndParentId(String categoryName, Integer pId) {
		CategoryExample categoryExample = new CategoryExample();
		categoryExample.createCriteria().andParentIdEqualTo(pId).andNameEqualTo(categoryName);
		if (cateDao.selectByExample(categoryExample).size() == 1) {
			return cateDao.selectByExample(categoryExample).get(0);
		} else {
			return null;
		}
	}

	@Override
	public List<Integer> getCategoryIdByParentId(Integer pId) {
		return cateDao.selectIdsByParentId(pId);
	}

	@Override
	public String getCategoryPathByProductId(Integer productId) {
		String result = "";
		for (CategoryProduct categoryProduct : categoryProductDao.findByProductId(productId)) {
			String categoryPath = "";
			Category category = cateDao.selectByPrimaryKey(categoryProduct.getCategoryId());
			for (String categoryId : category.getPath().split("/")) {
				categoryPath = categoryPath + cateDao.selectByPrimaryKey(Integer.parseInt(categoryId)).getName() + "/";
			}
			result = result + categoryPath + ";";
		}
		return result;
	}
}
