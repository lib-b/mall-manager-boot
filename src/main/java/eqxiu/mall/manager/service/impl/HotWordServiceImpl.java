package eqxiu.mall.manager.service.impl;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ZSetOperations.TypedTuple;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import eqxiu.mall.manager.service.HotWordService;
import eqxiu.mall.product.dao.HotWordDao;
import eqxiu.mall.product.model.HotWord;
import eqxiu.mall.product.model.HotWordExample;
import eqxiu.mall.product.model.HotWordExample.Criteria;

@Service
@Transactional
public class HotWordServiceImpl implements HotWordService {
	@Autowired
	private StringRedisTemplate redisTemplate;
	@Autowired
	private HotWordDao hotWordDao;

	@Override
	public boolean createOrUpdateHotWord(String keyword, String category) {
		if (keyword == null || "".equals(keyword.trim())) {
			return false;
		}
		redisTemplate.opsForZSet().incrementScore("hotword:" + category, keyword, 1);
		return true;
	}

	@Override
	public JSONArray getKeywordsByCategory(String category, int start, int end) throws Exception {
		Set<TypedTuple<String>> set = redisTemplate.opsForZSet().reverseRangeWithScores("hotword:" + category, start,
				end);
		JSONArray result = new JSONArray();
		for (TypedTuple<String> typedTuple : set) {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("name", typedTuple.getValue());
			jsonObject.put("value", typedTuple.getScore().intValue());
			result.add(jsonObject);
		}
		return result;
	}

	@Override
	public long getCountByCategory(String category) throws Exception {
		return redisTemplate.opsForZSet().size("hotword:" + category);
	}

	@Override
	public HotWord getHotwordById(Integer hotwordId) {
		return hotWordDao.selectByPrimaryKey(hotwordId);
	}

	@Override
	public List<HotWord> getHotwordByParam(HttpServletRequest request) {
		HotWordExample hotWordExample = new HotWordExample();
		Criteria criteria = hotWordExample.createCriteria();
		hotWordExample.setOrderByClause("sort " + request.getParameter("orderByClause").toString());
		if (!request.getParameter("hotwordName").equals("")) {
			criteria.andWordLike("%" + request.getParameter("hotwordName") + "%");
		}
		if (Byte.parseByte(request.getParameter("hotwordCategory")) != 0) {
			criteria.andCategoryEqualTo(Byte.parseByte(request.getParameter("hotwordCategory")));
		}
		if (Byte.parseByte(request.getParameter("hotwordType")) != 0) {
			criteria.andTypeEqualTo(Byte.parseByte(request.getParameter("hotwordType")));
		}
		return hotWordDao.selectByExample(hotWordExample);
	}

	@Override
	public int deleteHotwordById(Integer hotwordId) {
		return hotWordDao.deleteByPrimaryKey(hotwordId);
	}

	@Override
	public int insertHotword(HotWord hotword) {
		return hotWordDao.insert(hotword);
	}

	@Override
	public int updateHotword(HotWord hotWord) {
		return hotWordDao.updateByPrimaryKeySelective(hotWord);
	}
}
