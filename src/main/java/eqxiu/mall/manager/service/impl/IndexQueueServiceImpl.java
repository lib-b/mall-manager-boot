package eqxiu.mall.manager.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import cn.knet.common.mapper.JsonMapper;

import eqxiu.mall.manager.service.IndexQueueService;

@Component()
public class IndexQueueServiceImpl implements IndexQueueService {
	@Autowired(required = false)
	private RabbitTemplate rabbitTemplate;

	@Value("${rabbitmq.routingkey.search.indexer}")
	private String indexRoutingKey;

	@Override
	public void indexProduct(Integer id, IndexOption option) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("category", "mall");
		map.put("objectId", id);
		map.put("option", option.getOption());

		rabbitTemplate.convertAndSend(indexRoutingKey, JsonMapper.getInstance().toJson(map));
	}

}
