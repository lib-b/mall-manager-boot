package eqxiu.mall.manager.service.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import eqxiu.mall.manager.service.LogService;
import eqxiu.mall.product.dao.LogDao;
import eqxiu.mall.product.model.Log;
import eqxiu.mall.product.model.LogExample;
import eqxiu.mall.product.model.LogExample.Criteria;

@Service
@Transactional
public class LogServiceImpl implements LogService {

	@Autowired
	private LogDao logDao;

	@Override
	public int addLog(Log log) {
		return logDao.insert(log);
	}

	@Override
	public List<Log> selectLogsByParams(HttpServletRequest request) {
		LogExample logExample = new LogExample();
		Criteria criteria = logExample.createCriteria();
		if (!request.getParameter("userName").equals("")) {
			criteria.andUserLike("%" + request.getParameter("userName").trim() + "%");
		}
		if (!request.getParameter("createStartTime").equals("") && !request.getParameter("createEndTime").equals("")) {
			criteria.andTimeBetween(request.getParameter("createStartTime"), request.getParameter("createEndTime"));
		} else if (!request.getParameter("createStartTime").equals("")) {
			criteria.andTimeGreaterThanOrEqualTo(request.getParameter("createStartTime"));
		} else if (!request.getParameter("createEndTime").equals("")) {
			criteria.andTimeLessThanOrEqualTo(request.getParameter("createEndTime"));
		}
		return logDao.selectByExample(logExample);
	}

	@Override
	public int deleteLogsByParams(HttpServletRequest request) {

		LogExample logExample = new LogExample();
		Criteria criteria = logExample.createCriteria();
		if (!request.getParameter("userName").equals("")) {
			criteria.andUserLike("%" + request.getParameter("userName").trim() + "%");
		}
		if (!request.getParameter("createStartTime").equals("") && !request.getParameter("createEndTime").equals("")) {
			criteria.andTimeBetween(request.getParameter("createStartTime"), request.getParameter("createEndTime"));
		} else if (!request.getParameter("createStartTime").equals("")) {
			criteria.andTimeGreaterThanOrEqualTo(request.getParameter("createStartTime"));
		} else if (!request.getParameter("createEndTime").equals("")) {
			criteria.andTimeLessThanOrEqualTo(request.getParameter("createEndTime"));
		}
		return logDao.deleteByExample(logExample);
	}

}
