package eqxiu.mall.manager.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import eqxiu.mall.manager.service.OrderService;
import eqxiu.mall.order.dao.OrderDao;
import eqxiu.mall.order.model.Order;
import eqxiu.mall.order.model.OrderExample;
import eqxiu.mall.order.model.OrderProductExample.Criteria;
import eqxiu.mall.order.model.OrderProductExample;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {
	@Autowired
	private OrderDao orderDao;

	@Override
	public Order findOrder(Integer id) {
		return orderDao.selectByPrimaryKey(id);
	}

	@Override
	public int insertOrder(Order order) {
		return orderDao.insert(order);
	}

	@Override
	public int deleteByID(Integer id) {
		return orderDao.deleteByPrimaryKey(id);
	}

	@Override
	public int updateByID(Order record) {
		return orderDao.updateByPrimaryKeySelective(record);
	}

	@Override
	public List<Order> findByParams(HttpServletRequest request) {
		return orderDao.selectByExampleJoinProduct(getOrderExample(request));
	}

	@Override
	public Map<String, Object> getStatisticalParameterByParams(HttpServletRequest request) {
		Map<String, Object> result = new HashMap<>();
		try {
			if (orderDao.countOrdersByExampleJoinProduct(getOrderExample(request)) != null) {
				result.putAll(orderDao.countOrdersByExampleJoinProduct(getOrderExample(request)));
			}
			if (orderDao.sumTotalfeeByExampleJoinProduct(getOrderExample(request)) != null) {
				result.putAll(orderDao.sumTotalfeeByExampleJoinProduct(getOrderExample(request)));
			}
			if (orderDao.countCreateUsersByExampleJoinProduct(getOrderExample(request)) != null) {
				result.putAll(orderDao.countCreateUsersByExampleJoinProduct(getOrderExample(request)));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public Map<String, Object> getStatisticalParameterById(Integer orderId) {
		Map<String, Object> result = new HashMap<>();
		Order order = orderDao.selectByPrimaryKey(orderId);
		result.put("orders", 1);
		result.put("amount", order.getTotalFee());
		result.put("users", 1);
		return result;
	}

	public OrderProductExample getOrderExample(HttpServletRequest request) {
		OrderProductExample orderProductExample = new OrderProductExample();
		Criteria criteria = orderProductExample.createCriteria();
		if (!request.getParameter("outOrderId").trim().equals("")) {
			criteria.andProductOutOrderIdEqualTo(request.getParameter("outOrderId").trim());
		}
		if (!request.getParameter("productId").equals("")) {
			criteria.andProductProductIdEqualTo(Integer.parseInt(request.getParameter("productId").trim()));
		}
		if (!request.getParameter("platform").equals("0")) {
			criteria.andProductPlatformEqualTo(Byte.parseByte(request.getParameter("platform")));
		}
		if (!request.getParameter("createUser").equals("")) {
			criteria.andProductCreateUserEqualTo(request.getParameter("createUser").trim());
		}
		if (!request.getParameter("sourceId").trim().equals("")) {
			criteria.andProductSourceIdEqualTo(Integer.parseInt(request.getParameter("sourceId").trim()));
		}
		if (!request.getParameter("brandId").trim().equals("0")) {
			criteria.andProductBrandIdEqualTo(Integer.parseInt(request.getParameter("brandId").trim()));
		}
		if (!request.getParameter("createStartTime").equals("") && !request.getParameter("createEndTime").equals("")) {
			criteria.andProductCreateTimeBetween(request.getParameter("createStartTime"),
					request.getParameter("createEndTime"));
		} else if (!request.getParameter("createStartTime").equals("")) {
			criteria.andProductCreateTimeGreaterThanOrEqualTo(request.getParameter("createStartTime"));
		} else if (!request.getParameter("createEndTime").equals("")) {
			criteria.andProductCreateTimeLessThanOrEqualTo(request.getParameter("createEndTime"));
		}
		if (!request.getParameter("attrGroupId").trim().equals("")
				&& Integer.parseInt(request.getParameter("attrGroupId").trim()) != 0) {
			criteria.andProductAttrGroupIdEqualTo(Integer.parseInt(request.getParameter("attrGroupId").trim()));
		}
		if (!request.getParameter("status").trim().equals("")
				&& Integer.parseInt(request.getParameter("status").trim()) != 0) {
			criteria.andProductStatusEqualTo(Byte.parseByte(request.getParameter("status").trim()));
		}
		return orderProductExample;
	}

	@Override
	public List<Order> findByTime(String startTime, String endTime) {
		OrderExample orderExample = new OrderExample();
		orderExample.createCriteria().andCreateTimeBetween(startTime, endTime).andStatusEqualTo(Byte.parseByte("1"));
		return orderDao.selectByExample(orderExample);
	}

	@Override
	public List<Map<String, Object>> countByProductType(String startTime, String endTime, String orderByClause) {
		return orderDao.countByProductType(startTime, endTime, orderByClause);
	}

	@Override
	public List<Map<String, Object>> countByArtist(String startTime, String endTime, String orderByClause) {
		return orderDao.countByArtist(startTime, endTime, orderByClause);
	}
}
