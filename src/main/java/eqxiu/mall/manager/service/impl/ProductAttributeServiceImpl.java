package eqxiu.mall.manager.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import eqxiu.mall.manager.service.ProductAttributeService;
import eqxiu.mall.product.dao.ProductAttributeDao;
import eqxiu.mall.product.model.ProductAttribute;
import eqxiu.mall.product.model.ProductAttributeExample;

@Service
@Transactional
public class ProductAttributeServiceImpl implements ProductAttributeService {
	@Autowired
	private ProductAttributeDao productAttributeDao;

	@Override
	public int deleteByID(ProductAttribute record) {
		return productAttributeDao.deleteByPrimaryKey(record);
	}

	@Override
	public int insert(ProductAttribute record) {
		return productAttributeDao.insert(record);
	}

	@Override
	public int update(ProductAttribute record) {
		return productAttributeDao.updateByPrimaryKey(record);
	}

	@Override
	public List<ProductAttribute> findByProductId(Integer productid) {
		return productAttributeDao.findByProductId(productid);
	}

	@Override
	public ProductAttribute selectByKey(ProductAttribute record) {
		return productAttributeDao.selectByPrimaryKey(record);
	}

	@Override
	public List<Map<String, Object>> selectByProductId(Integer productId) {
		List<Map<String, String>> data = productAttributeDao.selectByProductId(productId);
		List<Map<String, Object>> result = new ArrayList<>();
		for (Map<String, String> map : data) {
			Map<String, Object> newMap = new HashMap<String, Object>();
			newMap.put("attrName", map.get("attrName"));
			newMap.put("attrType", map.get("attrType"));
			newMap.put("attrTitle", map.get("title"));
			if (!map.containsKey("attrValue")) {
				newMap.put("attrValue", "");
			} else {
				newMap.put("attrValue", map.get("attrValue"));
			}
			result.add(newMap);
		}
		return result;
	}

	@Override
	public List<ProductAttribute> findByAttrId(Integer attrId) {
		ProductAttributeExample productAttributeExample = new ProductAttributeExample();
		productAttributeExample.createCriteria().andAttrKeyIdEqualTo(attrId);
		return productAttributeDao.selectByExample(productAttributeExample);
	}

	@Override
	public int deleteByProductId(Integer productId) {
		return productAttributeDao.deleteByProductId(productId);
	}
}
