package eqxiu.mall.manager.service.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.ibm.icu.text.SimpleDateFormat;
import eqxiu.mall.commons.page.PageDTO;
import eqxiu.mall.manager.service.ProductService;
import eqxiu.mall.product.dao.CategoryProductDao;
import eqxiu.mall.product.dao.ProductDao;
import eqxiu.mall.product.dao.ProductTagDao;
import eqxiu.mall.product.model.Product;
import eqxiu.mall.product.model.ProductCategoryExample;
import eqxiu.mall.product.model.ProductExample;
import eqxiu.mall.product.model.ProductCategoryExample.Criteria;
import eqxiu.mall.product.model.ProductTagExample;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {
	@Autowired
	private ProductDao productDao;
	@Autowired
	private CategoryProductDao categoryProductDao;
	@Autowired
	private ProductTagDao productTagDao;

	@Override
	public int deleteProduct(int productId) {
		return productDao.deleteByPrimaryKey(productId);
	}

	@Override
	public Product getProduct(int productId) {
		return productDao.selectByPrimaryKey(productId);
	}

	@Override
	public int insertProduct(Product product) {
		return productDao.insert(product);
	}

	@Override
	public int updateProduct(Product product) {
		return productDao.updateByPrimaryKeySelective(product);
	}

	@Override
	public List<Product> selectByParams(HttpServletRequest request) {
		if (request.getParameter("category1").equals("0")) {
			ProductExample productExample = new ProductExample();
			eqxiu.mall.product.model.ProductExample.Criteria criteria = productExample.createCriteria();
			if (!request.getParameter("orderByClause").equals("default")) {
				productExample.setOrderByClause(
						request.getParameter("orderByProperty") + " " + request.getParameter("orderByClause"));
			}
			try {
				if (!request.getParameter("title").equals("")) {
					criteria.andTitleLike("%" + request.getParameter("title").trim() + "%");
				}
				if (!request.getParameter("brandId").equals("")
						&& Integer.parseInt(request.getParameter("brandId")) != 0) {
					criteria.andBrandIdEqualTo(Integer.parseInt(request.getParameter("brandId")));
				}
				if (!request.getParameter("artistId").equals("")) {
					criteria.andArtistIdEqualTo(Integer.parseInt(request.getParameter("artistId")));
				}
				if (!request.getParameter("attrGroupId").equals("")
						&& Integer.parseInt(request.getParameter("attrGroupId")) != 0) {
					criteria.andAttrGroupIdEqualTo(Integer.parseInt(request.getParameter("attrGroupId")));
				}
				if (!request.getParameter("platform").equals("")
						&& Integer.parseInt(request.getParameter("platform")) != 0) {
					criteria.andPlatformEqualTo(Byte.parseByte(request.getParameter("platform")));
				}
				if (!request.getParameter("status").equals("") && Byte.parseByte(request.getParameter("status")) != 0) {
					criteria.andStatusEqualTo(Byte.parseByte(request.getParameter("status")));
				}
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				if (!request.getParameter("startTime").equals("") && !request.getParameter("endTime").equals("")) {
					criteria.andCreateTimeBetween(sdf.parse(request.getParameter("startTime")),
							sdf.parse(request.getParameter("endTime")));
				} else if (!request.getParameter("startTime").equals("")) {
					criteria.andCreateTimeGreaterThanOrEqualTo(sdf.parse(request.getParameter("startTime")));
				} else if (!request.getParameter("endTime").equals("")) {
					criteria.andCreateTimeLessThanOrEqualTo(sdf.parse(request.getParameter("endTime")));
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
			return productDao.selectByExample(productExample);
		} else {
			List<Integer> categoryIds = new ArrayList<>();
			ProductCategoryExample productCategoryExample = new ProductCategoryExample();
			Criteria criteria = productCategoryExample.createCriteria();
			if (!request.getParameter("orderByClause").equals("default")) {
				productCategoryExample.setOrderByClause(
						request.getParameter("orderByProperty") + " " + request.getParameter("orderByClause"));
			}
			try {
				if (!request.getParameter("title").equals("")) {
					criteria.andTitleLike("%" + request.getParameter("title").trim() + "%");
				}
				if (!request.getParameter("brandId").equals("")
						&& Integer.parseInt(request.getParameter("brandId")) != 0) {
					criteria.andBrandIdEqualTo(Integer.parseInt(request.getParameter("brandId")));
				}
				if (!request.getParameter("artistId").equals("")) {
					criteria.andArtistIdEqualTo(Integer.parseInt(request.getParameter("artistId")));
				}
				if (!request.getParameter("attrGroupId").equals("")
						&& Integer.parseInt(request.getParameter("attrGroupId")) != 0) {
					criteria.andAttrGroupIdEqualTo(Integer.parseInt(request.getParameter("attrGroupId")));
				}
				if (!request.getParameter("platform").equals("")
						&& Integer.parseInt(request.getParameter("platform")) != 0) {
					criteria.andPlatformEqualTo(Byte.parseByte(request.getParameter("platform")));
				}
				if (!request.getParameter("status").equals("") && Byte.parseByte(request.getParameter("status")) != 0) {
					criteria.andStatusEqualTo(Byte.parseByte(request.getParameter("status")));
				}
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				if (!request.getParameter("startTime").equals("") && !request.getParameter("endTime").equals("")) {
					criteria.andCreateTimeBetween(sdf.parse(request.getParameter("startTime")),
							sdf.parse(request.getParameter("endTime")));
				} else if (!request.getParameter("startTime").equals("")) {
					criteria.andCreateTimeGreaterThanOrEqualTo(sdf.parse(request.getParameter("startTime")));
				} else if (!request.getParameter("endTime").equals("")) {
					criteria.andCreateTimeLessThanOrEqualTo(sdf.parse(request.getParameter("endTime")));
				}
				categoryIds.add(Integer.parseInt(request.getParameter("category3")));
				criteria.andCategoryIdIn(categoryIds);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			return productDao.selectByExampleJoinCategory(productCategoryExample);
		}
	}

	@Override
	public Product getProductByCode(String code) {
		return productDao.selectByCode(code);
	}

	@Override
	public List<Product> getProductByTitle(String title) {
		ProductExample productExample = new ProductExample();
		productExample.createCriteria().andTitleLike(title);
		return productDao.selectByExample(productExample);
	}

	@Override
	public List<Map<String, Object>> getAttrsByGroupId(Integer groupId) {
		return null;
	}

	@Override
	public PageDTO<Map<String, Object>> findProductsByCategoryId(Integer categoryId, int start, int pageSize) {
		int count = categoryProductDao.countByCategoryId(categoryId);
		List<Map<String, Object>> results = productDao.findProductsByCategoryId(categoryId, (start - 1) * pageSize,
				pageSize);
		return new PageDTO<Map<String, Object>>(start, pageSize).setCount(count).setList(results);
	}

	@Override
	public PageDTO<Map<String, Object>> findProductsByTagId(Integer tagId, int start, int pageSize) {
		ProductTagExample productTagExample = new ProductTagExample();
		productTagExample.createCriteria().andTagIdEqualTo(tagId);
		int count = productTagDao.countByExample(productTagExample);
		List<Map<String, Object>> results = productDao.findProductsByTagId(tagId, (start - 1) * pageSize, pageSize);
		return new PageDTO<Map<String, Object>>(start, pageSize).setCount(count).setList(results);
	}

	@Override
	public List<Product> getProductByGroupId(int groupId) {
		ProductExample productExample = new ProductExample();
		productExample.createCriteria().andAttrGroupIdEqualTo(groupId);
		return productDao.selectByExample(productExample);
	}

	@Override
	public List<Map<String, Object>> countProductsByBrand(String startTime, String endTime) {
		return productDao.countProductsByBrand(startTime, endTime);
	}

	@Override
	public List<Map<String, Object>> countProductsByGroupId(String startTime, String endTime) {
		return productDao.countProductsByGroupId(startTime, endTime);
	}

	@Override
	public List<Product> selectNonCategory() {
		return productDao.selectNonCategory();
	}
}
