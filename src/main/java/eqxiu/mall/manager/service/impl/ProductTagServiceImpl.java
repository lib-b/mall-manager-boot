package eqxiu.mall.manager.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import eqxiu.mall.product.dao.ProductTagDao;
import eqxiu.mall.product.model.ProductTag;
import eqxiu.mall.product.model.ProductTagExample;
import eqxiu.mall.manager.service.*;

@Service
@Transactional
public class ProductTagServiceImpl implements ProductTagService {
	@Autowired
	private ProductTagDao productTagDao;

	@Override
	public int deleteByID(ProductTag record) {
		return productTagDao.deleteByPrimaryKey(record);
	}

	@Override
	public int insert(ProductTag record) {
		return productTagDao.insert(record);
	}

	@Override
	public List<ProductTag> findByTagId(int tagid) {
		return productTagDao.selectByTag(tagid);
	}

	@Override
	public List<ProductTag> selectByProductId(int productid) {
		return productTagDao.selectByProductID(productid);
	}

	@Override
	public ProductTag selectByProductAndTagId(int tagid, int productid) {
		return productTagDao.selectByProductAndTag(tagid, productid);
	}

	@Override
	public int deleteByProductAndTagId(int tagid, int productid) {
		ProductTag productTag = new ProductTag();
		productTag.setProductId(productid);
		productTag.setTagId(tagid);
		return productTagDao.deleteByPrimaryKey(productTag);
	}

	@Override
	public List<ProductTag> findByProductId(int productId) {
		return productTagDao.selectByProductID(productId);
	}

	@Override
	public int getCountByTagId(Integer tagid) {
		ProductTagExample productTagExample = new ProductTagExample();
		productTagExample.createCriteria().andTagIdEqualTo(tagid);
		int result = productTagDao.countByExample(productTagExample);
		return result;
	}

	@Override
	public int deleteByProductId(Integer productId) {
		return productTagDao.deleteByProductId(productId);
	}

	@Override
	public int deleteByTagId(int tagid) {
		return productTagDao.deleteByTagId(tagid);
	}

}
