package eqxiu.mall.manager.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import eqxiu.mall.commons.page.PageDTO;
import eqxiu.mall.manager.service.SpecialProductService;
import eqxiu.mall.product.dao.SpecialProductDao;
import eqxiu.mall.product.model.SpecialProduct;
import eqxiu.mall.product.model.SpecialProductExample;
import eqxiu.mall.product.model.SpecialProductExample.Criteria;

@Service
@Transactional
public class SpecialProductServiceImpl implements SpecialProductService {

	@Autowired
	private SpecialProductDao specialProductDao;

	@Override
	public int deleteByID(SpecialProduct record) {
		return specialProductDao.deleteByPrimaryKey(record);
	}

	@Override
	public int insert(SpecialProduct record) {
		return specialProductDao.insert(record);
	}

	@Override
	public PageDTO<Map<String, Object>> findBySpecialId(int specialId, int pageNumber, int pageSize) {
		SpecialProductExample specialProductExample = new SpecialProductExample();
		specialProductExample.createCriteria().andSpecialIdEqualTo(specialId);
		int count = specialProductDao.countByExample(specialProductExample);
		List<Map<String, Object>> results = specialProductDao.selectBySpecialId(specialId, (pageNumber - 1) * pageSize,
				pageSize);
		return new PageDTO<Map<String, Object>>(pageNumber, pageSize).setCount(count).setList(results);
	}

	@Override
	public SpecialProduct selectByKey(Integer productId, Integer specialId) {
		SpecialProductExample specialProductExample = new SpecialProductExample();
		Criteria criteria = specialProductExample.createCriteria();
		criteria.andProductIdEqualTo(productId).andSpecialIdEqualTo(specialId);
		List<SpecialProduct> specialProducts = specialProductDao.selectByExample(specialProductExample);
		if (specialProducts.size() > 0) {
			return specialProducts.get(0);
		}
		return null;
	}

	@Override
	public int deleteBySpecialID(Integer specialId) {
		return specialProductDao.deleteBySpecialId(specialId);
	}
}
