package eqxiu.mall.manager.service.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import eqxiu.mall.manager.service.SpecialService;
import eqxiu.mall.product.dao.SpecialDao;
import eqxiu.mall.product.model.Special;
import eqxiu.mall.product.model.SpecialExample;
import eqxiu.mall.product.model.SpecialExample.Criteria;

@Service
@Transactional
public class SpecialServiceImpl implements SpecialService {

	@Autowired
	private SpecialDao specialDao;

	@Override
	public int deleteById(int id) {
		return specialDao.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(Special record) {
		return specialDao.insert(record);
	}

	@Override
	public int update(Special record) {
		return specialDao.updateByPrimaryKeySelective(record);
	}

	@Override
	public Special findById(int id) {
		return specialDao.selectByPrimaryKey(id);
	}

	@Override
	public Special findByCode(String code) {
		SpecialExample specialExample = new SpecialExample();
		specialExample.createCriteria().andCodeEqualTo(code);
		if (specialDao.selectByExample(specialExample).size() > 0) {
			return specialDao.selectByExample(specialExample).get(0);
		} else {
			return null;
		}
	}

	@Override
	public List<Special> findAllSpecials() {
		SpecialExample specialExample = new SpecialExample();
		return specialDao.selectByExample(specialExample);
	}

	@Override
	public List<Special> findSpecialsByParam(HttpServletRequest request) {
		SpecialExample specialExample = new SpecialExample();
		Criteria criteria = specialExample.createCriteria();
		specialExample.setOrderByClause("sort "+request.getParameter("orderByClause"));
		if (!request.getParameter("name").equals("")) {
			criteria.andNameEqualTo(request.getParameter("name"));
		}
		if (!request.getParameter("code").equals("")) {
			criteria.andCodeEqualTo(request.getParameter("code"));
		}
		if (!request.getParameter("status").equals("") && Byte.parseByte(request.getParameter("status")) != 0) {
			criteria.andStatusEqualTo(Byte.parseByte(request.getParameter("status")));
		}
		if (!request.getParameter("type").equals("") && Byte.parseByte(request.getParameter("type")) != 0) {
			criteria.andTypeEqualTo(Byte.parseByte(request.getParameter("type")));
		}
		return specialDao.selectByExample(specialExample);
	}

}
