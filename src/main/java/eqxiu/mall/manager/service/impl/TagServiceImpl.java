package eqxiu.mall.manager.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import eqxiu.mall.product.dao.TagDao;
import eqxiu.mall.product.model.Tag;
import eqxiu.mall.manager.service.*;

@Service
@Transactional
public class TagServiceImpl implements TagService {

	@Autowired
	private TagDao tagDao;

	@Override
	public int deleteByPrimaryKey(Integer id) {
		return tagDao.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(Tag record) {
		return tagDao.insert(record);
	}

	@Override
	public List<Tag> selectAllTags() {
		return tagDao.selectAllTags();
	}

	@Override
	public Tag selectByName(String name) {
		return tagDao.selectByName(name);
	}

	@Override
	public Tag selectByPrimaryKey(Integer id) {
		return tagDao.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKey(Tag record) {
		return tagDao.updateByPrimaryKeySelective(record);
	}

}
