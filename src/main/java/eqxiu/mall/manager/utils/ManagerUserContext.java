package eqxiu.mall.manager.utils;

import eqxiu.mall.manager.dto.UserDTO;

public class ManagerUserContext {

	public static ThreadLocal<UserDTO> currentUser = new ThreadLocal<UserDTO>();

	public static void setUser(UserDTO user) {
		currentUser.set(user);
	}

	public static UserDTO getUser() {
		return currentUser.get();
	}
}
