package eqxiu.mall.manager.utils;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import eqxiu.mall.manager.dto.UserDTO;

@Component
public class ManagerUserService {

	@Value("${userinfo.url}")
	private String userInfoURL;

	private Logger logger = LoggerFactory.getLogger(ManagerUserService.class);

	public UserDTO getUserInfo(String sessionId) {
		try {
			return invokeHttp(sessionId);
		} catch (UnirestException e) {
			logger.error(e.getMessage());
			try {
				return invokeHttp(sessionId);
			} catch (UnirestException e1) {
				e1.printStackTrace();
			}

		}
		return null;

	}

	private UserDTO invokeHttp(String sessionId) throws UnirestException {
		HttpResponse<JsonNode> resp = Unirest.get(userInfoURL).header("user-agent", "eqxiu-internal-mall")
				.header("Cookie", "JSESSIONID=" + sessionId).asJson();
		JSONObject respObj = resp.getBody().getObject();
		if (respObj.getBoolean("success")) {

			JSONObject userJson = respObj.getJSONObject("obj");
			String userId = userJson.getString("id");
			String name = null;
			try {
				if (userJson.has("name")) {
					Object tmp = userJson.get("name");
					if (tmp != null) {
						name = (String) tmp;
					}
				}
			} catch (Exception e) {

			}

			String loginName = userJson.getString("loginName");

			if (name == null) {
				name = loginName;
			}

			if (name == null) {
				if (userJson.has("nick"))
					name = userJson.getString("nick");
			}

			UserDTO user = new UserDTO(userId, loginName, name);
			return user;
		}
		return null;

	}
}
