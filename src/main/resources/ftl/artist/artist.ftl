<form class="form-inline" id="artist_list_form">
	<div class="form-group">
		<div class="input-group">
            <div class="input-group-addon"> 作者ID</div>
            <input type="text" class="form-control" name="id"/>
        </div>
	</div>
	<div class="form-group">
		<div class="input-group">
            <div class="input-group-addon"> 作者名称</div>
            <input type="text" class="form-control" name="artistName"/>
        </div>
	</div>
	<div class="form-group">
		<div class="input-group">
            <div class="input-group-addon"> 手机号码</div>
            <input type="text" class="form-control" name="phone"/>
        </div>
	</div>
	<br/>
	<div class="form-group">
		<div class="input-group">
            <div class="input-group-addon"> 作者类型</div>
            <select class="form-control" name="artistType">
                <option value="0" selected="selected">全部</option>
	            <option value="1">摄影师</option>
	            <option value="2">秀客</option>
	        </select>
        </div>
	</div>
	<div class="form-group">
		<div class="input-group">
            <div class="input-group-addon"> 审核状态</div>
            <select class="form-control" name="artistStatus">
                <option value="-1" selected="selected">全部</option>
                <option value="0">未申请</option>
	            <option value="1">正常</option>
	            <option value="2">申请审核中</option>
	            <option value="3">审核失败</option>
	            <option value="4">修改审核中</option>
	        </select>
        </div>
	</div>
    <div class="form-group">
        <div class="input-group">
            <div class="input-group-addon">样例创建时间</div>
            <input type="text" class="form-control" name="createStartTime" id="create_time"/>
            <div class="input-group-addon">-</div>
            <input type="text" class="form-control" name="createEndTime" id="end_time"/>
        </div>
    </div>
	<br/>
	<button type="button" class="btn btn-primary" onclick="query_artist_list('#artist_list_grid', '/m/mall/artist/artist/list.json')" >查询</button>
	<button type="button" class="btn btn-primary" onclick="batch_delete_artist()">删除选中作者</button>
</form>
<table id="artist_list_grid" data-toggle="table" data-toolbar="#artist_list_form" data-unique-id="id" data-query-params="artist_query_params" data-click-to-select="true" data-page-size="5">
	<thead>
	    <tr>
	   		<th data-valign="middle" data-checkbox="true">全选</th>
	    	<th data-field="id">ID</th>
	    	<th data-field="artistName">作者名称</th>
	    	<th data-field="type" data-formatter=formatter_type>作者类型</th>
	    	<th data-field="status" data-formatter=formatter_status>审核状态</th>
	    	<th data-field="shortIntroduction">简短介绍</th>
	    	<th data-field="introduction">详细介绍</th>
	    	<th data-field="qq">qq号</th>
	    	<th data-field="webchat">微信号</th>
	    	<th data-field="email">邮箱地址</th>
	    	<th data-field="phone">手机号码</th>
	    	<th data-field="joinTime" data-formatter="log_formatter_time">加入时间</th>
            <th data-field="exampleCount" data-sortable="true">提交样例数</th>
	    	<th data-valign="middle" data-field="id" data-align="center" data-formatter="business_operation_format" class="action-buttons" data-width="130">操作</th>				
	    </tr>
    </thead>
</table>
<!-- 新增产品业务的模型，主要是列出来需要的属性，这个模块在两个地方会用，一个是点击新增产品业务按钮，另一个是点击产品列表中的编辑。 -->
<div id="artist_model" class="modal fade" role="dialog" aria-labelledby="artist_model_title">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <form class="form-inline" id="artistForm">
                   <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">秀客ID</div>
                            <input type="text" id="id" value="" name="id" class="form-control" maxlength="50" style="width: 450px;">
                        </div>
                    </div>
                    <br/><br/>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">秀客名称</div>
                            <input type="text" id="artistName" value="" name="artistName" class="form-control" style="width: 450px;">
                        </div>
                    </div>
                    <br/><br/>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">简短介绍</div>
                            <input type="textarea" id="shortIntroduction" value="" name="shortIntroduction" class="form-control" style="width: 450px;">
                        </div>
                    </div>
                    <br/><br/>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">详细介绍</div>
                            <textarea id="introduction" value="" name="introduction" class="form-control" style="width: 450px;"/>
                        </div>
                    </div>
                    <br/><br/>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">qq号码</div>
                            <textarea id="qq" value="" name="qq" class="form-control" style="width: 450px;"/>
                        </div>
                    </div>
                    <br/><br/>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">微信号码</div>
                            <input type="text" id="wechat" value="" name="wechat" class="form-control" style="width: 450px;">
                        </div>
                    </div>
                    <br/><br/>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">邮箱地址</div>
                            <input type="text" id="email" value="" name="email" class="form-control" style="width: 450px;">
                        </div>
                    </div>
                    <br/><br/>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">手机号码</div>
                            <input type="text" id="phone" value="" name="phone" class="form-control" style="width: 450px;">
                        </div>
                    </div>
                    <br/><br/>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">作者类型</div>
                            <select id="type" value="" name="type" class="form-control" style="width: 450px;">
                               <option value="1" selected="selected">摄影师</option>
	                           <option value="2">秀客</option>
	                        </select>
                        </div>
                    </div>
                    <br/><br/>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">审核状态</div>
                            <select name="status" id="status" class="form-control" style="width: 450px;">
                                <option value="0">未申请</option>
                                <option value="1">正常</option>
                                <option value="2">申请审核中</option>
                                <option value="3">审核失败</option>
                                <option value="4">修改审核中</option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" onclick="submitartistForm()">更新</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
//查询
    function query_artist_list(id, url) {
        $(id).bootstrapTable('refresh', {url: url});
    }  
     function artist_query_params(params) {
        $('#artist_list_form :input').each(function() {
            if($(this).attr('name')) {
                params[$(this).attr('name')] = $(this).val();
            }
        });
        return params;
    }
    function business_operation_format(value, item) {
        return '<button type="button" class="btn btn-link" onclick="openUpdateartistWin('+value+')">编辑</button>';
    }
    function submitartistForm() {
        $.ajax({
            url :  "/m/mall/artist/artist/update",
            type : "POST",
            data: $('#artistForm').serialize(),
            success:function(data,ts,xhr){
                if(data.success){
                    $('#artist_model').modal('hide');
                    success(data.msg);
                }else{
                    error(data.msg);
                }
                $('#artist_list_grid').bootstrapTable('refresh');
            }
        });
    }
     function openCreateartistWin() {
        $('#artistId').removeAttr('readonly');
        $('#artistForm')[0].reset();
        $('#artist_model').modal('show');
    }
    
    function openUpdateartistWin(id) {
        $('#id').attr('readonly', true);
        $('#artistForm')[0].reset();
        setFormValue('artistForm', $('#artist_list_grid').bootstrapTable('getRowByUniqueId', id));
        $('#artist_model').modal('show');
    }

    var artist_status = {'0': '未申请', '1': '正常','2':'申请审核中','3':'审核失败','4':'修改审核中'};
	function formatter_status(cellvalue, rowObject) {
		if(artist_status[cellvalue]) {
			cellvalue = artist_status[cellvalue];
		}
		return cellvalue;
	}
	var artist_type = {'1': '摄影师', '2': '秀客'};
	function formatter_type(cellvalue, rowObject) {
		if(artist_type[cellvalue]) {
			cellvalue = artist_type[cellvalue];
		}
		return cellvalue;
	}  
	function log_formatter_time(value) {
      return new Date(value).format('yyyy-MM-dd hh:mm:ss');
    } 
    
    function batch_delete_artist(id) {
		var data = {};
		if(id) {
			data.id = id;
		} else {
			data = $('#artist_list_grid').bootstrapTable('getSelections');
		}
		if(!data || data.length <= 0) {
			error('请选择作者！');
			return;
		}
		var ids = [];
		$(data).each(function(idx, item) {
			ids.push(item.id);
		});
		var ajaxData = {
			ids: ids.join(';')
		};
		openDialog({
			title: '删除',
			message: '您确认要删除吗？',
			okAction: function() {
				closeDialog();
				$.ajax({
					type: "post",
					url: "/m/mall/artist/artist/delete",
					async: true,
					data: ajaxData,
					success: function(data) {
						if(data.success) {
							success(data.msg);
						} else {
							error("error");
						}
						$('#artist_list_grid').bootstrapTable('refresh');
					}
				});
			}
		})
	}

    $(document).ready(function(){
        ///日期控件
        $('#create_time,#end_time').datetimepicker();
    });
</script>