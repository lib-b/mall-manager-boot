<form class="form-inline" id="attribute_list_form">
	<div class="form-group">
		<div class="input-group">
            <div class="input-group-addon"> 属性名</div>
            <input type="text" class="form-control" name="name"/>
        </div>
	</div>
	<div class="form-group">
		<div class="input-group">
            <div class="input-group-addon"> 属性组名</div>
            <select id="attrGroupNameFilter" class="form-control" name="attrGroupId"/>
        </div>
	</div>
	<div class="form-group">
		<div class="input-group">
            <div class="input-group-addon"> 属性类型</div>
            <select class="form-control" name="type">
               <option value="0" selected="selected">全部</option>
               <option value="1">输入</option>
               <option value="2">上传</option>
            </select>
        </div>
	</div>
	<br/>
	<button type="button" class="btn btn-primary" onclick="query_attribute_list('#attribute_list_grid', '/m/mall/attribute/attribute/list.json')" >查询属性</button>
    <button type="button" class="btn btn-primary" onclick="openCreateattributeWin()">新增属性</button>
    <button type="button" class="btn btn-primary" onclick="batch_delete_attribute()">删除选中属性</button>
</form>
<table id="attribute_list_grid" data-toggle="table" data-toolbar="#attribute_list_form" data-unique-id="id" data-query-params="attribute_query_params" data-click-to-select="true" data-page-size="5">
	<thead>
	    <tr>
	        <th data-valign="middle" data-checkbox="true">全选</th>
	    	<th data-field="id">ID</th>
	    	<th data-field="name">属性名称</th>
	    	<th data-field="title">属性主题</th>
	    	<th data-field="type" data-formatter=formatter_type>属性类型</th>	
	    	<th data-field="defaultValue">默认值</th>
	    	<th data-field="groups">属性组名</th>
	    	<th data-valign="middle" data-field="id" data-align="center" data-formatter="attribute_operation_format" class="action-buttons" data-width="130">操作</th>				
	    </tr>
    </thead>
</table>
<div id="attribute_model" class="modal fade" role="dialog" aria-labelledby="attribute_model_title">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="attribute_model_title">属性业务</h4>
            </div>
            <div class="modal-body">
                <form class="form-inline" id="attributeForm">
                   <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">属性ID&nbsp;&nbsp;</div>
                            <input type="text" id="attributeId" value="" name="id" class="form-control" style="width: 450px;">
                        </div>
                    </div>
                    <br/><br/>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">属性名称</div>
                            <input type="text" id="attributeName" value="" name="name" class="form-control" style="width: 450px;">
                        </div>
                    </div>
                    <br/><br/>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">属性主题</div>
                            <input type="text" id="attributeTitle" value="" name="title" class="form-control" style="width: 450px;">
                        </div>
                    </div>
                     <br/><br/>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">属性类型</div>
                            <select id="attributeType" value="" name="type" class="form-control" style="width: 450px;">
                               <option value="1" selected="selected">输入</option>
                               <option value="2">上传</option>
                            </select>
                        </div>
                    </div>
                     <br/><br/>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">默认值&nbsp;&nbsp;&nbsp;&nbsp;</div>
                            <input type="text" id="attributeDefault_value" value="" name="defaultValue" class="form-control" style="width: 450px;">
                        </div>
                    </div>
                    <br/><br/>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">属性组&nbsp;&nbsp;&nbsp;&nbsp;</div>
                            <select id="attrGroupId" value="" multiple="multiple" name="groupIds" class="form-control" style="width: 450px;"/>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" onclick="submitAttributeForm()">保存</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div id="attributeUpdate_model" class="modal fade" role="dialog" aria-labelledby="attributeUpdate_model_title">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="attributeUpdate_model_title">属性业务</h4>
            </div>
            <div class="modal-body">
                <form class="form-inline" id="attributeUpdateForm">
                   <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">属性ID&nbsp;&nbsp;</div>
                            <input type="text" id="attributeIdUpdate" value="" name="id" class="form-control" style="width: 450px;">
                        </div>
                    </div>
                    <br/><br/>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">属性名称</div>
                            <input type="text" id="attributeName" value="" name="name" class="form-control" style="width: 450px;">
                        </div>
                    </div>
                    <br/><br/>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">属性主题</div>
                            <input type="text" id="attributeTitle" value="" name="title" class="form-control" style="width: 450px;">
                        </div>
                    </div>
                     <br/><br/>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">属性类型</div>
                            <select id="attributeType" value="" name="type" class="form-control" style="width: 450px;">
                               <option value="1" selected="selected">输入</option>
                               <option value="2">上传</option>
                            </select>
                        </div>
                    </div>
                     <br/><br/>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">默认值&nbsp;&nbsp;&nbsp;&nbsp;</div>
                            <input type="text" id="attributeDefault_value" value="" name="defaultValue" class="form-control" style="width: 450px;">
                        </div>
                    </div>
                    <br/><br/>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">属性组&nbsp;&nbsp;&nbsp;&nbsp;</div>
                            <select id="attrGroupIdUpdate" value="" multiple="multiple" name="groupIdsUpdate" class="form-control" style="width: 450px;"/>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" onclick="submitAttributeUpdateForm()">保存</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
     $(document).ready(function () {
          addItems("#attrGroupNameFilter","/m/mall/attribute/attributegroup/listAll.json");;
      });
     function addItems(id,url) {  
            $.ajax({  
                url: url,    
                type: "post",  
                dataType: "json",  
                contentType: "application/json",  
                traditional: true,  
                success: function (data) { 
                    $(id).empty();  
                    $(id).append("<option value='0' selected='selected'>全部</option>");
                    for (var i in data) {  
                      $(id).append("<option value=\"" + data[i].id + "\" >" + data[i].name + "</option>");  
                    }  
                },  
                error: function (msg) {  
                    alert("出错了！");  
                }  
            });            
        };
    function query_attribute_list(id, url) {
        $(id).bootstrapTable('refresh', {url: url});
    }  
     function attribute_query_params(params) {
        $('#attribute_list_form :input').each(function() {
            if($(this).attr('name')) {
                params[$(this).attr('name')] = $(this).val();
            }
        });
        return params;
    }
    function attribute_operation_format(value, item) {
        return '<button type="button" class="btn btn-link" onclick="openUpdateattributeWin('+value+', \''+item.groups+'\')">编辑</button>';
    }
    function submitAttributeForm() {
    <!-- 使用ajax发送请求信息 -->
                $.ajax({
            url : "/m/mall/attribute/attribute/save",
            type : "POST",
            data: $('#attributeForm').serialize(),
            success:function(data,ts,xhr){
                if(data.success){
                    $('#attribute_model').modal('hide');
                    success(data.msg);
                }else{
                    error(data.map["error"]);
                }
                $('#attribute_list_grid').bootstrapTable('refresh');
            }
        });
    }
     function submitAttributeUpdateForm() {
    <!-- 使用ajax发送请求信息 -->
                $.ajax({
            url : "/m/mall/attribute/attribute/update",
            type : "POST",
            data: $('#attributeUpdateForm').serialize(),
            success:function(data,ts,xhr){
                if(data.success){
                    $('#attributeUpdate_model').modal('hide');
                    success(data.msg);
                }else{
                    error(data.msg);
                }
                $('#attribute_list_grid').bootstrapTable('refresh');
            }
        });
    }
     function openCreateattributeWin() {
        $('#attributeId').attr('readonly', true);
        addattributeItems("-1","#attrGroupId","/m/mall/attribute/attributegroup/listAll.json");
        $('#attributeForm')[0].reset();
        $('#attribute_model').modal('show');
    }
    
    function openUpdateattributeWin(id,groupNames) {
        $('#attributeIdUpdate').attr('readonly', true);
        addattributeItems(groupNames,"#attrGroupIdUpdate","/m/mall/attribute/attributegroup/listAll.json");
        $('#attributeUpdateForm')[0].reset();
        setFormValue('attributeUpdateForm', $('#attribute_list_grid').bootstrapTable('getRowByUniqueId', id));
        $('#attributeUpdate_model').modal('show');
    }
     function addattributeItems(groupNames,id,url) {  
            $.ajax({  
                url: url,    
                type: "post",  
                dataType: "json",  
                contentType: "application/json",  
                traditional: true,  
                success: function (data) { 
                    $(id).empty();  
                    for (var i in data) {
                      if(groupNames.indexOf(data[i].name)>=0){  
                        $(id).append("<option value=\"" + data[i].id + "\" selected='selected'>" + data[i].name + "</option>"); 
                      }
                      else{
                        $(id).append("<option value=\"" + data[i].id + "\">" + data[i].name + "</option>"); 
                      } 
                    }  
                },  
                error: function (msg) {  
                    alert("出错了！");  
                }  
            });            
        };  

    function batch_delete_attribute(id) {
		var data = {};
		if(id) {
			data.id = id;
		} else {
			data = $('#attribute_list_grid').bootstrapTable('getSelections');
		}
		if(!data || data.length <= 0) {
			error('请选择属性！');
			return;
		}
		var ids = [];
		$(data).each(function(idx, item) {
			ids.push(item.id);
		});
		var ajaxData = {
			ids: ids.join(';')
		};
		openDialog({
			title: '删除',
			message: '您确认要删除这些属性吗？',
			okAction: function() {
				closeDialog();
				$.ajax({
					type: "post",
					url: "/m/mall/attribute/attribute/delete",
					data: ajaxData,
					success: function(data, ts, xhr) {
						if(data.success) {
							success(data.msg);
						} else {
							error("error");
						}
						$('#attribute_list_grid').bootstrapTable('refresh');
					}
				});
			}
		})

	}       
    var attribute_type = {'1': '输入', '2': '上传'};
	function formatter_type(cellvalue, rowObject) {
		if(attribute_type[cellvalue]) {
			cellvalue = attribute_type[cellvalue];
		}
		return cellvalue;
	}     
</script>
