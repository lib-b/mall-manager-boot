<form class="form-inline" id="attributegroup_list_form">
	<div class="form-group">
		<div class="input-group">
            <div class="input-group-addon"> 属性组名</div>
            <select id="attrGroupFilter" class="form-control" name="id"/>
        </div>
	</div>
	<br/>
	<button type="button" class="btn btn-primary" onclick="query_attributegroup_list('#attributegroup_list_grid', '/m/mall/attribute/attributegroup/list.json')" >查询属性组</button>
    <button type="button" class="btn btn-primary" onclick="openCreateattributegroupWin()" style="margin-right: 20px;">新增属性组</button>
</form>
<table id="attributegroup_list_grid" data-toggle="table" data-toolbar="#attributegroup_list_form" data-unique-id="id" data-query-params="attributegroup_query_params" data-click-to-select="true" data-page-size="5">
	<thead>
	    <tr>
	    	<th data-field="id">ID</th>
	    	<th data-field="name">属性组名称</th>
	    	<th data-field="status" data-visible="false">属性组状态</th>
	    	<th data-valign="middle" data-field="id" data-align="center" data-formatter="attributegroup_operation_format" class="action-buttons" data-width="130">操作</th>				
	    </tr>
    </thead>
</table>

<!-- 新增属性组业务的模型，主要是列出来需要的属性，这个模块在两个地方会用，一个是点击新增产品业务按钮，另一个是点击产品列表中的编辑。 -->
<div id="attributegroup_model" class="modal fade" role="dialog" aria-labelledby="attributegroup_model_title">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="attributegroup_model_title">新增属性组业务</h4>
            </div>
            <div class="modal-body">
                <form class="form-inline" id="attributegroupForm">
                   <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">属性组ID&nbsp;&nbsp;</div>
                            <input type="text" id="attributegroupId" value="" name="id" class="form-control" style="width: 450px;">
                        </div>
                    </div>
                    <br/><br/>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">属性组名称</div>
                            <input type="text" id="attributegroupName" value="" name="name" class="form-control" style="width: 450px;">
                        </div>
                    </div>
                    <br/><br/>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">属性组状态</div>
                            <input type="text" id="attributegroupStatus" value="" name="status" class="form-control" style="width: 450px;">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" onclick="submitattributegroupForm()">保存</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div id="productattr_modal" class="modal fade" role="dialog" aria-labelledby="productattr_model_title">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="productattr_model_title">属性信息</h4>
            </div>
            <div class="modal-body" id="productattr_modal_body">
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
 $(document).ready(function () {
          addGroupItems("#attrGroupFilter","/m/mall/attribute/attributegroup/listAll.json");;
      });
//查询
    function query_attributegroup_list(id, url) {
        $(id).bootstrapTable('refresh', {url: url});
    }  
     function attributegroup_query_params(params) {
        $('#attributegroup_list_form :input').each(function() {
            if($(this).attr('name')) {
                params[$(this).attr('name')] = $(this).val();
            }
        });
        return params;
    }
    function attributegroup_operation_format(value, item) {
        return '<button type="button" class="btn btn-link" onclick="openUpdateattributegroupWin('+value+')">编辑</button>' +
                '<button type="button" class="btn btn-link" onclick="deleteattributegroup('+value+', \''+item.title+'\')">删除</button>'+
                '<button type="button" class="btn btn-link" onclick="refreshattr('+value+', \''+item.title+'\')">属性集</button>';
    }
    function submitattributegroupForm() {
    <!-- 使用ajax发送请求信息 -->
                $.ajax({
            url : "/m/mall/attribute/attributegroup/save",
            type : "POST",
            data: $('#attributegroupForm').serialize(),
            success:function(data,ts,xhr){
                if(data.success){
                    $('#attributegroup_model').modal('hide');
                    success(data.msg);
                }else{
                    error(data.msg);
                }
                $('#attributegroup_list_grid').bootstrapTable('refresh');
            }
        });
    }
     function openCreateattributegroupWin() {
        $('#attributegroupId').attr('readonly', true);
        $('#attributegroupForm')[0].reset();
        $('#attributegroup_model').modal('show');
    }
    
    function openUpdateattributegroupWin(id) {
        $('#attributegroupId').attr('readonly', true);
        $('#attributegroupForm')[0].reset();

        setFormValue('attributegroupForm', $('#attributegroup_list_grid').bootstrapTable('getRowByUniqueId', id));
        $('#attributegroup_model').modal('show');
    }

    function deleteattributegroup(id, name) {
        openDialog({
            title: '删除产品',
            message: '您确定要删除 ['+name+']吗？',
            okAction: function() {
                closeDialog();
                $.ajax({
                    url :  "/m/mall/attribute/attributegroup/delete",
                    type : "POST",
                    data: {
                        id: id
                    },
                    success:function(data,ts,xhr){
                        if(data.success){
                            success(data.msg);
                        }else{
                            error(data.msg);
                        }
                        $('#attributegroup_list_grid').bootstrapTable('refresh');
                        parent.location.reload();
                    }
                });
            }
        });
    }  
    function addGroupItems(id,url) {  
            $.ajax({  
                url: url,    
                type: "post",  
                dataType: "json",  
                contentType: "application/json",  
                traditional: true,  
                success: function (data) { 
                    $(id).empty(); 
                    $(id).append("<option value='0' selected='selected'>全部</option>");
                    for (var i in data) {  
                      $(id).append("<option value=\"" + data[i].id + "\" >" + data[i].name + "</option>");  
                    }  
                },  
                error: function (msg) {  
                    alert("出错了！");  
                }  
            });            
        };
    function refreshattr(id, name) {
	        $.ajax({
	            url :  "/m/mall/attribute/attributegroup/attrbutelist",
	            type : "POST",
	            data: {
	                groupId: id
	            },
	            success:function(data,ts,xhr){
	                $('#productattr_modal_body').empty();
	                var head = "<table class='table table-striped table-bordered table-hover tree_table' style='width: 100%' border='1'><tr><th>属性名</th><th>属性描述</th></tr>";
                    for (var i in data) {  
                  		var line = "<tr><td>"+data[i].name+"</td><td>"+data[i].value+"</td></tr>";
                  		head += line;
                	}  
                	var foot = "</table>";
                	 $('#productattr_modal_body').append(head + foot);
	                 $('#productattr_modal').modal('show');
	            }
	        });
    }   
</script>
