<!-- 消息圆点通知CSS -->
<style type="text/css">
    .notice {
        position: absolute;
        right: 0;
        top: -7px;
        transform: scale(1.0);
        color : red;
        z-index : 100;
    }
</style>

<!--场景模板列表-->
<div class="row">
    <div class="col-xs-12">
        <ul id="nav-tabs" class="nav nav-tabs" style="padding-bottom:12px;">
            <li class="active" reload="false" body="#nav-body-attribute" action="/mall/attribute/attribute">
                <a href="javascript:;">属性业务</a>
            </li>
            <li reload="false" body="#nav-body-attributeGroup" action="/mall/attribute/attributeGroup">
                <a href="javascript:;">属性组业务</a>
            </li>
        </ul>

        <div id="nav-body-attribute"></div>
        <div id="nav-body-attributeGroup" style="display:none;"></div>
    </div><!-- /.col -->
</div><!-- /.row -->
<script type="text/javascript">
    $(function() {
        loadPage('#nav-body-attribute', '/mall/attribute/attribute');
    });

</script>