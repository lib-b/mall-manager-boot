<ul class="nav nav-tabs">
	<li class="active">
		<a onclick="callFunc('/mall/banner/banner.html')" href="#">banner列表</a>
	</li>
	<li>
		<a onclick="callFunc('/mall/banner/bannerConfig.html')" href="#">banner配置</a>
	</li>
</ul>
<form class="form-inline" id="Banner_list_form">
	<div class="form-group">
		<div class="input-group">
            <div class="input-group-addon"> 位置名称</div>
            <input type="text" class="form-control" name="name"/>
        </div>
	</div>
	<div class="form-group">
		<div class="input-group">
            <div class="input-group-addon"> 标识</div>
            <input type="text" class="form-control" name="code"/>
        </div>
	</div>
	<div class="input-group">
            <div class="input-group-addon"> 状态</div>
            <select id="status" name="status" class="form-control">
              <option value="0" selected="selected">全部</option>
              <option value="1">启用</option>
              <option value="2">停用</option>
            </select>
        </div>
	<br/>
	<button type="button" class="btn btn-primary" onclick="query_Banner_list('#Banner_list_grid', '/m/mall/banner/bannerList.json')" >查询</button>
	<button type="button" class="btn btn-primary" onclick="openCreateBannerWin()">新增</button>
	<button type="button" class="btn btn-primary" onclick="batch_delete_banner()">删除选中banner</button>
</form>
<table id="Banner_list_grid" data-toggle="table" data-toolbar="#Banner_list_form" data-unique-id="id" data-query-params="Banner_query_params" data-click-to-select="true" data-page-size="5">
	<thead>
	    <tr>
	        <th data-valign="middle" data-checkbox="true">全选</th>
	    	<th data-field="id">BannerID</th>
	    	<th data-field="name">位置名称</th>
	    	<th data-field="code">标识</th>
	    	<th data-field="url">页面URL</th>
	    	<th data-field="type" data-formatter=formatter_type>类型</th>	    	
	    	<th data-field="imageWidth">图片宽度</th>
	    	<th data-field="imageHeight">图片高度</th>
	    	<th data-field="status" data-formatter=formatter_status>状态</th>
	    	<th data-valign="middle" data-field="id" data-align="center" data-formatter="banner_operation_format" class="action-buttons" data-width="130">操作</th>				
	    </tr>
    </thead>
</table>
<div id="Banner_model" class="modal fade" role="dialog" aria-labelledby="Banner_model_title">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="banner_model_title">新增banner</h4>
            </div>
            <div class="modal-body">
                <form class="form-inline" id="BannerForm">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                        <input type="text" id="bannerId" value="" name="id" class="form-control" style="width: 450px;">
                    </div>
                 </div> 
                  <br/><br/>
                 <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">位置名称</div>
                        <input type="text" id="bannerName" value="" name="name" class="form-control" style="width: 450px;">
                    </div>
                 </div>
                  <br/><br/>
                 <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">标识&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                        <input type="text" id="bannerCode" value="" name="code" class="form-control" style="width: 450px;">
                    </div>
                 </div>
                  <br/><br/>
                 <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">页面URL&nbsp;</div>
                        <input type="text" id="bannerURL" value="" name="url" class="form-control" style="width: 450px;">
                    </div>
                 </div>
                 <br/><br/>
                 <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">类型&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                        <select id="bannerType" value="" name="type" class="form-control" style="width: 450px;">
           					<option value="1" selected="selected">关键字</option>
          					<option value="2">跳转链接</option>
          				</select>
                    </div>
                  </div>  
                  <br/><br/>                
	                <div class="form-group">
	                    <div class="input-group">
	                        <div class="input-group-addon">图片宽度</div>
	                        <input type="text" id="bannerImageWith" value="" name="imageWidth" class="form-control" style="width: 450px;">
	                    </div>
	                </div>
	                <br/><br/>
	                <div class="form-group">
	                    <div class="input-group">
	                        <div class="input-group-addon">图片高度</div>
	                        <input type="text" id="bannerImageHeight" value="" name="imageHeight" class="form-control" style="width: 450px;">
	                    </div>
	                </div>
					<br/><br/>
	                 <div class="form-group">
	                    <div class="input-group">
	                        <div class="input-group-addon">状态&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
	                        <select id="bannerStatus" value="" name="status" class="form-control" style="width: 450px;">
	           					<option value="1" selected="selected">启用</option>
	          					<option value="2">停用</option>
	          				</select>
	                    </div>
	                  </div>                    
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" onclick="submitBannerForm()">保存</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
function query_Banner_list(id, url) {
        $(id).bootstrapTable('refresh', {url: url});
    }  
     function Banner_query_params(params) {
        $('#Banner_list_form :input').each(function() {
            if($(this).attr('name')) {
                params[$(this).attr('name')] = $(this).val();
            }
        });
        return params;
    }
    function banner_operation_format(value, item) {
        return '<button type="button" class="btn btn-link" onclick="openUpdateBannerWin('+value+', \''+item.accounts+'\', \''+item.imageUrl+'\')">编辑</button>';
    }
    function submitBannerForm() {
    <!-- 使用ajax发送请求信息 -->
        $.ajax({
            url :  "/m/mall/banner/saveOrUpdateBanner",
            type : "POST",
            data: $('#BannerForm').serialize(),
            success:function(data,ts,xhr){
                if(data.success){
                    $('#Banner_model').modal('hide');
                    success(data.msg);
                }else{
                    error("error");
                }
                $('#Banner_list_grid').bootstrapTable('refresh');
            }
        });
    }
    function openCreateBannerWin() {
        $('#bannerId').attr('readonly', true);
        $('#BannerForm')[0].reset();
        $('#Banner_model').modal('show');
    }     
    
    function openUpdateBannerWin(id,accounts,imageURL) {
        $('#bannerId').attr('readonly', true);
        $('#BannerForm')[0].reset();
        setFormValue('BannerForm', $('#Banner_list_grid').bootstrapTable('getRowByUniqueId', id));
        $('#Banner_model').modal('show');
    }    
    function batch_delete_banner(id) {
		var data = {};
		if(id) {
			data.id = id;
		} else {
			data = $('#Banner_list_grid').bootstrapTable('getSelections');
		}
		if(!data || data.length <= 0) {
			error('请选择banner！');
			return;
		}
		var ids = [];
		$(data).each(function(idx, item) {
			ids.push(item.id);
		});
		var ajaxData = {
			ids: ids.join(';')
		};
		openDialog({
			title: '删除',
			message: '您确认要删除这些banner吗？',
			okAction: function() {
				closeDialog();
				$.ajax({
					type: "post",
					url: "/m/mall/banner/deleteBanner",
					async: true,
					data: ajaxData,
					success: function(data) {
						if(data.success) {
							success(data.msg);
						} else {
							error("error");
						}
						$('#Banner_list_grid').bootstrapTable('refresh');
					}
				});
			}
		})
	}  
    var banner_type = {'1': '关键字', '2': '跳转链接'};
	function formatter_type(cellvalue, rowObject) {
		if(banner_type[cellvalue]) {
			cellvalue = banner_type[cellvalue];
		}
		return cellvalue;
	}
	var banner_status = {'1': '启用', '2': '停用'};
	function formatter_status(cellvalue, rowObject) {
		if(banner_status[cellvalue]) {
			cellvalue = banner_status[cellvalue];
		}
		return cellvalue;
	}        
</script>