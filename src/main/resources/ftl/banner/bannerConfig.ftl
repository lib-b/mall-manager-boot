<ul class="nav nav-tabs">
	<li>
		<a onclick="callFunc('/mall/banner/banner.html')" href="#">banner列表</a>
	</li>
	<li class="active">
		<a onclick="callFunc('/mall/banner/bannerConfig.html')" href="#">banner配置</a>
	</li>
</ul>
<form class="form-inline" id="Banner_list_form">
	<div class="form-group">
		<div class="input-group">
			<div class="input-group-addon"> 位置名称</div>
			<select id="bannerNameList" class="form-control" name="bannerId">
			</select>
		</div>
	</div>
	<div class="form-group">
		<div class="input-group">
			<div class="input-group-addon"> 配置名称</div>
			<input type="text" class="form-control" name="bannerConfigName" />
		</div>
	</div>
	<div class="form-group">
		<div class="input-group">
			<div class="input-group-addon"> 适用账号</div>
			<select class="form-control" name="accounts">
				<option value="0" selected="selected">全部</option>
				<option value="1">个人账号</option>
				<option value="2">企业账号</option>
				<option value="3">服务账号</option>
				<option value="4">运维账号</option>
				<option value="5">未登录账号</option>
			</select>
		</div>
	</div>
	<div class="input-group">
		<div class="input-group-addon"> 状态</div>
		<select id="status" name="status" class="form-control">
			<option value="0" selected="selected">全部</option>
			<option value="1">启用</option>
			<option value="2">停用</option>
		</select>
	</div>
	<div class="form-group">
		<div class="input-group">
			<div class="input-group-addon">生效时间</div>
			<input type="text" class="form-control" name="startTime" id="start_time" />
			<div class="input-group-addon">-</div>
			<input type="text" class="form-control" name="endTime" id="end_time" />
		</div>
	</div>
	<button type="button" class="btn btn-primary" onclick="query_Banner_list('#Banner_list_grid', '/m/mall/banner/bannerConfigList.json')">查询</button>
	<button type="button" class="btn btn-primary" onclick="openCreateBannerWin()">新增</button>
	<button type="button" class="btn btn-primary" onclick="batch_delete_bannerConfig()">删除选中配置</button>
</form>
<table id="Banner_list_grid" data-toggle="table" data-toolbar="#Banner_list_form" data-unique-id="id" data-query-params="Banner_query_params" data-click-to-select="true" data-page-size="5">
	<thead>
		<tr>
			<th data-valign="middle" data-checkbox="true">全选</th>
			<th data-field="id">配置ID</th>
			<th data-field="bannerName">位置名称</th>
			<th data-field="name">配置名称</th>
			<th data-field="sort">顺序号</th>
			<th data-field="remark">说明</th>
			<th data-field="imageUrl" data-visible="false">图片URL</th>
			<th data-field="content">跳转链接URL或关键字</th>
			<th data-field="accounts" data-formatter="banner_formatter_accounts">适用账号范围</th>
			<th data-field="startTime" data-formatter="log_formatter_time">启用时间</th>
			<th data-field="endTime" data-formatter="log_formatter_time">停用时间</th>
			<th data-valign="middle" data-field="id" data-align="center" data-formatter="banner_operation_format" class="action-buttons" data-width="130">操作</th>
		</tr>
	</thead>
</table>
<div id="Banner_model" class="modal fade" role="dialog" aria-labelledby="Banner_model_title">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="banner_model_title">新增配置</h4>
			</div>
			<div class="modal-body">
				<form class="form-inline" id="BannerForm">
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">配置ID&nbsp;&nbsp;&nbsp;&nbsp;</div>
							<input type="text" id="bannerConfigId" value="" name="id" class="form-control" style="width: 450px;">
						</div>
					</div>
					<br/><br/>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">位置名称</div>
							<select id="bannerId" value="" name="bannerId" class="form-control" style="width: 450px;">
							</select>
						</div>
					</div>
					<br/><br/>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">配置名称</div>
							<input type="text" id="bannerName" value="" name="name" class="form-control" style="width: 450px;">
						</div>
					</div>
					<br/><br/>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">顺序号&nbsp;&nbsp;&nbsp;&nbsp;</div>
							<input type="text" id="sort" value="" name="sort" class="form-control" style="width: 450px;">
						</div>
					</div>
					<br/><br/>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">图片URL&nbsp;</div>
							<input type="text" id="bannerImageURL" value="" name="imageUrl" class="form-control" style="width: 450px;">
						</div>
					</div>
					<div id="container">
						<a class="btn btn-primary" id="pickfiles" style="width:160px" href="#">
							<i class="glyphicon glyphicon-plus"></i>
							<span>选择图片</span>
						</a>
						<a class="btn btn-primary" id="up_load_banner" style="width:160px" href="#">
							<span>确认上传</span>
						</a>
					</div>
					<br/>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">URL/关键字</div>
							<input type="text" id="bannerTypeContent" value="" name="content" class="form-control" style="width: 400px;">
						</div>
					</div>
					<br/><br/>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">适用账号</div>
							<select id="bannerAccounts" name="accounts" multiple="multiple" size="5" class="form-control" style="width: 450px;">
								<option value="1">个人账号</option>
								<option value="2">企业账号</option>
								<option value="3">服务账号</option>
								<option value="4">运维账号</option>
								<option id="未登录账号" value="5">未登录账号</option>
							</select>
						</div>
					</div>
					<br/><br/>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">启用时间</div>
							<input type="text" id="startTime" value="" name="startTime" class="form-control" style="width: 450px;">
						</div>
					</div>
					<br/><br/>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">停用时间</div>
							<input type="text" id="endTime" value="" name="endTime" class="form-control" style="width: 450px;">
						</div>
					</div>
					<br/><br/>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">状态&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
							<select id="bannerStatus" value="" name="status" class="form-control" style="width: 450px;">
								<option value="1" selected="selected">启用</option>
								<option value="2">停用</option>
							</select>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary" onclick="submitBannerForm()">保存</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div id="BannerUpdate_model" class="modal fade" role="dialog" aria-labelledby="BannerUpdate_model_title">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="bannerUpdate_model_title">更新配置</h4>
			</div>
			<div class="modal-body">
				<form class="form-inline" id="BannerUpdateForm">
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">配置ID&nbsp;&nbsp;&nbsp;&nbsp;</div>
							<input type="text" id="bannerConfigUpdateId" value="" name="id" class="form-control" style="width: 450px;">
						</div>
					</div>
					<br/><br/>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">位置名称</div>
							<input type="text" id="bannerNameUpdate" value="" name="bannerName" class="form-control" style="width: 450px;">
						</div>
					</div>
					<br/><br/>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">配置名称</div>
							<input type="text" id="bannerName" value="" name="name" class="form-control" style="width: 450px;">
						</div>
					</div>
					<br/><br/>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">顺序号&nbsp;&nbsp;&nbsp;&nbsp;</div>
							<input type="text" id="sort" value="" name="sort" class="form-control" style="width: 450px;">
						</div>
					</div>
					<br/><br/>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">图片URL&nbsp;</div>
							<input type="text" id="bannerImageURLUpdate" value="" name="imageUrlUpdate" class="form-control" style="width: 450px;">
						</div>
					</div>
					<div id="containerUpdate">
						<a class="btn btn-primary" id="pickfilesUpdate" style="width:160px" href="#">
							<i class="glyphicon glyphicon-plus"></i>
							<span>选择图片</span>
						</a>
						<a class="btn btn-primary" id="up_load_banner_update" style="width:160px" href="#">
							<span>确认上传</span>
						</a>
					</div>
					<br/>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">URL/关键字</div>
							<input type="text" id="bannerTypeContent" value="" name="content" class="form-control" style="width: 400px;">
						</div>
					</div>
					<br/><br/>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">适用账号</div>
							<select id="bannerUpdateAccounts" name="bannerConfigAccounts" multiple="multiple" size="5" class="form-control" style="width: 450px;">
							</select>
						</div>
					</div>
					<br/><br/>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">启用时间</div>
							<input type="text" id="startTimeUpdate" value="" name="startTime" class="form-control" style="width: 450px;">
						</div>
					</div>
					<br/><br/>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">停用时间</div>
							<input type="text" id="endTimeUpdate" value="" name="endTime" class="form-control" style="width: 450px;">
						</div>
					</div>
					<br/><br/>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">状态&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
							<select id="bannerStatus" value="" name="status" class="form-control" style="width: 450px;">
								<option value="1" selected="selected">启用</option>
								<option value="2">停用</option>
							</select>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary" onclick="submitBannerUpdateForm()">保存</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script src="https://cdn.staticfile.org/qiniu-js-sdk/1.0.14-beta/qiniu.min.js"></script>
<script src="//cdn.bootcss.com/plupload/2.1.9/plupload.full.min.js"></script>
<script type="text/javascript">
	var token;
	$(document).ready(function() {
		var date = new Date();
		///日期控件
		$('#start_time,#end_time,#startTimeUpdate,#endTimeUpdate,#startTime,#endTime').datetimepicker({
			startDate: date
		});
		addProductTypeItems("#bannerNameList", "/m/mall/banner/allBannerList", 1);
		$.ajax({
			url: "/m/mall/product/uploadToken",
			type: "POST",
			async: false,
			success: function(data, ts, xhr) {
				token = data.uptoken;
			}
		});
	});

	function addProductTypeItems(id, url, all) {
		$.ajax({
			url: url,
			type: "post",
			dataType: "json",
			contentType: "application/json",
			traditional: true,
			success: function(data) {
				$(id).empty();
				if(all == 1) {
					$(id).append("<option value='0' selected='selected'>全部</option>");
				}
				for(var i in data) {
					$(id).append("<option value=\"" + data[i].id + "\" >" + data[i].name + "</option>");
				}
			},
			error: function(msg) {
				alert("出错了！");
			}
		});
	};

	function query_Banner_list(id, url) {
		$(id).bootstrapTable('refresh', { url: url });
	}

	function Banner_query_params(params) {
		$('#Banner_list_form :input').each(function() {
			if($(this).attr('name')) {
				params[$(this).attr('name')] = $(this).val();
			}
		});
		return params;
	}

	function banner_operation_format(value, item) {
		return '<button type="button" class="btn btn-link" onclick="openUpdateBannerWin(' + value + ', \'' + item.accounts + '\', \'' + item.imageUrl + '\')">编辑</button>';
	}

	function submitBannerForm() {
		if(($('#startTime').val() == "") || ($('#endTime').val() == "")) {
			error("请选择启用和停用时间！");
		} else {
			<!-- 使用ajax发送请求信息 -->
			$.ajax({
				url: "/m/mall/banner/saveBannerConfig",
				type: "POST",
				data: $('#BannerForm').serialize(),
				success: function(data, ts, xhr) {
					if(data.success) {
						$('#Banner_model').modal('hide');
						success(data.msg);
					} else {
						error("error");
					}
					$('#Banner_list_grid').bootstrapTable('refresh');
				}
			});
		}
	}

	function submitBannerUpdateForm() {
		if(($('#startTimeUpdate').val() == "") || ($('#endTimeUpdate').val() == "")) {
			error("请选择启用和停用时间！");
		} else {
			$.ajax({
				url: "/m/mall/banner/updateBannerConfig",
				type: "POST",
				data: $('#BannerUpdateForm').serialize(),
				success: function(data, ts, xhr) {
					if(data.success) {
						$('#BannerUpdate_model').modal('hide');
						success(data.msg);
					} else {
						error("error");
					}
					$('#Banner_list_grid').bootstrapTable('refresh');
				}
			});
		}
	}

	function openCreateBannerWin() {
		$('#bannerConfigId').attr('readonly', true);
		$('#BannerForm')[0].reset();
		addProductTypeItems("#bannerId", "/m/mall/banner/allBannerList", 0)
		$('#Banner_model').modal('show');
	}
	var uploader = Qiniu.uploader({
		runtimes: 'html5,flash,html4', //上传模式,依次退化
		browse_button: pickfiles, //上传选择的点选按钮，**必需**
		uptoken: token, //若未指定uptoken_url,则必须指定 uptoken ,uptoken由其他程序生成
		domain: 'res.eqh5.com', //bucket 域名，下载资源时用到，**必需**
		// save_key: false,   // 默认 false。若在服务端生成uptoken的上传策略中指定了 `sava_key`，则开启，SDK会忽略对key的处理
		get_new_uptoken: false, //设置上传文件的时候是否每次都重新获取新的token
		container: container, //上传区域DOM ID，默认是browser_button的父元素，
		max_file_size: '100mb', //最大文件体积限制
		flash_swf_url: '/plupload/js/Moxie.swf', //引入flash,相对路径
		max_retries: 1, //上传失败最大重试次数
		dragdrop: true, //开启可拖曳上传
		//drop_element: container,        //拖曳上传区域元素的ID，拖曳文件或文件夹后可触发上传
		chunk_size: '4mb', //分块上传时，每片的体积
		auto_start: false, //选择文件后自动上传，若关闭需要自己绑定事件触发上传
		init: {
			'FilesAdded': function(up, files) {
				plupload.each(files, function(file) {
					document.getElementById("bannerImageURL").value = file.name + "(" + plupload.formatSize(file.size) + ")";                
				});
			},
			'BeforeUpload': function(up, file) {
				// 每个文件上传前,处理相关的事情
			},
			'UploadProgress': function(up, file) {
				document.getElementById("bannerImageURL").value = file.name + "(" + plupload.formatSize(file.size) + ")" + file.percent + "%";
			},
			'FileUploaded': function(up, file, info) {
				var res = jQuery.parseJSON(info);
				document.getElementById("bannerImageURL").value = res.key;
			},
			'Error': function(up, err, errTip) {
				document.getElementById("bannerImageURL").value = err.code + ":" + err.message;
			},
			'UploadComplete': function() {
				success('success');
			},
			'Key': function(up, file) {
				var filedata = file.name.split(".");
				var type = filedata[filedata.length - 1];
				var key = 'store/' + getUuid() + "." + type;
				return key;
			}
		}
	});
	uploader.bind('FileUploaded', function() {
		console.log('hello man,a file is uploaded');
	});

	$('#up_load_banner').on('click', function() {
		uploader.start();
	});
	
	var uploaderUpdate = Qiniu.uploader({
		runtimes: 'html5,flash,html4', //上传模式,依次退化
		browse_button: pickfilesUpdate, //上传选择的点选按钮，**必需**
		uptoken: token, //若未指定uptoken_url,则必须指定 uptoken ,uptoken由其他程序生成
		domain: 'res.eqh5.com', //bucket 域名，下载资源时用到，**必需**
		// save_key: false,   // 默认 false。若在服务端生成uptoken的上传策略中指定了 `sava_key`，则开启，SDK会忽略对key的处理
		get_new_uptoken: false, //设置上传文件的时候是否每次都重新获取新的token
		container: containerUpdate, //上传区域DOM ID，默认是browser_button的父元素，
		max_file_size: '100mb', //最大文件体积限制
		flash_swf_url: '/plupload/js/Moxie.swf', //引入flash,相对路径
		max_retries: 1, //上传失败最大重试次数
		dragdrop: true, //开启可拖曳上传
		//drop_element: container,        //拖曳上传区域元素的ID，拖曳文件或文件夹后可触发上传
		chunk_size: '4mb', //分块上传时，每片的体积
		auto_start: false, //选择文件后自动上传，若关闭需要自己绑定事件触发上传
		init: {
			'FilesAdded': function(up, files) {
				plupload.each(files, function(file) {
					document.getElementById("bannerImageURLUpdate").value = file.name + "(" + plupload.formatSize(file.size) + ")";                
				});
			},
			'BeforeUpload': function(up, file) {
				// 每个文件上传前,处理相关的事情
			},
			'UploadProgress': function(up, file) {
				document.getElementById("bannerImageURLUpdate").value = file.name + "(" + plupload.formatSize(file.size) + ")" + file.percent + "%";
			},
			'FileUploaded': function(up, file, info) {
				var res = jQuery.parseJSON(info);
				document.getElementById("bannerImageURLUpdate").value = res.key;
			},
			'Error': function(up, err, errTip) {
				document.getElementById("bannerImageURLUpdate").value = err.code + ":" + err.message;
			},
			'UploadComplete': function() {
				success('success');
			},
			'Key': function(up, file) {
				var filedata = file.name.split(".");
				var type = filedata[filedata.length - 1];
				var key = 'store/' + getUuid() + "." + type;
				return key;
			}
		}
	});
	
	uploaderUpdate.bind('FileUploaded', function() {
		console.log('hello man,a file is uploaded');
	});

	$('#up_load_banner_update').on('click', function() {
		uploaderUpdate.start();
	});

	function openUpdateBannerWin(id, accounts, imageURL) {
		$('#bannerConfigUpdateId').attr('readonly', true);
		$('#bannerNameUpdate').attr('readonly', true);
		$('#BannerUpdateForm')[0].reset();
		setAccountStatus(accounts);
		setImageURL(imageURL);
		setFormValue('BannerUpdateForm', $('#Banner_list_grid').bootstrapTable('getRowByUniqueId', id));
		$('#BannerUpdate_model').modal('show');
	}

	var banner_accounts = { '1': '个人账号', '2': '企业账号', '3': '服务账号', '4': '运维账号', '5': '未登录账号' };

	function banner_formatter_accounts(value) {
		var result = "";
		var accountId = value.split(";");
		for(var i = 0; i < accountId.length - 1; i++) {
			result = result + banner_accounts[accountId[i]] + ";";
		}
		return result;
	}

	function setAccountStatus(accounts) {
		$('#bannerUpdateAccounts').empty();
		for(var key in banner_accounts) {
			if(accounts.indexOf(key) >= 0) {
				$('#bannerUpdateAccounts').append("<option value=\"" + key + "\" selected='selected'>" + banner_accounts[key] + "</option>");
			} else {
				$('#bannerUpdateAccounts').append("<option value=\"" + key + "\">" + banner_accounts[key] + "</option>");
			}
		}
	}

	function setImageURL(imageURL) {
		$('#bannerImageURLUpdate').val(imageURL);
	}

	function batch_delete_bannerConfig(id) {
		var data = {};
		if(id) {
			data.id = id;
		} else {
			data = $('#Banner_list_grid').bootstrapTable('getSelections');
		}
		if(!data || data.length <= 0) {
			error('请选择配置！');
			return;
		}
		var ids = [];
		$(data).each(function(idx, item) {
			ids.push(item.id);
		});
		var ajaxData = {
			ids: ids.join(';')
		};
		openDialog({
			title: '删除',
			message: '您确认要删除这些配置吗？',
			okAction: function() {
				closeDialog();
				$.ajax({
					type: "post",
					url: "/m/mall/banner/deleteBannerConfig",
					async: true,
					data: ajaxData,
					success: function(data) {
						if(data.success) {
							success(data.msg);
						} else {
							error("error");
						}
						$('#Banner_list_grid').bootstrapTable('refresh');
					}
				});
			}
		})
	}

	function getUuid() {
		var len = 32; //32长度
		var radix = 16; //16进制
		var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');
		var uuid = [],
			i;
		radix = radix || chars.length;
		if(len) {
			for(i = 0; i < len; i++)
				uuid[i] = chars[0 | Math.random() * radix];
		} else {
			var r;
			uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
			uuid[14] = '4';
			for(i = 0; i < 36; i++) {
				if(!uuid[i]) {
					r = 0 | Math.random() * 16;
					uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r];
				}
			}
		}
		return uuid.join('');
	}

	function log_formatter_time(value) {
		return new Date(value).format('yyyy-MM-dd hh:mm:ss');
	}
	var banner_type = { '1': '关键字', '2': '跳转链接' };

	function formatter_type(cellvalue, rowObject) {
		if(banner_type[cellvalue]) {
			cellvalue = banner_type[cellvalue];
		}
		return cellvalue;
	}
</script>