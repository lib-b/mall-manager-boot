<form class="form-inline" id="brand_list_form">
	<div class="form-group">
		<div class="input-group">
            <div class="input-group-addon"> 商标名称</div>
            <input type="text" class="form-control" name="name"/>
        </div>
	</div>
	<br/>
	<button type="button" class="btn btn-primary" onclick="query_brand_list('#brand_list_grid', '/m/mall/brand/brand/list.json')" >查询</button>
    <button type="button" class="btn btn-primary" onclick="openCreatebrandWin()">新增商标</button>
    <button type="button" class="btn btn-primary" onclick="batch_delete_brand()">删除选中商标</button>
</form>
<table id="brand_list_grid" data-toggle="table" data-toolbar="#brand_list_form" data-unique-id="id" data-query-params="brand_query_params" data-click-to-select="true" data-page-size="5">
	<thead>
	    <tr>
	        <th data-valign="middle" data-checkbox="true">全选</th>
	    	<th data-field="id">ID</th>
	    	<th data-field="name">商标名称</th>
	    	<th data-field="description">商标描述</th>
	    	<th data-field="url">商标url</th>
	    	<th data-field="logo">商标logo</th>
	    	<th data-field="createTime" data-formatter="log_formatter_time">创建时间</th>
	    	<th data-field="createUser">创建者</th>
	    	<th data-field="divide">分成比例</th>
	    	<th data-valign="middle" data-field="id" data-align="center" data-formatter="business_operation_format" class="action-buttons" data-width="130">操作</th>				
	    </tr>
    </thead>
</table>
<!-- 新增产品业务的模型，主要是列出来需要的属性，这个模块在两个地方会用，一个是点击新增产品业务按钮，另一个是点击产品列表中的编辑。 -->
<div id="brand_model" class="modal fade" role="dialog" aria-labelledby="brand_model_title">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="brand_model_title">商标业务</h4>
            </div>
            <div class="modal-body">
                <form class="form-inline" id="brandForm">
                   <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">商标ID&nbsp;&nbsp;&nbsp;&nbsp;</div>
                            <input type="text" id="brandId" value="" name="id" class="form-control" style="width: 450px;">
                        </div>
                    </div>
                    <br/><br/>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">商标名称</div>
                            <input type="text" id="brandName" value="" name="name" class="form-control" style="width: 450px;">
                        </div>
                    </div>
                    <br/><br/>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">商标描述</div>
                            <input type="text" id="brandDescription" value="" name="description" class="form-control" style="width: 450px;">
                        </div>
                    </div>
                                        <br/><br/>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">商标url&nbsp;&nbsp;&nbsp;</div>
                            <input type="text" id="brandURL" value="" name="url" class="form-control" style="width: 450px;">
                        </div>
                    </div>
                    <br/><br/>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">商标logo&nbsp;</div>
                            <input type="text" id="brandLogo" value="" name="logo" class="form-control" style="width: 450px;">
                        </div>
                    </div> 
                    <br/><br/>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">分成比例</div>
                            <input type="text" id="brandDivide" value="" name="divide" class="form-control" style="width: 450px;">
                        </div>
                    </div>   
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" onclick="submitbrandForm()">保存</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
//查询
    function query_brand_list(id, url) {
        $(id).bootstrapTable('refresh', {url: url});
    }  
     function brand_query_params(params) {
        $('#brand_list_form :input').each(function() {
            if($(this).attr('name')) {
                params[$(this).attr('name')] = $(this).val();
            }
        });
        return params;
    }
    function business_operation_format(value, item) {
        return '<button type="button" class="btn btn-link" onclick="openUpdatebrandWin('+value+')">编辑</button>';
    }
    function submitbrandForm() {
    <!-- 使用ajax发送请求信息 -->
        $.ajax({
            url :  "/m/mall/brand/brand/save",
            type : "POST",
            data: $('#brandForm').serialize(),
            success:function(data,ts,xhr){
                if(data.success){
                    $('#brand_model').modal('hide');
                    success(data.msg);
                }else{
                    error(data.msg);
                }
                $('#brand_list_grid').bootstrapTable('refresh');
            }
        });
    }
     function openCreatebrandWin() {
        $('#brandId').attr('readonly', true);
        $('#brandForm')[0].reset();
        $('#brand_model').modal('show');
    }
    
    function openUpdatebrandWin(id) {
        $('#brandId').attr('readonly', true);
        $('#brandForm')[0].reset();
        setFormValue('brandForm', $('#brand_list_grid').bootstrapTable('getRowByUniqueId', id));
        $('#brand_model').modal('show');
    }

    function log_formatter_time(value) {
       return new Date(value).format('yyyy-MM-dd hh:mm:ss');
    }
    
    function batch_delete_brand(id) {
		var data = {};
		if(id) {
			data.id = id;
		} else {
			data = $('#brand_list_grid').bootstrapTable('getSelections');
		}
		if(!data || data.length <= 0) {
			error('请选择商标！');
			return;
		}
		var ids = [];
		$(data).each(function(idx, item) {
			ids.push(item.id);
		});
		var ajaxData = {
			ids: ids.join(';')
		};
		openDialog({
			title: '删除',
			message: '您确认要删除这些商标吗？',
			okAction: function() {
				closeDialog();
				$.ajax({
					type: "post",
					url: "/m/mall/brand/brand/delete",
					async: true,
					data: ajaxData,
					success: function(data) {
						if(data.success) {
							success(data.msg);
						} else {
							error("error");
						}
						$('#brand_list_grid').bootstrapTable('refresh');
					}
				});
			}
		})
	}
</script>