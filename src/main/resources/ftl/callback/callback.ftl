<form class="form-inline" id="callback_list_form">
	<div class="form-group">
		<div class="input-group">
            <div class="input-group-addon"> 回执ID</div>
            <input type="text" class="form-control" name="id"/>
        </div>
	</div>
	<div class="form-group">
		<div class="input-group">
            <div class="input-group-addon"> 订单ID</div>
            <input type="text" class="form-control" name="orderId"/>
        </div>
	</div>
	<div class="form-group">
		<div class="input-group">
            <div class="input-group-addon"> 商品ID</div>
            <input type="text" class="form-control" name="productId"/>
        </div>
	</div>
	<div class="form-group">
		<div class="input-group">
            <div class="input-group-addon"> 创建者</div>
            <input type="text" class="form-control" name="createUser"/>
        </div>
	</div>
	<div class="form-group">
        <div class="input-group">
            <div class="input-group-addon">创建时间</div>
            <input type="text" class="form-control" name="createStartTime" id="create_time"/>
             <div class="input-group-addon">-</div>
            <input type="text" class="form-control" name="createEndTime" id="end_time"/>
        </div>
    </div>
	<div class="form-group">
		<div class="input-group">
            <div class="input-group-addon"> 商品类型</div>
            <select name="type" class="form-control">
				<option value="0" selected="selected">全部</option>
				<option value="1">图片</option>
				<option value="2">场景</option>
				<option value="3">字体</option>
				<option value="4">音乐</option>
			</select>
        </div>
	</div>
	<div class="form-group">
		<div class="input-group">
            <div class="input-group-addon">付费</div>
            <select name="tee" class="form-control">
				<option value="0" selected="selected">全部</option>
				<option value="1">已付费</option>
				<option value="2">未付费</option>				
			</select>
        </div>
	</div>
	<div class="form-group">
		<div class="input-group">
            <div class="input-group-addon">出错信息</div>
            <select name="errorMsg" class="form-control">
				<option value="全部" selected="selected">全部</option>
				<option value="样例模板不存在">样例模板不存在</option>
				<option value="订单回调异步创建异常">订单回调异步创建异常</option>
				<option value="免费使用次数已用完">免费使用次数已用完</option>
				<option value="未定义商品类型">未定义商品类型</option>
				<option value="字体不存在">字体不存在</option>				
			</select>
        </div>
	</div>	
	<div class="input-group">
		<div class="input-group-addon"> 状态</div>
			<select id="callbackStatus" name="status" class="form-control">
				<option value="-1" selected="selected">全部</option>
				<option value="0">初创订单</option>
				<option value="1">执行结束</option>
				<option value="2">执行出错</option>
			</select>
		</div>
	</div>	
	<button type="button" class="btn btn-primary" onclick="query_callback_list('#callback_list_grid', '/m/mall/callback/list.json')" >查询</button>
</form>
<table id="callback_list_grid" data-toggle="table" data-toolbar="#callback_list_form" data-unique-id="id" data-query-params="callback_query_params" data-click-to-select="true" data-page-size="5">
	<thead>
	    <tr>
	        <!--<th data-valign="middle" data-checkbox="true">全选</th>-->
	    	<th data-field="id">ID</th>
	    	<th data-field="orderId">订单ID</th>
	    	<th data-field="productId"  data-formatter="tpl_formatter_id">商品ID</th>
	    	<th data-field="type" data-formatter=formatter_type>商品类型</th>
	    	<th data-field="status"  data-formatter=formatter_status>状态</th>
	    	<th data-field="externalId" data-visible="false">样例oracle端ID</th>
	    	<th data-field="source" data-formatter=formatter_source>来源</th>
	    	<th data-field="createUser">创建者</th>	    	
	    	<th data-field="createTime" data-formatter="log_formatter_time">创建时间</th>
	    	<th data-field="updateUser">更新者</th>
	    	<th data-field="updateTime" data-formatter="log_formatter_time">修改时间</th>
	    	<th data-valign="middle" data-field="id" data-align="center" data-formatter="business_operation_format" class="action-buttons" data-width="130">操作</th>				
	    </tr>
    </thead>
</table>
<!-- 新增产品业务的模型，主要是列出来需要的属性，这个模块在两个地方会用，一个是点击新增产品业务按钮，另一个是点击产品列表中的编辑。 -->
<div id="callback_model" class="modal fade" role="dialog" aria-labelledby="callback_model_title">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="callback_model_title">回执业务</h4>
            </div>
            <div class="modal-body">
                <form class="form-inline" id="callbackForm">
                   <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">回执ID&nbsp;&nbsp;&nbsp;</div>
                            <input type="text" id="callbackId" value="" name="id" class="form-control" style="width: 450px;">
                        </div>
                    </div>
                    <br/><br/>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">回执状态</div>
                            <select id="callbackStatus" name="status" class="form-control" style="width: 450px;">
								<option value="0">初创订单</option>
								<option value="1">执行结束</option>
								<option value="2">执行出错</option>
							</select>
                        </div>
                    </div>
                    <br/><br/>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">出错信息</div>
                            <input type="text" id="callbackErrorMsg" value="" name="errorMsg" class="form-control" style="width: 450px;">
                        </div>
                    </div>              
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" onclick="submitcallbackForm()">保存</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
//查询
    function query_callback_list(id, url) {
        $(id).bootstrapTable('refresh', {url: url});
    }  
     function callback_query_params(params) {
        $('#callback_list_form :input').each(function() {
            if($(this).attr('name')) {
                params[$(this).attr('name')] = $(this).val();
            }
        });
        return params;
    }
    function business_operation_format(value, item) {
        return '<button type="button" class="btn btn-link" onclick="openUpdateCallbackWin('+value+')">编辑</button>';
    }
    function submitcallbackForm() {
    <!-- 使用ajax发送请求信息 -->
        $.ajax({
            url :  "/m/mall/callback/update",
            type : "POST",
            data: $('#callbackForm').serialize(),
            success:function(data,ts,xhr){
                if(data.success){
                    $('#callback_model').modal('hide');
                    success(data.msg);
                }else{
                    error("error");
                }
                $('#callback_list_grid').bootstrapTable('refresh');
            }
        });
    }
    
    function openUpdateCallbackWin(id) {
        $('#callbackId').attr('readonly', true);
        $('#callbackOrderId').attr('readonly', true);
        $('#callbackProductId').attr('readonly', true);
        $('#callbackType').attr('readonly', true);

        
        $('#callbackForm')[0].reset();
        setFormValue('callbackForm', $('#callback_list_grid').bootstrapTable('getRowByUniqueId', id));
        $('#callback_model').modal('show');
    }

    function log_formatter_time(value) {
       return new Date(value).format('yyyy-MM-dd hh:mm:ss');
    }
    
	function tpl_formatter_id(value, o) {
		return '<a target="_blank" href="http://store.eqxiu.com/'+o.productId+'.html">'+value+'</a>';
	}
	var callback_status = {
		'0': '初创订单',
		'1': '执行结束',
		'2': '执行出错'
	};

	function formatter_status(cellvalue, rowObject) {
		if(callback_status[cellvalue]) {
			cellvalue = callback_status[cellvalue];
		}
		return cellvalue;
	}
	var callback_source = {
		'1': '商城',
		'2': '编辑器'
	};

	function formatter_source(cellvalue, rowObject) {
		if(callback_source[cellvalue]) {
			cellvalue = callback_source[cellvalue];
		}
		return cellvalue;
	}
	
	var product_type = {
		'1': '图片',
		'2': '场景',
		'3': '字体',
		'4': '音乐'
	};

	function formatter_type(cellvalue, rowObject) {
		if(product_type[cellvalue]) {
			cellvalue = product_type[cellvalue];
		}
		return cellvalue;
	}
	$(document).ready(function(){
        ///日期控件
        $('#create_time,#end_time').datetimepicker();
    });
</script>