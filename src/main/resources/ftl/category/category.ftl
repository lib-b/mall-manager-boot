<div class="row">
	<div class="col-xs-12">
		<ul class="nav nav-tabs" style="padding-bottom:12px;">
			<li class="active">
				<a onclick="callFunc('/mall/category/category.html')" href="#">分类列表</a>
			</li>
			<li>
				<a onclick="callFunc('/mall/category/category/categoryDetail.html')" href="#">分类详情</a>
			</li>
		</ul>
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-hover tree_table" id="treeTable">
				<tr>
					<th class="center">分类名称</th>
					<th class="center">分类ID</th>
					<th class="center">分类描述</th>
					<th class="center">操作</th>
				</tr>
				<#list funcs as func>
					<tr id="${func.id}" pId="${func.parentId}" <#if func.haschild>haschild="true"</#if>>
						<td>${func.name}</td>
						<td>${func.id}</td>
						<td>${func.description}</td>
						<td>
						<a onclick="callFunc('/mall/category/category/categoryDetail.html?categoryId=${func.id}')" href="#" style="padding-right:5px;">详情</a>	
						</td>						
					</tr>
				</#list>
			</table>
		</div>	
    </div>
</div>
<script type="text/javascript">
	var files = [];
	files[0] = "${assets}/assets/jquery-treeTable/jquery.treeTable.min.js";
	var option = {
        expandLevel : 1,
        beforeExpand : function($treeTable, id) {
            //判断id是否已经有了孩子节点，如果有了就不再加载，这样就可以起到缓存的作用
            if ($('.' + id, $treeTable).length) { return; }
            //这里的html可以是ajax请求          
             $.ajax({
            url :  "/m/mall/category/category/children",
            type : "POST",
            data: {
              categoryId:id
            },
            success:function(data,ts,xhr){
                var html="";
                
               // alert(data.length);
                for(var i in data){
                	var row ="";
                    var url = "'"+"/mall/category/category/categoryDetail.html?categoryId="+data[i].id+"'";  
                    if(data[i].haschild){                               
                       row += '<tr id='+data[i].id+' pId='+id+' haschild="true">';                      
                       row += '<td>'+data[i].name+'</td>';
                       row += '<td>'+data[i].id+'</td>';
                       row += '<td>'+data[i].description+'</td>';
                       row += '<td><a onclick="callFunc('+url+')" href="#" style="padding-right:5px;">详情</a></td>';
                       row += '</tr>';
                    }else{
                       row += '<tr id='+data[i].id+' pId='+id+'>';             
                       row += '<td>'+data[i].name+'</td>';
                       row += '<td>'+data[i].id+'</td>';
                       row += '<td>'+data[i].description+'</td>';
                       row += '<td><a onclick="callFunc('+url+')" href="#" style="padding-right:5px;">详情</a></td>';
                       row += '</tr>';
                    } 
                    html = html+row;                 
                }
                $treeTable.addChilds(html);              
            }
          })                      
        },
        onSelect : function($treeTable, id) {
            window.console && console.log('onSelect:' + id);   
        }
    };
	$(document).ready(function(){
		include(files,function(){
			$("#treeTable").treeTable(option);
		});
	});
</script>