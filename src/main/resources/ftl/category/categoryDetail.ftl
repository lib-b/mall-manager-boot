<div class="row">
	<div class="col-xs-12">
		<ul class="nav nav-tabs" style="padding-bottom:12px;">
			<li>
				<a onclick="callFunc('/mall/category/category.html')" href="#">分类列表</a>
			</li>
			<li class="active">
				<a onclick="callFunc('/mall/category/category/categoryDetail.html')" href="#">分类详情</a>
			</li>
		</ul>
	</div>
</div>
<form class="form-inline" id="category_list_form">
	<div class="form-group">
		<div class="input-group">
			<div class="input-group-addon"> 分类ID</div>
			<input type="text" class="form-control" name="id" />
		</div>
		<div class="input-group">
			<div class="input-group-addon"> 分类名称</div>
			<input type="text" class="form-control" name="name" />
		</div>
		<div class="input-group">
			<div class="input-group-addon"> 分类父ID</div>
			<input type="text" class="form-control" name="parentId" />
		</div>
		<div class="input-group">
			<div class="input-group-addon"> 分类状态</div>
			<select class="form-control" name="status">
				<option value="0" selected="selected">全部</option>
				<option value="1">可用</option>
				<option value="2">不可用</option>
			</select>
		</div>
		<div class="input-group">
			<div class="input-group-addon"> 排序&nbsp;&nbsp;&nbsp;&nbsp;</div>
			<select class="form-control" name="orderByClause">
				<option value="desc">降序</option>
				<option value="asc">升序</option>
			</select>
		</div>
	</div>
	<br/>
	<button type="button" class="btn btn-primary" onclick="query_category_list('#category_list_grid', '/m/mall/category/category/list.json',0)">查询</button>
	<button type="button" class="btn btn-primary" onclick="openCreatecategoryWin()" style="margin-right: 20px;">新增分类业务</button>
</form>
<table id="category_list_grid" data-toggle="table" data-toolbar="#category_list_form" data-unique-id="id" data-query-params="category_query_params" data-click-to-select="true" data-page-size="5">
	<thead>
		<tr>
			<th data-field="id">ID</th>
			<th data-field="name">分类名称</th>
			<th data-field="description">分类描述</th>
			<th data-field="parentId">父ID</th>
			<th data-field="sort">排序</th>
			<th data-field="type" data-visible="false">类型</th>
			<th data-valign="middle" data-field="id" data-align="center" data-formatter="category_operation_format" class="action-buttons" data-width="130">操作</th>
		</tr>
	</thead>
</table>
<form class="form-inline" id="product_list_form">
	<div class="form-group">
		<div class="input-group">
			<div class="input-group-addon"> 分类ID</div>
			<input id="categoryDealId" type="text" class="form-control" name="id" />
		</div>
	</div>
	<div class="form-group">
		<div class="input-group">
			<div class="input-group-addon"> 商品ID</div>
			<input type="text" class="form-control" name="productIds" placeholder="多个用分号隔开" />
		</div>
	</div>
	<br/>
	<button type="button" class="btn btn-primary" onclick="query_category_list('#product_list_grid', '/m/mall/category/category/productlist',1)">查询商品</button>
	<button type="button" class="btn btn-primary" onclick="add_category_product('#product_list_grid', '/m/mall/category/category/addProduct')">新增商品</button>
	<button type="button" class="btn btn-primary" onclick="batch_delete_product()">删除选中商品</button>
</form>
<table id="product_list_grid" data-toggle="table" data-toolbar="#product_list_form" data-unique-id="id" data-query-params="product_query_params" data-click-to-select="true" data-page-size="5">
	<thead>
		<tr>
			<th data-valign="middle" data-checkbox="true">全选</th>
			<th data-field="productId" data-formatter="tpl_formatter_id">商品ID</th>
			<th data-field="categoryId" data-visible="false">分类ID</th>
			<th data-field="productTitle">商品标题</th>
			<th data-field="productStatus" data-formatter=formatter_status>商品状态</th>		
		</tr>
	</thead>
</table>
</div>
<!-- 新增产品业务的模型，主要是列出来需要的属性，这个模块在两个地方会用，一个是点击新增产品业务按钮，另一个是点击产品列表中的编辑。 -->
<div id="category_model" class="modal fade" role="dialog" aria-labelledby="category_model_title">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="category_model_title">新增分类业务</h4>
			</div>
			<div class="modal-body">
				<form class="form-inline" id="categoryForm">
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">分类名称</div>
							<input type="text" id="newCategoryName" value="" name="name" class="form-control" style="width: 450px;">
						</div>
					</div>
					<br/><br/>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">分类描述</div>
							<textarea id="categoryDescription" value="" name="description" class="form-control" style="width: 450px;" />
						</div>
					</div>
					<br/><br/>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">分类状态</div>
							<select id="categoryStatus" value="" name="status" class="form-control" style="width: 450px;">
								<option value="1">可用</option>
								<option value="2">不可用</option>
							</select>
						</div>
					</div>
					<br/><br/>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">父ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
							<input type="text" id="newCategoryParentId" value="" name="parentId" class="form-control" style="width: 450px;">
						</div>
					</div>
					<br/><br/>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">排序&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
							<input type="text" id="categorySort" value="" name="sort" class="form-control" style="width: 450px;">
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary" onclick="submitForm()">保存</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div id="categoryUpdate_model" class="modal fade" role="dialog" aria-labelledby="categoryUpdate_model_title">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="category_model_title">更新分类业务</h4>
			</div>
			<div class="modal-body">
				<form class="form-inline" id="categoryUpdateForm">
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">分类ID&nbsp;&nbsp;&nbsp;&nbsp;</div>
							<input type="text" id="categoryID" value="" name="id" class="form-control" style="width: 450px;">
						</div>
					</div>
					<br/><br/>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">分类名称</div>
							<input type="text" id="categoryName" value="" name="name" class="form-control" style="width: 450px;">
						</div>
					</div>
					<br/><br/>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">分类描述</div>
							<textarea id="categoryDescription" value="" name="description" class="form-control" style="width: 450px;" />
						</div>
					</div>
					<br/><br/>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">分类状态</div>
							<select id="categoryStatus" value="" name="status" class="form-control" style="width: 450px;">
								<option value="1">可用</option>
								<option value="2">不可用</option>
							</select>
						</div>
					</div>
					<br/><br/>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">父ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
							<input type="text" id="categoryParent_id" name="parentId" class="form-control" style="width: 450px;">
						</div>
					</div>
					<br/><br/>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">排序&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
							<input type="text" id="categorySort" value="" name="sort" class="form-control" style="width: 450px;">
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary" onclick="submitUpdateForm()">保存</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script type="text/javascript">
	$(document).ready(function() {
		setFormValue("category_list_form", ${formData});
		setFormValue("product_list_form", ${formData});
	});

	function log_formatter_time(value) {
		return new Date(value).format('yyyy-MM-dd hh:mm:ss');
	}
	//查询
	function query_category_list(id, url,type) {
	    if(type==1){
	       if($("#categoryDealId").val()==""){
	          error("请输入分类ID！")
	       }else{
	          $(id).bootstrapTable('refresh', {url: url});
	       }
	    }else{
	      $(id).bootstrapTable('refresh', {url: url});
	    }
		
	}

	function category_query_params(params) {
		$('#category_list_form :input').each(function() {
			if($(this).attr('name')) {
				params[$(this).attr('name')] = $(this).val();
			}
		});
		return params;
	}

	function product_query_params(params) {
		$('#product_list_form :input').each(function() {
			if($(this).attr('name')) {
				params[$(this).attr('name')] = $(this).val();
			}
		});
		return params;
	}

	function category_operation_format(value, item) {
		return '<button type="button" class="btn btn-link" onclick="openUpdatecategoryWin(' + value + ')">编辑</button>' +
			'<button type="button" class="btn btn-link" onclick="deletecategory(' + value + ', \'' + item.name + '\')">删除</button>';
	}

	function add_category_product(id, url) {
		<!-- 使用ajax发送请求信息 -->
		$.ajax({
			url: url,
			type: "POST",
			data: $('#product_list_form').serialize(),
			success: function(data, ts, xhr) {
				if(data.success) {
					success(data.msg);
				} else {
					error("error");
				}
				$('#product_list_grid').bootstrapTable('refresh');
			}
		});
	}

	function checkCategoryName(pid, name) {
		var result = "true";
		$.ajax({
			url: "/m/mall/category/category/children",
			type: "post",
			data: {
				categoryId: pid
			},
			async: false,
			success: function(data, ts, xhr) {
				for(var i in data) {
					if(data[i].name == name) {
						result = "false";
					}
				}
			}
		});
		return result;
	}

	function submitForm() {
		if(checkCategoryName($('#newCategoryParentId').val(), $('#newCategoryName').val()) == "false") {
			error("该父类下已经存在同名的分类，请重新输入名称！");
		} else {
			$.ajax({
				url: "/m/mall/category/category/save",
				type: "POST",
				data: $('#categoryForm').serialize(),
				success: function(data, ts, xhr) {
					if(data.success) {
						$('#category_model').modal('hide');
						success(data.msg);
					} else {
						error(data.msg);
					}
					$('#category_list_grid').bootstrapTable('refresh');
				}
			});
		}
	}

	function submitUpdateForm() {
		if($("#categoryParent_id").val()==""||$("#categoryParent_id").val() < 0){
			error("父ID填写有误！");
		}else{
			$.ajax({
				url: "/m/mall/category/category/update",
				type: "POST",
				data: $('#categoryUpdateForm').serialize(),
				success: function(data, ts, xhr) {
					if(data.success) {
						$('#categoryUpdate_model').modal('hide');
						success(data.msg);
					} else {
						error(data.msg);
					}
					$('#category_list_grid').bootstrapTable('refresh');
				}
			});
		}
	}

	function openCreatecategoryWin() {
		$('#categoryId').attr('readonly', true);
		$('#categoryForm')[0].reset();
		$('#category_model').modal('show');
	}

	function openUpdatecategoryWin(id) {
		$('#categoryId').attr('readonly', true);
		$('#categoryUpdateForm')[0].reset();

		setFormValue('categoryUpdateForm', $('#category_list_grid').bootstrapTable('getRowByUniqueId', id));
		$('#categoryUpdate_model').modal('show');
	}

	function deletecategory(id, name) {
		openDialog({
			title: '删除产品',
			message: '您确定要删除 [' + name + ']吗？',
			okAction: function() {
				closeDialog();
				$.ajax({
					url: "/m/mall/category/category/delete",
					type: "POST",
					data: {
						id: id
					},
					success: function(data, ts, xhr) {
						if(data.success) {
							success(data.msg);
						} else {
							error(data.msg);
						}
						$('#category_list_grid').bootstrapTable('refresh');
					}
				});
			}
		});
	}
	var product_status = {
		'1': '已经发布',
		'2': '草稿',
		'3': '下线'
	};

	function formatter_status(cellvalue, rowObject) {
		if(product_status[cellvalue]) {
			cellvalue = product_status[cellvalue];
		}
		return cellvalue;
	}

	function batch_delete_product(id) {
		var data = {};
		if(id) {
			data.id = id;
		} else {
			data = $('#product_list_grid').bootstrapTable('getSelections');
		}
		if(!data || data.length <= 0) {
			error('请选择商品！');
			return;
		}
		var ids = [];
		$(data).each(function(idx, item) {
			ids.push(item.productId);
		});
		var ajaxData = {
			categoryId: $('#categoryDealId').val(),
			ids: ids.join(';')
		};
		$.ajax({
			type: "post",
			url: "/m/mall/category/category/batchDeleteProduct",
			async: true,
			data: ajaxData,
			success: function(data) {
				if(data.success) {
					success(data.msg);
				} else {
					error("error");
				}
				$('#product_list_grid').bootstrapTable('refresh');
			}
		});
	}
	function tpl_formatter_id(value, o) {
		return '<a target="_blank" href="http://store.eqxiu.com/'+o.productId+'.html">'+value+'</a>';
	}  
</script>