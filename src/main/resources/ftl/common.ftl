<#macro head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="description" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	
	<!-- basic styles-->
	<link href="${assets}/css/bootstrap.min.css" rel="stylesheet" />
	 
	<link rel="stylesheet" href="${assets}/css/font-awesome.min.css" />

	<!--[if IE 7]>
	<link rel="stylesheet" href="${assets}/css/font-awesome-ie7.min.css" />
	<![endif]-->

	<!-- page specific plugin styles -->

	<!-- fonts -->
	<link rel="stylesheet" href="${assets}/css/ace-fonts.css" />

	<!-- ace styles -->
	<!----><link rel="stylesheet" href="${assets}/css/ace.min.css" />
	<link rel="stylesheet" href="${assets}/css/xt-basic.css" />
	<link rel="stylesheet" href="${assets}/css/xt-nav.css" />
	<link rel="stylesheet" href="${assets}/css/xt-menu.css" />

	<link rel="stylesheet" href="${assets}/css/ace-rtl.min.css" />
	<link rel="stylesheet" href="${assets}/css/ace-skins.min.css" />

	<!--[if lte IE 8]>
	<link rel="stylesheet" href="${assets}/css/ace-ie.min.css" />
	<![endif]-->

	<!-- inline styles related to this page -->

	<!-- ace settings handler -->

	<script src="${assets}/js/ace-extra.min.js"></script>

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	
	<!-- basic scripts -->
	<!--[if !IE]> -->
	<script type="text/javascript">
		window.jQuery || document.write("<script src='${assets}/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
	</script>
	<!-- <![endif]-->

	<script type="text/javascript">var ctx = "${ctx}",mpath="${mpath}";</script>
</#macro>
<#-- 顶部导航条 -->
<#macro navbar>
	<div class="navbar navbar-default" id="navbar">
		<script type="text/javascript">
			try{ace.settings.check('navbar' , 'fixed')}catch(e){}
		</script>
		<!-- 页头 -->
		<div class="navbar-container" id="navbar-container">
			<!-- LOGO区 -->
			<div class="navbar-header pull-left">
			    <img src="<@parameter key='image.logo.pc'/>">
	            <small class="topic"><b>&nbsp;&nbsp;免费好用的移动场景营销专家</b></small>
			</div><!-- /.navbar-header -->
			<!-- 功能模块区 -->
			<div class="navbar-header pull-right" role="navigation">
				<ul class="nav ace-nav">
					<@shiro permission="module:home">
					<li class="blue">
						<a class="dropdown-toggle" href="<@parameter key='frontServer.url'/>">
							<img src="${assets}/images/index1.png">
						</a>
					</li>
					</@shiro>
					<@shiro permission="module:studio">
                    <li class="green">
						<a class="dropdown-toggle" href="http://v.eqxiu.cn/s/inD6uxT2" target="_blank">
							<img src="${assets}/images/workhome.png">
						</a>
					</li>
					</@shiro>
				    <!-- li class="set">
	                  	<a href="#">设置<img src="${assets}/images/set.png" width="20" height="15"></a>
					</li -->
				    <li class="set">
						<a href="${ctx}/logout">退出<img src="${assets}/images/exit.png" width="20" height="15"></a>
					</li>
				</ul><!-- /.ace-nav -->
				
			</div><!-- /.navbar-header -->
		</div><!-- /.container -->
	</div>
</#macro>
<#--左侧菜单条-->
<#macro sidebar>
	<a class="menu-toggler" id="menu-toggler" href="#">
		<span class="menu-text"></span>
	</a>
	<!--左侧菜单条-->
	<div class="sidebar" id="sidebar"></div>
	<script type="text/javascript">
		$.ajax({
			url:"${mpath}/base/func/sidebar.html",
			type:"get",
			success:function(data,ts,xhr){
				$("#sidebar").empty();
				$("#sidebar").html("");
				$("#sidebar").append(data);
				callFunc("${openUrl}","${activeFunc}","${target}");
			}
		});
	</script>
</#macro>
<#--面包屑导航条-->
<#macro breadcrumbs>
	<div class="breadcrumbs" id="breadcrumbs">
		<ul class="breadcrumb">
			<li>
				<i class="icon-home home-icon"></i>
				<a href="${mpath}/studio.html">我的工作室</a>
			</li>
			<#if bcFuncs??>
			<#list bcFuncs as func>
			<li>
				<#if func.icon??>
				<i class="${func.icon!}"></i>
				</#if>
				<#if func_has_next>
				<a href="${mpath}/studio.html?func=${func.id}&target=${func.target}">${func.name}</a>
				<#else>
				${func.name}
				</#if>
			</li>
			</#list>
			</#if>
			<!--<li class="active">信息技术在教育中的应用</li>-->
		</ul><!-- .breadcrumb -->
	
		<div class="nav-search" id="nav-search">
			<form class="form-search">
				<span class="input-icon">
					<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
					<i class="icon-search nav-search-icon"></i>
				</span>
			</form>
		</div><!-- #nav-search -->
	</div>
</#macro>

<#--个性设置-->
<#macro settings>
	<div class="ace-settings-container" id="ace-settings-container">
		<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
			<i class="icon-cog bigger-150"></i>
		</div>
	
		<div class="ace-settings-box" id="ace-settings-box">
			<div>
				<div class="pull-left">
					<select id="skin-colorpicker" class="hide">
						<option data-skin="default" value="#438EB9">#438EB9</option>
						<option data-skin="skin-1" value="#222A2D">#222A2D</option>
						<option data-skin="skin-2" value="#C6487E">#C6487E</option>
						<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
					</select>
				</div>
				<span>&nbsp; Choose Skin</span>
			</div>
	
			<div>
				<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-navbar" />
				<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
			</div>
	
			<div>
				<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-sidebar" />
				<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
			</div>
	
			<div>
				<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-breadcrumbs" />
				<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
			</div>
	
			<div>
				<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" />
				<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
			</div>
	
			<div>
				<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-add-container" />
				<label class="lbl" for="ace-settings-add-container">
					Inside
					<b>.container</b>
				</label>
			</div>
		</div>
	</div><!-- /#ace-settings-container -->
</#macro>

<#-- 验证码
name 表单名称
inputCssStyle 验证码样式
-->
<#macro validateCode name inputCssStyle="">
<input type="text" id="${name}" name="${name}" class="form-control" style="display:inline-block;font-weight:bold;width:80px;${inputCssStyle}"/>&nbsp;
<img src="${ctx}/servlet/validateCodeServlet" onclick="$('.${name}Refresh').click();" class="mid ${name}"/>&nbsp;
<a href="javascript:" onclick="$('.${name}').attr('src','${ctx}/servlet/validateCodeServlet?'+new Date().getTime());" class="mid ${name}Refresh">看不清楚</a>
</#macro>



<#--
id 编号
name 隐藏域名称（ID）
value 隐藏域值（ID）
labelName 输入框名称（Name）
labelValue 输入框值（Name）
title 选择框标题
url 树结构数据地址
checked 是否显示复选框
extId 排除掉的编号（不能选择的编号）
notAllowSelectRoot 不允许选择根节点
notAllowSelectParent 不允许选择父节点
module 过滤栏目模型（只显示指定模型，仅针对CMS的Category树）
selectScopeModule 选择范围内的模型（控制不能选择公共模型，不能选择本栏目外的模型）（仅针对CMS的Category树）
allowClear 是否允许清除
cssClass css样式
cssStyle css样式
disabled 是否限制选择，如果限制，设置为disabled
-->
<#macro treeselect id name value labelName labelValue title url 
	checked="" extId="" notAllowSelectRoot="true" notAllowSelectParent="true" 
	module="" selectScopeModule="true" allowClear="true" cssClass="" cssStyle="" disabled="">
<div class="input-append">
	<input id="${id}Id" name="${name}" class="${cssClass}" type="hidden" value="${value}"/>
	<input id="${id}Name" name="${labelName}" readonly="readonly" type="text" value="${labelValue}" maxlength="50"
		class="${cssClass}" style="${cssStyle}"/><a id="${id}Button" href="javascript:" class="btn ${disabled}"><i class="icon-search"></i></a>&nbsp;&nbsp;
</div>
<script type="text/javascript">
	$("#${id}Button").click(function(){
		// 是否限制选择，如果限制，设置为disabled
		if ("${disabled}" == "disabled"){
			return true;
		}
		// 正常打开	
		top.$.jBox.open("iframe:${ctx}/tag/treeselect?url="+encodeURIComponent("${url}")+"&module=${module}&checked=${checked}&extId=${extId}&selectIds="+$("#${id}Id").val(), "选择${title}", 300, 420, {
			buttons:{"确定":"ok", <#if allowClear=="true">"清除":"clear",</#if>"关闭":true}, submit:function(v, h, f){
				if (v=="ok"){
					var tree = h.find("iframe")[0].contentWindow.tree;//h.find("iframe").contents();
					var ids = [], names = [], nodes = [];
					if ("${checked}" == "true"){
						nodes = tree.getCheckedNodes(true);
					}else{
						nodes = tree.getSelectedNodes();
					}
					for(var i=0; i<nodes.length; i++) {
						<#if checked="true">
						if (nodes[i].isParent){
							continue; // 如果为复选框选择，则过滤掉父节点
						}
						</#if>
						<#if notAllowSelectRoot=="true">
						if (nodes[i].level == 0){
							top.$.jBox.tip("不能选择根节点（"+nodes[i].name+"）请重新选择。");
							return false;
						}
						</#if>
						<#if notAllowSelectParent=="true">
						if (nodes[i].isParent){
							top.$.jBox.tip("不能选择父节点（"+nodes[i].name+"）请重新选择。");
							return false;
						}
						</#if>
						<#if selectScopeModule=="true">
						if (nodes[i].module == ""){
							top.$.jBox.tip("不能选择公共模型（"+nodes[i].name+"）请重新选择。");
							return false;
						}else if (nodes[i].module != "${module}"){
							top.$.jBox.tip("不能选择当前栏目以外的栏目模型，请重新选择。");
							return false;
						}
						</#if>
						ids.push(nodes[i].id);
						names.push(nodes[i].name);
						break; // 如果为非复选框选择，则返回第一个选择  
					}
					$("#${id}Id").val(ids);
					$("#${id}Name").val(names);
				}<#if allowClear=="true">
				else if (v=="clear"){
					$("#${id}Id").val("");
					$("#${id}Name").val("");
                }</#if>
			}, loaded:function(h){
				$(".jbox-content", top.document).css("overflow-y","hidden");
			}
		});
	});
</script>
</#macro>