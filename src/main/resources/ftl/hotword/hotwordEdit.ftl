<ul class="nav nav-tabs">
	<li class="active">
		<a onclick="callFunc('/mall/hotword/hotwordEdit.html')" href="#">热词编辑</a>
	</li>
	<li>
		<a onclick="callFunc('/mall/hotword/hotwordStatistics.html')" href="#">热词统计</a>
	</li>
</ul>
<form class="form-inline" id="Hotword_list_form">
	<div class="form-group">
		<div class="input-group">
            <div class="input-group-addon"> 热词ID</div>
            <input type="text" class="form-control" name="id"/>
        </div>
	</div>
	<div class="form-group">
		<div class="input-group">
            <div class="input-group-addon"> 热词名称</div>
            <input type="text" class="form-control" name="hotwordName"/>
        </div>
	</div>
	<div class="form-group">
		<div class="input-group">
            <div class="input-group-addon"> 热词分类</div>
            <select class="form-control" name="hotwordCategory">
                <option value="0" selected="selected">全部</option>
	            <option value="1">图片</option>
	            <option value="2">样例</option>
	            <option value="3">字体</option>
	            <option value="4">音乐</option>
	        </select>
        </div>
	</div>
	<div class="form-group">
		<div class="input-group">
            <div class="input-group-addon"> 广告词</div>
            <select class="form-control" name="hotwordType">
                <option value="0" selected="selected">全部</option>
	            <option value="1">是</option>
	            <option value="2">否</option>
	        </select>
        </div>
	</div>
    <div class="input-group">
        <div class="input-group-addon"> 排序&nbsp;&nbsp;&nbsp;&nbsp;</div>
        <select class="form-control" name="orderByClause">
          <option value="desc">降序</option>
          <option value="asc">升序</option>
        </select>
    </div>
	<br/>
	<button type="button" class="btn btn-primary" onclick="query_Hotword_list('#Hotword_list_grid', '/m/mall/hotword/hotwordList.json')" >查询</button>
	<button type="button" class="btn btn-primary" onclick="openCreateHotwordWin()">新增热词</button>
	<button type="button" class="btn btn-primary" onclick="batch_delete_hotword()">删除选中热词</button>
</form>
<table id="Hotword_list_grid" data-toggle="table" data-toolbar="#Hotword_list_form" data-unique-id="id" data-query-params="Hotword_query_params" data-click-to-select="true" data-page-size="5">
	<thead>
	    <tr>
	        <th data-valign="middle" data-checkbox="true">全选</th>
	    	<th data-field="id">ID</th>
	    	<th data-field="word">热词名称</th>
	    	<th data-field="sort">排序</th>
	    	<th data-field="createTime" data-formatter="log_formatter_time">创建时间</th>
	    	<th data-valign="middle" data-field="id" data-align="center" data-formatter="business_operation_format" class="action-buttons" data-width="130">操作</th>				
	    </tr>
    </thead>
</table>
<!-- 新增产品业务的模型，主要是列出来需要的属性，这个模块在两个地方会用，一个是点击新增产品业务按钮，另一个是点击产品列表中的编辑。 -->
<div id="Hotword_model" class="modal fade" role="dialog" aria-labelledby="Hotword_model_title">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="hotword_model_title">热词业务</h4>
            </div>
            <div class="modal-body">
                <form class="form-inline" id="HotwordForm">
                   <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">热词ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                            <input type="text" id="id" value="" name="id" class="form-control" style="width: 450px;">
                        </div>
                    </div>
                    <br/><br/>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">热词名称&nbsp;&nbsp;&nbsp;&nbsp;</div>
                            <input type="text" id="hotwordName" value="" name="word" class="form-control" style="width: 450px;">
                        </div>
                    </div>
                    <br/><br/>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">热词分类&nbsp;&nbsp;&nbsp;&nbsp;</div>
                            <select id="hotwordName" value="" name="category" class="form-control" style="width: 450px;">
                                <option value="1" selected="selected">图片</option>
					            <option value="2">样例</option>
					            <option value="3">字体</option>
					            <option value="4">音乐</option>
                            </select>
                        </div>
                    </div>
                    <br/><br/>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">是否广告词</div>
                            <select id="hotwordName" value="" name="type" class="form-control" style="width: 450px;">
					            <option value="1" selected="selected">是</option>
					            <option value="2">否</option>
					        </select>
                        </div>
                    </div>
                     <br/><br/>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">热词排序&nbsp;&nbsp;&nbsp;&nbsp;</div>
                            <input type="text" id="hotwordSort" value="" name="sort" class="form-control" style="width: 450px;">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" onclick="submitHotwordForm()">保存</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
//查询
    function query_Hotword_list(id, url) {
        $(id).bootstrapTable('refresh', {url: url});
    }  
     function Hotword_query_params(params) {
        $('#Hotword_list_form :input').each(function() {
            if($(this).attr('name')) {
                params[$(this).attr('name')] = $(this).val();
            }
        });
        return params;
    }
    function business_operation_format(value, item) {
        return '<button type="button" class="btn btn-link" onclick="openUpdateHotwordWin('+value+')">编辑</button>';
    }
    function submitHotwordForm() {       
        $.ajax({
            url :  "/m/mall/hotword/hotwordSaveOrUpdate",
            type : "POST",
            data: $('#HotwordForm').serialize(),
            success:function(data,ts,xhr){
                if(data.success){
                    $('#Hotword_model').modal('hide');
                    success(data.msg);
                }else{
                    error("error");
                }
                $('#Hotword_list_grid').bootstrapTable('refresh');
            }
        });
    }
     function openCreateHotwordWin() {
        $('#id').attr('readonly', true);
        $('#HotwordForm')[0].reset();
        $('#Hotword_model').modal('show');
    }
    
    function openUpdateHotwordWin(id) {
        $('#id').attr('readonly', true);
        $('#HotwordForm')[0].reset();
        setFormValue('HotwordForm', $('#Hotword_list_grid').bootstrapTable('getRowByUniqueId', id));
        $('#Hotword_model').modal('show');
    }
    function log_formatter_time(value) {
      return new Date(value).format('yyyy-MM-dd hh:mm:ss');
    }
    function batch_delete_hotword(id) {
		var data = {};
		if(id) {
			data.id = id;
		} else {
			data = $('#Hotword_list_grid').bootstrapTable('getSelections');
		}
		if(!data || data.length <= 0) {
			error('请选择热词！');
			return;
		}
		var ids = [];
		$(data).each(function(idx, item) {
			ids.push(item.id);
		});
		var ajaxData = {
			ids: ids.join(';')
		};
		openDialog({
			title: '删除',
			message: '您确认要删除这些热词吗？',
			okAction: function() {
				closeDialog();
				$.ajax({
					type: "post",
					url: "/m/mall/hotword/hotwordDelete",
					async: true,
					data: ajaxData,
					success: function(data) {
						if(data.success) {
							success(data.msg);
						} else {
							error("error");
						}
						$('#Hotword_list_grid').bootstrapTable('refresh');
					}
				});
			}
		})
	}
</script>