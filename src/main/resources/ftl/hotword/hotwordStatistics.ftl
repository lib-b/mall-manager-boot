<ul class="nav nav-tabs">
	<li>
		<a onclick="callFunc('/mall/hotword/hotwordEdit.html')" href="#">热词编辑</a>
	</li>
	<li class="active">
		<a onclick="callFunc('/mall/hotword/hotwordStatistics.html')" href="#">热词统计</a>
	</li>
</ul>
<form class="form-inline" id="hotword_amount_form">
   <div class="form-group">
		<div class="input-group">
            <div class="input-group-addon"> 分类</div>
             <select name="category" class="input-group-addon" maxlength="50" style="width: 150px; height:35px;">
                <option value="default">默认</option>
                <option value="mall">商城</option>
                <option value="scene">场景</option>
                <option value="show">秀场</option>
                <option value="template">模板</option>
             </select>
        </div>
        <div class="input-group">
            <div class="input-group-addon">范围</div>
             <select name="end" class="input-group-addon" maxlength="50" style="width: 150px; height:35px;">
                <option value="9" selected="selected">前十</option>
             </select>
        </div>
	</div>
	<button type="button" class="btn btn-primary" onclick="query_hotword_list()" >查询搜索量</button>
</form>  
<!-- 为ECharts准备一个具备大小（宽高）的Dom -->
<div id="main" style="height:400px"></div>
<!-- ECharts单文件引入 -->
<script src="http://echarts.baidu.com/build/dist/echarts-all.js"></script>
<script type="text/javascript">
    function query_hotword_list(){
     // 基于准备好的dom，初始化echarts图表
     var myChart = echarts.init(document.getElementById('main'));        
     myChart.showLoading();    //数据加载完之前先显示一段简单的loading动画
     var names=[];    //类别数组（实际用来盛放X轴坐标值）
     var nums=[];    //销量数组（实际用来盛放Y坐标值）
     $.ajax({
     type : "post",
     async : true,            //异步请求（同步请求将会锁住浏览器，用户其他操作必须等待请求完成才可以执行）
     url : "/m/mall/hotword/list",    //请求发送到TestServlet处
     data : $('#hotword_amount_form').serialize(),
     dataType : "json",        //返回数据形式为json
     success : function(result) {
         //请求成功时执行该函数内容，result即为服务器返回的json对象
         if (result) {
                for(var i=0;i<result.length;i++){       
                   names.push(result[i].name);    //挨个取出类别并填入类别数组 
                 }
                for(var i=0;i<result.length;i++){       
                    nums.push(result[i].value);    //挨个取出销量并填入销量数组
                  }
                myChart.hideLoading();    //隐藏加载动画
                myChart.setOption({        //加载数据图表
		           tooltip: {
		                show: true
		            },
		            legend: {
		                data:['搜索量']
		            },
		            xAxis : [
		                {
		                    type : 'category',
		                    data : names
		                }
		            ],
		            yAxis : [
		                {
		                    type : 'value'
		                }
		            ],
		            series : [
		                {
		                    "name":"搜索量",
		                    "type":"bar",
		                    "data":nums
		                }
		            ]
                });
                
         }
     
    },
     error : function(errorMsg) {
         //请求失败时执行该函数
     alert("图表请求数据失败!");
     myChart.hideLoading();
     }
})
        }
</script>