<form class="form-inline" id="log_list_form">
	<div class="form-group">
		<div class="input-group">
            <div class="input-group-addon"> 操作人</div>
            <input type="text" class="form-control" name="userName"/>
        </div>
	</div>
	<div class="form-group">
        <div class="input-group">
            <div class="input-group-addon">操作时间</div>
            <input type="text" class="form-control" name="createStartTime" id="log_create_time"/>
             <div class="input-group-addon">-</div>
            <input type="text" class="form-control" name="createEndTime" id="log_End_time"/>
        </div>
    </div>
	<button type="button" class="btn btn-primary" onclick="query_log_list('#log_list_grid', '/m/mall/log/list.json')" >查询</button>
	<button type="button" class="btn btn-primary" onclick="deleteLogs()" >删除</button>
</form>
<table id="log_list_grid" data-toggle="table" data-toolbar="#log_list_form" data-unique-id="id" data-query-params="log_query_params" data-click-to-select="true" data-page-size="5">
	<thead>
	    <tr>
	    	<th data-field="id">ID</th>
	    	<th data-field="user">操作人</th>
	    	<th data-field="time" data-formatter="log_formatter_time">操作时间</th>
	    	<th data-field="message">日志信息</th>
	    </tr>
    </thead>
</table>
<script type="text/javascript">
//查询
    function query_log_list(id, url) {
        $(id).bootstrapTable('refresh', {url: url});
    }  
     function log_query_params(params) {
        $('#log_list_form :input').each(function() {
            if($(this).attr('name')) {
                params[$(this).attr('name')] = $(this).val();
            }
        });
        return params;
    }
    function log_formatter_time(value) {
        return new Date(value).format('yyyy-MM-dd hh:mm:ss');
    }
    function deleteLogs(){
         $.ajax({
                    url :  "/m/mall/log/delete",
                    type : "POST",
                    data: $('#log_list_form').serialize(),
                    success:function(data,ts,xhr){
                        if(data.success){
                            success(data.msg);
                        }else{
                            error("error");
                        }
                        $('#log_list_grid').bootstrapTable('refresh');
                    }
                });
    }
     $(document).ready(function(){
        ///日期控件
        $('#log_create_time,#log_End_time').datetimepicker();
    });
</script>