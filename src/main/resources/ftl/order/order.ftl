<form class="form-inline" id="order_list_form">
	<div class="form-group">
		<div class="input-group">
			<div class="input-group-addon"> 订单ID</div>
			<input type="text" class="form-control" name="id" />
		</div>
	</div>
	<div class="form-group">
		<div class="input-group">
			<div class="input-group-addon"> 外部订单ID</div>
			<input type="text" class="form-control" name="outOrderId" />
		</div>
	</div>
	<div class="form-group">
		<div class="input-group">
			<div class="input-group-addon"> 商品ID</div>
			<input type="text" class="form-control" name="productId" />
		</div>
	</div>
	<br/>
	<div class="form-group">
		<div class="input-group">
			<div class="input-group-addon"> 订单来源ID</div>
			<input type="text" class="form-control" name="sourceId" />
		</div>
	</div>
	<div class="form-group">
		<div class="input-group">
			<div class="input-group-addon"> 商标</div>
			<select id="productBrandId" name="brandId" class="form-control" style="width:120px;">
			</select>
		</div>
	</div>
	<div class="form-group">
		<div class="input-group">
			<select id="userType" name="userType" class="form-control" style="width:80px;">
				<option value="0" selected="selected">登录名</option>
				<option value="1">用户Id</option>
			</select>
			<input id="userData" type="text" name="createUser" class="form-control" style="width:150px;" />
			<input id="userID" type="hidden" name="userID" class="form-control" style="width:150px;" />
		</div>
	</div>
	<div class="input-group">
		<div class="input-group-addon"> 商品平台</div>
		<select id="productPlatForm" name="platform" class="form-control">
			<option value="0" selected="selected">全部</option>
			<option value="1">PC</option>
			<option value="2">IOS</option>
			<option value="3">Android</option>
			<option value="4">全平台</option>
			<option value="5">mobile</option>
		</select>
	</div>
	<br/>
	<div class="form-group">
		<div class="input-group">
			<div class="input-group-addon">创建时间</div>
			<input type="text" class="form-control" name="createStartTime" id="order_create_time" />
			<div class="input-group-addon">-</div>
			<input type="text" class="form-control" name="createEndTime" id="order_End_time" />
		</div>
	</div>
	<div class="input-group">
		<div class="input-group-addon"> 商品类型</div>
		<select id="orderType" name="attrGroupId" class="form-control">
		</select>
	</div>
	<div class="input-group">
		<div class="input-group-addon"> 订单状态</div>
		<select id="orderType" name="status" class="form-control">
			<option value="0" selected="selected">全部</option>
			<option value="1">支付</option>
			<option value="2">未支付</option>
		</select>
	</div>
	<button type="button" class="btn btn-primary" onclick="query_order_list('#order_list_grid', '/m/mall/order/list.json')">查询</button>
</form>
<form class="form-inline">
	<div class="input-group">
		<div class="input-group-addon">订单总数</div>
		<input type="text" id="orderNumber" readonly="readonly" class="form-control" name="orderNumber" maxlength="50" style="width: 100px;">
	</div>
	<div class="input-group">
		<div class="input-group-addon">订单总金额(元)</div>
		<input type="text" id="totalAmount" readonly="readonly" class="form-control" name="totalAmount" maxlength="50" style="width: 100px;">
	</div>
	<div class="input-group">
		<div class="input-group-addon">总用户数</div>
		<input type="text" id="totalUsers" readonly="readonly" class="form-control" name="totalUsers" maxlength="50" style="width: 100px;">
	</div>
</form>
<table id="order_list_grid" data-toggle="table" data-toolbar="#order_list_form" data-unique-id="id" data-query-params="order_query_params" data-click-to-select="true" data-page-size="5">
	<thead>
		<tr>
			<th data-field="id">订单ID</th>
			<th data-field="name">订单名称</th>
			<th data-field="totalFee">订单总价</th>
			<th data-field="content">订单内容</th>
			<th data-field="status" data-formatter=formatter_status>状态</th>
			<th data-field="createUser">用户名</th>
			<th data-field="transactionId">交易ID</th>
			<th data-field="createTime" data-formatter="log_formatter_time">创建时间</th>
			<th data-field="outOrderId">外部订单ID</th>
			<th data-field="productId" data-formatter="tpl_formatter_id">商品ID</th>
			<th data-field="remark">备注</th>
		</tr>
	</thead>
</table>
<div id="order_model" class="modal fade" role="dialog" aria-labelledby="order_model_title">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<form class="form-inline" id="orderForm">
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">订单ID</div>
							<input type="text" id="id" value="" name="id" class="form-control" style="width: 450px;">
						</div>
					</div>
					<br/><br/>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">订单名称</div>
							<input type="text" id="name" value="" name="name" class="form-control" style="width: 450px;">
						</div>
					</div>
					<br/><br/>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">订单总价(元)</div>
							<input type="text" id="totalFee" value="" name="totalFee" class="form-control" style="width: 450px;">
						</div>
					</div>
					<br/><br/>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">订单内容</div>
							<input type="text" id="content" value="" name="content" class="form-control" style="width: 450px;">
						</div>
					</div>
					<br/><br/>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">用户名</div>
							<input type="text" id="createUser" value="" name="createUser" class="form-control" style="width: 450px;">
						</div>
					</div>
					<br/><br/>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">交易ID</div>
							<input type="text" id="transactionId" value="" name="transactionId" class="form-control" style="width: 450px;">
						</div>
					</div>
					<br/><br/>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">外部订单ID</div>
							<input type="text" id="outOrderId" value="" name="outOrderId" class="form-control" style="width: 450px;">
						</div>
					</div>
					<br/><br/>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">商品ID</div>
							<input type="text" id="productId" value="" name="productId" class="form-control" style="width: 450px;">
						</div>
					</div>
					<br/><br/>
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon">备注</div>
							<input type="text" id="remark" value="" name="remark" class="form-control" style="width: 450px;">
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary" onclick="submitorderForm()">更新</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script type="text/javascript">
	var order_status = { '1': '支付', '2': '未支付' };
	$(document).ready(function() {
		///日期控件
		$('#order_create_time,#order_End_time').datetimepicker();
		//订单类型下拉框
		addOrderTypeItems("#orderType", "/m/mall/attribute/attributegroup/listAll.json");
		initBrandId();
	});

	function formatter_status(cellvalue, rowObject) {
		if(order_status[cellvalue]) {
			cellvalue = order_status[cellvalue];
		}
		return cellvalue;
	}

	function initBrandId() {
		$.ajax({
			url: "/m/mall/brand/brand/listAll",
			type: "POST",
			async: false, //想要给ajax外的变量赋值，必须把异步设置为false.
			success: function(data, ts, xhr) {
				$("#productBrandId").append("<option value='0' selected='selected'>全部</option>");
				for(var i in data) {
					$("#productBrandId").append("<option value=\"" + data[i].id + "\" >" + data[i].name + "</option>");
				}
			}
		});
	}
	//查询
	function query_order_list(id, url) {
		if($("#userData").val() != "" && $("#userType").val() == "0") {
			$.ajax({
				type: "post",
				url: "mall/order/userId",
				data: {
					loginName: $("#userData").val()
				},
				async: true,
				success: function(data, ts, xhr) {
					if(data.success) {
						$("#userID").val(data.map["userId"]);
						$(id).bootstrapTable('refresh', { url: url });
						getAllNumbers();
					} else {
						error("该用户不存在！")
					}
				}
			});
		} else {
			$(id).bootstrapTable('refresh', { url: url });
			getAllNumbers();
		}
	}

	function order_query_params(params) {
		$('#order_list_form :input').each(function() {
			if($(this).attr('name')) {
				params[$(this).attr('name')] = $(this).val();
			}
		});
		return params;
	}

	function business_operation_format(value, item) {
		return '<button type="button" class="btn btn-link" onclick="deleteorder(' + value + ', \'' + item.name + '\')">删除</button>';
	}

	function addOrderTypeItems(id, url) {
		$.ajax({
			url: url,
			type: "post",
			dataType: "json",
			contentType: "application/json",
			traditional: true,
			success: function(data) {
				$(id).empty();
				$(id).append("<option value='0' selected='selected'>全部</option>");
				for(var i in data) {
					$(id).append("<option value=\"" + data[i].id + "\" >" + data[i].name + "</option>");
				}
			},
			error: function(msg) {
				alert("出错了！");
			}
		});
	};

	function submitorderForm() {
		$.ajax({
			url: "/m/mall/order/update",
			type: "POST",
			data: $('#orderForm').serialize(),
			success: function(data, ts, xhr) {
				if(data.success) {
					$('#order_model').modal('hide');
					success(data.msg);
				} else {
					error(data.msg);
				}
				$('#order_list_grid').bootstrapTable('refresh');
			}
		});
	}

	function getAllNumbers() {
		$.ajax({
			url: "/m/mall/order/numbers",
			type: "POST",
			data: $('#order_list_form').serialize(),
			success: function(data, ts, xhr) {
				$('#orderNumber').val(data[0].orders);
				$('#totalAmount').val(data[0].amount);
				$('#totalUsers').val(data[0].users);
			}
		});
	}

	function openCreateorderWin() {
		$('#orderId').removeAttr('readonly');
		$('#orderForm')[0].reset();
		$('#order_model').modal('show');
	}

	function openUpdateorderWin(id) {
		$('#id').attr('readonly', true);
		$('#orderForm')[0].reset();
		setFormValue('orderForm', $('#order_list_grid').bootstrapTable('getRowByUniqueId', id));
		$('#order_model').modal('show');
	}

	function deleteorder(id, name) {
		openDialog({
			title: '删除订单',
			message: '您确定要删除 [' + name + ']吗？',
			okAction: function() {
				closeDialog();
				$.ajax({
					url: "/m/mall/order/delete",
					type: "POST",
					data: {
						id: id
					},
					success: function(data, ts, xhr) {
						if(data.success) {
							success(data.msg);
						} else {
							error(data.msg);
						}
						$('#order_list_grid').bootstrapTable('refresh');
					}
				});
			}
		});
	}

	function log_formatter_time(value) {
		return new Date(value).format('yyyy-MM-dd hh:mm:ss');
	}

	function tpl_formatter_id(value, o) {
		return '<a target="_blank" href="http://store.eqxiu.com/' + o.productId + '.html">' + value + '</a>';
	}
</script>