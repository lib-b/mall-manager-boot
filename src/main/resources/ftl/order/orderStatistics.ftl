<form class="form-inline" id="order_form">
	<div class="form-group">
		<div class="input-group">
			<div class="input-group-addon">创建时间</div>
			<input type="text" class="form-control" name="startTime" id="create_time" />
			<div class="input-group-addon">-</div>
			<input type="text" class="form-control" name="endTime" id="end_time" />
		</div>
	</div>

	<div class="input-group">
		<div class="input-group-addon">排序</div>
		<select class="form-control" name="sort">
			<option value="number" selected="selected">销量</option>
			<option value="amount">总价</option>
		</select>
	</div>
	</br>
	</br>
	<button type="button" class="btn btn-primary" onclick="statisticsByProductType()">商品维度统计</button>
	<button type="button" class="btn btn-primary" onclick="statisticsByArtist()">作者维度统计</button>
	</br>
	</br>
	<button type="button" class="btn btn-primary" onclick="exportExcel()">统计订单数据</button>
	<a href="/m/mall/order/downLoadExcel">下载订单数据</a>
</form>
<br>
<br>
<div id="table">
</div>
<script type="text/javascript">
	$(document).ready(function() {
		///日期控件
		$('#create_time,#end_time').datetimepicker();
	});

	function exportExcel() {
		if($("#create_time").val() == "" || $("#end_time").val() == "") {
			error("请先设置起始和终止时间！");
		} else {
			$.ajax({
				url: "/m/mall/order/export",
				type: "POST",
				data: $('#order_form').serialize(),
				success: function(data, ts, xhr) {
					if(data.success) {
						openDialog({
							title: '统计完成',
							message: '您可以点击下面的链接下载统计数据了！',
							okAction: function() {
								closeDialog();
							}
						})
					} else {
						error("error");
					}
				}
			});
		}
	}

	var product_group = {
		'1': '图片',
		'2': '样例',
		'3': '音乐',
		'4': '字体'
	};

	function statisticsByProductType() {
		if($("#create_time").val() == "" || $("#end_time").val() == "") {
			error("请先设置起始和终止时间！");
		} else {
			$.ajax({
				url: "/m/mall/order/statisticByProductType",
				type: "POST",
				data: $('#order_form').serialize(),
				success: function(data, ts, xhr) {
					if(data.success) {
						var list = data.list;
						var line = "";
						document.getElementById("table").innerHTML = "";
						line += "<table border='1' style='width: 50%;'>";
						line += "<tr style='font-size:16px; font-weight: bold'>";
						line += "<th class='center'>序号</th>";
						line += "<th class='center'>商品类型</th>";
						line += "<th class='center'>销量</th>";
						line += "<th class='center'>总价</th>";
						line += "</tr>";
						for(var i in list) {
							line += "<tr style='font-weight: normal'>";
							line += "<th class='center'>" + i + "</th>";
							line += "<th class='center'>" + product_group[list[i].type] + "</th>";
							line += "<th class='center'>" + list[i].number + "</th>";
							line += "<th class='center'>" + list[i].amount + "</th>";
							line += "</tr>";
						}
						line += "</table>";
						$("#table").append(line);
					}
				}
			});
		}
	}

	function statisticsByArtist() {
		if($("#create_time").val() == "" || $("#end_time").val() == "") {
			error("请先设置起始和终止时间！");
		} else {
			$.ajax({
				url: "/m/mall/order/statisticsByArtist",
				type: "POST",
				data: $('#order_form').serialize(),
				success: function(data, ts, xhr) {
					if(data.success) {
						var list = data.list;
						var line = "";
						document.getElementById("table").innerHTML = "";
						line += "<table border='1' style='width: 50%;'>";
						line += "<tr style='font-size:16px; font-weight: bold'>";
						line += "<th class='center'>序号</th>";
						line += "<th class='center'>作者</th>";
						line += "<th class='center'>销量</th>";
						line += "<th class='center'>总价</th>";
						line += "</tr>";
						for(var i in list) {
							line += "<tr style='font-weight: normal'>";
							line += "<th class='center'>" + i + "</th>";
							line += "<th class='center'>" + list[i].name + "</th>";
							line += "<th class='center'>" + list[i].number + "</th>";
							line += "<th class='center'>" + list[i].amount + "</th>";
							line += "</tr>";
						}
						line += "</table>";
						$("#table").append(line);
					}
				}
			});
		}
	}
</script>