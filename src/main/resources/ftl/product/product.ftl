<ul class="nav nav-tabs">
	<li class="active">
		<a onclick="callFunc('/mall/product/product.html')" href="#">商品列表</a>
	</li>
	<li>
		<a onclick="callFunc('/mall/product/productUpload.html')" href="#">商品上传</a>
	</li>
	<li>
		<a onclick="callFunc('/mall/product/productStatistics.html')" href="#">商品统计</a>
	</li>
	<form class="form-inline" id="product_list_form">
		<div class="input-group">
			<div class="input-group-addon">分类路径</div>
			<select id="category1" class="form-control" name="category1" onchange="category1Change()">
			</select>
		</div>
		<div class="input-group">
			<select id="category2" class="form-control" name="category2" onchange="category2Change()">
			</select>
		</div>
		<div class="input-group">
			<select id="category3" class="form-control" name="category3">
			</select>
		</div>
		<br>
		<div class="form-group">
			<div class="input-group">
				<div class="input-group-addon"> 商品ID&nbsp;&nbsp;&nbsp;&nbsp;</div>
				<input type="text" class="form-control" name="id" />
			</div>
			<div class="input-group">
				<div class="input-group-addon"> 商品编码</div>
				<input type="text" class="form-control" name="code" />
			</div>
			<div class="input-group">
				<div class="input-group-addon"> 商品标题</div>
				<input type="text" class="form-control" placeholder="模糊匹配" name="title" />
			</div>
			<div class="input-group">
				<div class="input-group-addon"> 作者ID</div>
				<input type="text" class="form-control" name="artistId" />
			</div>
			<br/>
			<div class="input-group">
				<div class="input-group-addon"> 商标名称</div>
				<select id="brandNameId" class="form-control" name="brandId">
				</select>
			</div>
			<div class="input-group">
				<div class="input-group-addon"> 商品类型</div>
				<select id="productType" name="attrGroupId" class="form-control">
				</select>
			</div>
			<div class="input-group">
				<div class="input-group-addon"> 商品平台</div>
				<select id="productPlatForm" name="platform" class="form-control">
					<option value="0" selected="selected">全部</option>
					<option value="1">PC</option>
					<option value="2">IOS</option>
					<option value="3">Android</option>
					<option value="4">全平台</option>
					<option value="5">mobile</option>
				</select>
			</div>
			<div class="input-group">
				<div class="input-group-addon"> 排序类型</div>
				<select class="form-control" id="orderByClause" name="orderByClause" onchange="orderByClauseChange()">
					<option value="default" selected="selected">默认</option>
					<option value="desc">降序</option>
					<option value="asc">升序</option>
				</select>
			</div>
			<div class="input-group">
				<select class="form-control" id="orderByProperty" name="orderByProperty">
					<option value="sort" selected="selected">sort</option>
					<option value="price">价格</option>
					<option value="create_time">创建时间</option>
				</select>
			</div>
			<div class="input-group">
				<div class="input-group-addon"> 状态</div>
				<select class="form-control" name="status">
					<option value="0" selected="selected">全部</option>
					<option value="1">已发布</option>
					<option value="2">草稿</option>
					<option value="3">下线</option>
				</select>
			</div>
			<div class="form-group">
				<div class="input-group">
					<div class="input-group-addon">创建时间</div>
					<input type="text" class="form-control" name="startTime" id="start_time" />
					<div class="input-group-addon">-</div>
					<input type="text" class="form-control" name="endTime" id="end_time" />
				</div>
			</div>
			<button type="button" class="btn btn-primary" onclick="query_product_list('#product_list_grid', '/m/mall/product/list.json')">查询</button>
			<button type="button" class="btn btn-primary" onclick="openCreateproductWin()">新增商品</button>
			<button type="button" class="btn btn-primary" onclick="batch_delete_product()">删除选中商品</button>
		</div>
		<br/>
		<button id="print_check_pass_btn" type="button" class="btn btn-primary" onclick="batch_add();">批量设置</button>
		<div class="input-group">
			<select id="dealTypeAdd" class="form-control" name="dealTypeAdd" onchange="gradeChange('add')">
				<option value="1" selected="selected">标签</option>
				<option value="2">专题</option>
				<option value="3">分类</option>
			</select>
		</div>
		<div class="input-group">
			<input id="batchSetInput" name="batchSetInput" class="form-control" placeholder="标签名称" />
		</div>
		<button id="print_check_pass_btn" type="button" class="btn btn-primary" onclick="batch_delete();">批量删除</button>
		<div class="input-group">
			<select id="dealTypeDelete" class="form-control" name="dealTypeDelete" onchange="gradeChange('delete')">
				<option value="1" selected="selected">标签</option>
				<option value="2">专题</option>
				<option value="3">分类</option>
			</select>
		</div>
		<div class="input-group">
			<input id="batchDeleteInput" name="batchDeleteInput" class="form-control" placeholder="标签名称" />
		</div>

	</form>

	<table id="product_list_grid" data-toggle="table" data-toolbar="#product_list_form" data-unique-id="id" data-query-params="product_query_params" data-click-to-select="true" data-page-size="5">
		<thead>
			<tr>
				<th data-valign="middle" data-checkbox="true">全选</th>
				<th data-field="id" data-formatter="tpl_formatter_id">ID</th>
				<th data-field="code">商品编码</th>
				<th data-field="title">商品标题</th>
				<!--<th data-field="categoryPath">分类路径</th>-->
				<th data-field="subtitle" data-visible="false">商品副标题</th>
				<th data-field="description" data-align="center" data-visible="false">商品描述</th>
				<th data-field="status" data-formatter=formatter_status>商品状态</th>
				<th data-field="attrGroupId" data-align="center" data-visible="false">商品属性组</th>
				<th data-field="createTime" data-formatter="log_formatter_time">创建时间</th>
				<th data-field="createUser" data-visible="false">上传者</th>
				<th data-field="sourceId" data-align="center" data-visible="false">商品源</th>
				<th data-field="brandId" data-formatter=formatter_brand>商品商标</th>
				<th data-field="price">商品价格</th>
				<th data-field="sort">排序</th>
				<th data-field="artistId">作者ID</th>
				<th data-valign="middle" data-field="id" data-align="center" data-formatter="business_operation_format" class="action-buttons" data-width="130">操作</th>
			</tr>
		</thead>
	</table>
	<!-- 新增商品业务的模型，主要是列出来需要的属性，这个模块在两个地方会用，一个是点击新增商品业务按钮，另一个是点击商品列表中的编辑。 -->
	<div id="product_model" class="modal fade" role="dialog" aria-labelledby="product_model_title">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="product_model_title">新增商品业务</h4>
				</div>
				<div class="modal-body">
					<form class="form-inline" id="productForm">
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">商品标题</div>
								<input type="text" id="productTitle" value="" name="title" class="form-control" style="width: 450px;">
							</div>
						</div>
						<br/><br/>
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">商品code</div>
								<input type="text" id="productCode" value="" placeholder="可以为空" name="code" class="form-control" style="width: 450px;">
							</div>
						</div>
						<br/><br/>
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">副标题&nbsp;&nbsp;&nbsp;&nbsp;</div>
								<input type="text" id="productSubTitle" value="" name="subtitle" class="form-control" style="width: 450px;">
							</div>
						</div>
						<br/><br/>
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">商品描述</div>
								<textarea id="productDescription" value="" name="description" class="form-control" style="width: 450px;" />
							</div>
						</div>
						<br/><br/>
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">商品状态</div>
								<select id="productStatus" value="" name="status" class="form-control" style="width: 450px;">
									<option value="1" selected="selected">已发布</option>
									<option value="2">草稿</option>
									<option value="3">下线</option>
								</select>
							</div>
						</div>
						<br/><br/>
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">商品平台</div>
								<select id="productStatus" value="" name="platform" class="form-control" style="width: 450px;">
									<option value="1">PC</option>
									<option value="2">IOS</option>
									<option value="3">Android</option>
									<option value="4" selected="selected">全平台</option>
									<option value="5">mobile</option>
								</select>
							</div>
						</div>
						<br/><br/>
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">属性组&nbsp;&nbsp;&nbsp;&nbsp;</div>
								<select name="attrGroupId" id="newproductAttrGroupID" class="form-control" style="width: 450px;" >
								</select>
								<input type="hidden" name="attrGroupId" id="newattrGroupId" />
							</div>
						</div>
						<br/><br/>
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">商品源ID&nbsp;</div>
								<input type="text" id="productSourceId" value="" name="sourceId" class="form-control" style="width: 450px;">
							</div>
						</div>
						<br/><br/>
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">商品商标</div>
								<select id="newproductBrandId" value="" name="brandId" class="form-control" style="width: 450px;">
								</select>
								<input type="hidden" name="brandId" id="newbrandId" />
							</div>
						</div>
						<br/><br/>
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">商品价格</div>
								<input type="text" id="productPrice" value="" name="price" class="form-control" style="width: 450px;">
							</div>
						</div>
						<br/><br/>
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">商品排序</div>
								<input type="text" id="productSort" value="" name="sort" class="form-control" style="width: 450px;">
							</div>
						</div>
						<br/><br/>
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">创建者ID</div>
								<input type="text" id="productArtist" value="" name="artistId" class="form-control" style="width: 450px;">
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" onclick="submitProductForm()">保存 </button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	<div id="productupdate_model" class="modal fade" role="dialog" aria-labelledby="product_model_title">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="product_model_title">修改商品业务</h4>
				</div>
				<div class="modal-body">
					<form class="form-inline" id="productupdateForm">
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">商品ID&nbsp;&nbsp;&nbsp;&nbsp;</div>
								<input type="text" id="productUpdateId" value="" name="id" class="form-control" style="width: 450px;">
							</div>
						</div>
						<br/><br/>
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">商品Code</div>
								<input type="text" id="productUpdateCode" value="" name="code" class="form-control" style="width: 450px;">
							</div>
						</div>
						<br/><br/>
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">商品标题</div>
								<input type="text" id="productTitle" value="" name="title" class="form-control" style="width: 450px;">
							</div>
						</div>
						<br/><br/>
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">副标题&nbsp;&nbsp;&nbsp;&nbsp;</div>
								<input type="text" id="productSubTitle" value="" name="subtitle" class="form-control" style="width: 450px;">
							</div>
						</div>
						<br/><br/>
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">商品描述</div>
								<textarea id="productDescription" value="" name="description" class="form-control" style="width: 450px;" />
							</div>
						</div>
						<br/><br/>
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">商品状态</div>
								<select id="productStatus" value="" name="status" class="form-control" style="width: 450px;">
									<option value="1" selected="selected">已发布</option>
									<option value="2">草稿</option>
									<option value="3">下线</option>
								</select>
							</div>
						</div>
						<br/><br/>
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">商品平台</div>
								<select id="productStatus" value="" name="platform" class="form-control" style="width: 450px;">
									<option value="1">PC</option>
									<option value="2">IOS</option>
									<option value="3">Android</option>
									<option value="4" selected="selected">全平台</option>
									<option value="5">mobile</option>
								</select>
							</div>
						</div>
						<br/><br/>
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">商品源ID&nbsp;</div>
								<input type="text" id="productSourceId" value="" name="sourceId" class="form-control" style="width: 450px;">
							</div>
						</div>
						<br/><br/>
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">商品商标</div>
								<select id="productBrandId" value="" name="brandId" class="form-control" style="width: 450px;">
								</select>
								<input type="hidden" name="brandId" id="brandId" />
							</div>
						</div>
						<br/><br/>
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">商品属性组</div>
								<select id="productAttrGroupID" value="" name="attrGroupId" class="form-control" style="width: 450px;">
								</select>
								<input type="hidden" name="attrGroupId" id="attrGroupId" />
							</div>
						</div>
						<br/><br/>
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">商品价格</div>
								<input type="text" id="productPrice" value="" name="price" class="form-control" style="width: 450px;">
							</div>
						</div>
						<br/><br/>
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">创建者ID</div>
								<input type="text" id="artistId" value="" name="artistId" class="form-control" style="width: 450px;">
							</div>
						</div>
						<br/><br/>
						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">商品排序</div>
								<input type="text" id="productSort" value="" name="sort" class="form-control" style="width: 450px;">
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" onclick="submitProductUpdateForm()">更新</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->

	<div id="productattr_modal" class="modal fade" role="dialog" aria-labelledby="productattr_model_title">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="productattr_model_title">商品属性信息</h4>
				</div>
				<div class="modal-body" id="productattr_modal_body">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" onclick="submitAttrUpdateForm()">保存</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	<div id="productCategory_modal" class="modal fade" role="dialog" aria-labelledby="productCategory_model_title">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="productCategory_model_title">商品分类信息</h4>
				</div>
				<div class="modal-body" id="productCategory_modal_body">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" onclick="addCategory()">新增</button>
					<button type="button" class="btn btn-primary" onclick="submitCategory()">保存</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	<div id="productTag_modal" class="modal fade" role="dialog" aria-labelledby="productTag_model_title">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="productTag_model_title">商品标签信息</h4>
				</div>
				<div class="modal-body" id="productTag_modal_body">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" onclick="addTag()">新增</button>
					<button type="button" class="btn btn-primary" onclick="submitTag()">保存</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	<script src="https://cdn.staticfile.org/qiniu-js-sdk/1.0.14-beta/qiniu.min.js"></script>
	<script src="//cdn.bootcss.com/plupload/2.1.9/plupload.full.min.js"></script>
	<script type="text/javascript">
		var brandNames = {};
		var date = new Date();
        var sourceId = '';
		//打开页面的时候填充属性组下拉框
		$(document).ready(function() {
			addProductTypeItems("#productType", "/m/mall/attribute/attributegroup/listAll.json");
			initBrandNames();
			addBrandNames();
			$('#start_time,#end_time').datetimepicker();
			initCategoryItems("#category1", 0,0);			
			//隐藏掉分类2和分类3
			category2.style.display = "none";
			category3.style.display = "none";
			orderByProperty.style.display = "none";

            $('#newproductAttrGroupID').on('change',function(data){
                var selected = $('#newproductAttrGroupID option:selected').val();
                if(selected == 4){
                    sourceId = $('#productSourceId').val();
                    $('#productSourceId').val('');
                    $('#productSourceId').attr('disabled','disabled');
                }else{
                    $('#productSourceId').val(sourceId);
                    $('#productSourceId').removeAttr('disabled');
                }

            });
		});

		function initCategoryItems(id, pid, type) {
			$.ajax({
				type: "post",
				url: "/m/mall/category/category/children",
				data: {
					categoryId: pid
				},
				success: function(data, ts, xhr) {
					$(id).empty();
					if(type==0){
						$(id).append("<option value='0' selected='selected'>全部</option>");
						$(id).append("<option value='-1'>未分类</option>");
					}
					if(type==1){
						$(id).append("<option value='-1' selected='selected'></option>");
					}
					for(var i in data) {
						if(data[i].id != 888888) {
							$(id).append("<option value=\"" + data[i].id + "\" >" + data[i].name + "</option>");
						}
					}
				}
			});
		}

		function orderByClauseChange() {
			var selectedItem = $("#orderByClause").val();
			if(selectedItem == "default") {
				orderByProperty.style.display = "none";
			} else {
				orderByProperty.style.display = "block";
			}
		}

		function category1Change() {
			var selectedItem = $("#category1").val();
			if(selectedItem == 0 || selectedItem == -1) {
				//如果选择全部隐藏掉分类2和分类3
				category2.style.display = "none";
				category3.style.display = "none";
			} else {
				category2.style.display = "block";
				initCategoryItems("#category2", selectedItem,0);
			}
		}

		function category2Change() {
			var selectedItem = $("#category2").val();
			if(selectedItem == 0) {
				//如果选择全部隐藏掉分类3
				category3.style.display = "none";
			} else {
				category3.style.display = "block";
				initCategoryItems("#category3", selectedItem,0);
			}
		}		

		function addTag() {
			$('#productTag_modal_form').append("<div class='form-group'><div class='input-group'><div class='input-group-addon'>标签</div><input type='text' name='tag' class='form-control' maxlength='50' style='width: 300px;'></div></div><br/><br/>");
		}

		function submitTag() {
			$.ajax({
				url: "/m/mall/product/updateTag",
				type: "POST",
				data: $('#productTag_modal_form').serialize(),
				success: function(data, ts, xhr) {
					if(data.success) {
						$('#productTag_modal_form').modal('hide');
						success(data.msg);
					} else {
						error("标签:" + data.map["tagName"] + "不存在！");
					}
				}
			});
		}
		//查询
		function query_product_list(id, url) {
			//检查分类全路径是否合法
			if($("#category1").val() != 0 && ($("#category2").val() == 0 || $("#category3").val() == 0)) {
				error("分类路径不是全路径，请重新设置!");
			} else {
				$(id).bootstrapTable('refresh', {
					url: url
				});
			}
		}

		function query_productattr_list(id, attrtable, url) {
			$(attrtable).bootstrapTable('refresh', {
				url: url
			});
		}

		function product_query_params(params) {
			$('#product_list_form :input').each(function() {
				if($(this).attr('name')) {
					params[$(this).attr('name')] = $(this).val();
				}
			});
			return params;
		}

		function business_operation_format(value, item) {
			return '<button type="button" class="btn btn-link" onclick="openUpdateproductWin(' + value + ')">编辑</button>' +
				'<button type="button" class="btn btn-link" onclick="deleteproduct(' + value + ', \'' + item.title + '\')">删除</button>' +
				'<button type="button" class="btn btn-link" onclick="refreshAttr(' + value + ', \'' + item.title + '\')">属性</button>' +
				'<button type="button" class="btn btn-link" onclick="refreshProductCategory(' + value + ')">分类</button>' +
				'<button type="button" class="btn btn-link" onclick="refreshProductTag(' + value + ')">标签</button>';
		}

		function submitProductForm() {
			$.ajax({
				url: "/m/mall/product/save",
				type: "POST",
				data: $('#productForm').serialize(),
				success: function(data, ts, xhr) {
					if(data.success) {
						$('#product_model').modal('hide');
						success(data.msg);
					} else {
						error(data.map["codeError"]);
					}
					$('#product_list_grid').bootstrapTable('refresh');
				}
			});
		}

		function openCreateproductWin() {
			$('#productId').attr('readonly', true);
			addItems("#newproductAttrGroupID", "#newattrGroupId", "/m/mall/attribute/attributegroup/listAll.json");
			addItems("#newproductBrandId", "#newbrandId", "/m/mall/brand/brand/listAll.json");
			$('#productForm')[0].reset();
			$('#product_model').modal('show');
            $('#productSourceId').removeAttr('disabled');
		}

		function openCreateproductattrWin() {
			$('#productattrForm')[0].reset();
			$('#productattr_modal').modal('show');
		}

		function addProductTypeItems(id, url) {
			$.ajax({
				url: url,
				type: "post",
				dataType: "json",
				contentType: "application/json",
				traditional: true,
				success: function(data) {
					$(id).empty();
					$(id).append("<option value='0' selected='selected'>全部</option>");
					for(var i in data) {
						$(id).append("<option value=\"" + data[i].id + "\" >" + data[i].name + "</option>");
					}
				},
				error: function(msg) {
					alert("出错了！");
				}
			});
		};

		function addItems(id, hidden, url) {
			$.ajax({
				url: url,
				type: "post",
				dataType: "json",
				contentType: "application/json",
				traditional: true,
				success: function(data) {
					var brandId = $(hidden).val();
					$(id).empty();
					for(var i in data) {
						if(brandId == data[i].id) {
							$(id).append("<option value=\"" + data[i].id + "\" selected='selected'>" + data[i].name + "</option>");
						} else {
							$(id).append("<option value=\"" + data[i].id + "\" >" + data[i].name + "</option>");
						}
					}
				},
				error: function(msg) {
					alert("出错了！");
				}
			});
		};

		function submitProductUpdateForm() {
			$.ajax({
				url: "/m/mall/product/update",
				type: "POST",
				data: $('#productupdateForm').serialize(),
				success: function(data, ts, xhr) {
					if(data.success) {
						$('#productupdate_model').modal('hide');
						success(data.msg);
					} else {
						error(data.map["codeError"]);
					}
					$('#product_list_grid').bootstrapTable('refresh');
				}
			});
		}

		function openUpdateproductWin(id) {
			$('#productUpdateId').attr('readonly', true);
			$('#productupdateForm')[0].reset();
			setFormValue('productupdateForm', $('#product_list_grid').bootstrapTable('getRowByUniqueId', id));
			addItems("#productAttrGroupID", "#attrGroupId", "/m/mall/attribute/attributegroup/listAll.json");
			addItems("#productBrandId", "#brandId", "/m/mall/brand/brand/listAll.json");
			$('#productupdate_model').modal('show');
		}

		function deleteproduct(id, name) {
			openDialog({
				title: '删除商品',
				message: '您确定要删除 [' + name + ']吗？',
				okAction: function() {
					closeDialog();
					var ids = [];
					ids.push(id);
					$.ajax({
						url: "/m/mall/product/delete",
						type: "POST",
						data: {
							ids: ids.join(";")
						},
						success: function(data, ts, xhr) {
							if(data.success) {
								success(data.msg);
							} else {
								error("error");
							}
							$('#product_list_grid').bootstrapTable('refresh');
						}
					});
				}
			});
		}

		function refreshAttr(productId, name) {
			$.ajax({
				url: "/m/mall/product/attrbutelist",
				type: "POST",
				data: {
					productId: productId
				},
				success: function(data, ts, xhr) {
					$('#productattr_modal_body').empty();
					var head = "<form class='form-inline' id='productattr_modal_form'><div class='form-group'><div class='input-group'><div class='input-group-addon'>" +
						"商品ID" +
						"</div><input type='text' readonly='true' name=" + "productId" + " value=" + productId + " class='form-control' maxlength='50' style='width: 200px;'></div></div><br/><br/>";
					for(var i in data) {
						var value = data[i].value;
						var type = data[i].type;
						var line = "";
						if(value == null || value == "") {
							if(type == "2") {//包含上传按钮的
								line += "<form class='form-inline' id='productattr_modal_form'>";
								line += "<div class='form-group'>";
								line += "<div class='input-group-addon' style='width: 200px;'>" +data[i].title +"</div>";
								line += "<div class='input-group'>";								
								line += "<div class='input-group-addon'>" +data[i].name +"</div>";									
								line +=	"<input type='text' name=" + data[i].name + " class='form-control' maxlength='500' style='width: 200px;'>";						
								line += "</div></div>";
								line += "<div id=" + data[i].name + 'container' +">";
								line += "<button type='button' id=" +data[i].name + 'Select' +" class='btn btn-primary' name=" +productId + " onclick='upload(id,name)' style='margin-right: 20px;'>上传文件</button></div><br/>";
							} else {//不包含上传按钮的
								line += "<form class='form-inline' id='productattr_modal_form'>";
								line += "<div class='form-group'>";
								line += "<div class='input-group-addon' style='width: 200px;'>" +data[i].title +"</div>";
								line += "<div class='input-group'>";								
								line +="<div class='input-group-addon'>" +data[i].name +"</div>";
								line +="<input type='text' name=" + data[i].name + " class='form-control' maxlength='500' style='width: 200px;'></div></div><br/><br/>";
							}
						} else {
							if(type == "2") {
								line += "<form class='form-inline' id='productattr_modal_form'>";
								line += "<div class='form-group'>";
								line += "<div class='input-group-addon' style='width: 200px;'>" +data[i].title +"</div>";
								line += "<div class='input-group'>";
								line += "<div class='input-group-addon'>" +data[i].name +"</div>";									
								line +=	"<input type='text' name=" + data[i].name + " value=" + data[i].value + " class='form-control' maxlength='500' style='width: 200px;'>";						
								line += "</div></div>";
								line += "<div id=" + data[i].name + 'container' +">";
								line += "<button type='button' id=" +data[i].name + 'Select' +" class='btn btn-primary' name=" +productId + " onclick='upload(id,name)' style='margin-right: 20px;'>上传文件</button></div><br/>";								
							} else {
								line += "<form class='form-inline' id='productattr_modal_form'>";
								line += "<div class='form-group'>";
								line += "<div class='input-group-addon' style='width: 200px;'>" +data[i].title +"</div>";
								line += "<div class='input-group'>";
								line +="<div class='input-group-addon'>" +data[i].name +"</div>";
								line +="<input type='text' name=" + data[i].name + " value=" + data[i].value + " class='form-control' maxlength='500' style='width: 200px;'></div></div><br/><br/>";								
							}
						}
						head += line;
					}
					var foot = "</form>";
					$('#productattr_modal_body').append(head + foot);
					$('#productattr_modal').modal('show');
				}
			});
		}
		
		var oldCategoryIds = [];
		var newCategoryIds = [];
		function refreshProductCategory(productId) {
			$.ajax({
				url: "/m/mall/product/categoryList",
				type: "POST",
				data: {
					productId: productId
				},
				success: function(data, ts, xhr) {
					$('#productCategory_modal_body').empty();
					var head = "<form class='form-inline' id='productCategory_modal_form'><div class='form-group'><div class='input-group'><div class='input-group-addon'>" +
						"商品ID" +
						"</div><input type='text' readonly='true' name=" + "productId" + " value=" + productId + " class='form-control' maxlength='50' style='width: 200px;'></div></div>";
					//清空存放分类的数组
					oldCategoryIds.splice(0,oldCategoryIds.length);
					newCategoryIds.splice(0,newCategoryIds.length);
					
					for(var i in data) {
						var pathlist = data[i];
						var line = "";
						//第一级分类
						if(addCategoryOptions(pathlist[0]) != "") {
							line += "<div id='" + productId + i + "' class='input-group' style='margin-top:10px'>";
							line += "<div class='input-group'>";
							line += "<div id='oldCategory"+ i + "' class='input-group-addon'>分类路径</div>";
							line += "<select id='oldCategory1"+ i + "' class='form-control' name='oldCategory1"+i+"' onchange='category1Edit(" + i + ")'>";
							line += addCategoryOptions(pathlist[0]);
							line += "</select></div>";
						}
						//第二级分类
						line += "<div class='input-group'>";
						line += "<select id='oldCategory2"+ i + "' class='form-control' name='oldCategory2"+i+"' onchange='category2Edit(" + i + ")'>";
						if(addCategoryOptions(pathlist[1]) != "") {
							line += addCategoryOptions(pathlist[1]);
						}
						line += "</select></div>";
						//第三级分类
						line += "<div class='input-group'>";
						line += "<select id='oldCategory3"+ i + "' class='form-control' name='oldCategory3"+i+"'>";
						if(addCategoryOptions(pathlist[2]) != "") {
							line += addCategoryOptions(pathlist[2]);
						}
						line += "</select></div>";
						//删除按钮
						line += "<button id='" + productId + i + "button' type='button' class='btn btn-primary' onclick='deleteCategoryPath(" + productId +","+ i + ","+0+")'>删除</button></div>"
						head += line;
						oldCategoryIds.push(i);
					}
					var foot = "<input type='hidden'  name='newCategoryIds' id='newCategoryIds'/><input type='hidden'  name='oldCategoryIds' id='oldCategoryIds'/></form>";
					$('#productCategory_modal_body').append(head + foot);
					$('#productCategory_modal').modal('show');
					
				}
			});
		}
		var newId = 0;		
		function addCategory() {
			var html ="";
			var curid = "newcategory" + newId;
			html += "<br><div id='" + curid + "' class='input-group' style='margin-top:10px'>";
			html += "<div class='input-group'>";
			html += "<div class='input-group-addon'>分类路径</div>";
			html += "<select id='newcategory1" + newId + "' class='form-control' name='newcategory1" + newId + "' onchange='newcategory1Edit(" + newId + ")'>";
			if(getCategory0Items()!=""){
				html += "<option value='-1' selected='selected'></option>";
				html+=getCategory0Items();
			}
			html += "</select></div>";
			html += "<div class='input-group'>";
			html += "<select id='newcategory2" + newId + "' class='form-control' name='newcategory2" + newId + "' onchange='newcategory2Edit(" + newId +")' >";
			html += "</select></div>";
			html += "<div class='input-group'>";
			html += "<select id='newcategory3" + newId + "' class='form-control' name='newcategory3" + newId + "' >";
			html += "</select></div>";
			html += "<button id='" + curid + "newbutton' type='button' class='btn btn-primary' onclick='deleteCategoryPath("+1+"," + newId + ","+1+")'>删除</button></div>";
			$('#productCategory_modal_form').append(html);
			newCategoryIds.push(newId);
			newId = newId + 1;
		}
		//map:key是categoryid，value是通分类下的所有分类id和name组成的string，用“：”隔开
		function addCategoryOptions(map) {
			var html = "";
			for(var key in map) {
				var list = map[key];
				for(var i in list) {
					var id = list[i].split(":")[0];
					var name = list[i].split(":")[1];
					if(id != 888888) {
						if(key == id) {
							html += "<option value=\"" + id + "\" selected='selected' >" + name + "</option>"
						} else {
							html += "<option value=\"" + id + "\" >" + name + "</option>"
						}
					}

				}
			}
			return html;
		}

		function getCategory0Items() {
			var html = ""
			$.ajax({
				type: "post",
				url: "/m/mall/category/category/children",
				data: {
					categoryId: 0
				},
				async: false,
				success: function(data, ts, xhr) {
					for(var i in data) {
						if(data[i].id != 888888) {
							html += "<option value=\"" + data[i].id + "\" >" + data[i].name + "</option>";
						}
					}
				}
			});
			return html;
		}

		function category1Edit(id) {
			var category1 ="#oldCategory1"+ id;
			var category2 = "#oldCategory2"+ id;
			var category3 = "#oldCategory3"+id;
			var selectedItem = $(category1).val();
			//更新分类2
			initCategoryItems(category2, selectedItem, 1);
			var selectedItem2 = $(category2).val();
			//清空分类3
			$(category3).empty();
		}
		
		function category2Edit(id) {
			var category2 = "#oldCategory2"+ id;
			var category3 = "#oldCategory3"+ id;
			var selectedItem = $(category2).val();
			initCategoryItems(category3, selectedItem, 1);
		}
		
		function newcategory1Edit(id) {
			var category1 = "#newcategory1"+ id;
			var category2 = "#newcategory2"+ id;
			var category3 = "#newcategory3"+ id;
			var selectedItem = $(category1).val();
			initCategoryItems(category2, selectedItem, 1);
			//清空分类3
			$(category3).empty();
		}
		
		function newcategory2Edit(id) {
			var category2 ="#newcategory2" + id;
			var category3 = "#newcategory3" + id;
			var selectedItem = $(category2).val();
			initCategoryItems(category3, selectedItem, 1);
		}
		
		function deleteCategoryPath(productId, id, type) {
			if(type == 0) {
				$("#" + productId + id).remove();
				for(var i in oldCategoryIds){
					if(oldCategoryIds[i]==id){
						oldCategoryIds.splice(i,1);
						break;
					}
				}
			} else {
				$("#newcategory" + id).remove();				
				for(var i in newCategoryIds){
					if(newCategoryIds[i]==id){
						newCategoryIds.splice(i,1);
						break;
					}
				}
			}
		}
		
		function submitCategory() {
			//检查老分类是否合法
			for(var i=0;i<oldCategoryIds.length;i++){
				if($("#oldCategory2"+oldCategoryIds[i]+" option").size()==0||
				   $("#oldCategory3"+oldCategoryIds[i]+" option").size()==0||
				   $("#oldCategory2"+oldCategoryIds[i]).val()==-1||
				   $("#oldCategory3"+oldCategoryIds[i]).val()==-1){
					error("路径设置不合法，请设置全路径！");
					return;
				}				
			}
			//检查新分类是否合法	
			for(var i=0;i<newCategoryIds.length;i++){
				//alert(newCategoryIds[i]);
				if($("#newcategory2"+newCategoryIds[i]+" option").size()==0||
				  $("#newcategory3"+newCategoryIds[i]+" option").size()==0||
				  $("#newcategory2"+newCategoryIds[i]).val()==-1||
				  $("#newcategory3"+newCategoryIds[i]).val()==-1){
					error("路径设置不合法，请设置全路径！");
					return;
				}				
			}
			$("#newCategoryIds").val(newCategoryIds);
			$("#oldCategoryIds").val(oldCategoryIds);
			$.ajax({
				url: "/m/mall/product/updateCategory",
				type: "POST",
				data: $('#productCategory_modal_form').serialize(),
				success: function(data, ts, xhr) {
					if(data.success) {
						$('#productCategory_modal').modal('hide');
						$('#product_list_grid').bootstrapTable('refresh');
						success(data.msg);
					} else {
						error("error");
					}
				}
			});
		}
				
		
     	function refreshProductTag(productId) {
			$.ajax({
				url:"/m/mall/product/tagList",
				type: "POST",
				data: {
					productId: productId
				},
				success: function(data, ts, xhr) {
					$('#productTag_modal_body').empty();
					var head = "<form class='form-inline' id='productTag_modal_form'><div class='form-group'><div class='input-group'><div class='input-group-addon'>" +
						"商品ID" +
						"</div><input type='text' readonly='true' name=" + "productId" + " value=" + productId + " class='form-control' maxlength='50' style='width: 200px;'></div></div><br/><br/>";
					for(var i in data) {
						var value = data[i].value;
						var line;
						var label = "标签";
						if(value == null || value == "") {
							line = "<form class='form-inline' id='productTag_modal_form'><div class='form-group'><div class='input-group'><div class='input-group-addon'>" +
								label +
								"</div><input type='text' name='tag' class='form-control' maxlength='50' style='width: 200px;'></div></div><br/><br/>";
						} else {
							line = "<form class='form-inline' id='productTag_modal_form'><div class='form-group'><div class='input-group'><div class='input-group-addon'>" +
								label +
								"</div><input type='text' name='tag' value=" + data[i].value + " class='form-control' maxlength='50' style='width: 300px;'></div></div><br/><br/>";
						}
						head += line;
					}
					var foot = "</form>";
					$('#productTag_modal_body').append(head + foot);
					$('#productTag_modal').modal('show');
				}
			});
		}

		function submitAttrUpdateForm() {
			<!-- 使用ajax发送请求信息 -->
			$.ajax({
				url: "/m/mall/product/updateAttributeValue",
				type: "POST",
				data: $('#productattr_modal_form').serialize(),
				success: function(data, ts, xhr) {
					if(data.success) {
						$('#productattr_modal').modal('hide');
						success(data.msg);
					} else {
						error(data.msg);
					}
				}
			});
		}

		function log_formatter_time(value) {
			return new Date(value).format('yyyy-MM-dd hh:mm:ss');
		}

		function upload(attrId, productId) {
			var inputData = attrId.replace(/Select/, "");
			var container = attrId.replace(/Select/, "container");
			var uploadButton = attrId.replace(/Select/, "Upload");
			var token = "";
			var brandId = "";
			$.ajax({
				url: "/m/mall/product/uploadToken",
				type: "POST",
				data: {
					productId: productId
				},
				async: false,
				success: function(data, ts, xhr) {
					token = data.uptoken;
					brandId = data.brandId;
				}
			})
			var uploader = Qiniu.uploader({
				runtimes: 'html5,flash,html4', //上传模式,依次退化
				browse_button: attrId, //上传选择的点选按钮，**必需**
				uptoken: token, //若未指定uptoken_url,则必须指定 uptoken ,uptoken由其他程序生成
				domain: 'res.eqh5.com', //bucket 域名，下载资源时用到，**必需**
				// save_key: false,   // 默认 false。若在服务端生成uptoken的上传策略中指定了 `sava_key`，则开启，SDK会忽略对key的处理
				get_new_uptoken: true, //设置上传文件的时候是否每次都重新获取新的token
				container: container, //上传区域DOM ID，默认是browser_button的父元素，
				max_file_size: '100mb', //最大文件体积限制
				flash_swf_url: '/plupload/js/Moxie.swf', //引入flash,相对路径
				max_retries: 1, //上传失败最大重试次数
				dragdrop: true, //开启可拖曳上传
				//drop_element: container,        //拖曳上传区域元素的ID，拖曳文件或文件夹后可触发上传
				chunk_size: '4mb', //分块上传时，每片的体积
				auto_start: false, //选择文件后自动上传，若关闭需要自己绑定事件触发上传
				init: {
					'FilesAdded': function(up, files) {
						plupload.each(files, function(file) {
							document.getElementsByName(inputData)[0].value = file.name + "(" + plupload.formatSize(file.size) + ")";               
							uploader.start();               
							return false;
						});
					},
					'BeforeUpload': function(up, file) {
						// 每个文件上传前,处理相关的事情
					},
					'UploadProgress': function(up, file) {
						document.getElementsByName(inputData)[0].value = file.name + "(" + plupload.formatSize(file.size) + ")" + file.percent + "%";
					},
					'FileUploaded': function(up, file, info) {
						var res = jQuery.parseJSON(info);
						document.getElementsByName(inputData)[0].value = res.key;
					},
					'Error': function(up, err, errTip) {
						document.getElementsByName(inputData)[0].value = err.code + ":" + err.message;
						error(err.message);
					},
					'UploadComplete': function() {
						success('success');
					},
					'Key': function(up, file) {
						var filedata = file.name.split(".");
						var type = filedata[filedata.length - 1];
						var key = 'store/' + brandId + "/" + getUuid() + "." + type;
						return key;
					}
				}
			});
		}

		function getUuid() {
			var len = 32; //32长度
			var radix = 16; //16进制
			var chars = '0123456789abcdefghijklmnopqrstuvwxyz'.split('');
			var uuid = [],
				i;
			radix = radix || chars.length;
			if(len) {
				for(i = 0; i < len; i++)
					uuid[i] = chars[0 | Math.random() * radix];
			} else {
				var r;
				uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
				uuid[14] = '4';
				for(i = 0; i < 36; i++) {
					if(!uuid[i]) {
						r = 0 | Math.random() * 16;
						uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r];
					}
				}
			}
			return uuid.join('');
		}
		var product_status = {
			'1': '已经发布',
			'2': '草稿',
			'3': '下线'
		};

		function formatter_status(cellvalue, rowObject) {
			if(product_status[cellvalue]) {
				cellvalue = product_status[cellvalue];
			}
			return cellvalue;
		}

		function formatter_brand(cellvalue, rowObject) {
			if(brandNames[cellvalue]) {
				cellvalue = brandNames[cellvalue];
			}
			return cellvalue;
		}

		function initBrandNames() {
			$.ajax({
				url: "/m/mall/brand/brand/listAll",
				type: "POST",
				async: false, //想要给ajax外的变量赋值，必须把异步设置为false.
				success: function(data, ts, xhr) {
					for(var i in data) {
						brandNames[data[i].id] = data[i].name;
					}
				}
			});
		}

		function addBrandNames() {
			$("#brandNameId").append("<option value='0' selected='selected'>全部</option>");
			$.each(brandNames, function(key, value) {
				$("#brandNameId").append("<option value=\"" + key + "\" >" + value + "</option>");
			});
		}

		var typeData = {
			'1': "标签名称",
			'2': "专题code",
			'3': "分类路径"
		}

		function batch_add(id) {
			var data = {};
			var type = $('#dealTypeAdd').val();
			//alert(type);
			if(id) {
				data.id = id;
			} else {
				data = $('#product_list_grid').bootstrapTable('getSelections');
			}
			if(!data || data.length <= 0) {
				error('请选择商品！');
				return;
			}
			if($('#batchSetInput').val() == "") {
				error('请填写' + typeData[type] + '!');
				return;
			}
			var ids = [];
			$(data).each(function(idx, item) {
				ids.push(item.id);
			});
			var ajaxData = {
				type: type,
				value: $('#batchSetInput').val(),
				ids: ids.join(';')
			};
			$.ajax({
				type: "post",
				url: "/m/mall/product/batchAdd",
				data: ajaxData,
				success: function(data, ts, xhr) {
					if(type == 1) {
						var tagNames = ":";
						for(var i = 0; i < data.list.length; i++) {
							tagNames = tagNames + data.list[i] + ";";
						}
						if(data.list.length > 0) {
							error("未插入标签(" + tagNames + ")请先在标签管理中插入上述标签");
						} else {
							success(data.msg);
						}
					} else {
						if(data.success) {
							success(data.msg);
						} else {
							if(type == 2) {
								error("该主题不存在！");
							} else {
								error("(" + data.map["parentCategoryName"] + ")分类下不存在名称为(" + data.map["categoryName"] + ")的分类！");
							}
						}
					}
				}
			})
		}

		function batch_delete(id) {
			var data = {};
			var type = $('#dealTypeDelete').val();
			//alert(type);
			if(id) {
				data.id = id;
			} else {
				data = $('#product_list_grid').bootstrapTable('getSelections');
			}
			if(!data || data.length <= 0) {
				error('请选择商品！');
				return;
			}
			if($('#batchDeleteInput').val() == "") {
				error('请填写' + typeData[type] + '!');
				return;
			}
			var ids = [];
			$(data).each(function(idx, item) {
				ids.push(item.id);
			});
			var ajaxData = {
				type: type,
				value: $('#batchDeleteInput').val(),
				ids: ids.join(';')
			};
			openDialog({
				title: '删除',
				message: '您确认要删除吗？',
				okAction: function() {
					closeDialog();
					$.ajax({
						type: "post",
						url: "/m/mall/product/batchDelete",
						data: ajaxData,
						success: function(data, ts, xhr) {
							if(data.success) {
								success(data.msg);
							} else {
								error("error");
							}
						}
					});
				}
			})

		}

		function gradeChange(type) {
			var selectType = "";
			if(type == "add") {
				selectType = $('#dealTypeAdd').val();
				if(selectType == "1") {
					$('#batchSetInput').attr("placeholder", "标签名称");
				} else if(selectType == "2") {
					$('#batchSetInput').attr("placeholder", "专题code");
				} else {
					$('#batchSetInput').attr("placeholder", "分类全路径");
				}
			} else {
				selectType = $('#dealTypeDelete').val();
				if(selectType == "1") {
					$('#batchDeleteInput').attr("placeholder", "标签名称");
				} else if(selectType == "2") {
					$('#batchDeleteInput').attr("placeholder", "专题code");
				} else {
					$('#batchDeleteInput').attr("placeholder", "分类全路径");
				}
			}

		}

		function batch_delete_product(id) {
			var data = {};
			if(id) {
				data.id = id;
			} else {
				data = $('#product_list_grid').bootstrapTable('getSelections');
			}
			if(!data || data.length <= 0) {
				error('请选择商品！');
				return;
			}
			var ids = [];
			$(data).each(function(idx, item) {
				ids.push(item.id);
			});
			var ajaxData = {
				ids: ids.join(';')
			};
			openDialog({
				title: '删除',
				message: '您确认要删除吗？',
				okAction: function() {
					closeDialog();
					$.ajax({
						type: "post",
						url: "/m/mall/product/delete",
						data: ajaxData,
						success: function(data, ts, xhr) {
							if(data.success) {
								success(data.msg);
							} else {
								error("error");
							}
							$('#product_list_grid').bootstrapTable('refresh');
						}
					});
				}
			})

		}

		function tpl_formatter_id(value, o) {
			return '<a target="_blank" href="http://store.eqxiu.com/' + o.id + '.html">' + value + '</a>';
		}
	</script>