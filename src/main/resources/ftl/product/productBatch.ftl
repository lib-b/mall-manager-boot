<ul class="nav nav-tabs">
	<li>
		<a onclick="callFunc('/mall/product/product.html')" href="#">商品列表</a>
	</li>
	<li class="active">
		<a onclick="callFunc('/mall/product/productBatch.html')" href="#">批量处理</a>
	</li>
</ul>

<form id="fileUpload" class="form-inline" action="/m/mall/product/batchAddProducts" enctype="multipart/form-data" method="post">
	<a href="/m/mall/product/downLoadExcel">请先下载模板</a>
	</br>
	</br>
	<div class="form-group">
		<div class="input-group">
			<input id="excelFile" name="file" type="file" />
		</div>
	</div>
	<button type="button" class="btn btn-primary" onclick="submitExcel()" style="margin-right: 20px;">批量添加商品</button>
</form>
<iframe frameborder="0" id="Iframe1" name="Iframe1" scrolling="yes" src="" style="visibility: inherit; width: 0px; z-index: 0; height: 0px;">
</iframe>

<!-- /.modal -->
<script src="//cdn.bootcss.com/plupload/2.1.9/plupload.full.min.js"></script>
<script type="text/javascript">	
	function submitExcel() {
		var excelFile = $("#excelFile").val();
		if(excelFile == '') {
			error("请选择需上传的文件!");
			return false;
		}
		if(excelFile.indexOf('.xls') == -1) {
			error("文件格式不正确，请选择正确的Excel文件(后缀名.xls)！");
			return false;
		}
		$.ajax({
			url: '/m/mall/product/batchAddProducts',
			type: 'POST',
			cache: false,
			data: new FormData($('#fileUpload')[0]),
			processData: false,
			contentType: false
		}).done(function(res) {
			success(res.msg);
		}).fail(function(res) {
			error(res.msg);
		});
	}

	function downExcel() {
		var str = "http://test.res.eqh5.com/store/batchUploadProducts.xls";
		window.frames["Iframe1"].location.href = str;
		sa();
	}

	function sa() {
		if(window.frames["Iframe1"].document.readyState != "complete")
			setTimeout("sa()", 100);
		else
			window.frames["Iframe1"].document.execCommand('SaveAs', 'false', 'batch');
	}
</script>