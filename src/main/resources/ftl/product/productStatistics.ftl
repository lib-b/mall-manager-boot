<ul class="nav nav-tabs">
	<li>
		<a onclick="callFunc('/mall/product/product.html')" href="#">商品列表</a>
	</li>
	<li>
		<a onclick="callFunc('/mall/product/productUpload.html')" href="#">商品上传</a>
	</li>
	<li class="active">
		<a onclick="callFunc('/mall/product/productStatistics.html')" href="#">商品统计</a>
	</li>
</ul>
<form class="form-inline" id="product_form">
	<div class="form-group">
		<div class="input-group">
			<div class="input-group-addon">商品创建时间</div>
			<input type="text" class="form-control" name="createStartTime" id="order_create_time" />
			<div class="input-group-addon">-</div>
			<input type="text" class="form-control" name="createEndTime" id="order_End_time" />
		</div>
	</div>
	</br>
	</br>
	<button type="button" class="btn btn-primary" onclick="statisticByBrand()">按商标统计</button>
	<button type="button" class="btn btn-primary" onclick="statisticByGroupId()">按商品类型统计</button>
</form>
</br>
</br>
<div id="table">
</div>

<script type="text/javascript">
	$(document).ready(function() {
		///日期控件
		$('#order_create_time,#order_End_time').datetimepicker();
	});

	function statisticByBrand() {
		if($("#order_create_time").val() == "" || $("#order_End_time").val() == "") {
			error("请先设置起始和终止时间！");
		} else {
			$.ajax({
				url: "/m/mall/product/countByBrand",
				data: $("#product_form").serialize(),
				type: "POST",
				async: false,
				success: function(data, ts, xhr) {
					var list = data.list;
					var line = "";
					document.getElementById("table").innerHTML = "";
					line += "<table border='1' style='width: 50%;'>";
					line += "<tr style='font-size:16px; font-weight: bold'>";
					line += "<th class='center'>序号</th>";
					line += "<th class='center'>商标名称</th>";
					line += "<th class='center'>总数量</th>";
					line += "</tr>";
					for(var i in list) {
						line += "<tr style='font-weight: normal'>";
						line += "<th class='center'>" + i + "</th>";
						line += "<th class='center'>" + list[i].name + "</th>";
						line += "<th class='center'>" + list[i].number + "</th>";
						line += "</tr>";
					}
					line += "</table>";
					$("#table").append(line);
				}
			});
		}
	}

	function statisticByGroupId() {
		if($("#order_create_time").val() == "" || $("#order_End_time").val() == "") {
			error("请先设置起始和终止时间！");
		} else {
			$.ajax({
				url: "/m/mall/product/countByGroupId",
				type: "POST",
				async: false,
				data: $("#product_form").serialize(),
				success: function(data, ts, xhr) {
					var list = data.list;
					var line = "";
					document.getElementById("table").innerHTML = "";
					line += "<table border='1' style='width: 50%;'>";
					line += "<tr style='font-size:16px; font-weight: bold'>";
					line += "<th class='center'>序号</th>";
					line += "<th class='center'>商品类型</th>";
					line += "<th class='center'>总数量</th>";
					line += "</tr>";
					for(var i in list) {
						line += "<tr style='font-weight: normal'>";
						line += "<th class='center'>" + i + "</th>";
						line += "<th class='center'>" + list[i].name + "</th>";
						line += "<th class='center'>" + list[i].number + "</th>";
						line += "</tr>";
					}
					line += "</table>";
					$("#table").append(line);
				}
			});
		}
	}
</script>