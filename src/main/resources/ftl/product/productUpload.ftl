<ul class="nav nav-tabs">
	<li>
		<a onclick="callFunc('/mall/product/product.html')" href="#">商品列表</a>
	</li>
	<li class="active">
		<a onclick="callFunc('/mall/product/productUpload.html')" href="#">商品上传</a>
	</li>
	<li>
		<a onclick="callFunc('/mall/product/productStatistics.html')" href="#">商品统计</a>
	</li>
</ul>
<div id="content">
	<h3>商品上传流程：</h3>
	<h4>1.根据商品属性不同，选择相应的上传方式 ;</h4>
	<h4>2.先配置商品的属性等字段，再将相应文件拖动到上传区域;</h4>
	<h4>3.文件上传后，可根据具体情况再编辑个别商品；如上传有误，可选择删除该商品;</h4>
	<h4>4.商品上传后，应及时添加分类，并更改状态为“已发布”;</h4>
	<h4>5.商品上传后，务必在前端检查是否上传成功;是否与预期相符.</h4>
</div>
<div id="bts" style="margin:0 auto;text-align: center;">
	<button type="button" class="btn btn-primary" onclick="uploadMusic()" style="margin-right: 20px;">上传音乐</button>
	<button type="button" class="btn btn-primary" onclick="uploadFont()" style="margin-right: 20px;">上传字体</button>
	<button type="button" class="btn btn-primary" onclick="uploadPicture()" style="margin-right: 20px;">上传图片</button>
</div>
<!--music-model-->
<div id="music_modal">
	<div class="productCommon" style="margin:0 auto;text-align: center;">
		<div class="productCommon-body" style="width: 270px; margin: 0 auto;">
			<h4 class="modal-title" id="product_model_title">新增商品业务-设置音乐属性</h4>
		</div>
		<br>
		<div class="productCommon-body">
			<form class="form-inline" id="musicUploadForm">
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">属性组&nbsp;&nbsp;&nbsp;</div>
						<input type="text" readonly="true" name="attrGroupId" id="musicAttrGroupId" value="音乐属性组" class="form-control" style="width: 300px;" />
					</div>
				</div>
				<br>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">商标&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
						<select id="musicBrandID" value="" name="brandId" class="form-control" style="width: 300px;">
						</select>
					</div>
				</div>
				<br>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">平台&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
						<select id="musicPlatForm" value="" name="platForm" class="form-control" style="width: 300px;">
							<option value="1">PC</option>
							<option value="2">IOS</option>
							<option value="3">Android</option>
							<option value="4" selected="selected">全平台</option>
							<option value="5">mobile</option>
						</select>
					</div>
				</div>
				<br>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">作者&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
						<input type="text" id="musicArtist" value="" name="artistId" placeholder="请输入作者ID" class="form-control" style="width: 247px;">
						<div class="input-group-addon">必填</div>
					</div>
				</div>
				<br>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">价格&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
						<input type="text" id="musicPrice" value="" name="price" placeholder="默认为0" class="form-control" style="width: 300px;">
					</div>
				</div>
				<br>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">专辑封面</div>
						<input type="text" id="musicCover" value="" name="cover" class="form-control" style="width: 297px;">
					</div>
					<div id="container_form" style="text-align:center;">
						<a class="btn btn-primary" id="cover" style="width:160px" href="#">
							<i class="glyphicon glyphicon-plus"></i>
							<span>选择封面</span>
						</a>
						<a class="btn btn-primary" id="up_load_form" style="width:160px" href="#">
							<span>上传封面</span>
						</a>
					</div>
				</div>
				<br>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">分类&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
						<input type="text" id="musicCategory" name="category" value="" placeholder="请输入分类全路径" class="form-control" style="width: 247px;">
						<div class="input-group-addon">必填</div>
					</div>
				</div>
				<br>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">标签&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
						<input type="text" id="musicTags" name="tags" value="" placeholder="多个标签用分号隔开;" class="form-control" style="width: 300px;">
					</div>
				</div>
				<br>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">排序&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
						<input type="text" id="musicSort" placeholder="默认为0" value="" name="sort" class="form-control" style="width: 300px;">
					</div>
				</div>
				<br>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">状态&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
						<select id="musicStatus" value="" name="status" class="form-control" style="width: 300px;">
							<option value="1">已发布</option>
							<option value="2" selected="selected">草稿</option>
							<option value="3">下线</option>
						</select>
					</div>
				</div>
				<br>
				<div class="form-group">
					<div class="input-group">
						<!--<div class="input-group-addon">文件&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>-->
						<input type="hidden" id="musicTitles" value="" name="titles" class="form-control" style="width: 300px;">
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<div id="musicContainer" style="text-align:center;">
	<a class="btn btn-primary" id="musicPickfiles" style="width:160px" href="#">
		<i class="glyphicon glyphicon-plus"></i>
		<span>选择文件</span>
	</a>
	<a class="btn btn-primary" id="up_load_music" style="width:160px" href="#">
		<span>确认上传</span>
	</a>
	<a class="btn btn-primary" id="stop_load_music" style="width:160px" href="#">
		<span>暂停上传</span>
	</a>
</div>
<div id="musicfiles">
</div>
<!--font-model-->
<div id="font_modal">
	<div class="productCommon" style="margin:0 auto;text-align: center;">
		<div class="productCommon-body" style="width: 270px; margin: 0 auto;">
			<h4 class="modal-title" id="product_model_title">新增商品业务-设置字体属性</h4>
		</div>
		<br>
		<div class="productCommon-body">
			<form class="form-inline" id="fontUploadForm">
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">属性组&nbsp;&nbsp;&nbsp;</div>
						<input type="text" readonly="true" name="attrGroupId" id="fontAttrGroupId" value="字体属性组" class="form-control" style="width: 300px;" />
					</div>
				</div>
				<br>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">商标&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
						<select id="fontBrandID" value="" name="brandId" class="form-control" style="width: 300px;">
						</select>
					</div>
				</div>
				<br>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">平台&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
						<select id="fontPlatForm" value="" name="platForm" class="form-control" style="width: 300px;">
							<option value="1">PC</option>
							<option value="2">IOS</option>
							<option value="3">Android</option>
							<option value="4" selected="selected">全平台</option>
							<option value="5">mobile</option>
						</select>
					</div>
				</div>
				<br>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">作者&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
						<input type="text" id="fontArtist" value="" name="artistId" placeholder="请输入作者ID" class="form-control" style="width: 247px;">
						<div class="input-group-addon">必填</div>
					</div>
				</div>
				<br>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">个人价格</div>
						<input type="text" id="fontPersonPrice" value="" name="personPrice" placeholder="默认为0" class="form-control" style="width: 300px;">
					</div>
				</div>
				<br>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">企业价格</div>
						<input type="text" id="fontCompanyPrice" value="" name="companyPrice" placeholder="默认为100" class="form-control" style="width: 300px;">
					</div>
				</div>
				<br>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">预览文字</div>
						<input type="text" id="fontText" value="" name="text" placeholder="免费好用易企秀" class="form-control" style="width: 297px;">
					</div>
				</div>
				<br>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">分类&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
						<input type="text" id="fontCategory" name="category" value="" placeholder="请输入分类全路径" class="form-control" style="width: 247px;">
						<div class="input-group-addon">必填</div>
					</div>
				</div>
				<br>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">标签&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
						<input type="text" id="fontTags" name="tags" value="" placeholder="多个标签用分号隔开;" class="form-control" style="width: 300px;">
					</div>
				</div>
				<br>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">排序&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
						<input type="text" id="fontSort" placeholder="默认为0" value="" name="sort" class="form-control" style="width: 300px;">
					</div>
				</div>
				<br>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">状态&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
						<select id="fontStatus" value="" name="status" class="form-control" style="width: 300px;">
							<option value="1">已发布</option>
							<option value="2" selected="selected">草稿</option>
							<option value="3">下线</option>
						</select>
					</div>
				</div>
				<br>
				<div class="form-group">
					<div class="input-group">
						<!--<div class="input-group-addon">文件&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>-->
						<input type="hidden" id="fontTitles" value="" name="titles" class="form-control" style="width: 300px;">
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<div id="fontContainer" style="text-align:center;">
	<a class="btn btn-primary" id="fontPickfiles" style="width:160px" href="#">
		<i class="glyphicon glyphicon-plus"></i>
		<span>选择文件</span>
	</a>
	<a class="btn btn-primary" id="up_load_font" style="width:160px" href="#">
		<span>确认上传</span>
	</a>
	<a class="btn btn-primary" id="stop_load_font" style="width:160px" href="#">
		<span>暂停上传</span>
	</a>
</div>
<div id="fontfiles">
</div>
<!--picture-model-->
<div id="picture_modal">
	<div class="productCommon" style="margin:0 auto;text-align: center;">
		<div class="productCommon-body" style="width: 270px; margin: 0 auto;">
			<h4 class="modal-title" id="product_model_title">新增商品业务-设置图片属性</h4>
		</div>
		<br>
		<div class="productCommon-body">
			<form class="form-inline" id="pictureUploadForm">
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">属性组&nbsp;&nbsp;&nbsp;</div>
						<input type="text" readonly="true" name="attrGroupId" id="pictureAttrGroupId" value="图片属性组" class="form-control" style="width: 300px;" />
					</div>
				</div>
				<br>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">商标&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
						<select id="pictureBrandID" value="" name="brandId" class="form-control" style="width: 300px;">
						</select>
					</div>
				</div>
				<br>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">平台&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
						<select id="picturePlatForm" value="" name="platForm" class="form-control" style="width: 300px;">
							<option value="1">PC</option>
							<option value="2">IOS</option>
							<option value="3">Android</option>
							<option value="4" selected="selected">全平台</option>
							<option value="5">mobile</option>
						</select>
					</div>
				</div>
				<br>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">作者&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
						<input type="text" id="pictureArtist" value="" name="artistId" placeholder="请输入作者ID" class="form-control" style="width: 247px;">
						<div class="input-group-addon">必填</div>
					</div>
				</div>
				<br>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">价格&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
						<input type="text" id="picturePrice" value="" name="price" placeholder="默认为0" class="form-control" style="width: 300px;">
					</div>
				</div>
				<br>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">分类&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
						<input type="text" id="pictureCategory" name="category" value="" placeholder="请输入分类全路径" class="form-control" style="width: 247px;">
						<div class="input-group-addon">必填</div>
					</div>
				</div>
				<br>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">标签&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
						<input type="text" id="pictureTags" name="tags" value="" placeholder="多个标签用分号隔开;" class="form-control" style="width: 300px;">
					</div>
				</div>
				<br>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">排序&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
						<input type="text" id="pictureSort" placeholder="默认为0" value="" name="sort" class="form-control" style="width: 300px;">
					</div>
				</div>
				<br>
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">状态&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
						<select id="pictureStatus" value="" name="status" class="form-control" style="width: 300px;">
							<option value="1">已发布</option>
							<option value="2" selected="selected">草稿</option>
							<option value="3">下线</option>
						</select>
					</div>
				</div>
				<br>
				<div class="form-group">
					<div class="input-group">
						<!--<div class="input-group-addon">文件&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>-->
						<input type="hidden" id="pictureTitles" value="" name="titles" class="form-control" style="width: 300px;">
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<div id="pictureContainer" style="text-align:center;">
	<a class="btn btn-primary" id="picturePickfiles" style="width:160px" href="#">
		<i class="glyphicon glyphicon-plus"></i>
		<span>选择文件</span>
	</a>
	<a class="btn btn-primary" id="up_load_picture" style="width:160px" href="#">
		<span>确认上传</span>
	</a>
	<a class="btn btn-primary" id="stop_load_picture" style="width:160px" href="#">
		<span>暂停上传</span>
	</a>
</div>
<div id="picturefiles">
</div>
<script src="https://cdn.staticfile.org/qiniu-js-sdk/1.0.14-beta/qiniu.min.js"></script>
<script src="//cdn.bootcss.com/plupload/2.1.9/plupload.full.min.js"></script>
<script type="text/javascript">
	var token;
	var filenames = [];

	function getUuid() {
		var len = 32; //32长度
		var radix = 16; //16进制
		var chars = '0123456789abcdefghijklmnopqrstuvwxyz'.split('');
		var uuid = [],
			i;
		radix = radix || chars.length;
		if(len) {
			for(i = 0; i < len; i++)
				uuid[i] = chars[0 | Math.random() * radix];
		} else {
			var r;
			uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
			uuid[14] = '4';
			for(i = 0; i < 36; i++) {
				if(!uuid[i]) {
					r = 0 | Math.random() * 16;
					uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r];
				}
			}
		}
		return uuid.join('');
	}

	function uploadMusic() {
		//bts.style.display = "none";
		musicContainer.style.display = "block";
		music_modal.style.display = "block";
		fontContainer.style.display = "none";
		font_modal.style.display = "none";
		pictureContainer.style.display = "none";
		picture_modal.style.display = "none";
		addItems("#musicBrandID", "/m/mall/brand/brand/listAll.json");
	}

	function uploadFont() {
		//bts.style.display = "none";
		musicContainer.style.display = "none";
		music_modal.style.display = "none";
		fontContainer.style.display = "block";
		font_modal.style.display = "block";
		pictureContainer.style.display = "none";
		picture_modal.style.display = "none";
		addItems("#fontBrandID", "/m/mall/brand/brand/listAll.json");
	}

	function uploadPicture() {
		//bts.style.display = "none";
		musicContainer.style.display = "none";
		music_modal.style.display = "none";
		fontContainer.style.display = "none";
		font_modal.style.display = "none";
		pictureContainer.style.display = "block";
		picture_modal.style.display = "block";
		addItems("#pictureBrandID", "/m/mall/brand/brand/listAll.json");
	}

	function addItems(id, url) {
		$.ajax({
			url: url,
			type: "post",
			dataType: "json",
			contentType: "application/json",
			traditional: true,
			success: function(data) {
				$(id).empty();
				for(var i in data) {
					$(id).append("<option value=\"" + data[i].id + "\" >" + data[i].name + "</option>");
				}
			},
			error: function(msg) {
				alert("出错了！");
			}
		});
	};

	function checkPath(path) {
		var result = true;
		$.ajax({
			url: "/m/mall/category/category/checkPath",
			type: "POST",
			data: {
				path: path
			},
			async: false,
			success: function(data, ts, xhr) {
				if(!data.success) {
					result = false;
				}
			}
		})
		return result;
	};
	$(document).ready(function() {
		//隐藏上传按钮部分		
		musicContainer.style.display = "none";
		fontContainer.style.display = "none";
		pictureContainer.style.display = "none";
		//隐藏form
		music_modal.style.display = "none";
		font_modal.style.display = "none";
		picture_modal.style.display = "none";

		$.ajax({
			url: "/m/mall/product/uploadToken",
			type: "POST",
			async: false,
			success: function(data, ts, xhr) {
				token = data.uptoken;
			}
		});

		//上传音乐开始--------------------------------------------------------------
		var musicUploader = Qiniu.uploader({
			runtimes: 'html5,flash,html4', //上传模式,依次退化
			browse_button: musicPickfiles, //上传选择的点选按钮，**必需**
			uptoken: token, //若未指定uptoken_url,则必须指定 uptoken ,uptoken由其他程序生成
			domain: 'res.eqh5.com', //bucket 域名，下载资源时用到，**必需**
			// save_key: false,   // 默认 false。若在服务端生成uptoken的上传策略中指定了 `sava_key`，则开启，SDK会忽略对key的处理
			get_new_uptoken: false, //设置上传文件的时候是否每次都重新获取新的token
			container: musicContainer, //上传区域DOM ID，默认是browser_button的父元素，
			max_file_size: '100mb', //最大文件体积限制
			flash_swf_url: '/plupload/js/Moxie.swf', //引入flash,相对路径
			max_retries: 1, //上传失败最大重试次数
			dragdrop: true, //开启可拖曳上传
			drop_element: musicContainer, //拖曳上传区域元素的ID，拖曳文件或文件夹后可触发上传
			chunk_size: '4mb', //分块上传时，每片的体积
			auto_start: false, //选择文件后自动上传，若关闭需要自己绑定事件触发上传
			init: {
				'FilesAdded': function(up, files) {
					//清除之前已经上传的文件
					document.getElementById("musicfiles").innerHTML = "";
					plupload.each(files, function(file) { 
						var filedata = file.name.split(".");
						var filename = filedata[filedata.length - 2];
						var html = "";
						html += '<br><div class="input-group" style="margin:0 auto;text-align: center;"><label id=' + filename + ' class="input-group-addon" style="width:480px;margin: 0 auto;">' + '文件:' + file.name + '大小:' + plupload.formatSize(file.size) + '</label></div>';
						$("#musicfiles").append(html);
						filenames.push(filename);
					});
				},
				'BeforeUpload': function(up, file) {},
				'UploadProgress': function(up, file) {
					var filedata = file.name.split(".");
					var filename = filedata[filedata.length - 2];
					document.getElementById(filename).textContent = file.name + "(" + plupload.formatSize(file.size) + ")" + file.percent + "%";
				},
				'FileUploaded': function(up, file, info) {
					var filedata = file.name.split(".");
					var filename = filedata[filedata.length - 2];
					var res = jQuery.parseJSON(info);
					document.getElementById(filename).textContent = "文件名：" + file.name + " 路径:" + res.key;
					document.getElementById("musicTitles").value = filename + ":" + res.key;
					$.ajax({
						url: "/m/mall/product/uploadMusic",
						type: "POST",
						data: $("#musicUploadForm").serialize(),
						async: false,
						success: function(data, ts, xhr) {
							success(filename + "上传成功！");
						}
					});
				},
				'Error': function(up, err, errTip) {},
				'UploadComplete': function() {},
				'Key': function(up, file) {
					var filedata = file.name.split(".");
					var type = filedata[filedata.length - 1];
					var key = 'store/' + getUuid() + "." + type;
					return key;
				}
			}
		});

		musicUploader.bind('FileUploaded', function() {
			console.log('hello man,a file is uploaded');
		});

		$('#up_load_music').on('click', function() {
			if(filenames.length == 0) {
				error("请先选择文件再进行上传!");
			} else if(document.getElementById("musicArtist").value == "") {
				error("请填写作者ID!");
			} else if(document.getElementById("musicCategory").value == "") {
				error("请填写分类路径!");
			} else if(!checkPath(document.getElementById("musicCategory").value)) {
				error("分类路径有误,请重新填写!");
			} else {
				musicUploader.start();
			}
		});

		$('#stop_load_music').on('click', function() {
			musicUploader.stop();
		});

		var uploaderform = Qiniu.uploader({
			runtimes: 'html5,flash,html4', //上传模式,依次退化
			browse_button: cover, //上传选择的点选按钮，**必需**
			uptoken: token, //若未指定uptoken_url,则必须指定 uptoken ,uptoken由其他程序生成
			domain: 'res.eqh5.com', //bucket 域名，下载资源时用到，**必需**
			// save_key: false,   // 默认 false。若在服务端生成uptoken的上传策略中指定了 `sava_key`，则开启，SDK会忽略对key的处理
			get_new_uptoken: false, //设置上传文件的时候是否每次都重新获取新的token
			container: container_form, //上传区域DOM ID，默认是browser_button的父元素，
			max_file_size: '100mb', //最大文件体积限制
			flash_swf_url: '/plupload/js/Moxie.swf', //引入flash,相对路径
			max_retries: 1, //上传失败最大重试次数
			dragdrop: false, //开启可拖曳上传
			//drop_element: container_form, //拖曳上传区域元素的ID，拖曳文件或文件夹后可触发上传
			chunk_size: '4mb', //分块上传时，每片的体积
			auto_start: false, //选择文件后自动上传，若关闭需要自己绑定事件触发上传
			//multi_selection: false,
			init: {
				'FilesAdded': function(up, files) {
					plupload.each(files, function(file) { 
						document.getElementById("musicCover").value = file.name + "(" + plupload.formatSize(file.size) + ")";   
					});
				},
				'BeforeUpload': function(up, file) {

				},
				'UploadProgress': function(up, file) {
					document.getElementById("musicCover").value = file.name + "(" + plupload.formatSize(file.size) + ")" + file.percent + "%";
				},
				'FileUploaded': function(up, file, info) {
					var res = jQuery.parseJSON(info);
					document.getElementById("musicCover").value = res.key;
				},
				'Error': function(up, err, errTip) {
					//document.getElementById(filename).textContent = err.code + ":" + err.message;
				},
				'UploadComplete': function() {
					success('success');
				},
				'Key': function(up, file) {
					var filedata = file.name.split(".");
					var type = filedata[filedata.length - 1];
					var key = 'store/' + getUuid() + "." + type;
					return key;
				}
			}
		});

		uploaderform.bind('FileUploaded', function() {
			console.log('hello man,a file is uploaded');
		});

		$('#up_load_form').on('click', function() {
			uploaderform.start();
		});
		//上传音乐结束--------------------------------------------------------------
		//上传字体开始--------------------------------------------------------------
		var fontUploader = Qiniu.uploader({
			runtimes: 'html5,flash,html4', //上传模式,依次退化
			browse_button: fontPickfiles, //上传选择的点选按钮，**必需**
			uptoken: token, //若未指定uptoken_url,则必须指定 uptoken ,uptoken由其他程序生成
			domain: 'res.eqh5.com', //bucket 域名，下载资源时用到，**必需**
			// save_key: false,   // 默认 false。若在服务端生成uptoken的上传策略中指定了 `sava_key`，则开启，SDK会忽略对key的处理
			get_new_uptoken: false, //设置上传文件的时候是否每次都重新获取新的token
			container: fontContainer, //上传区域DOM ID，默认是browser_button的父元素，
			max_file_size: '100mb', //最大文件体积限制
			flash_swf_url: '/plupload/js/Moxie.swf', //引入flash,相对路径
			max_retries: 1, //上传失败最大重试次数
			dragdrop: true, //开启可拖曳上传
			drop_element: fontContainer, //拖曳上传区域元素的ID，拖曳文件或文件夹后可触发上传
			chunk_size: '4mb', //分块上传时，每片的体积
			auto_start: false, //选择文件后自动上传，若关闭需要自己绑定事件触发上传
			init: {
				'FilesAdded': function(up, files) {
					//清除之前已经上传的文件
					document.getElementById("fontfiles").innerHTML = "";
					plupload.each(files, function(file) { 
						var filedata = file.name.split(".");
						var filename = filedata[filedata.length - 2];
						var html = "";
						html += '<br><div class="input-group" style="margin:0 auto;text-align: center;"><label id=' + filename + ' class="input-group-addon" style="width:480px;margin: 0 auto;">' + '文件:' + file.name + '大小:' + plupload.formatSize(file.size) + '</label></div>';
						$("#fontfiles").append(html);
						filenames.push(filename);
					});
				},
				'BeforeUpload': function(up, file) {},
				'UploadProgress': function(up, file) {
					var filedata = file.name.split(".");
					var filename = filedata[filedata.length - 2];
					document.getElementById(filename).textContent = file.name + "(" + plupload.formatSize(file.size) + ")" + file.percent + "%";
				},
				'FileUploaded': function(up, file, info) {
					var filedata = file.name.split(".");
					var filename = filedata[filedata.length - 2];
					var res = jQuery.parseJSON(info);
					document.getElementById(filename).textContent = "文件名：" + file.name + " 路径:" + res.key;
					document.getElementById("fontTitles").value = filename + ":" + res.key;
					$.ajax({
						url: "/m/mall/product/uploadFont",
						type: "POST",
						data: $("#fontUploadForm").serialize(),
						async: false,
						success: function(data, ts, xhr) {
							success(filename + "上传成功！");
						}
					});
				},
				'Error': function(up, err, errTip) {},
				'UploadComplete': function() {},
				'Key': function(up, file) {
					var filedata = file.name.split(".");
					var type = filedata[filedata.length - 1];
					var key = 'store/' + getUuid() + "." + type;
					return key;
				}
			}
		});

		musicUploader.bind('FileUploaded', function() {
			console.log('hello man,a font is uploaded');
		});

		$('#up_load_font').on('click', function() {
			if(filenames.length == 0) {
				error("请先选择文件再进行上传!");
			} else if(document.getElementById("fontArtist").value == "") {
				error("请填写作者ID!");
			} else if(document.getElementById("fontCategory").value == "") {
				error("请填写分类路径!");
			} else if(!checkPath(document.getElementById("fontCategory").value)) {
				error("分类路径有误,请重新填写!");
			} else {
				fontUploader.start();
			}
		});

		$('#stop_load_font').on('click', function() {
			fontUploader.stop();
		});
		//上传字体结束--------------------------------------------------------------
		//上传图片开始--------------------------------------------------------------
		var pictureUploader = Qiniu.uploader({
			runtimes: 'html5,flash,html4', //上传模式,依次退化
			browse_button: picturePickfiles, //上传选择的点选按钮，**必需**
			uptoken: token, //若未指定uptoken_url,则必须指定 uptoken ,uptoken由其他程序生成
			domain: 'res.eqh5.com', //bucket 域名，下载资源时用到，**必需**
			// save_key: false,   // 默认 false。若在服务端生成uptoken的上传策略中指定了 `sava_key`，则开启，SDK会忽略对key的处理
			get_new_uptoken: false, //设置上传文件的时候是否每次都重新获取新的token
			container: pictureContainer, //上传区域DOM ID，默认是browser_button的父元素，
			max_file_size: '100mb', //最大文件体积限制
			flash_swf_url: '/plupload/js/Moxie.swf', //引入flash,相对路径
			max_retries: 1, //上传失败最大重试次数
			dragdrop: true, //开启可拖曳上传
			drop_element: pictureContainer, //拖曳上传区域元素的ID，拖曳文件或文件夹后可触发上传
			chunk_size: '4mb', //分块上传时，每片的体积
			auto_start: false, //选择文件后自动上传，若关闭需要自己绑定事件触发上传
			init: {
				'FilesAdded': function(up, files) {
					//清除之前已经上传的文件
					document.getElementById("picturefiles").innerHTML = "";
					plupload.each(files, function(file) { 
						var filedata = file.name.split(".");
						var filename = filedata[filedata.length - 2];
						var html = "";
						html += '<br><div class="input-group" style="margin:0 auto;text-align: center;"><label id=' + filename + ' class="input-group-addon" style="width:480px;margin: 0 auto;">' + '文件:' + file.name + '大小:' + plupload.formatSize(file.size) + '</label></div>';
						$("#picturefiles").append(html);
						filenames.push(filename);
					});
				},
				'BeforeUpload': function(up, file) {},
				'UploadProgress': function(up, file) {
					var filedata = file.name.split(".");
					var filename = filedata[filedata.length - 2];
					document.getElementById(filename).textContent = file.name + "(" + plupload.formatSize(file.size) + ")" + file.percent + "%";
				},
				'FileUploaded': function(up, file, info) {
					var filedata = file.name.split(".");
					var filename = filedata[filedata.length - 2];
					var res = jQuery.parseJSON(info);
					document.getElementById(filename).textContent = "文件名：" + file.name + " 路径:" + res.key;
					document.getElementById("pictureTitles").value = filename + ":" + res.key;
					$.ajax({
						url: "/m/mall/product/uploadPicture",
						type: "POST",
						data: $("#pictureUploadForm").serialize(),
						async: false,
						success: function(data, ts, xhr) {
							success(filename + "上传成功！");
						}
					});
				},
				'Error': function(up, err, errTip) {},
				'UploadComplete': function() {},
				'Key': function(up, file) {
					var filedata = file.name.split(".");
					var type = filedata[filedata.length - 1];
					var key = 'store/' + getUuid() + "." + type;
					return key;
				}
			}
		});

		musicUploader.bind('FileUploaded', function() {
			console.log('hello man,a picture is uploaded');
		});

		$('#up_load_picture').on('click', function() {
			if(filenames.length == 0) {
				error("请先选择文件再进行上传!");
			} else if(document.getElementById("pictureArtist").value == "") {
				error("请填写作者ID!");
			} else if(document.getElementById("pictureCategory").value == "") {
				error("请填写分类路径!");
			} else if(!checkPath(document.getElementById("pictureCategory").value)) {
				error("分类路径有误,请重新填写!");
			} else {
				pictureUploader.start();
			}
		});

		$('#stop_load_picture').on('click', function() {
			pictureUploader.stop();
		});
		//上传字体结束--------------------------------------------------------------
	});
</script>