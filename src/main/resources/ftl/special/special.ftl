<form class="form-inline" id="special_list_form">
	<div class="form-group">
		<div class="input-group">
            <div class="input-group-addon"> 专题名称</div>
            <input type="text" class="form-control" name="name"/>
        </div>
	</div>
	<div class="form-group">
		<div class="input-group">
            <div class="input-group-addon"> 专题标识</div>
            <input type="text" class="form-control" name="code"/>
        </div>
	</div>
	<div class="input-group">
		<div class="input-group-addon">状态</div>
		<select name="status" class="form-control">
			<option value="0" selected="selected">全部</option>
			<option value="1">启用</option>
			<option value="2">停用</option>			
		</select>
	</div>
	<div class="input-group">
		<div class="input-group-addon">类型</div>
		<select name="type" class="form-control">
			<option value="0" selected="selected">全部</option>
			<option value="1">专题推荐页</option>
			<option value="2">商城首页</option>		
			<option value="3">移动端</option>
		</select>
	</div>
	<div class="input-group">
		<div class="input-group-addon"> 排序</div>
		<select class="form-control" id="orderByClause" name="orderByClause">
			<option value="desc" selected="selected">降序</option>
			<option value="asc">升序</option>
		</select>
	</div>
	<br/>
	<button type="button" class="btn btn-primary" onclick="query_special_list('#special_list_grid', '/m/mall/special/list.json',0)" >查询</button>
    <button type="button" class="btn btn-primary" onclick="openCreateSpecialWin()">新增专题业务</button>
    <button type="button" class="btn btn-primary" onclick="batch_delete_special()">删除选中专题</button>
</form>
<table id="special_list_grid" data-toggle="table" data-toolbar="#special_list_form" data-unique-id="id" data-query-params="special_query_params" data-click-to-select="true" data-page-size="5">
	<thead>
	    <tr>
	    	<th data-valign="middle" data-checkbox="true">全选</th>
	    	<th data-field="id">ID</th>
	    	<th data-field="name">专题名称</th>
	    	<th data-field="code">专题标识</th>
	    	<th data-field="title">专题主题</th>
	    	<th data-field="imageUrl">图片url</th>
	    	<th data-field="status" data-formatter=formatter_status>专题状态</th>
	    	<th data-field="type" data-formatter=formatter_type>专题类型</th>
	    	<th data-field="sort">排序</th>
	    	<th data-valign="middle" data-field="id" data-align="center" data-formatter="business_operation_format" class="action-buttons" data-width="130">操作</th>				
	    </tr>
    </thead>
</table>
<form class="form-inline" id="product_list_form">
	<div class="form-group">
		<div class="input-group">
            <div class="input-group-addon"> 专题ID</div>
            <input id="specialPid" type="text" class="form-control" name="specialPid"/>
        </div>
	</div>
	<div class="form-group">
		<div class="input-group">
			<div class="input-group-addon"> 商品ID</div>
			<input type="text" class="form-control" name="productIds" placeholder="多个用分号隔开" />
		</div>
	</div>
	<br/>
	<button type="button" class="btn btn-primary" onclick="query_special_list('#product_list_grid', '/m/mall/special/productlist',1)" >查询商品</button>
	<button type="button" class="btn btn-primary" onclick="add_special_product('#product_list_grid', '/m/mall/special/addProduct')">新增商品</button>
	<button type="button" class="btn btn-primary" onclick="batch_delete_product()">删除选中商品</button>
</form>
<table id="product_list_grid" data-toggle="table" data-toolbar="#product_list_form" data-unique-id="id" data-query-params="product_query_params" data-click-to-select="true" data-page-size="5">
	<thead>
	    <tr>
	        <th data-valign="middle" data-checkbox="true">全选</th>
	    	<th data-field="productId" data-formatter="tpl_formatter_id">商品ID</th>
            <th data-field="specialId" data-visible="false">专题ID</th>
	    	<th data-field="productTitle">商品标题</th>
	    	<th data-field="sort">商品排序</th>
	    	<th data-field="productStatus" data-formatter=product_formatter_status>商品状态</th>
	    </tr>
    </thead>
</table>
<!-- 新增产品业务的模型，主要是列出来需要的属性，这个模块在两个地方会用，一个是点击新增产品业务按钮，另一个是点击产品列表中的编辑。 -->
<div id="special_model" class="modal fade" role="dialog" aria-labelledby="special_model_title">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="special_model_title">商标业务</h4>
            </div>
            <div class="modal-body">
                <form class="form-inline" id="specialForm">
                   <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">专题ID&nbsp;&nbsp;&nbsp;</div>
                            <input type="text" id="specialId" value="" name="id" class="form-control" style="width: 450px;">
                        </div>
                    </div>
                    <br/><br/>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">专题名称</div>
                            <input type="text" id="specialName" value="" name="name" class="form-control" style="width: 450px;">
                        </div>
                    </div>
                    <br/><br/>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">专题主题</div>
                            <input type="text" id="specialTitle" value="" name="title" class="form-control" style="width: 450px;">
                        </div>
                    </div>                    
                    <br/><br/>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">专题标识</div>
                            <input type="text" id="specialCode" value="" name="code" class="form-control" style="width: 450px;">
                        </div>
                    </div>
                    <br/><br/>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">图片url&nbsp;&nbsp;</div>
                            <input type="text" id="specialImageUrl" value="" name="imageUrl" class="form-control" style="width: 450px;">
                        </div>
                    </div>
                    <div id="container">
                       <button type="button" id="select" class="btn btn-primary" onclick="uploadImage('container',id,'imageUrl')" style="margin-right: 20px;">上传图片</button>
                    </div>                       
                    <br/>
	                <div class="form-group">
	                   <div class="input-group">
	                      <div class="input-group-addon">专题状态</div>
	                       <select id="specialStatus" value="" name="status" class="form-control" style="width: 450px;">
	           				  <option value="1" selected="selected">启用</option>
	          				  <option value="2">停用</option>
	          			   </select>
	                    </div>
	                 </div>
	                 <br/>
	                 <br/>
	                <div class="form-group">
	                   <div class="input-group">
	                      <div class="input-group-addon">专题类型</div>
	                       <select id="specialType" value="" name="type" class="form-control" style="width: 450px;">
	           				  <option value="1">专题推荐页</option>
	          				  <option value="2">商城首页</option>
	          			   </select>
	                    </div>
	                 </div>
	                 <br/><br/>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">排序&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                            <input type="text" id="specialSort" value="" name="sort" class="form-control" style="width: 450px;">
                        </div>
                    </div>   
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" onclick="submitSpecialForm()">保存</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script src="https://cdn.staticfile.org/qiniu-js-sdk/1.0.14-beta/qiniu.min.js"></script> 
<script src="//cdn.bootcss.com/plupload/2.1.9/plupload.full.min.js"></script>
<script type="text/javascript">

	function add_special_product(id, url) {
		<!-- 使用ajax发送请求信息 -->
		$.ajax({
			url: url,
			type: "POST",
			data: $('#product_list_form').serialize(),
			success: function(data, ts, xhr) {
				if(data.success) {
					success(data.msg);
				} else {
					error("error");
				}
				$('#product_list_grid').bootstrapTable('refresh');
			}
		});
	}
    function query_special_list(id, url, type) {
        if(type==1){
           if($("#specialPid").val()==""){
             error("请填写专题ID！")
           }else{
             $(id).bootstrapTable('refresh', {url: url});
           }
        }else{
          $(id).bootstrapTable('refresh', {url: url});
        }   
    }  
     function special_query_params(params) {
        $('#special_list_form :input').each(function() {
            if($(this).attr('name')) {
                params[$(this).attr('name')] = $(this).val();
            }
        });
        return params;
    }
    function business_operation_format(value, item) {
        return '<button type="button" class="btn btn-link" onclick="openUpdateSpecialWin('+value+')">编辑</button>';
    }
    function submitSpecialForm() {
    <!-- 使用ajax发送请求信息 -->
        $.ajax({
            url :  "/m/mall/special/saveOrUpdateSpecial",
            type : "POST",
            data: $('#specialForm').serialize(),
            success:function(data,ts,xhr){
                if(data.success){
                    $('#special_model').modal('hide');
                    success(data.msg);
                }else{
                    error("error");
                }
                $('#special_list_grid').bootstrapTable('refresh');
            }
        });
    }
     function openCreateSpecialWin() {
        $('#specialId').attr('readonly', true);
        $('#specialForm')[0].reset();
        $('#special_model').modal('show');
    }
    
    function openUpdateSpecialWin(id) {
        $('#specialId').attr('readonly', true);
        $('#specialForm')[0].reset();
        setFormValue('specialForm', $('#special_list_grid').bootstrapTable('getRowByUniqueId', id));
        $('#special_model').modal('show');
    }

    function deleteSpecial(id, name) {
        openDialog({
            title: '删除产品',
            message: '您确定要删除 ['+name+']吗？',
            okAction: function() {
                closeDialog();
                $.ajax({
                    url :  "/m/mall/special/delete",
                    type : "POST",
                    data: {
                        id: id
                    },
                    success:function(data,ts,xhr){
                        if(data.success){
                            success(data.msg);
                        }else{
                            error("error");
                        }
                        $('#special_list_grid').bootstrapTable('refresh');
                    }
                });
            }
        });
    }
         function uploadImage(container,buttonId,inputData){
	    $.ajax({
	        url : "/m/mall/product/uploadToken",
	        type : "POST",
	        success:function(data,ts,xhr){   
                var uploader = Qiniu.uploader({
                runtimes: 'html5,flash,html4',    //上传模式,依次退化
                browse_button: buttonId,       //上传选择的点选按钮，**必需**
                uptoken : data.uptoken, //若未指定uptoken_url,则必须指定 uptoken ,uptoken由其他程序生成
                domain: 'res.eqh5.com',   //bucket 域名，下载资源时用到，**必需**
               // save_key: false,   // 默认 false。若在服务端生成uptoken的上传策略中指定了 `sava_key`，则开启，SDK会忽略对key的处理
                get_new_uptoken: false,  //设置上传文件的时候是否每次都重新获取新的token
                container: container,           //上传区域DOM ID，默认是browser_button的父元素，
                max_file_size: '100mb',           //最大文件体积限制
                flash_swf_url: '/plupload/js/Moxie.swf',  //引入flash,相对路径
                max_retries: 1,                   //上传失败最大重试次数
                dragdrop: true,                   //开启可拖曳上传
                //drop_element: container,        //拖曳上传区域元素的ID，拖曳文件或文件夹后可触发上传
                chunk_size: '4mb',                //分块上传时，每片的体积
                auto_start: false,                 //选择文件后自动上传，若关闭需要自己绑定事件触发上传
                init: {
                    'FilesAdded': function(up, files) {
                        plupload.each(files, function(file) {
                           document.getElementsByName(inputData)[0].value = file.name+ "(" + plupload.formatSize(file.size) + ")";
			                  uploader.start();
			                  return false;
                        });
                    },
                    'BeforeUpload': function(up, file) {
                           // 每个文件上传前,处理相关的事情
                    },
                    'UploadProgress': function(up, file) {
                          document.getElementsByName(inputData)[0].value = file.name+"("+plupload.formatSize(file.size)+")" + file.percent + "%";
                    },
                    'FileUploaded': function(up, file, info) {
                         var res = jQuery.parseJSON(info);
				         document.getElementsByName(inputData)[0].value = res.key;					                       
                    },
                    'Error': function(up, err, errTip) {
                        document.getElementsByName(inputData)[0].value = err.code +":"+err.message;
						error(err.message);
                    },
                    'UploadComplete': function() {
                            success('success');                                 
                    }, 
                    'Key': function(up, file) {   
                        var filedata = file.name.split(".");
                        var type = filedata[filedata.length-1];
				        var key = 'store/' + getUuid() +"." + type;
                        return key;
                    }                             
                }
            });
	       }  
	      })
    }
    function getUuid(){
		  var len=32;//32长度
		  var radix=16;//16进制
		  var chars='0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');
		  var uuid=[],i;radix=radix||chars.length;
		  if(len){
		    for(i=0;i<len;i++)
		      uuid[i]=chars[0|Math.random()*radix];
		      }
		    else{
		    var r;
		    uuid[8]=uuid[13]=uuid[18]=uuid[23]='-';
		    uuid[14]='4';
		    for(i=0;i<36;i++){
		      if(!uuid[i]){
		        r=0|Math.random()*16;
		        uuid[i]=chars[(i==19)?(r&0x3)|0x8:r];
		        }
		       }
		     }
		  return uuid.join('');
		}  
    function log_formatter_time(value) {
    return new Date(value).format('yyyy-MM-dd hh:mm:ss');
    }
    var banner_status = {'1': '启用', '2': '停用'};
	function formatter_status(cellvalue, rowObject) {
		if(banner_status[cellvalue]) {
			cellvalue = banner_status[cellvalue];
		}
		return cellvalue;
	}
    var banner_type = {'1': '专题推荐页', '2': '商城首页'};
	function formatter_type(cellvalue, rowObject) {
		if(banner_type[cellvalue]) {
			cellvalue = banner_type[cellvalue];
		}
		return cellvalue;
	}   
	var product_status = {'1': '已经发布', '2': '草稿','3':'下线'};
	function product_formatter_status(cellvalue, rowObject) {
		if(product_status[cellvalue]) {
			cellvalue = product_status[cellvalue];
		}
		return cellvalue;
	}   
    function product_query_params(params) {
        $('#product_list_form :input').each(function() {
            if($(this).attr('name')) {
                params[$(this).attr('name')] = $(this).val();
            }
        });
        return params;
    }  
    
    function batch_delete_product(id) {
		var data = {};
		if(id) {
			data.id = id;
		} else {
			data = $('#product_list_grid').bootstrapTable('getSelections');
		}
		if(!data || data.length <= 0) {
			error('请选择商品！');
			return;
		}
		var ids = [];
		$(data).each(function(idx, item) {
			ids.push(item.productId);
		});
		var ajaxData = {
		    specialId: $('#specialPid').val(),
			ids: ids.join(';')
		};
		openDialog({
			title: '删除',
			message: '您确认要删除吗？',
			okAction: function() {
				closeDialog();
				$.ajax({
					type: "post",
					url: "/m/mall/special/deleteProduct",
					data: ajaxData,
					success: function(data, ts, xhr) {
						if(data.success) {
							success(data.msg);
						} else {
							error("error");
						}
						$('#product_list_grid').bootstrapTable('refresh');
					}
				});
			}
		})

	}  
	function batch_delete_special(id) {
		var data = {};
		if(id) {
			data.id = id;
		} else {
			data = $('#special_list_grid').bootstrapTable('getSelections');
		}
		if(!data || data.length <= 0) {
			error('请选择专题！');
			return;
		}
		var ids = [];
		$(data).each(function(idx, item) {
			ids.push(item.id);
		});
		var ajaxData = {
			ids: ids.join(';')
		};
		openDialog({
			title: '删除',
			message: '您确认要删除这些专题吗？',
			okAction: function() {
				closeDialog();
				$.ajax({
					type: "post",
					url: "/m/mall/special/delete",
					data: ajaxData,
					success: function(data, ts, xhr) {
						if(data.success) {
							success(data.msg);
						} else {
							error("error");
						}
						$('#special_list_grid').bootstrapTable('refresh');
					}
				});
			}
		})

	}
	function tpl_formatter_id(value, o) {
		return '<a target="_blank" href="http://store.eqxiu.com/'+o.productId+'.html">'+value+'</a>';
	}    
</script>