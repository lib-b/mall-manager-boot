<form class="form-inline" id="producttag_list_form">
	<div class="form-group">
		<div class="input-group">
            <div class="input-group-addon"> 产品ID</div>
            <input type="text" class="form-control" name="productId"/>
        </div>
	</div>
		<div class="form-group">
		<div class="input-group">
            <div class="input-group-addon"> 标签ID</div>
            <input type="text" class="form-control" name="tagId"/>
        </div>
	</div>
	<br/>
	<br/>
	<button type="button" class="btn btn-primary" onclick="query_producttag_list('#producttag_list_grid', '/m/mall/tag/producttag/list.json')">查询</button>
    <button type="button" class="btn btn-primary" onclick="openCreateBusinessWin()" style="margin-right: 20px;">新增产品标签业务</button>
</form>
<table id="producttag_list_grid" data-toggle="table" data-toolbar="#producttag_list_form" data-query-params="producttag_query_params" data-click-to-select="true" data-page-size="5">
	<thead>
	    <tr>
	    	<th data-field="productId">产品ID</th>
	    	<th data-field="tagId">标签id</th>
	    	<th data-valign="middle" data-field="productId" data-align="center" data-formatter="business_operation_format" class="action-buttons" data-width="130">操作</th>				
	    </tr>
    </thead>
</table>
<div id="producttag_model" class="modal fade" role="dialog" aria-labelledby="producttag_model_title">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="producttag_model_title">新增产品业务</h4>
            </div>
            <div class="modal-body">
                <form class="form-inline" id="producttagForm">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">产品ID&nbsp;&nbsp;&nbsp;&nbsp;</div>
                            <input type="text" id="productId" value="" name="productId" class="form-control" style="width: 450px;">
                        </div>
                    </div>
                     <br/><br/>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">标签ID</div>
                            <input type="text" id="tagId" value="" name="tagId" class="form-control" style="width: 450px;">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" onclick="submitForm()">保存</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
//查询
    function query_producttag_list(id, url) {
        $(id).bootstrapTable('refresh', {url: url});
    }  
     function producttag_query_params(params) {
        $('#producttag_list_form :input').each(function() {
            if($(this).attr('name')) {
                params[$(this).attr('name')] = $(this).val();
            }
        });
        return params;
    }
    function business_operation_format(value, item) {
        return '<button type="button" class="btn btn-link" onclick="deleteBusiness('+item.tagId+','+item.productId+')">删除</button>';
    }
    function submitForm() {
    <!-- 使用ajax发送请求信息 -->
        $.ajax({
            url :  "/m/mall/tag/producttag/save",
            type : "POST",
            data: $('#producttagForm').serialize(),
            success:function(data,ts,xhr){
                if(data.success){
                    $('#producttag_model').modal('hide');
                    success(data.msg);
                }else{
                    error(data.msg);
                }
                $('#producttag_list_grid').bootstrapTable('refresh');
            }
        });
    }
     function openCreateBusinessWin() {
        $('#productId').removeAttr('readonly');
        $('#producttagForm')[0].reset();
        $('#producttag_model').modal('show');
    }
    
    function openUpdateBusinessWin(productId) {
        $('#productId').attr('readonly', true);
        $('#producttagForm')[0].reset();

        setFormValue('producttagForm', $('#producttag_list_grid').bootstrapTable('getRowByUniqueId', productId));
        $('#producttag_model').modal('show');
    }

    function deleteBusiness(tagId,productId) {
        openDialog({
            title: '删除产品',
            message: '您确定要删除该条记录吗？',
            okAction: function() {
                closeDialog();
                $.ajax({
                    url :  "/m/mall/tag/producttag/delete",
                    type : "POST",
                    data: {
                        productId: productId,
                        tagId: tagId
                    },
                    success:function(data,ts,xhr){
                        if(data.success){
                            success(data.msg);
                        }else{
                            error(data.msg);
                        }
                        $('#producttag_list_grid').bootstrapTable('refresh');
                    }
                });
            }
        });
    }
    
</script>