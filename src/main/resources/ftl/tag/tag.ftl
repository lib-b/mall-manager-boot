<form class="form-inline" id="tag_list_form">
	<div class="form-group">
		<div class="input-group">
            <div class="input-group-addon"> 标签名称</div>
            <input type="text" class="form-control" name="name"/>
        </div>
	</div>
	<br/>
	<button type="button" class="btn btn-primary" onclick="query_tag_list('#tag_list_grid', '/m/mall/tag/tag/list.json', 0)" >查询</button>
    <button type="button" class="btn btn-primary" onclick="openCreateTagWin()">新增标签业务</button>
    <button type="button" class="btn btn-primary" onclick="batch_delete_tag()">删除选中标签</button>
</form>
<table id="tag_list_grid" data-toggle="table" data-toolbar="#tag_list_form" data-unique-id="id" data-query-params="tag_query_params" data-click-to-select="true" data-page-size="5">
	<thead>
	    <tr>
	    	<th data-valign="middle" data-checkbox="true">全选</th>
	    	<th data-field="id">ID</th>
	    	<th data-field="name">标签名称</th>
	    	<th data-field="bizType" data-visible="false">标签类型</th>
	    	<th data-valign="middle" data-field="id" data-align="center" data-formatter="business_operation_format" class="action-buttons" data-width="130">操作</th>				
	    </tr>
    </thead>
</table>
<form class="form-inline" id="product_list_form">
	<div class="form-group">
		<div class="input-group">
            <div class="input-group-addon"> 标签ID</div>
            <input id="tagIdDeal" type="text" class="form-control" name="id"/>
        </div>
	</div>
	
	<br/>
	<button type="button" class="btn btn-primary" onclick="query_tag_list('#product_list_grid', '/m/mall/tag/tag/productlist', 1)" >查询商品</button>
	<button type="button" class="btn btn-primary" onclick="batch_delete_product()">删除选中商品</button>
</form>
<table id="product_list_grid" data-toggle="table" data-toolbar="#product_list_form" data-unique-id="id" data-query-params="product_query_params" data-click-to-select="true" data-page-size="5">
	<thead>
	    <tr>
	    	<th data-valign="middle" data-checkbox="true">全选</th>
	    	<th data-field="productId" data-formatter="tpl_formatter_id">商品ID</th>
            <th data-field="tagId" data-visible="false">标签ID</th>
	    	<th data-field="productTitle">商品标题</th>
	    	<th data-field="productStatus" data-formatter=formatter_status>商品状态</th>
	    </tr>
    </thead>
</table>
<!-- 新增产品业务的模型，主要是列出来需要的属性，这个模块在两个地方会用，一个是点击新增产品业务按钮，另一个是点击产品列表中的编辑。 -->
<div id="tag_model" class="modal fade" role="dialog" aria-labelledby="tag_model_title">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="tag_model_title">新增产品业务</h4>
            </div>
            <div class="modal-body">
                <form class="form-inline" id="tagForm">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">标签名称</div>
                            <input type="text" id="tagName" value="" name="name" class="form-control" style="width: 450px;">
                        </div>
                    </div>
                    <br/><br/>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">标签类型</div>
                            <input type="text" id="tagBizType" value="" name="bizType" class="form-control" style="width: 450px;">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" onclick="submitTagForm()">保存</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
//查询
   function query_tag_list(id, url, type) {
       if(type==1){
           if($("#tagIdDeal").val()==""){
              error("请输入标签ID")
           }else{
              $(id).bootstrapTable('refresh', {url: url});
           }
        }else{
           $(id).bootstrapTable('refresh', {url: url});
        }        
    }

     function tag_query_params(params) {
        $('#tag_list_form :input').each(function() {
            if($(this).attr('name')) {
                params[$(this).attr('name')] = $(this).val();
            }
        });
        return params;
    }
    function product_query_params(params) {
        $('#product_list_form :input').each(function() {
            if($(this).attr('name')) {
                params[$(this).attr('name')] = $(this).val();
            }
        });
        return params;
    }
    function business_operation_format(value, item) {
        return '<button type="button" class="btn btn-link" onclick="openUpdateTagWin('+value+')">编辑</button>';
    }
    function submitTagForm() {
    <!-- 使用ajax发送请求信息 -->
        $.ajax({
            url :  "/m/mall/tag/tag/save",
            type : "POST",
            data: $('#tagForm').serialize(),
            success:function(data,ts,xhr){
                if(data.success){
                    $('#tag_model').modal('hide');
                    success(data.msg);
                }else{
                    error(data.msg);
                }
                $('#tag_list_grid').bootstrapTable('refresh');
            }
        });
    }
     function openCreateTagWin() {
        $('#tagId').removeAttr('readonly');
        $('#tagForm')[0].reset();
        $('#tag_model').modal('show');
    }
    
    function openUpdateTagWin(id) {
        $('#tagId').attr('readonly', true);
        $('#tagForm')[0].reset();
        setFormValue('tagForm', $('#tag_list_grid').bootstrapTable('getRowByUniqueId', id));
        $('#tag_model').modal('show');
    }

    function log_formatter_time(value) {
      return new Date(value).format('yyyy-MM-dd hh:mm:ss');
    }
    var product_status = {'1': '已经发布', '2': '草稿','3':'下线'};
	function formatter_status(cellvalue, rowObject) {
		if(product_status[cellvalue]) {
			cellvalue = product_status[cellvalue];
		}
		return cellvalue;
	}  
    
    function batch_delete_tag(id) {
		var data = {};
		if(id) {
			data.id = id;
		} else {
			data = $('#tag_list_grid').bootstrapTable('getSelections');
		}
		if(!data || data.length <= 0) {
			error('请选择标签！');
			return;
		}
		var ids = [];
		$(data).each(function(idx, item) {
			ids.push(item.id);
		});
		var ajaxData = {
			ids: ids.join(';')
		};
		$.ajax({
			type: "post",
			url: "/m/mall/tag/tag/delete",
			async: true,
			data: ajaxData,
			success: function(data) {
				if(data.success) {
					success(data.msg);
				} else {
					error("error");
				}
				$('#tag_list_grid').bootstrapTable('refresh');
			}
		});
	}
	
	function batch_delete_product(id) {
		var data = {};
		if(id) {
			data.id = id;
		} else {
			data = $('#product_list_grid').bootstrapTable('getSelections');
		}
		if(!data || data.length <= 0) {
			error('请选择商品！');
			return;
		}
		var ids = [];
		$(data).each(function(idx, item) {
			ids.push(item.productId);
		});
		var ajaxData = {
			tagId: $('#tagIdDeal').val(),
			ids: ids.join(';')
		};
		$.ajax({
			type: "post",
			url: "/m/mall/tag/tag/deleteProduct",
			async: true,
			data: ajaxData,
			success: function(data) {
				if(data.success) {
					success(data.msg);
				} else {
					error("error");
				}
				$('#product_list_grid').bootstrapTable('refresh');
			}
		});
	}
	function tpl_formatter_id(value, o) {
		return '<a target="_blank" href="http://store.eqxiu.com/'+o.productId+'.html">'+value+'</a>';
	} 
</script>