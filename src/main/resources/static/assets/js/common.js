/*
* 生成Checkbox列表
 * name 外层divID、checkbox name
 * data 数组
 * valueProperty 值属性，默认为value
 * nameProperty  名称属性，默认为name
 */
function addCheckbox(name,data,valueProperty,nameProperty){
	if(arguments.length < 3){
		valueProperty = "value";
	}
	if(arguments.length < 4){
		nameProperty = "name";
	}
	var chk = "";
	for(var i=0;i<data.length;i++){
		chk += "<label><input type='checkbox' class='ace' name='"+name+"' value='"+eval("data[i]."+valueProperty)+"'><span class='lbl'>"+eval("data[i]."+nameProperty)+"</span></label>";
	}
	document.getElementById(name).innerHTML = chk;
}

function inArray(value, arr) {
	var i = 0,
	len = arr.length;
	for ( ; i < len; i++ ) {
		if (value && arr[i] == value ) {
			return i;
		}
	}
	return -1;
}

//设置iframe滚动条
var pagestyle = function (){
	var rframe = $("#page_iframe");
	var h = $(window).height() - rframe.offset().top - 15;
	rframe.height(h);
}

/*
 * 用JSON对象填充Form
 * form 表单ID
 * obj JSON对象
 */
function setFormValue(form,obj){
	var fileds = $("#"+form).find(":input");
	$.each(fileds, function() {
		var _this = $(this);
		var _data = obj[_this.attr('name')];
		if(typeof(_data) == 'undefined') return;
		if(/text|password|hidden|file/i.test(_this.attr('type'))) {
			_this.val(_data);
		}else if(_this.attr('type') == 'radio') {
			if(_this.attr('value') && _this.attr('value') == _data) {
				_this.prop('checked', true);
			}
		}else if(_this.attr('type') == 'checkbox') {
			if($.isArray(_data)) {
				if(inArray(_this.attr('value'), _data) != -1) {
					_this.prop('checked', true);
				}
			}else if(_this.attr('value') == _data) {
				_this.prop('checked', true);
			}
		}else if(_this.is('textarea')) {
			_this.val(_data);
		}else if(_this.is('select')) {
			_this.find('option[value='+_data+']').prop('selected', true);
		}
	});
	/**
	for(var i=0;i<fileds.length;i++){
		if(!eval("obj.hasOwnProperty('"+fileds[i].name+"')"))continue;
		if(fileds[i].type == "text" || fileds[i].type == "hidden"){
			fileds[i].value = eval("obj."+fileds[i].name);
		}else if(fileds[i].type == "radio"){
			if(fileds[i].value == eval("obj."+fileds[i].name)){
				fileds[i].checked = "checked";
			}
		}else if(fileds[i].type == "checkbox"){
			var dataArr = eval("obj."+fileds[i].name);
			if(typeof(dataArr) != 'object'){
				if(fileds[i].value == dataArr){
					fileds[i].checked = "checked";
				}
			}else{
				for(var j=0;j<dataArr.length;j++){
					if(fileds[i].value == dataArr[j]){
						fileds[i].checked = "checked";
					}
				}
			}
		}
	}
	var selects = $("#"+form).find("select");
	for(var i=0;i<selects.length;i++){
		if(!eval("obj.hasOwnProperty('"+selects[i].name+"')"))continue;
		selects[i].value = eval("obj."+selects[i].name);
	}
	var textarea = $("#"+form).find("textarea");
	for(var i=0;i<textarea.length;i++){
		if(!eval("obj.hasOwnProperty('"+textarea[i].name+"')"))continue;
		textarea[i].value = eval("obj."+textarea[i].name);
	}**/
}

/**
 * 清空表单
 * @param form
 */
function clearForm(form){
	var fileds = $("#"+form).find(":input");
	$.each(fileds,function(){
		var _this = $(this);
		//文本框
		if(/text|password|hidden|file/i.test(_this.attr('type'))) {
			_this.val('');
		}else if(_this.attr('type') == 'radio') {
			_this.prop('checked', false);
		}else if(_this.attr('type') == 'checkbox'){
			_this.prop('checked', function(){
				return false;
			});
		}else if(_this.attr('type') == 'textarea'){
			_this.val('');
		}else if(_this.is('select')) {
			//选择“全部”选项
			if(_this.find('option[value=""]')){
				_this.find('option[value=""]').prop('selected', true);
			}else{
				_this.find('option[value="-1"]').prop('selected', true);
			}
		}
	});
}
/*
 * 给Radio 添加点击事件
 * name radio名称
 * callback 回调函数
 */
function addRadioEvent(name,callback){
	var arr = document.getElementsByName(name);
	for(var i=0;i<arr.length;i++){
		arr[i].onclick = callback;
	}
}
/*
 * 给SELECT 元素添加 OPTION
 * id selectID
 * data 字典数据对象
 */
function addOptions(id,data,hasAll){
	
	var select = document.getElementById(id);
	if(arguments.length == 3 && hasAll == 0){
		select.options.add(new Option("全部","-1"));
	}
	for(var i=0;i<data.length;i++){
		var dict = data[i];
		if(dict.value == null){
			select.options.add(new Option(dict.name,dict.id));
		}else if(dict.value==hasAll){//hasAll的值为选中值
			select.options.add(new Option(dict.name,dict.value,true,true));
		}else{
			select.options.add(new Option(dict.name,dict.value));
		}
	}
	if(arguments.length == 3 && hasAll == 999){
		select.options.add(new Option("其他","999"));
	}
}

// 引入js和css文件
var $_LOADED_FILE = [];//已载入的文件
/*
 * 动态加js|css文件
 * url 文件完整路径:逗号分隔的字符串或字符串数组
 * callback 回调函数
 */
function include(url,callback){
	var files = url;
	if(typeof(files) == "string"){files = url.split(",");}
	var HEAD = document.getElementsByTagName("head").item(0) || document.documentElement;
	var s = new Array();
	var last = files.length - 1;
	var recursiveLoad = function(i) { //递归
		if($_LOADED_FILE[files[i]]){ //检查文件是否已加载
			if(i != last ){ //是否最后一个文件,不是继续加载，
				recursiveLoad(i+1);
			}else{
				//加载完执行回调
				if(typeof(callback) == "function") {
					callback();
				}
				return ;
			}
		}else{
			if(files[i].indexOf(".js") != -1){
				s[i] = document.createElement("script");
				s[i].setAttribute("type","text/javascript");
				s[i].setAttribute("src",files[i]);
			}else if(files[i].indexOf(".css") != -1){
				s[i] = document.createElement("link");
				s[i].setAttribute("rel","stylesheet");
				s[i].setAttribute("type","text/css");
				s[i].setAttribute("href",files[i]);
			}else{
				alert("引入非法文件"+files[i]);return ;
			}
			if (s[i].readyState) { // for IE
				s[i].onreadystatechange = function() {
					if(s[i].readyState == "loaded" || s[i].readyState == "complete") {
						s[i].onreadystatechange = null; 
						s[i].parentNode.removeChild(s[i]);
						if(i != last){
							recursiveLoad(i + 1); 
						}else if(typeof(callback) == "function") {
							callback();
						}
					}
				}
			}else{//Other
				s[i].onload  = function() {
					s[i].onload = null;
					if(i != last){
						recursiveLoad(i + 1); 
					}else if(typeof(callback) == "function") {
						callback();
					}
				}
			}
			try{
	    	   HEAD.appendChild(s[i]);
	    	   $_LOADED_FILE[files[i]] = true; //标记已加载
			}catch(e){
	    	   alert("加载文件异常!"+e);
			}
		}
	};
	recursiveLoad(0);
}

// 打开一个窗体
function windowOpen(url, name, width, height){
	var top=parseInt((window.screen.height-height)/2,10),left=parseInt((window.screen.width-width)/2,10),
		options="location=no,menubar=no,toolbar=no,dependent=yes,minimizable=no,modal=yes,alwaysRaised=yes,"+
		"resizable=yes,scrollbars=yes,"+"width="+width+",height="+height+",top="+top+",left="+left;
	window.open(url ,name , options);
}

// 显示加载框
function loading(message){
	$.jBox.tip.mess = null;
	$.jBox.tip(message,'loading',{opacity:0});
}
function success(message){
	$.jBox.tip.mess = null;
	$.jBox.tip(message, 'success');
}
function error(message){
	$.jBox.tip.mess = null;
	$.jBox.tip(message, 'error');	
}
function message(data){
	$.jBox.tip.mess = null;
	$.jBox.tip(data.message, data.type);	
}

// 确认对话框
function confirmx(mess, href){
	$('#common-modal-title').text('系统提示'); //标题
	$('#common-modal-body').html('<p>'+mess+'</p>'); //内容
	$('#common-modal-btn-ok').text('确定').off(); //移除所有事件
	$('#common-modal-btn-ok').on('click', function() {
		$('#common-modal').modal("hide");
		loading('正在提交，请稍等...');
		location = href;
		$('.jbox-body .jbox-icon').css('top','55px');
	});
	$('#common-modal').modal("show");
	return false;
	
	/**$.jBox.confirm(mess,'系统提示',function(v,h,f){
		if(v=='ok'){
			loading('正在提交，请稍等...');
			location = href;
		}
	},{buttonsFocus:1});**/
	
}
/*
 * 通用表格型删除数据操作
 * a 操作链接对象,
 */
function delTableData(a,url){
	$('#common-modal-title').text('确认'); //标题
	$('#common-modal-body').html('<p>确定删除吗？</p>'); //内容
	$('#common-modal-btn-ok').text('确定').off(); //移除所有事件
	$('#common-modal-btn-ok').on('click', function() {
		$('#common-modal').modal("hide");
		$.ajax({
			url : mpath + url,
			type : "POST",
			success:function(data,ts,xhr){
				if(data.success){
					$(a).parents("tr").remove();
					success(data.msg);
					return ;
				}
				if(data.type == "success"){
					$(a).parents("tr").remove();
				}
				message(data);
			}
		});
	});
	$('#common-modal').modal("show");
}

function openDialog(options) {
	var __options = {
			title: '温馨提示',
			message: '',
			okText: '确定',
			okAction: null
	};
	options = $.extend(__options, options);
	
	$('#common-modal-title').text(options.title); //标题
	$('#common-modal-body').html(options.message); //内容
	$('#common-modal-btn-ok').text(options.okText).off(); //移除所有事件
	if(typeof options.okAction == 'function') {
		$('#common-modal-btn-ok').on('click', function() {
			options.okAction();
		});
	}
	$('#common-modal').modal("show");
}

function closeDialog() {
	$('#common-modal').modal("hide");
}

/*
 * 通用Ajax 提交Form表单
 * form:form表单对象,
 * url提交地址
 */
 function submitByAjax(form,url){
	loading('正在提交，请稍等...');
	$.ajax({
		url : mpath + url,
		data : form.serialize(),
		type : "POST",
		success : function(data){
			if(data.success){
				$.jBox.tip.mess = null;
				$.jBox.tip(data.msg, "success");	
				return;
			}
			if(data.type){
				message(data);
			}else if(data.exception){
				error(data.exception.message);
			}else{
				//返回的是html代码
				$("#page-content").empty();
				$("#page-content").html("");
				$("#page-content").append(data);
			}
		},error : function(xhr){
			error(xhr.statusText);
		}
	});
 }
var uploadDefaults = {
	language: 'zh', //设置语言
	uploadAsync : false, //设置异步上传
	showUpload: false, //是否显示上传按钮
	showCaption: false,//是否显示标题
	maxFileSize : 20000, //上传文件大小（KB）
	maxPreviewFileSize : 1000,
//	resizeImage: true,
//	validateInitialCount:true,
//	minFileCount : 1, //上传文件最小数量
//	maxImageWidth : 150, //图片宽度px
//    maxImageHeight : 150,//图片高度px
    showUploadedThumbs : false,
//  showPreview : false,
//    enctype: 'multipart/form-data',
//    allowedPreviewTypes:['image'],//允许预览的类型(图片/视频/音频)
	allowedFileTypes : ['image','audio','object'],//允许上传的文件类型
	allowedFileExtensions : ["mp3",'jpg', 'png','svg','ttf','woff']//接收的文件后缀
};
//初始化fileinput控件（第一次初始化）
function initFileInput(id,options) {
	var control = $('#' + id);
	if(!options.uploadUrl){
		console.error('uploadUrl must not be null!');
		return false;
	}
	control.fileinput($.extend({},uploadDefaults,options));
}

/*
 * 通用Ajax 翻页数据提交、查询
 * form:查询form表单对象,
 * url提交地址
 */
 function pagaAjax(url,form){
	$.ajax({
		url : mpath + url,
		type : "POST",
		data : arguments.length>1?form.serialize():null,
		success:function(data,ts,xhr){
			$("#page-content").empty();
			$("#page-content").html("");
			$("#page-content").append(data);
		}
	});
 }
 
 //////////////////////////jqGrid/////////////////////////////////
 
 function resizeJqGrid(selector, parent) {
	 if(typeof parent != 'string') {
		 parent =$(selector).parent();
	 }else{
		 parent = $(parent);
	 }
	$(selector).jqGrid('setGridWidth', parent.width());
	//$(selector).jqGrid('setGridHeight', parent.height());
} 
 
 ///////////////////////日期格式化///////////////////////////////
//对Date的扩展，将 Date 转化为指定格式的String   
//月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，   
//年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)   
//例子：   
//(new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423   
//(new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18   
;Date.prototype.format = function(fmt)   {
	var o = {   
	  "M+" : this.getMonth()+1,                 //月份   
	  "d+" : this.getDate(),                    //日   
	  "h+" : this.getHours(),                   //小时   
	  "m+" : this.getMinutes(),                 //分   
	  "s+" : this.getSeconds(),                 //秒   
	  "q+" : Math.floor((this.getMonth()+3)/3), //季度   
	  "S"  : this.getMilliseconds()             //毫秒   
	};   
	if(/(y+)/.test(fmt))   
		fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));   
	for(var k in o)   
		if(new RegExp("("+ k +")").test(fmt))   
			fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));   
	return fmt;   
};