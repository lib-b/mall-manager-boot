var hollyglobal = {
	//挂断触发
    hangupEvent: function (peer) {
    	$("#online_call_model").modal("hide");
    	//挂断后退出登录
    	if (holly.session.logined) {
            holly.phone._phone_exit(true);
        }
    	var data = peer.Data;
    	if(!data){
    		return false;
    	}
    	var ajaxData = {
    		type : 1,//呼出
    		phone : data.ListenExten,
    		duration : Math.round(data.EndTime - data.BeginTime),//通话时长-四舍五入精确到秒
    		calledUid : hollyglobal.calledUid //被叫用户ID
    	};
    	$.ajax({
    		type : "post",
    		url : mpath+"/call/save",
    		data : ajaxData,
    		success : function(data){
    			success("通话已结束!");
    		},
    		error : function(err){console.log(err.statusText);}
    	});
    	
    },
    //响铃触发
    ringEvent: function (peer) {
		
    },
    //接听触发
    talkingEvent: function (peer) {},
    //登陆成功触发
    loginSuccessCallback: function(peer){
    	//自动呼出
    	$("#dialout_input").val(hollyglobal.phone);
    	holly.phone.phone_dialout(hollyglobal.phone);
	},
	//登陆失败触发
    loginFailureCallback: function (peer) {
    	error(peer);
    },
    pbxs: [
        {
            PBX: '2.3.1.100',
            pbxNick: '101',
            NickName: '101',
            proxyUrl: "http://182.140.221.94"
        }
    ],
    accountId: 'N000000007121',
    sipConfigId: '2.3.1.100',
    monitorPassword: '7pu3refwds98172e',
    monitorUserID: '2387rufhinjvcx73rfws',
    loginBusyType: "0",
    loginExtenType: "Local",
    loginPassword: "123abc",
    loginUser: "8027@blzw",
    loginProxyUrl: "http://182.140.221.94",
    isDisplayInvestigate: false,//转调查按钮
    isDisplayConsult: false,
    isHiddenNumber: false,
    isMonitorPage: false,
    isDisplayTransfer: false
};